@allLogin
Feature: Login giru

  @Automated @Happy_path @functional_testing @LOGIN_GIRU
  Scenario Outline: [HAPPY PATH] Valida login usuario con satisfactorio
    Given que el usuario accede a la aplicacion GIRU
    When se ingresa usuario <user> y contrasena <password>
    And selecciona la opcion iniciar sesion
    Then se valida que el acceso sea correcto con un perfil <profile> determinado
#    And selecciona la opcion cerrar sesion

    Examples: Usuario loguea satisfactoriamente con diferentes perfiles
      | user   | password  | profile          |
      | B34097 | Abc12345$ | Asesor           |
      | B9949  | Abc12345$ | Asesor           |
      | B39504 | Abc12345$ | Asesor           |
      | B39505 | Abc12345$ | Asesor           |
      | B35719 | Abc12345$ | Asesor           |
      | B37718 | Abc12345$ | Asesor           |
      | B37065 | Abc12345$ | Asesor Retencion |
      | B38219 | Abc12345$ | Asesor Retencion |
      | B9980  | Abc12345$ | Especialista     |
      | B29742 | Abc12345$ | Especialista     |
