@services
Feature:  RestApi Login Giru


  Scenario Outline: Login successful of the most recurring profiles
    Given that <actor> is registered in Giru
    When he requests to login with his credentials: <user> and <password>
    Then he gets a response showing his data: <actor> and <area>

    Examples:
      |actor   |user  |password      |area                            |
      |Roger   |B37482|Lima321.      |GESTIÓN DE PEDIDOS Y RECLAMOS TC|
      |Bruno   |B26192|Interbank2    |GESTIÓN DE PEDIDOS Y RECLAMOS TC|
      |Miriam  |B32909|javier&0202   |GESTIÓN DE PEDIDOS Y RECLAMOS TC|
      |Diana   |B33470|Interbank2    |GESTIÓN DE PEDIDOS Y RECLAMOS TC|
      |Jose    |XT9283|Callao\\@@2929|GESTIÓN DE PEDIDOS Y RECLAMOS TC|
      |Thalia  |XT9398|Interbank2000$|GESTIÓN DE PEDIDOS Y RECLAMOS TC|
      |Carolina|XT9443|Maxsydney3108 |DPTO-SEGUROS                    |
      |Yuri    |B34603|Interbank2    |DPTO-SEGUROS                    |
      |Victor  |XT9282|Noviembre2019 |DPTO-SEGUROS                    |
      |Manuel  |XT9419|Interbank2    |DPTO-SEGUROS                    |
      |Joel    |XT7668|Interbank3000 |DPTO-SEGUROS                    |
      |Marisol |S28671|Arabella1201* |BT PREMIUM                      |
      |Eliana  |XT9402|Interbank2    |EBP CONTACT CENTER 1            |
      |Luis    |B36142|Interbank2    |DPTO. CONTACT CENTER            |
      |Juan    |XT9420|Interbank2    |DPTO. CONTACT CENTER            |

