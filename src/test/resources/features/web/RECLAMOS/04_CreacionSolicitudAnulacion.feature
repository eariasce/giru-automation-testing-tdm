@allSolicitudAnulacion
Feature: Solicitud Anulación Giru

  @Automated @Happy_path @functional_testing @SOLICITUD_ANULACION
  Scenario Outline: Solicitud Tipologia Anulación
    Given ingresamos al sistema con un <usuario>, <contra> y un <perfil> determinado
    When hacemos la consulta por <tipodocumento> y <numerodocumento> de un cliente
    And indicamos la tarjeta del titular, Solicitud y <tipologia>
    And verificaremos los mensajes segun el tipo de anulación
    And ingresa un comentariocliente y un comentariointerno opcional
    And se adjuntara un archivo <archivo> en caso sea necesario
    And selecciona o añade segun el <indicador> y <operador>
    And seleciona el medio de respuesta <respuesta> segun la configuracion de la respuesta <configuracionrespuesta>
    And dependiendo del perfil se verificara el reclamo en Resumen, Seguimiento y cerramos sesión
    And iniciamos sesión con el supervisor de retención
    And buscamos el número de reclamo para poder asignarlo a un asesor
    And iniciamos sesión con el asesor
    And elegimos el tramite para que sea <evaluado>, en caso derive indicara en una <lista> que se le ofrecio al cliente, el <motivo> por el cual el cliente se retira y un comentario comentario

    Examples:
      | usuario | contra       | perfil | tipodocumento | numerodocumento | tipologia | archivo | indicador | operador | respuesta | configuracionrespuesta | evaluado | lista  | motivo |
      | xt9928  | Callao2121++ | Asesor | DNI           | 10011232        | 5         | Si      | S-        | MOVISTAR | S-ADDRESS | 0                      | DE       | 562748 | 10     |