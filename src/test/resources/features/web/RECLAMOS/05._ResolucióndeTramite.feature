@Regresion
@Resol
Feature:Resolucíón de tramites

  @ResolCIEXO
  Scenario Outline: Procede y No Procede Tramites
    Given Ingresar con el <usuario> y password que tenga el tramite asignado
    When ingresa en la busqueda el <tramite> para seleccionarlo
    And se abre la vista del tramite de Resumen selecciona <TipoResol>
    And elige si se envia <carta> , selecciona plantilla <TipoCarta> descarga y la adjunta
    And selecciona medio respuesta <Mediorespuesta> y selecciona o agrega <configuración> segun medio de respuesta
    And ingresa comentario comentariomodal en el modal
    And se adjunta un archivo <archivo> en caso sea necesario
    Then se verificara el reclamo en la bandeja Historial en su Seguimiento
       # PROCEDE
         # - R - COBROS INDEBIDOS TIPO CARTA > 2
         # - P - EXONERACION DE COBROS TIPO CARTA > 4
         # CARTA >  Si - No
         # ARCHIVO >  Si - No
         # TIPORESOL > P - NO
       #NO PROCEDE
        # - TipoResol > No
        # -  R - COBROS INDEBIDOS TIPO CARTA > 1
        # -  P - EXONERACION DE COBROS TIPO CARTA > 3

    Examples:
      | usuario | tramite | Mediorespuesta | configuración                                     | carta | TipoCarta | archivo | TipoResol  |
      #| B31890 | 40843   | A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 1        | Si      | No |
      #| B31890 | 40845    | A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 1        | Si      | No |
      #| B31890 | 40847   | A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 4         | No      | P |
      #| B31890 | 40849   | A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 4         | Si      | P |
      #| B31890 | 40851   | A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 4         | Si      | P |
     # | B31890 | 40804   | A-EMAIL       | test@gmail.com                          | Si    | 4         | No      | P |
      | B31890 |  40795   | A-EMAIL       | test@gmail.com                          | Si    | 2         | Sí      | P  |

  @ResolCNR
  Scenario Outline: Procede y No Procede Tramites CNR
    Given Ingresar con el <usuario> y password que tenga el tramite asignado
    When ingresa en la busqueda el <tramite> para seleccionarlo
    And se abre la vista del tramite de Resumen selecciona <TipoResol>
    And eliges los movimientos con el tipo de abono de la carta <TipoAbono>
    And eliges si se envia <carta> , descarga y la adjunta
    And selecciona medio respuesta <Mediorespuesta> y selecciona o agrega <configuración> segun medio de respuesta
    And ingresa comentario comentariomodal en el modal
    And se adjunta un archivo <archivo> en caso sea necesario
    Then se verificara el reclamo en la bandeja Historial en su Seguimiento
       # PROCEDE
         # -  R - COBROS INDEBIDOS TIPO CARTA > 2
         # - P - EXONERACION DE COBROS TIPO CARTA > 4
         # CARTA >  Si - No
         # ARCHIVO >  Si - No
         # TIPORESOL > P - NO


    Examples:
      | usuario | tramite | Mediorespuesta | configuración                                     | carta | TipoCarta | archivo | TipoResol | TipoAbono |
      | b22182  | 34347   | A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 2         | Si      | P         | Mixto     |
      #| Xt9402  |  37933   | A-EMAIL       | hansonaron1101@gmail.com                          | Si    | 2         | No      | P | Mixto|
      #| Xt9402 |  29006   | A-EMAIL       | hansonaron1101@gmail.com                          | Si    | 2         | Sí      | P  |Definitivo|



  @Creacion_ResolCIEXO_ConDerivacion
  Scenario Outline: Procede y No Procede Tramites
    #creacion del tranmite
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    When hace la consulta por <tipodocumento> y <numerodocumento> de un cliente
    And indica el <numerotarjeta>, <tramite> y <tipologia>
    And elige la <cantidad> de <motivos> y configura la fecha <fecha> tipo de moneda <tipomoneda> y monto <monto> por motivo segun la <configuracion>
    And en caso los motivos tuviera Comision de Membresia se aplicaria la Razon Operativa <razonoperativa> y seleccionar el Tipo de Razon Operativa <tiporazon>
    And ingresa un comentariocliente y un comentariointerno opcional
   And se adjuntara un archivo <archivo> en caso sea necesario
    And selecciona o añade segun el <indicador> y <operador>
    And seleciona el medio de respuesta <respuesta> segun la configuracion de la respuesta <configuracionrespuesta>
    Then dependiendo del perfil se verificara el reclamo en Resumen y Seguimiento
   #Se valida usuario asignado
    And se valida el usuario reasignado por servicios
    And se cierra sesion
    Given Ingresar con el usuario y password que tenga el tramite reasignado
    When ingresa en la busqueda el <tramite> para seleccionar
    And se abre la vista del tramite del Resumen selecciona derivacion
    And se selecciona la razon <Razon> y el area <Area> al cual se derivara el tranmite
   # And elige si se envia <carta> , selecciona plantilla <TipoCarta> descarga y la adjunta
    #And selecciona medio respuesta <Mediorespuesta> y selecciona o agrega <configuración> segun medio de respuesta
    #And ingresa comentario comentariomodal en el modal
    #And se adjunta un archivo <archivo> en caso sea necesario
    #Then se verificara el reclamo en la bandeja Historial en su Seguimiento
    And se valida el usuario reasignado por servicios
    And se cierra sesion
       #Se repite el fluijo con nuevo asesor reasignado
    Given Ingresar con el usuario y password que tenga el tramite reasignado
    When ingresa en la busqueda el <tramite> para seleccionar
    And se abre la vista del tramite de Resumen derivado selecciona <TipoResol>
    #And elige si se envia <carta> , selecciona plantilla <TipoCarta> descarga y la adjunta
    #And selecciona medio respuesta <Mediorespuesta> y selecciona o agrega <configuración> segun medio de respuesta
    And ingresa comentarios validacion comentariomodal en el modal
    And se adjunta un archivo <archivo> en caso sea necesario
    Then se verificara el reclamo derivado en la bandeja Historial en su Seguimiento
      #Se valida usuario asignado
    And se valida el usuario reasignado por servicios
    And se cierra sesion
    Given Ingresar con el usuario y password que tenga el tramite reasignado
    When ingresa en la busqueda el <tramite> para seleccionar
    And se abre la vista del tramite de Resumen selecciona <TipoResol>
    And elige si se envia <carta> , selecciona plantilla <TipoCarta> descarga y la adjunta
    And selecciona medio respuesta <Mediorespuesta> y selecciona o agrega <configuracion> segun medio de respuesta
    And ingresa comentario comentariomodal en el modal
    And se adjunta un archivo <archivo> en caso sea necesario
    Then se verificara el reclamo en la bandeja Historial en su Seguimiento
       # PROCEDE
         # - R - COBROS INDEBIDOS TIPO CARTA > 2
         # - P - EXONERACION DE COBROS TIPO CARTA > 4
         # CARTA >  Si - No
         # ARCHIVO >  Si - No
         # TIPORESOL > P - NO
       #NO PROCEDE
        # - TipoResol > No
        # -  R - COBROS INDEBIDOS TIPO CARTA > 1
        # -  P - EXONERACION DE COBROS TIPO CARTA > 3

    Examples:

      | usuario | contra       | perfil       | tipodocumento | numerodocumento | numerotarjeta    | tramite | tipologia | cantidad | motivos | fecha      | tipomoneda | monto  | configuracion | razonoperativa | tiporazon | archivo | indicador | operador | respuesta | configuracionrespuesta                            |   Mediorespuesta | configuracion  | carta | TipoCarta | archivo | TipoResol  | Razon | Area |
   ##COBROS INDEBIDOS
      #----------------------------------------REGISTRO CI - PERFIL ESPECIALISTA ------------------------------------------------------
  # # N°1 CREAR RECLAMO - 1 MOTIVO - MONTO 499 - S/ - NO Aplica RazonOperativa Motivo Comisión- A MOVISTAR - SI ADJ ARCHIVO - AGREGA CORREO - Procede Automatico
    #  |   B31847  | Callao2121++ | Especialista | DNI           | 40402131        | 491337******0792 | Reclamo | 1         | 1        | 2        | 01/03/2019                                                        | PEN                     | 499                    | 1             | No             |           | Si      | A-991798199 | MOVISTAR  | A-EMAIL | hansonaron1101@gmail.com                          |      A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 4        | Si      | P |
      |   B31847  | Callao2121++ | Especialista | DNI           | 40402131        | 491337******0792 | Reclamo | 1         | 1        | 2        | 01/03/2019                                                        | PEN                     | 499                    | 1             | No             |           | Si      | A-991798199 | MOVISTAR  | A-EMAIL | hansonaron1101@gmail.com                          |      A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 1        | Si      | No | 0 | 0 |

#EXO
  #    | XT9614  | Callao2121++ | Especialista | DNI           | 17616184        | 544359******2277 | Pedido  | 2         | 1        | 10       | 01/03/2019                                                                                         | PEN                                 | 244.44                                  | 1             | NO             |           | Si      | A-991798199        | MOVISTAR | A-EMAIL   | hansonaron1101@gmail.com                                |      A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 2        | Si      | No | 0 |0|



  @CNR_ConDerivacion_CierraControversia
  Scenario Outline: Reclamo Tipologia CNR Giru cierra controversias
    Given se accedera al sistema con su <usuario> y <contra> con un <perfil> determinado
    When realiza la consulta por <tipodocumento> y <numerodocumento> de un cliente
    And selecciona el <numerotarjeta>, <tramite> y <tipologia>
    And elegimos la <cantidad> de <motivos> y configura el tipo de moneda <tipomoneda>, monto <monto>, fecha <fecha> y nombre de comercio <nombrecom> segun la <configuracion>
    And escogemos el motivo <motivosucedido> de lo sucedido con la tarjeta
    And se <indicara> si el cliente presenta morosidad o ha bloqueado su tarjeta
    And se agregara un archivo <archivo> en caso sea necesario
    And selecciona o añade segun el <indicador> y <operador>
    And seleciona el medio de respuesta <respuesta> segun la configuracion de la respuesta <configuracionrespuesta>
    Then segun del perfil se verificara el reclamo en Resumen y Seguimiento
    #Se valida usuario asignado
    And se valida el usuario reasignado por servicios en cnr
    And se cierra sesion
    #usuario derivado
    Given Ingresar con el usuario y password que tenga el tramite reasignado en cnr
    When ingresa en cnr la busqueda el <tramite> para seleccionar
    And selecciona la opcion si si cuenta con seguro y comentario comentario
    #And se abre la vista del tramite del Resumen selecciona derivacion
    #And se selecciona la razon y el area al cual se derivara el tranmite
    And se valida el usuario reasignado por servicios en cnr seguros
    And se cierra sesion
       #Se repite el fluijo con nuevo asesor reasignado
    Given Ingresar con el usuario y password que tenga el tramite reasignado en cnr
    When ingresa en cnr la busqueda el <tramite> para seleccionar
    And se abre la vista del tramite de Resumen selecciona <TipoResol>
    And eliges los movimientos con el tipo de abono de la carta <TipoAbono>
    And eliges si se envia <carta> , descarga y la adjunta
    And selecciona medio respuesta <Mediorespuesta> y selecciona o agrega <configuración> segun medio de respuesta
    And ingresa comentario comentariomodal en el modal
    And se adjunta un archivo <archivo> en caso sea necesario
    Then se verificara el reclamo en la bandeja Historial en su Seguimiento
  #And se selecciona la razon y el area al cual se derivara el tranmite
    And se valida el usuario reasignado por servicios en cnr
    And se cierra sesion
       #Se repite el fluijo con nuevo asesor reasignado

    Examples:
      | usuario | contra       | perfil       | tipodocumento | numerodocumento | numerotarjeta    | tramite | tipologia | cantidad | motivos | tipomoneda          | monto               | fecha                                                  | nombrecom                          | configuracion | motivosucedido                        | indicara | archivo | indicador | operador | respuesta | configuracionrespuesta   |   Mediorespuesta | configuración  | carta | TipoCarta | archivo | TipoResol  |  TipoAbono |
                                     #############---------El cliente nunca contrató el producto-------###############
      # # 1----------REGISTRO RECLAMO - CLIENTE CON SEGURO - MEDIO VIRTUALES -  MONTO MENOR A 1000 - SI BLOQUEO TC--->>> SE VA A SEGUROS LUEGO DANIEL CHAVEZ
      | B17486  | Callao2121++ | Especialista       | DNI           | 40614144        | 491337******7390 | Reclamo | 3         | 1        | 19      | PEN-PEN             | 499.99-500          | 02/05/2020-01/04/2020                                  | TALMA SAC - COMPU SAC              | 2             | El cliente nunca contrató el producto | si       | Si      | S         | CLARO    | A-EMAIL   | hansonaron1101@gmail.com  |      A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 2        | Si      | No | Mixto |





  @CNR_ConDerivacion_CierraSeguro_Controversia
  Scenario Outline: Reclamo Tipologia CNR Giru cierra seguro luego controversia
    Given se accedera al sistema con su <usuario> y <contra> con un <perfil> determinado
    When realiza la consulta por <tipodocumento> y <numerodocumento> de un cliente
    And selecciona el <numerotarjeta>, <tramite> y <tipologia>
    And elegimos la <cantidad> de <motivos> y configura el tipo de moneda <tipomoneda>, monto <monto>, fecha <fecha> y nombre de comercio <nombrecom> segun la <configuracion>
    And escogemos el motivo <motivosucedido> de lo sucedido con la tarjeta
    And se <indicara> si el cliente presenta morosidad o ha bloqueado su tarjeta
    And se agregara un archivo <archivo> en caso sea necesario
    And selecciona o añade segun el <indicador> y <operador>
    And seleciona el medio de respuesta <respuesta> segun la configuracion de la respuesta <configuracionrespuesta>
    Then segun del perfil se verificara el reclamo en Resumen y Seguimiento
    #Se valida usuario asignado
    And se valida el usuario reasignado por servicios en cnr
    And se cierra sesion
    #usuario derivado
    Given Ingresar con el usuario y password que tenga el tramite reasignado en cnr
    When ingresa en cnr la busqueda el <tramite> para seleccionar
    And selecciona la opcion si si cuenta con seguro y comentario comentario
    #And se abre la vista del tramite del Resumen selecciona derivacion
    #And se selecciona la razon y el area al cual se derivara el tranmite
    And se valida el usuario reasignado por servicios en cnr seguros
   #And se selecciona la razon y el area al cual se derivara el tranmite
    And se valida el usuario reasignado por servicios en cnr
    And se cierra sesion
    Given Ingresar con el usuario y password que tenga el tramite reasignado en cnr
    When ingresa en cnr la busqueda el <tramite> para seleccionar
    #Completar flujo seguros
    And se abre la vista del tramite de Resumen seguros selecciona <TipoResol>
    And escribe comentario interno "comentario"
    And se adjunta un archivo <archivo> en caso sea necesario
    And selecciona boton confirmar
 #Se valida usuario asignado
    And se valida el usuario reasignado por servicios en cnr
    And se cierra sesion
    #usuario derivado
          #Se repite el fluijo con nuevo asesor reasignado
    Given Ingresar con el usuario y password que tenga el tramite reasignado en cnr
    When ingresa en cnr la busqueda el <tramite> para seleccionar
    And se abre la vista del tramite de Resumen selecciona <TipoResol>
    And eliges los movimientos con el tipo de abono de la carta <TipoAbono>
    And eliges si se envia <carta> , descarga y la adjunta
    And selecciona medio respuesta <Mediorespuesta> y selecciona o agrega <configuración> segun medio de respuesta
    And ingresa comentario comentariomodal en el modal
    And se adjunta un archivo <archivo> en caso sea necesario
    Then se verificara el reclamo en la bandeja Historial en su Seguimiento
       #Se repite el fluijo con nuevo asesor reasignado

    Examples:
      | usuario | contra       | perfil       | tipodocumento | numerodocumento | numerotarjeta    | tramite | tipologia | cantidad | motivos | tipomoneda          | monto               | fecha                                                  | nombrecom                          | configuracion | motivosucedido                        | indicara | archivo | indicador | operador | respuesta | configuracionrespuesta   |   Mediorespuesta | configuración  | carta | TipoCarta | archivo | TipoResol  |  TipoAbono |
                                     #############---------El cliente nunca contrató el producto-------###############
      # # 1----------REGISTRO RECLAMO - CLIENTE CON SEGURO - MEDIO VIRTUALES -  MONTO MENOR A 1000 - SI BLOQUEO TC--->>> SE VA A SEGUROS LUEGO DANIEL CHAVEZ
      | B17486  | Callao2121++ | Especialista       | DNI           | 40614144        | 491337******7390 | Reclamo | 3         | 1        | 19      | PEN-PEN             | 499.99-500          | 02/05/2020-01/04/2020                                  | TALMA SAC - COMPU SAC              | 2             | El cliente nunca contrató el producto | si       | Si      | S         | CLARO    | A-EMAIL   | hansonaron1101@gmail.com  |      A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 2        | Si      | No | Mixto |

@CNR_ConDerivacion_SinSeguro_Controversia
Scenario Outline: Reclamo Tipologia CNR Giru cierra seguro luego controversia
  Given se accedera al sistema con su <usuario> y <contra> con un <perfil> determinado
  When realiza la consulta por <tipodocumento> y <numerodocumento> de un cliente
  And selecciona el <numerotarjeta>, <tramite> y <tipologia>
  And elegimos la <cantidad> de <motivos> y configura el tipo de moneda <tipomoneda>, monto <monto>, fecha <fecha> y nombre de comercio <nombrecom> segun la <configuracion>
  And escogemos el motivo <motivosucedido> de lo sucedido con la tarjeta
  And se <indicara> si el cliente presenta morosidad o ha bloqueado su tarjeta
  And se agregara un archivo <archivo> en caso sea necesario
  And selecciona o añade segun el <indicador> y <operador>
  And seleciona el medio de respuesta <respuesta> segun la configuracion de la respuesta <configuracionrespuesta>
  Then segun del perfil se verificara el reclamo en Resumen y Seguimiento
    #Se valida usuario asignado
  And se valida el usuario reasignado por servicios en cnr
  And se cierra sesion
    #usuario derivado
  Given Ingresar con el usuario y password que tenga el tramite reasignado en cnr
  When ingresa en cnr la busqueda el <tramite> para seleccionar
  And selecciona la opcion si si cuenta con seguro y comentario comentario
    #And se abre la vista del tramite del Resumen selecciona derivacion
    #And se selecciona la razon y el area al cual se derivara el tranmite
  And se valida el usuario reasignado por servicios en cnr seguros
   #And se selecciona la razon y el area al cual se derivara el tranmite
  And se valida el usuario reasignado por servicios en cnr
  And se cierra sesion
          #Se repite el fluijo con nuevo asesor reasignado
  Given Ingresar con el usuario y password que tenga el tramite reasignado en cnr
  When ingresa en cnr la busqueda el <tramite> para seleccionar
  And se abre la vista del tramite de Resumen selecciona <TipoResol>
  And eliges los movimientos con el tipo de abono de la carta <TipoAbono>
  And eliges si se envia <carta> , descarga y la adjunta
  And selecciona medio respuesta <Mediorespuesta> y selecciona o agrega <configuración> segun medio de respuesta
  And ingresa comentario comentariomodal en el modal
  And se adjunta un archivo <archivo> en caso sea necesario
  Then se verificara el reclamo en la bandeja Historial en su Seguimiento
       #Se repite el fluijo con nuevo asesor reasignado

  Examples:
    | usuario | contra       | perfil       | tipodocumento | numerodocumento | numerotarjeta    | tramite | tipologia | cantidad | motivos | tipomoneda          | monto               | fecha                                                  | nombrecom                          | configuracion | motivosucedido                        | indicara | archivo | indicador | operador | respuesta | configuracionrespuesta   |   Mediorespuesta | configuración  | carta | TipoCarta | archivo | TipoResol  |  TipoAbono |
                                     #############---------El cliente nunca contrató el producto-------###############
      # # 1----------REGISTRO RECLAMO - CLIENTE CON SEGURO - MEDIO VIRTUALES -  MONTO MENOR A 1000 - SI BLOQUEO TC--->>> SE VA A SEGUROS LUEGO DANIEL CHAVEZ
    | B17486  | Callao2121++ | Especialista       | DNI           | 40614144        | 491337******7390 | Reclamo | 3         | 1        | 19      | PEN-PEN             | 499.99-500          | 02/05/2020-01/04/2020                                  | TALMA SAC - COMPU SAC              | 2             | El cliente nunca contrató el producto | si       | Si      | S         | CLARO    | A-EMAIL   | hansonaron1101@gmail.com  |      A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 2        | Si      | No | Mixto |


  @CMP22_ConDerivacion
  Scenario Outline:Reclamo Tipologia CMP22
    Given se ingresa al sistema con su <usuario> y <contra> con un <perfil> determinado
    When efectua la consulta por <tipodocumento> y <numerodocumento> de un cliente
    And elige el <numerotarjeta>, <tramite> y <tipologia>
    And elegimos la <cantidad> de <motivos> y configura el tipo de moneda <tipomoneda>, monto <monto>, fecha <fecha>, nombre de comercio <nombrecom>, se adquiere un <ps>, un servicio <servicio> y una fecha de recepcion <fecharecep> segun la <configuracion>
    And ingresa un comentariocliente y un comentariointerno opcional
    And se anexara un archivo <archivo> en caso sea necesario
    And selecciona o añade segun el <indicador> y <operador>
    And elegimos el medio de respuesta <respuesta> segun la configuracion de la respuesta <configuracionrespuesta>
    And se procedera a revisar la lista de documentos que el cliente tiene que enviar en caso se haya habilitado la opcion
    Then segun el perfil ingresado se podra verificar el reclamo en su Resumen y Seguimiento
  #Se valida usuario asignado
    And se valida el usuario reasignado por servicios en cmp
    And se cierra sesion
    #usuario derivado
    Given Ingresar con el usuario y password que tenga el tramite reasignado en cmp
    When ingresa en cmp la busqueda el <tramite> para seleccionar
    And se abre la vista del tramite del Resumen selecciona derivacion
    And se selecciona la razon <Razon> y el area <Area> al cual se derivara el tranmite
    And se valida el usuario reasignado por servicios en cmp
    And se cierra sesion
       #Se repite el fluijo con nuevo asesor reasignado
    Given Ingresar con el usuario y password que tenga el tramite reasignado en cmp
    When ingresa en cmp la busqueda el <tramite> para seleccionar
    And se abre la vista del tramite de Resumen derivado selecciona <TipoResol>
    #And elige si se envia <carta> , selecciona plantilla <TipoCarta> descarga y la adjunta
    #And selecciona medio respuesta <Mediorespuesta> y selecciona o agrega <configuración> segun medio de respuesta
    And ingresa comentarios validacion comentariomodal en el modal
    And se adjunta un archivo <archivo> en caso sea necesario
    Then se verificara el reclamo derivado en la bandeja Historial en su Seguimiento

    Examples:
      | usuario | contra       | perfil       | tipodocumento | numerodocumento | numerotarjeta    | tramite | tipologia | cantidad | motivos | tipomoneda  | monto       | fecha                            | nombrecom                        | ps    | servicio | fecharecep                       | configuracion | archivo | indicador | operador | respuesta | configuracionrespuesta                            |   Mediorespuesta | configuración  | carta | TipoCarta | archivo | TipoResol  | Razon | Area |
#------------------------PERFIL Asesor -------------------
     #1 RECLAMO CLIENTE DNI / SI ADJUNTA ARCHIVO / MOVISTAR / AGREGA CORREO >>> RECLAMO CMP SE ASIGNA A ELIZABETH GALLARDO O BRENDA CACEDA
      | B34649  | Callao2121++ | Asesor       | DNI           | 12312312        | 411093******9687 | Reclamo | 4         | 1        | 22      | PEN-USD-PEN | 100-250-100        | 01/01/2020-02/04/2019-02/02/2020 | TALMA SAC - WILSON SAC-EVERIS PO | S-S-P | RH-PT-NA | 02/02/2020-03/03/2020-04/04/2020 | 3             | Si      | S-        | MOVISTAR | A-EMAIL   | hansonaron1101@gmail.com                          |   A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 2        | Si      | No | 0 | 0 |


  @CMP_ConDerivacion
  Scenario Outline: Reclamo Tipologia CMP
    Given se ingresa al sistema con su <usuario> y <contra> con un <perfil> determinado
    When efectua la consulta por <tipodocumento> y <numerodocumento> de un cliente
    And elige el <numerotarjeta>, <tramite> y <tipologia>
    And escogemos la <cantidad> de <motivos> y configura el tipo de moneda <tipomoneda>, monto <monto>, fecha <fecha> y nombre de comercio <nombrecom> segun la <configuracion>
    And ingresa un comentariocliente y un comentariointerno opcional
    And se anexara un archivo <archivo> en caso sea necesario
    And selecciona o añade segun el <indicador> y <operador>
    And elegimos el medio de respuesta <respuesta> segun la configuracion de la respuesta <configuracionrespuesta>
    And se procedera a revisar la lista de documentos que el cliente tiene que enviar en caso se haya habilitado la opcion
    Then segun el perfil ingresado se podra verificar el reclamo en su Resumen y Seguimiento
 #Se valida usuario asignado
    And se valida el usuario reasignado por servicios en cmp
    And se cierra sesion
    #usuario derivado
    Given Ingresar con el usuario y password que tenga el tramite reasignado en cmp
    When ingresa en cmp la busqueda el <tramite> para seleccionar
    And se abre la vista del tramite del Resumen selecciona derivacion
    And se selecciona la razon <Razon> y el area <Area> al cual se derivara el tranmite
    And se valida el usuario reasignado por servicios en cmp
    And se cierra sesion
       #Se repite el fluijo con nuevo asesor reasignado
    Given Ingresar con el usuario y password que tenga el tramite reasignado en cmp
    When ingresa en cmp la busqueda el <tramite> para seleccionar
    And se abre la vista del tramite de Resumen derivado selecciona <TipoResol>
    #And elige si se envia <carta> , selecciona plantilla <TipoCarta> descarga y la adjunta
    #And selecciona medio respuesta <Mediorespuesta> y selecciona o agrega <configuración> segun medio de respuesta
    And ingresa comentarios validacion comentariomodal en el modal
    And se adjunta un archivo <archivo> en caso sea necesario
    Then se verificara el reclamo derivado en la bandeja Historial en su Seguimiento


    Examples:
      | usuario | contra       | perfil       | tipodocumento | numerodocumento | numerotarjeta    | tramite | tipologia | cantidad | motivos  | tipomoneda                          | monto                             | fecha                                                                                               | nombrecom                                                                                            | configuracion | archivo | indicador | operador | respuesta | configuracionrespuesta                            | Mediorespuesta | configuración  | carta | TipoCarta | archivo | TipoResol  | Razon | Area |
               # ----PERFIL Asesor--------

        #Registrar Reclamo CMP/ DNI / Por cada motivo  / 3 montos / SI Adj Archivo / Agrega Email  -- >>> RECLAMO CMP SE ASIGNA A ELIZABETH GALLARDO O BRENDA CACEDA
      | B34103  | Callao2121++ | Asesor       | CE            | 1001033E        | 377753*****0835 | Reclamo | 4         | 2        | 21-23    | PEN-USD-PEN-USD                     | 100-200-60-20                     | 01/03/2019-02/04/2019-01/03/2019-01/03/2019-02/04/2019-01/03/2019                                   | TALMA SAC - WILSON SAC-EVERIS PO-COMPU SAC                                                           | 2-2           | Si      | S-        | MOVISTAR | S-ADDRESS | 0                                                 |   A-ADDRESS      | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi | Si    | 2        | Si      | No | 0 | 0 |

