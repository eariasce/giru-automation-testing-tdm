@allCMP22
Feature:Reclamo CMP Giru

  @CMP22_1
  Scenario Outline:Reclamo Tipologia CMP22
    Given se ingresa al sistema con su <usuario> y <contra> con un <perfil> determinado
    When efectua la consulta por <tipodocumento> y <numerodocumento> de un cliente
    And elige el <numerotarjeta>, <tramite> y <tipologia>
    And elegimos la <cantidad> de <motivos> y configura el tipo de moneda <tipomoneda>, monto <monto>, fecha <fecha>, nombre de comercio <nombrecom>, se adquiere un <ps>, un servicio <servicio> y una fecha de recepcion <fecharecep> segun la <configuracion>
    And ingresa un regresion STG 3.0.5 y un regresion STG 3.0.5 opcional
    And se anexara un archivo <archivo> en caso sea necesario
    And selecciona o añade segun el <indicador> y <operador>
    And elegimos el medio de respuesta <respuesta> segun la configuracion de la respuesta <configuracionrespuesta>
    And se procedera a revisar la lista de documentos que el cliente tiene que enviar en caso se haya habilitado la opcion
    Then segun el perfil ingresado se podra verificar el reclamo en su Resumen y Seguimiento

    Examples:
      | usuario | contra       | perfil | tipodocumento | numerodocumento | numerotarjeta    | tramite | tipologia | cantidad | motivos | tipomoneda  | monto       | fecha                            | nombrecom                        | ps    | servicio | fecharecep                       | configuracion | archivo | indicador | operador | respuesta | configuracionrespuesta   |
#------------------------PERFIL Asesor -------------------
     #1 RECLAMO CLIENTE DNI / SI ADJUNTA ARCHIVO / MOVISTAR / AGREGA CORREO >>> RECLAMO CMP SE ASIGNA A ELIZABETH GALLARDO O BRENDA CACEDA
      | B34649  | Callao2121++ | Asesor | DNI           | 12312312        | 411093******9687 | Reclamo | 4         | 1        | 22      | PEN-USD-PEN | 100-250-100 | 01/01/2020-02/04/2019-02/02/2020 | TALMA SAC - WILSON SAC-EVERIS PO | S-S-P | RH-PT-NA | 02/02/2020-03/03/2020-04/04/2020 | 3             | Si      | S-        | MOVISTAR | A-EMAIL   | hansonaron1101@gmail.com |


  @CMP22_2
  Scenario Outline:Reclamo Tipologia CMP22
    Given se ingresa al sistema con su <usuario> y <contra> con un <perfil> determinado
    When efectua la consulta por <tipodocumento> y <numerodocumento> de un cliente
    And elige el <numerotarjeta>, <tramite> y <tipologia>
    And elegimos la <cantidad> de <motivos> y configura el tipo de moneda <tipomoneda>, monto <monto>, fecha <fecha>, nombre de comercio <nombrecom>, se adquiere un <ps>, un servicio <servicio> y una fecha de recepcion <fecharecep> segun la <configuracion>
    And ingresa un regresion STG 3.0.5 y un regresion STG 3.0.5 opcional
    And se anexara un archivo <archivo> en caso sea necesario
    And selecciona o añade segun el <indicador> y <operador>
    And elegimos el medio de respuesta <respuesta> segun la configuracion de la respuesta <configuracionrespuesta>
    And se procedera a revisar la lista de documentos que el cliente tiene que enviar en caso se haya habilitado la opcion
    Then segun el perfil ingresado se podra verificar el reclamo en su Resumen y Seguimiento

    Examples:
      | usuario | contra       | perfil       | tipodocumento | numerodocumento | numerotarjeta    | tramite | tipologia | cantidad | motivos | tipomoneda  | monto       | fecha                            | nombrecom                        | ps    | servicio | fecharecep                       | configuracion | archivo | indicador | operador | respuesta | configuracionrespuesta                            |
#------------------------PERFIL Asesor -------------------
     #1 RECLAMO CLIENTE DNI / SI ADJUNTA ARCHIVO / MOVISTAR / AGREGA CORREO >>> RECLAMO CMP SE ASIGNA A ELIZABETH GALLARDO O BRENDA CACEDA
      | B26355  | Callao2121++ | Asesor       | DNI           | 12312312        | 544402******1553 | Reclamo | 4         | 1        | 22      | PEN-USD-PEN | 100-250-100 | 01/01/2020-02/04/2019-02/02/2020 | TALMA SAC - WILSON SAC-EVERIS PO | S-S-P | RH-PT-NA | 02/02/2020-03/03/2020-04/04/2020 | 3             | Si      | S-        | MOVISTAR | A-EMAIL   | hansonaron1101@gmail.com                          |
#3 RECLAMO CLIENTE DNI / NO ADJUNTA ARCHIVO / AGREGA EMAIL>>>> VALIDAR QUE EL RECLAMO SE ENCUENTRE EN DOCUMENTACION PENDIENTE
      | B30137  | Callao2121++ | Asesor       | DNI           | 47404036        | 454775******1046 | Reclamo | 4         | 1        | 22      | PEN-PEN-PEN | 10-20-60    | 01/01/2020-02/04/2019-02/02/2020 | TALMA SAC - WILSON SAC-EVERIS PO | S-S-P | PT-CP-NA | 02/02/2020-03/03/2020-04/04/2020 | 3             | No      | S-        | CLARO    | A-EMAIL   | hansonaron1101@gmail.com                          |
#     #4 RECLAMO CLIENTE CE / NO ADJUNTA ARCHIVO / AGREGA DIRECCION >>>>> VALIDAR QUE EL RECLAMO SE ENCUENTRE EN DOCUMENTACION PENDIENTE
      | B9381   | Callao2121++ | Asesor       | CE            | 1001033E        | 377753*****0835  | Reclamo | 4         | 1        | 22      | PEN         | 10          | 01/01/2020                       | TALMA SAC                        | P     | NA       | 16/02/2020                       | 1             | No      | S-        | MOVISTAR | A-ADDRESS | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi |
#
#       #------------------------PERFIL ESPECIALSITA-------------------
#      ###PABLO MONTALVO>> XT7177
#     #5 RECLAMO CLIENTE DNI / SI ADJUNTA ARCHIVO / MOVISTAR / AGREGA CORREO >>> RECLAMO CMP SE ASIGNA A ELIZABETH GALLARDO O BRENDA CACEDA
      | B31847  | Callao2121++ | ESPECIALISTA | DNI           | 45453132        | 544402******4576 | Reclamo | 4         | 1        | 22      | PEN-USD-PEN | 100-250-100 | 01/01/2020-02/04/2019-02/02/2020 | TALMA SAC - WILSON SAC-EVERIS PO | S-S-P | RH-PT-NA | 02/02/2020-03/03/2020-04/04/2020 | 3             | Si      | S-        | MOVISTAR | A-EMAIL   | hansonaron1101@gmail.com                          |
#     #6 RECLAMO CLIENTE CE / SI ADJUNTA ARCHIVO / CLARO / SELECCIONA DIRECCION >>> RECLAMO CMP SE ASIGNA A ELIZABETH GALLARDO O BRENDA CACEDA
      | B31847  | Callao2121++ | ESPECIALISTA | CE            | 1001033E        | 454775******5609 | Reclamo | 4         | 1        | 22      | PEN-USD     | 499-499     | 01/01/2020-02/04/2020            | TALMA SAC-EVERIS PO              | P-S   | NA-CP    | 16/02/2020-02/03/2020            | 2             | Si      | S-        | CLARO    | S-ADDRESS | 0                                                 |

#------------------------PERFIL Asesor -------------------
     #1 RECLAMO CLIENTE DNI / SI ADJUNTA ARCHIVO / MOVISTAR / AGREGA CORREO >>> RECLAMO CMP SE ASIGNA A ELIZABETH GALLARDO O BRENDA CACEDA
      | B31847  | Callao2121++ | ESPECIALISTA | DNI           | 12312312        | 544402******1553 | Reclamo | 4         | 1        | 22      | PEN-USD-PEN | 100-250-100 | 01/01/2020-02/04/2019-02/02/2020 | TALMA SAC - WILSON SAC-EVERIS PO | S-S-P | RH-PT-NA | 02/02/2020-03/03/2020-04/04/2020 | 3             | Si      | S-        | MOVISTAR | A-EMAIL   | hansonaron1101@gmail.com                          |
#3 RECLAMO CLIENTE DNI / NO ADJUNTA ARCHIVO / AGREGA EMAIL>>>> VALIDAR QUE EL RECLAMO SE ENCUENTRE EN DOCUMENTACION PENDIENTE
      | B31847  | Callao2121++ | ESPECIALISTA | DNI           | 47404036        | 454775******1046 | Reclamo | 4         | 1        | 22      | PEN-PEN-PEN | 10-20-60    | 01/01/2020-02/04/2019-02/02/2020 | TALMA SAC - WILSON SAC-EVERIS PO | S-S-P | PT-CP-NA | 02/02/2020-03/03/2020-04/04/2020 | 3             | No      | S-        | CLARO    | A-EMAIL   | hansonaron1101@gmail.com                          |
#     #4 RECLAMO CLIENTE CE / NO ADJUNTA ARCHIVO / AGREGA DIRECCION >>>>> VALIDAR QUE EL RECLAMO SE ENCUENTRE EN DOCUMENTACION PENDIENTE
      | B31847  | Callao2121++ | ESPECIALISTA | CE            | 1001033E        | 377753*****0835  | Reclamo | 4         | 1        | 22      | PEN         | 10          | 01/01/2020                       | TALMA SAC                        | P     | NA       | 16/02/2020                       | 1             | No      | S-        | MOVISTAR | A-ADDRESS | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi |


  @CMP_3
  Scenario Outline: Reclamo Tipologia CMP
    Given se ingresa al sistema con su <usuario> y <contra> con un <perfil> determinado
    When efectua la consulta por <tipodocumento> y <numerodocumento> de un cliente
    And elige el <numerotarjeta>, <tramite> y <tipologia>
    And escogemos la <cantidad> de <motivos> y configura el tipo de moneda <tipomoneda>, monto <monto>, fecha <fecha> y nombre de comercio <nombrecom> segun la <configuracion>
    And ingresa un comentariocliente y un comentariointerno opcional
    And se anexara un archivo <archivo> en caso sea necesario
    And selecciona o añade segun el <indicador> y <operador>
    And elegimos el medio de respuesta <respuesta> segun la configuracion de la respuesta <configuracionrespuesta>
    And se procedera a revisar la lista de documentos que el cliente tiene que enviar en caso se haya habilitado la opcion
    Then segun el perfil ingresado se podra verificar el reclamo en su Resumen y Seguimiento

    Examples:
      | usuario | contra       | perfil       | tipodocumento | numerodocumento | numerotarjeta    | tramite | tipologia | cantidad | motivos  | tipomoneda                          | monto                             | fecha                                                                                               | nombrecom                                                                                            | configuracion | archivo | indicador | operador | respuesta | configuracionrespuesta                            |
               # ----PERFIL Asesor--------

        #Registrar Reclamo CMP/ DNI / Por cada motivo  / 3 montos / SI Adj Archivo / Agrega Email  -- >>> RECLAMO CMP SE ASIGNA A ELIZABETH GALLARDO O BRENDA CACEDA
      | B34103  | Callao2121++ | Asesor       | CE            | 1001033E        | 377753*****0835  | Reclamo | 4         | 2        | 21-23    | PEN-USD-PEN-USD                     | 100-200-60-20                     | 01/03/2019-02/04/2019-01/03/2019-01/03/2019-02/04/2019-01/03/2019                                   | TALMA SAC - WILSON SAC-EVERIS PO-COMPU SAC                                                           | 2-2           | Si      | S-        | MOVISTAR | S-ADDRESS | 0                                                 |
#         #Registrar Reclamo CMP /CE / motivo 21 y 23 y 24 / NO Adj Archivo /  Selecciona Direccion -- >>> VALIDAR RESUMEN Y SEGUIMIENTO DE TRAMITE SE ENCUENTRE EN ESTADO " DOCUMENTACION PENDIENTE"
      | B22444  | Callao2121++ | Asesor       | CE            | 1001033E        | 377753*****0835  | Reclamo | 4         | 3        | 21-23-24 | PEN-USD-PEN-USD-PEN-USD             | 100-200-60-20-60-200              | 01/03/2019-02/04/2019-01/03/2019-01/03/2019-02/04/2019-01/03/2019                                   | TALMA SAC - WILSON SAC-EVERIS PO-COMPU SAC-WILSON SAC-EVERIS PO                                      | 2-2-2         | No      | S-        | MOVISTAR | S-ADDRESS | 0                                                 |
#        # Registrar Reclamo CMP /CE / Si Adj Archivo / Agrega Direccion -- >>>RECLAMO CMP SE ASIGNA A ELIZABETH GALLARDO O BRENDA CACEDA
      | B22444  | Callao2121++ | Asesor       | CE            | 1001033E        | 454775******5609 | Reclamo | 4         | 3        | 21-23-24 | PEN-USD-PEN-USD-PEN-USD-PEN-USD-PEN | 15-50-90-300-150-50-200-3.99-1.22 | 01/12/2019-02/08/2019-01/12/2019-02/08/2019-01/12/2019-02/08/2019-01/12/2019-02/08/2019- 01/03/2020 | TALMA SAC - COMPU SAC - WILSON SAC-EVERIS PO-TALMA SAC - COMPU SAC - WILSON SAC-EVERIS PO-WILSON SAC | 3-3-3         | Si      | S-        | MOVISTAR | A-ADDRESS | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi |
#
#
#                                                          # ----PERFIL ESPECIALISTA---------
#
#        # Registrar Reclamo CMP / DNI / 2motivos / 2 montos / SI Adj Archivo / Agrega Email -- >>> >>> RECLAMO CMP SE ASIGNA A ELIZABETH GALLARDO O BRENDA CACEDA
      | B31847  | Callao2121++ | Especialista | DNI           | 47404036        | 454775******1046 | Reclamo | 4         | 2        | 21-25    | PEN-USD-PEN-USD                     | 100-30-30-50                      | 01/03/2019-02/04/2019-01/03/2019-02/04/2019                                                         | TALMA SAC - WILSON SAC-EVERIS PO-COMPU SAC                                                           | 2-2           | Si      | S-        | MOVISTAR | A-EMAIL   | hansonaron1101@gmail.com                          |
#        # Registrar Reclamo CMP/ CE / 2motivos / 2 montos / SI Adj Archivo / Agrega Email -- >>>>>> RECLAMO CMP SE ASIGNA A ELIZABETH GALLARDO O BRENDA CACEDA
      | B31847  | Callao2121++ | Especialista | CE            | 1001033E        | 454775******5609 | Reclamo | 4         | 2        | 21-23    | PEN-PEN-PEN-PEN                     | 120-50-30-50                      | 01/03/2019-02/04/2019-01/03/2019-02/04/2019                                                         | TALMA SAC - WILSON SAC-EVERIS PO-COMPU SAC                                                           | 2-2           | Si      | S-        | MOVISTAR | A-EMAIL   | hansonaron1101@gmail.com                          |
