@Regresion
@Derivacion
Feature:Derivación Optimizada

  Scenario Outline:Derivación
    Given se verificara si existen los numeros de reclamo con el especialista asignado
    When se accede con el usuario y contrasenia del especialista
   # And busca el reclamo y procedera a la <evaluacion> y asiganar el <areaderivacion>
    And si deriva, se verifica en Historial el especialista asignado
   # And se inicia sesion con dicho especialista del <area> asignada para evaluar el reclamo segun la <configuracion> y la <evaluacionfinal>
    And verificamos el estado del reclamo en historial
    Then eliminamos el numero de reclamo usado y reescribimo los numeros de reclamos a utilizar

    Examples:
      | Numero | actor  | evaluacion | areaderivacion | area | configuracion | evaluacionfinal |
      | 1      | Eliana | NF         |                |      |               |                 |
      | 2      | Eliana | NF         |                |      |               |                 |
      | 3      | Eliana | NF         |                |      |               |                 |
      | 4      | Eliana | NF         |                |      |               |                 |
      | 5      | Eliana | NF         |                |      |               |                 |
      | 6      | Eliana | NF         |                |      |               |                 |
      | 7      | Eliana | NF         |                |      |               |                 |
      | 8      | Eliana | NF         |                |      |               |                 |
      | 9      | Eliana | NF         |                |      |               |                 |
      | 10     | Eliana | NF         |                |      |               |                 |