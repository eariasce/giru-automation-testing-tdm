@Regresion
Feature: Validaciones Front

  @ValidacionesUsuario
  Scenario Outline: Validación Mensaje Eliminar Movimiento, Eliminar Motivo y Cambiar Tipo de Trámite
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido el area seleccionada <Area>
    And valido las opciones en la franja superior del usuario por su <perfil>

    Examples:
    #Validar Franjas verdes
      | usuario| contra| perfil | Area |
      #Especialista
      | B9948 | Callao2121++   | Especialista| USPR |
        #Supervisor
      | XT9656   | Callao2121++ | Supervisor   | GDPR  |
       #Asesor
      | B9948 | Callao2121++   | Asesor| USPR |
      #Jefe
      | XT9283  | Callao2121++ | Jefe   | GDPR  |
       #Asesor_R
      | B31407  | Callao2121++ | Asesor_R   | RMTC  |

  Scenario Outline:Validación de Áreas y Usuarios
    Given nos encontramos logueados en Giru con usuario <usuario> y password <password>
    Then se validara que el usuario tenga el <arearegistrada> correcta
    Examples:
      | usuario | password      | arearegistrada                   |
      | XT9283  | Callao2121++  | GESTIÓN DE PEDIDOS Y RECLAMOS TC |
      | XT9402  | Interbank1812 | GESTIÓN DE PEDIDOS Y RECLAMOS TC |

  Scenario:Validación Mensaje Cambio de Tarjeta
    Given nos encontramos logueados en Giru con usuario XT9402 y password Interbank1812
    When realizara la consulta por DNI y numero 12312312
    And seleccionamos un numero de tarjeta y despues seleccionamos otro numero de tarjeta
    Then nos deberia aparecer el mensaje ¿Estás seguro que deseas cambiar de producto?

  @Validaciones
  Scenario Outline: Validación Mensaje Eliminar Movimiento, Eliminar Motivo y Cambiar Tipo de Trámite
    Given nos encontramos logueados en Giru con usuario XT9402 y password Interbank1812
    When realizara la consulta por DNI y numero 12312312
    And seleccionamos un numero de tarjeta
    And seleccionamos el <tramite>, <tipologia> y <motivos>
    And agregamos un monto, al eliminar un monto debe aparecer el mensaje ¿Estás seguro que deseas eliminar el movimiento?
    And al seleccionar otro tramite nos debe aparecer el mensaje Si cambias de tipo de trámite, la tipología, los motivos y montos ingresados se perderán ¿Estás seguro que deseas proceder?
    Examples:
      | tramite | tipologia | motivos |
      | Reclamo | 1         | 1       |
      | Pedido  | 2         | 11      |


 # Scenario Outline: Validación Mensaje CAMPO OBLIGATORIO
 #   Given el usuario se encuentra logueado en Giru con usuario B32909 y password Callao2121++
 #   When realizara la consulta por DNI y 12312312
 #   And seleccionamos un numero de tarjeta
 #   And seleccionamos el <tramite>, <tipologia> y <motivos>
 #   And agregamos dos montos y hacemos click al boton Registrar
 #   Then se pondra en color rojo los campos faltantes con el <mensaje>
 #   Examples:
 #     | tramite | tipologia | motivos | mensaje             |
 #     | Reclamo | 1         | 1       | *CAMPO OBLIGATORIO. |


