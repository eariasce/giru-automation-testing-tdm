@Regresion
Feature: Validacion Descarga Bandeja

#Bandeja inicial por perfil
  @ValidarDescargaBandeja
  Scenario Outline:Busca un tranmite y lo descarga del excel

    Given se ingresaran al sistema con su <usuario> y <contra> con un <perfil> determinado
    And ingresa al menu de tramites depdendiendo del <perfil>
    And ordena la bandeja de tramites con fecha actual a anterior
    And ingresa el primer tramite de la lista
    And le click al boton de buscar
    And selecciona el boton de descargar
    Then se valida la descarga del archivo excel
    Examples:

    | usuario | contra       | perfil       |
    | xt9402  | Callao2121++ | Especialista |

