@Regresion
@Filtros
Feature: Validaciones Front


  @FiltrosBandejaTipoTipologiaMotivoJefeSupervisor
  Scenario Outline: Realizar la validacion de Filtros en Bandeja en ESPECIALISTA DE GPYR RECLAMOS
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido area seleccionada en panel tramites <Area>
    And se aplica el filtro de tipo tramite <Tramite>
   # And se valida la columna de <ColumnaTramite> el valor de <ValorTramite>
    And se aplica el filtro de tipo tipologia <Tipologia>
    And se aplica el filtro de tipo motivo <Motivo>
   # And se valida la columna de <ColumnaMotivo> el valor de <ValorMotivo>
    And se aplica el filtro de tipo moneda <Moneda> y monto <Monto>
   # And se valida la columna monto <ColumnaMoneda> el valor de <Monto>
    #Tramites
    # 1: Reclamo
    # 2: Pedido
    # 3: Solicitud
    #Columna 9: TipoTramite
    #Tipologias
    #1 : Cobro Indebido
    #2 : CNR
    #3 : CMP
    #Columno Examples:

    #Motivos
#1 : Penalidad por incumplimiento de Pago
#2: Comisión de membresía
#3: Interés de crédito normal y/o a financiar
#4: Interés por Disposición de Efectivo
#5: Comisión por uso de canales
#6: Comisión por envío físico de EECC
#7: Comisión por reposición de plástico
#8: Seguro de desgravamen
#9: Tipo de Cambio

    #Moneda
    #0: Soles
    #1: Dolares
    Examples:
    #Validar Franjas verdes
      | usuario| contra| perfil | Area | Tramite| ColumnaTramite | ValorTramite | Tipologia | Motivo | ColumnaMotivo | ValorMotivo| Moneda | Monto | ColumnaMoneda |
      #Especialista
      | b10049 | Callao2121++   | Especialista| USPR |1|     9    | Reclamo | 1| 1      | 6                    | Penalidad por incumplimiento de Pago                 |1 |300| 7 |
      #| B9948 | Callao2121++   | Especialista| USPR |2|     9    | Reclamo | 2| 2      | 6                    | Comisión de membresía                 |          |1 |300| 7|
      #| B9948 | Callao2121++   | Especialista| USPR |3|     9    | Reclamo | 3| 3      |6                     | Interés de crédito normal y/o a financiar                 | |1 |300| 7|



  #FILTRO SUPERVISOR Y JEFE (UAT)
  @FiltrosBandejaTipoTramiteSupervisorJefe
  Scenario Outline: Realizar la validacion de Filtros en Bandeja en ESPECIALISTA DE GPYR RECLAMOS
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido area seleccionada en panel tramites <Area>
    And se aplica el filtro de tipo tramite <Tramite>
    And se valida la columna de <Columna> el valor de <Valor>
    #Tramite 1: Reclamo
    #Tramite 2: Pedido
    #Tramite 3: Solicitud
    #Columna 9: TipoTramite
    Examples:
    #Validar Franjas verdes
      | usuario| contra| perfil | Area | Tramite| Columna | Valor |
      #Especialista
      | b10049 | Callao2121++   | Especialista| USPR |1|     9    | Reclamo |


  #FILTRO ESPECIALISTA
  @FiltrosBandejaTipoTramite
  Scenario Outline: Realizar la validacion de Filtros en Bandeja en ESPECIALISTA DE GPYR RECLAMOS
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido el area seleccionada <Area>
    And se aplica el filtro de tipo tramite <Tramite>
    And se valida la columna de <Columna> el valor de <Valor>
    #Tramite 1: Reclamo
    #Tramite 2: Pedido
    #Tramite 3: Solicitud
    #Columna 9: TipoTramite
    Examples:
    #Validar Franjas verdes
      | usuario| contra| perfil | Area | Tramite| Columna | Valor |
      #Especialista
      | B9948 | Callao2121++   | Especialista| USPR |1|     9    | Reclamo |


#GPYR
  @FiltrosBandejaPorTipologia
  Scenario Outline: Realizar la validacion de Filtros en Bandeja en ESPECIALISTA DE GPYR RECLAMOS
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido el area seleccionada <Area>
    And se selecciona <Tipologia> en el menu de tipologias
    And se aplica el filtro de Vencimiento <Vencimiento>
    #And se valida la columna de <ColumnaTramite> el valor de <ValorTramite>
    And se aplica el filtro de tipo motivo <Motivo>
    And se valida la columna de <ColumnaMotivo> el valor de <ValorMotivo>
    And se aplica el filtro de tipo moneda <Moneda> y monto <Monto>
    And se valida la columna monto <ColumnaMoneda> el valor de <Monto>

    #Tipologias en GPYR
    #1:CI
    #2:EXO
    #3:ANULACION
    #4:Finalizacion

    #Vencimientos
    #1:Nuevos
    #2:Por vencer
    #3:Vencidos del área
    #4:Vencidos ante el cliente
    Examples:
    #Validar Franjas verdes
      | usuario| contra| perfil | Area | Vencimiento| ColumnaTramite | ValorTramite | Tipologia | Motivo | ColumnaMotivo | ValorMotivo| Moneda | Monto | ColumnaMoneda |
      #Especialista
      | B10282  | Callao2121++   | Especialista| GDPR |1|     9    | Reclamo | 1| 1      | 6                    | Penalidad por incumplimiento de Pago                 |1 |300| 7 |



  @FiltrosBandejaTipoTipologiaMotivo
  Scenario Outline: Realizar la validacion de Filtros en Bandeja en ESPECIALISTA DE GPYR RECLAMOS
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido el area seleccionada <Area>
    And se aplica el filtro de tipo tramite <Tramite>
    And se valida la columna de <ColumnaTramite> el valor de <ValorTramite>
    And se aplica el filtro de tipo tipologia <Tipologia>
    And se aplica el filtro de tipo motivo <Motivo>
    And se valida la columna de <ColumnaMotivo> el valor de <ValorMotivo>
    And se aplica el filtro de tipo moneda <Moneda> y monto <Monto>
    And se valida la columna monto <ColumnaMoneda> el valor de <Monto>
    #Tramites
    # 1: Reclamo
    # 2: Pedido
    # 3: Solicitud
    #Columna 9: TipoTramite

    #Tipologias
    #1 : Cobro Indebido
    #2 : CNR
    #3 : CMP
    #Columno Examples:

    #Motivos
#1 : Penalidad por incumplimiento de Pago
#2: Comisión de membresía
#3: Interés de crédito normal y/o a financiar
#4: Interés por Disposición de Efectivo
#5: Comisión por uso de canales
#6: Comisión por envío físico de EECC
#7: Comisión por reposición de plástico
#8: Seguro de desgravamen
#9: Tipo de Cambio

    #Moneda
    #0: Soles
    #1: Dolares
    Examples:
    #Validar Franjas verdes
      | usuario| contra| perfil | Area | Tramite| ColumnaTramite | ValorTramite | Tipologia | Motivo | ColumnaMotivo | ValorMotivo| Moneda | Monto | ColumnaMoneda |
      #Especialista
     # | B9948 | Callao2121++   | Especialista| USPR |1|     9    | Reclamo | 1| 1      | 6                    | Penalidad por incumplimiento de Pago                 |1 |300| 7 |
      | B10282  | Callao2121++   | Especialista| GDPR |1|     9    | Reclamo | 1| 1      | 6                    | Penalidad por incumplimiento de Pago                 |1 |300| 7 |






