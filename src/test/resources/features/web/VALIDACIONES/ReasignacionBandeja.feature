@Regresion
Feature: Validacion Bandeja


  #Reasignación como Jefe (UAT)
  @FiltrosBandejaReasignacionTramiteJefeAJefe
  Scenario Outline: Realizar la validacion de reasignacion en Bandeja Jefe a su misma bandeja
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido el area seleccionada <Area>
    When ingresa en bandeja supervisor el <tramite> para seleccionar
    #And selecciona la opcion si si cuenta con seguro y comentario comentario
    And se selecciona un usuario con los tramites para realizar la derivacion
    And se valida el usuario reasignado por servicios en bandeja
    And se cierra sesion
    #Se repite el flujo con nuevo asesor reasignado para validar area seguros
    Given Ingresar con el usuario y password que tenga el tramite reasignado en bandeja
    When ingresa en bandeja supervisor el <tramite> para validar
    And valido nombre y area del asesor
    #And valido descarga del reclamo del <perfilReasignado>

    Examples:
    #Razon
    #0: Necesita validación de área correspondiente
    #1: Requiere ser retipificado
    #Area
    #0: Dpto Seguros
    #1: EECC TC
    #2: Custodia de documentos

      | usuario| contra| perfil | Area | tramite|perfilReasignado |
      | XT9656 | Callao2121++   | Jefe | GDPR |1609|JEFE|

  #Reasignación como Jefe (UAT)
  @FiltrosBandejaReasignacionTramiteJefeASupervisor
  Scenario Outline: Realizar la validacion de reasignacion en Bandeja Jefe a supervisor
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido el area seleccionada <Area>
    When ingresa en bandeja supervisor el <tramite> para seleccionar
    #And selecciona la opcion si si cuenta con seguro y comentario comentario
    And se selecciona un <supervisor> en especifico con los tramites para realizar la derivacion
    And se valida el usuario reasignado por servicios en bandeja
    And se cierra sesion
    #Se repite el flujo con nuevo asesor reasignado para validar area seguros
    Given Ingresar con el usuario y password que tenga el tramite reasignado en bandeja
    When ingresa en bandeja supervisor el <tramite> para validar
    And valido nombre y area del asesor
    #And valido descarga del reclamo del <perfilReasignado>

    Examples:
    #Razon
    #0: Necesita validación de área correspondiente
    #1: Requiere ser retipificado
    #Area
    #0: Dpto Seguros
    #1: EECC TC
    #2: Custodia de documentos

      | usuario| contra| perfil | Area | tramite| supervisor |perfilReasignado |
      #| XT9656 | Callao2121++   | Jefe | GDPR |1592|Jose Yarrin|SUPERVISOR|
      | XT9656 | Callao2121++   | Jefe | GDPR |1599|Asto Temple|ESPECIALISTA|


    #Reasignación como Jefe (UAT)
  @FiltrosBandejaReasignacionTramiteJefeAEspecialista
  Scenario Outline: Realizar la validacion de reasignacion en Bandeja Jefe a especialista
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido el area seleccionada <Area>
    When ingresa en bandeja supervisor el <tramite> para seleccionar
    #And selecciona la opcion si si cuenta con seguro y comentario comentario
    And se selecciona los tramites para realizar la derivacion
    And se valida el usuario reasignado por servicios en bandeja
    And se cierra sesion
    #Se repite el flujo con nuevo asesor reasignado para validar area seguros
    Given Ingresar con el usuario y password que tenga el tramite reasignado en bandeja
    When ingresa en la busqueda el <tramite> para seleccionarlo
    And valido nombre y area del asesor
    #And valido descarga del reclamo del <perfilReasignado>

    Examples:
    #Razon
    #0: Necesita validación de área correspondiente
    #1: Requiere ser retipificado
    #Area
    #0: Dpto Seguros
    #1: EECC TC
    #2: Custodia de documentos

      | usuario| contra| perfil | Area | tramite|perfilReasignado |
      | XT9656 | Callao2121++   | Jefe | GDPR |34821|ESPECIALISTA|
   #   | B9725  | Callao2121++   | Jefe | Controversias | 11111| ESPECIALISTA |
    #  | XT9443 | Callao2121++   | Jefe | Seguros | 11111| ESPECIALISTA |

   # Reasignación como Supervisor
  @FiltrosBandejaReasignacionTramiteComoSupervisorReasigna
  Scenario Outline: Realizar la validacion de reasignacion en Bandeja Supervisor a su misma bandeja
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido el area seleccionada <Area>
    When ingresa en bandeja supervisor el <tramite> para seleccionar
    #And selecciona la opcion si si cuenta con seguro y comentario comentario
    And se selecciona un usuario con los tramites para realizar la derivacion
    And se valida el usuario reasignado por servicios en bandeja
    And se cierra sesion
    #Se repite el flujo con nuevo asesor reasignado para validar area seguros
    Given Ingresar con el usuario y password que tenga el tramite reasignado en bandeja
    When ingresa en bandeja supervisor el <tramite> para validar
    And valido nombre y area del asesor
    #And valido descarga del reclamo del <perfilReasignado>

    Examples:
    #Razon
    #0: Necesita validación de área correspondiente
    #1: Requiere ser retipificado
    #Area
    #0: Dpto Seguros
    #1: EECC TC
    #2: Custodia de documentos

      | usuario| contra| perfil | Area | tramite|perfilReasignado |
    #  | B9068 | Callao2121++   | Supervisor | GDPR |17884| SUPERVISOR|
     #  | B34603 | Callao2121++   | Supervisor | Seguros |17884| SUPERVISOR|
      | B9725 | Callao2121++   | Supervisor | CONTROVERSIAS |17884| SUPERVISOR|

    #Supervisor a Especialista
  @FiltrosBandejaReasignacionTramiteSuperVisorEspecialista1
  Scenario Outline: Realizar la validacion de reasignacion en Bandeja Supervisor a Especialista
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido el area seleccionada <Area>
    When ingresa en bandeja supervisor el <tramite> para seleccionar
    #And selecciona la opcion si si cuenta con seguro y comentario comentario
    And se selecciona los tramites para realizar la derivacion
    And se valida el usuario reasignado por servicios en bandeja
    And se cierra sesion
    #Se repite el flujo con nuevo asesor reasignado para validar area seguros
    Given Ingresar con el usuario y password que tenga el tramite reasignado en bandeja
    When ingresa en la busqueda el <tramite> para seleccionarlo
    And valido nombre y area del asesor
    #And valido descarga del reclamo del <perfilReasignado>

    Examples:
    #Razon
    #0: Necesita validación de área correspondiente
    #1: Requiere ser retipificado
    #Area
    #0: Dpto Seguros
    #1: EECC TC
    #2: Custodia de documentos

      | usuario| contra| perfil | Area | tramite| perfilReasignado |
      | xt9283 | Callao2121++   | Supervisor| GDPR |36316| ESPECIALISTA |











  #AREA GDRP Deriva a seguros
  @FiltrosBandejaReasignacionTramite
  Scenario Outline: Realizar la validacion de Filtros en Bandeja en ESPECIALISTA DE GPYR RECLAMOS
    Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    And valido el area seleccionada <Area>
    When ingresa en bandeja la busqueda el <tramite> para seleccionar
    #And selecciona la opcion si si cuenta con seguro y comentario comentario
    And se abre la vista del tramite del Resumen selecciona derivacion
    And se selecciona la razon <Razon> y el area <area> al cual se derivara el tranmite
    And se valida el usuario reasignado por servicios en bandeja
    And se cierra sesion
    #Se repite el flujo con nuevo asesor reasignado para validar area seguros
    Given Ingresar con el usuario y password que tenga el tramite reasignado en bandeja
    And valido el area seleccionada <AreaValidar>
    When ingresa en bandeja la busqueda el <tramite> para seleccionar

    Examples:
    #Razon
    #0: Necesita validación de área correspondiente
    #1: Requiere ser retipificado
    #Area
    #0: Dpto Seguros
    #1: EECC TC
    #2: Custodia de documentos

      | usuario| contra| perfil | Area | tramite|  Razon | area | AreaValidar|
      #Especialista
      | B17486 | Callao2121++   | Especialista| GDPR |40325| 0 | 0| Seguros |

