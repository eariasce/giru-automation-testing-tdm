@Regresion
Feature: Descarga Excel

  @ValidarHistorialDescarga
  Scenario Outline:Busca un tranmite y lo descarga del excel

    Given se ingresaran al sistema con su <usuario> y <contra> con un <perfil> determinado
    And ingresa al menu de historial
    And ingresa el numero de <tramite>
    And le click al boton de buscar
    And selecciona el boton de descargar
    Then se valida la descarga del archivo excel
    Examples:
      | usuario | contra       | perfil       | tramite |
      | xt9402  | Callao2121++ | Especialista | 27513    |


  #Scenario Outline:Crea un tranmite y lo descarga excel

    #Given se ingresara al sistema con su <usuario> y <contra> con un <perfil> determinado
    #When hace la consulta por <tipodocumento> y <numerodocumento> de un cliente
    #And indica el <numerotarjeta>, <tramite> y <tipologia>
    #And elige la <cantidad> de <motivos> y configura la fecha <fecha> tipo de moneda <tipomoneda> y monto <monto> por motivo segun la <configuracion>
    #And en caso los motivos tuviera Comision de Membresia se aplicaria la Razon Operativa <razonoperativa> y seleccionar el Tipo de Razon Operativa <tiporazon>
    #And ingresa un comentariocliente y un comentariointerno opcional
    #And se adjuntara un archivo <archivo> en caso sea necesario
    #And selecciona o añade segun el <indicador> y <operador>
    #And seleciona el medio de respuesta <respuesta> segun la configuracion de la respuesta <configuracionrespuesta>
    #Then dependiendo del perfil se verificara el reclamo descargando el Historial
    #Examples:
     # | usuario | contra       | perfil       | tipodocumento | numerodocumento | numerotarjeta    | tramite | tipologia | cantidad | motivos | fecha      | tipomoneda | monto  | configuracion | razonoperativa | tiporazon | archivo | indicador | operador | respuesta | configuracionrespuesta                            |
      #| xt9402  | Callao2121++ | Especialista | DNI           | 12312312        | 544402******1684 | Reclamo | 1         | 1        | 1       | 01/03/2019 | PEN        | 150.99 | 1             | No             |           | No      | S-        | MOVISTAR | A-ADDRESS | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi |
      #| xt9656  | Callao2121++ | Supervisor | DNI           | 12312312        | 544402******1684 | Reclamo | 1         | 1        | 1       | 01/03/2019 | PEN        | 150.99 | 1             | No             |           | No      | S-        | MOVISTAR | A-ADDRESS | 14-01-01-1-pepito-los jasmines-315-B-3-20-Por ahi |


