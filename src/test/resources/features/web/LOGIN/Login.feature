@allLogin
Feature: Login - Giru

  @Automated @Happy_path @functional_testing @LOGIN_GIRU
  Scenario Outline: [HAPPY PATH] Valida login exitoso Giru
    Given se accedera a la web de Giru
    When ingresara al sistema con su <usuario> y <contrasenia> con un perfil <perfil> determinado
    And verificara que el acceso sea correcto segun el perfil
    Then cerrara sesion

    Examples: Usuario loguea satisfactoriamente con diferentes perfiles
      | usuario | contrasenia  | perfil |
      | xt9928  | Callao2121++ | Asesor |
      #| xt9402  | Callao2121++ | Especialista |
      #| B32909  | Callao2121++ | Especialista |
      #| xt9283  | Callao2121++ | Supervisor   |

  @Automated @Unhappy_path @functional_testing @LOGIN_GIRU
  Scenario Outline: [UNHAPPY PATH] Login con Usuario y/o Contraseña Erronea
    Given se accedera a la web de Giru
    When ingresara con usuario <usuario> y contrasenia <contrasenia>
    Then se validara el mensaje de error *EL ID O CONTRASEÑA NO COINCIDEN en caso el usuario y/o la contrasenia sean incorrectas

    Examples: Usuario loguea insatisfactoriamente con su perfiles
      | usuario | contrasenia  |
      | xr4545  | Callao2121++ |

