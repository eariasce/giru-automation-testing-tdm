@Regresion
@Bandeja
Feature:Bandeja Giru

  Scenario Outline: Verficación de Filtros Avanzados
    Given se ingresara a la web de Giru para verificar los datos de un registro con filtros
    And accedemos a tramites y elegiremos el <Vencimiento> y <TipoTrámite>
    And despues de haber seleccionado el tipo de tramite se habilitara la opcion <Tipologia> respectiva y <Motivos>
    And elegiremos el <Segmento>, <Estado>, <NivelServicio>, <MonedaMonto>
    And terminaremos eligiendo <MásFiltros>
    And ingresaremos el numero de reclamo para verificar los filtros
    Then eliminamos el registro usado y reescribimos el registro a utilizar

    Examples:
      | Vencimiento | TipoTrámite | Tipologia | Motivos | Segmento | Estado     | NivelServicio | MonedaMonto | MásFiltros      |
      | Nuevo       | Reclamo     | CI        |         | Joven    | En proceso |               | USD-1-300   | NA-Si-Domicilio |


      #| Filtros avanzados-2 | Usuario2 | Nuevo       | Reclamo     | Penalidad por incumplimiento de Pago |          | En proceso |               | USD-1-100   | NA-NA-Domicilio |
     # | Filtros avanzados-3 | Usuario3 |             | Pedido      | Comisión por envío físico de EECC    |          |            |               |             | NA-NA-Email     |
      #| Filtros avanzados-4 | Usuario4 |             | Pedido      | Seguro de desgravamen                |          | En proceso |               | S-1-100     | NA-NA-Email     |