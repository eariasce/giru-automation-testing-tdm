@allExoneracionCobros
Feature: Exoneración de Cobros

  @Automated @Happy_path @functional_testing @EXONERACION_COBROS
  Scenario Outline: [HAPPY PATH] Valida la creación de registro trámite pedidos exoneración de cobros
    Given que el usuario accede a la aplicacion GIRU
    When se ingresa usuario <user> y contrasena <password>
    And selecciona la opcion iniciar sesion
    Then se valida que el acceso sea correcto con un perfil <profile> determinado
    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
 #  And se registra Reclamo de la tarjeta
    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
      |test|PreCon-Motivo|	PreCon-Cantidad de Motivos|	PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
      |3|Motivo 1-15-16-17|3|1|USD-USD-USD|10/20/30|25/02/2021|
      |8|Motivo 4-11|1|2|PEN|10|28/05/2021|
      |9|Motivo 4-11|1|2|PEN|20|23/05/2021|

    And ingresa un "<PreCon-Respuesta Cliente>" y un "<PreCon-Agregar Comentarios>" opcional
    And se adjuntara un archivo "<PreCon-Adjuntar Archivo>" en caso sea necesario
    And selecciona o añade segun el "<PreCon-Numero Celular>" y "<PreCon-Operador>"
    And seleciona el medio de respuesta "<PreCon-Medio Respuesta>" segun la configuracion de la respuesta "<PreCon-Nuevo Registro Respuesta>"
    Examples: Registro tramite reclamo cobros indebidos
      |numeroescenario|user|password|profile|numeroTarjeta|tipoDocumento|numeroDocumento|tramite|tipologia|PreCon-Agregar Comentarios|PreCon-Respuesta Cliente|PreCon-Adjuntar Archivo|PreCon-Medio Respuesta|PreCon-Nuevo Registro Respuesta|PreCon-Numero Celular|PreCon-Operador|PreCon-RazonOperativa|PreCon-TipoRazonOperativa|
      |3|B34097|Abc12345$|Asesor|491337******7390|DNI|40614144|Pedido|2|No|Sí|No|Dirección|Registrado|Registrado|ENTEL|Ninguno|Ninguno|
      |8|B34097|Abc12345$|Asesor|491337******7390|DNI|40614144|Pedido|2|Sí|Sí|Sí|Correo|Registrado|950276375|CLARO|No|Ninguno|
      |9|B34097|Abc12345$|Asesor|491337******7390|DNI|40614144|Pedido|2|Sí|Sí|Sí|Correo|prueba@gmail.com|Registrado|CLARO|No|Ninguno|

  @Automated @Happy_path @functional_testing @EXONERACION_COBROS_RAZON_OPERATIVA
  Scenario Outline: [HAPPY PATH] Valida la creación de registro tramite pedido exoneración de cobros con razón operativa
    Given que el usuario accede a la aplicacion GIRU
    When se ingresa usuario <user> y contrasena <password>
    And selecciona la opcion iniciar sesion
    Then se valida que el acceso sea correcto con un perfil <profile> determinado
    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
#    And se registra Reclamo de la tarjeta
    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
      |test|PreCon-Motivo|	PreCon-Cantidad de Motivos|	PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
      |4|Motivo 4-11|1|1|PEN|20|20/02/2021 |
      |5|Motivo 4-11|1|1|USD|20|15/03/2021 |
      |6|Motivo 4-11|1|1|USD|20|15/04/2021 |
      |10|Motivo 4-11-18|2|1|PEN-PEN|20/20|21/01/2021 - 24/04/2021|
    And en caso los motivos tuviera Comision de Membresia se aplicaria la Razon Operativa "<PreCon-RazonOperativa>" y seleccionar el Tipo de Razon Operativa "<PreCon-TipoRazonOperativa>"
    And ingresa un "<PreCon-Respuesta Cliente>" y un "<PreCon-Agregar Comentarios>" opcional
    And se adjuntara un archivo "<PreCon-Adjuntar Archivo>" en caso sea necesario
    And selecciona o añade segun el "<PreCon-Numero Celular>" y "<PreCon-Operador>"
    And seleciona el medio de respuesta "<PreCon-Medio Respuesta>" segun la configuracion de la respuesta "<PreCon-Nuevo Registro Respuesta>"
    And seleccionar los datos de registro de nueva DIRECCION "<numeroescenario>" "<PreCon-Nuevo Registro Respuesta>"
      |test|Dpto   |	Provincia|	Distrito|	Tipo de Via|	Nombre de via|	Urbanizacion| Numero| Manzana|Lote|Interior|Referencia|
      |4   |  14   |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |5   |   14  |     1409     |    140901  |      1         |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |6   |   14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |10  |   14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |

    Examples: Registro tramite pedido de exoneración de cobros
      |numeroescenario|user|password|profile|numeroTarjeta|tipoDocumento|numeroDocumento|tramite|tipologia|PreCon-Agregar Comentarios|PreCon-Respuesta Cliente|PreCon-Adjuntar Archivo|PreCon-Medio Respuesta|PreCon-Nuevo Registro Respuesta|PreCon-Numero Celular|PreCon-Operador|PreCon-RazonOperativa|PreCon-TipoRazonOperativa|
      |4|B34097|Abc12345$|Asesor|491337******7390|DNI|40614144|Pedido|2|No|Sí|Sí|Dirección|Nuevo|950276375|MOVISTAR|Sí|RO001|
      |5|B34097|Abc12345$|Asesor|377753*****0835|CE|1001033E  |Pedido|2|No|Sí|No|Correo|prueba@gmail.com|Registrado|CLARO|Sí|RO002|
      |6|B34097|Abc12345$|Asesor|377753*****0835|CE|1001033E  |Pedido|2|Sí|Sí|Sí|Dirección|Nuevo|950276375|CLARO|Sí|RO003|
      |10|B34097|Abc12345$|Asesor|377753*****0835|CE|1001033E |Pedido|2|Sí|Sí|Sí|Dirección|Nuevo|950276375|CLARO|Sí|RO004|

  @Automated @Happy_path @functional_testing @EXONERACION_COBROS_NUEVA_DIRECCION
  Scenario Outline: [HAPPY PATH] Valida registro tramite pedido exoneración de cobros con una nueva dirección
    Given que el usuario accede a la aplicacion GIRU
    When se ingresa usuario <user> y contrasena <password>
    And selecciona la opcion iniciar sesion
    Then se valida que el acceso sea correcto con un perfil <profile> determinado
    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
   # And se registra Reclamo de la tarjeta
    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
     |test     |PreCon-Motivo    |PreCon-Cantidad de Motivos|PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
     |1        |Motivo 1-30      |1                      |1                                      |PEN           |50            |15/02/2021     |
     |2        |Motivo 1-12-14   |2                      |1                                      |USD-USD       |20/20         |21/02/2021 - 10/04/2021|
     |7        |Motivo 4-11      |1                      |1                                      |PEN           |50            |14/02/2021|

    And ingresa un "<PreCon-Respuesta Cliente>" y un "<PreCon-Agregar Comentarios>" opcional
    And se adjuntara un archivo "<PreCon-Adjuntar Archivo>" en caso sea necesario
    And selecciona o añade segun el "<PreCon-Numero Celular>" y "<PreCon-Operador>"
    And seleciona el medio de respuesta "<PreCon-Medio Respuesta>" segun la configuracion de la respuesta "<PreCon-Nuevo Registro Respuesta>"
    And seleccionar los datos de registro de nueva DIRECCION "<numeroescenario>" "<PreCon-Nuevo Registro Respuesta>"
    |test|Dpto|	Provincia|	Distrito|	Tipo de Via|	Nombre de via|	Urbanizacion| Numero| Manzana|Lote|Interior|Referencia|
    |1   |  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
    |2   |   14 |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
    |7   |   14 |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |

    Examples: Registro tramite reclamo cobros indebidos
    |user|password|profile|numeroTarjeta|tipoDocumento|numeroDocumento|tramite|tipologia|PreCon-Agregar Comentarios|PreCon-Respuesta Cliente|PreCon-Adjuntar Archivo|PreCon-Medio Respuesta|PreCon-Nuevo Registro Respuesta|PreCon-Numero Celular|PreCon-Operador|PreCon-RazonOperativa|PreCon-TipoRazonOperativa|
    |B34097|Abc12345$|Asesor|491337******7390|DNI|40614144|Pedido|2|Sí|Sí|Sí|Dirección|Nuevo|Registrado|CLARO|Ninguno|Ninguno|
    |B34097|Abc12345$|Asesor|491337******7390|DNI|40614144|Pedido|2|Sí|Sí|Sí|Dirección|Nuevo|950276375|BITEL|Ninguno|Ninguno|
    |B34097|Abc12345$|Asesor|491337******7390|DNI|40614144|Pedido|2|Sí|Sí|Sí|Dirección|Nuevo|950276375|CLARO|No|Ninguno|



#  @Automated @Unhappy_path @functional_testing @COBROS_INDEBIDOS_FECHA_INCORRECTA
#  Scenario Outline: [HAPPY PATH] Valida registro tramite pedido exoneracion de cobros con casos de error fecha incorrecta
#    Given que el usuario accede a la aplicacion GIRU
#    When se ingresa usuario <user> y contrasena <password>
#    And selecciona la opcion iniciar sesion
#    Then se valida que el acceso sea correcto con un perfil <profile> determinado
#    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
#    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
#    And se registra Reclamo de la tarjeta
#    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
#      |num |PreCon-Motivo|	PreCon-Cantidad de Motivos|	PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
#      |2 |Motivo 1-29-9|	2|	1|	USD-USD|	20/50|	21/02/2023-15/02/2023|
#    And se valida fecha incorrecta
#    Examples: Registro tramite reclamo cobros indebidos
#      |numeroescenario | user   |	 password  |	 profile |	 tipoDocumento |	 numeroDocumento |	 numeroTarjeta    |	 tramite |	 tipologia |	 |	Atencion|	Perfil|	No.|	Desestimada|	Version|	PreCon-Tipologia|	PreCon-Agregar Comentarios|	PreCon-Respuesta Cliente|	PreCon-Adjuntar Archivo|	PreCon-Medio Respuesta|	PreCon-Numero Celular|	PreCon-Nuevo Registro Respuesta|	PreCon-Tipo Tarjeta|	PreCon-Numero documento|	PreCon-RazonOperativa|	PreCon-TipoCMPMalConcretado|	PreCon-Operador|	PreCon-FechaConsumoMalConcretado|	PreCon-TipoRazonOperativa|	PreCon-MotivoSucedidoTarjeta|	PreCon-SeleccionarNoBloqueado|	PreCon-DescripcionNoBloqueado|	PreCon-NombreComercio|	PreCon-CantidadTarjetas|
#      |2 | B34097 |	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	36|	|	3.0.7|	CI|	Sí|	Sí|	No|	Correo|	Registrado|	prueba@gmail.com|	Visa|	Correcto|	Ninguno|	Ninguno|	ENTEL|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Uno|
#
#  @Automated @Unhappy_path @functional_testing @COBROS_INDEBIDOS_SIN_RESPUESTA_CLIENTE
#  Scenario Outline: [HAPPY PATH] Valida registro tramite pedido exoneracion de cobros con casos de error no agrega respuesta
#    Given que el usuario accede a la aplicacion GIRU
#    When se ingresa usuario <user> y contrasena <password>
#    And selecciona la opcion iniciar sesion
#    Then se valida que el acceso sea correcto con un perfil <profile> determinado
#    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
#    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
#    And se registra Reclamo de la tarjeta
#    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
#      |num |PreCon-Motivo|	PreCon-Cantidad de Motivos|	PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
#      |9 |Motivo 4-2-7|	2|	3|	PEN-PEN|	90-90-10/20-30-30|	21/02/2021-15/02/2020|
#    And ingresa un "<PreCon-Respuesta Cliente>" y un "<PreCon-Agregar Comentarios>" opcional
#    And se valida que no ingresa una respuesta para el cliente
#    Examples: Registro tramite reclamo cobros indebidos
#      |numeroescenario | user   |	 password  |	 profile |	 tipoDocumento |	 numeroDocumento |	 numeroTarjeta    |	 tramite |	 tipologia |	 |	Atencion|	Perfil|	No.|	Desestimada|	Version|	PreCon-Tipologia|	PreCon-Agregar Comentarios|	PreCon-Respuesta Cliente|	PreCon-Adjuntar Archivo|	PreCon-Medio Respuesta|	PreCon-Numero Celular|	PreCon-Nuevo Registro Respuesta|	PreCon-Tipo Tarjeta|	PreCon-Numero documento|	PreCon-RazonOperativa|	PreCon-TipoCMPMalConcretado|	PreCon-Operador|	PreCon-FechaConsumoMalConcretado|	PreCon-TipoRazonOperativa|	PreCon-MotivoSucedidoTarjeta|	PreCon-SeleccionarNoBloqueado|	PreCon-DescripcionNoBloqueado|	PreCon-NombreComercio|	PreCon-CantidadTarjetas|
#      |9 | B34097 |	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	55|	|	3.0.7|	CI|	Sí|	~No|	Sí|	Dirección|	950276375|	Nuevo|	Visa|	Correcto|	Si|	Ninguno|	MOVISTAR|	Ninguno|	RO003|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Varias|
#
#
#
#  @Automated @Unhappy_path @functional_testing @EXONERACIÓN_COBROS_MONTO_CERO
#  Scenario Outline: [HAPPY PATH] Valida registro tramite pedido exoneracion de cobros con casos de error monto cero
#    Given que el usuario accede a la aplicacion GIRU
#    When se ingresa usuario <user> y contrasena <password>
#    And selecciona la opcion iniciar sesion
#    Then se valida que el acceso sea correcto con un perfil <profile> determinado
#    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
#    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
#    And se registra Reclamo de la tarjeta
#    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
#      |numeroescenario|PreCon-Motivo|	PreCon-Cantidad de Motivos|	PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
#      |10 |Motivo 4-2-8|	2|	1|	PEN-PEN|	0-0|	21/02/2021-15/02/2020|
#    And se valida un monto ingresado incorrecto
#    Examples: Registro tramite reclamo cobros indebidos
#      |numeroescenario | user   |	 password  |	 profile |	 tipoDocumento |	 numeroDocumento |	 numeroTarjeta    |	 tramite |	 tipologia |	 |	Atencion|	Perfil|	No.|	Desestimada|	Version|	PreCon-Tipologia|	PreCon-Agregar Comentarios|	PreCon-Respuesta Cliente|	PreCon-Adjuntar Archivo|	PreCon-Medio Respuesta|	PreCon-Numero Celular|	PreCon-Nuevo Registro Respuesta|	PreCon-Tipo Tarjeta|	PreCon-Numero documento|	PreCon-RazonOperativa|	PreCon-TipoCMPMalConcretado|	PreCon-Operador|	PreCon-FechaConsumoMalConcretado|	PreCon-TipoRazonOperativa|	PreCon-MotivoSucedidoTarjeta|	PreCon-SeleccionarNoBloqueado|	PreCon-DescripcionNoBloqueado|	PreCon-NombreComercio|	PreCon-CantidadTarjetas|
#      |10 | B34097 |	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	57|	|	3.0.7|	CI|	Sí|	Sí|	No|	Dirección|	Registrado|	Nuevo|	Visa|	Correcto|	Si|	Ninguno|	BITEL|	Ninguno|	RO004|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Uno|
#
