@allExoneracionCobros
Feature: Exoneración de Cobros

  @Automated @Happy_path @functional_testing @COBROS_INDEBIDOS
  Scenario Outline: [HAPPY PATH] Valida registro de creación tramite reclamo cobros indebidos,seleccionando motivo comisión de membresía
    Given que el usuario accede a la aplicacion GIRU
    #When se ingresa usuario <user> y contrasena <password>
    When se ingresa los datos de usuario de giru "<numeroescenario>"
    And selecciona la opcion iniciar sesion
   # Then se valida que el acceso sea correcto con un perfil <profile> determinado
    Then se valida que el acceso sea correcto con un perfil determinado "<numeroescenario>"
   # And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
    And realiza la identificacion del cliente "<numeroescenario>"
      #And realiza la identificacion por cantidad de tarjetas 1
    #And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
    And selecciona tarjeta, elige el tipo de tramite y selecciona la tipologia "<numeroescenario>"
    And se registra Reclamo de la tarjeta
    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
      |test|PreCon-Motivo|	PreCon-Cantidad de Motivos|	PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
      |1        |Motivo 1-5-8 |	2                         |  	1                                    |    PEN-PEN|      	20/50|	21/02/2021-15/02/2020|
      |3|Motivo 1-3-6-7|	3|	1|	USD-USD-USD|	20/50/70|	1/02/2021-15/02/2020-13/04/2019|
      |8|Motivo 4-2-9|	2|	2|	PEN-PEN|	50-50/40-30|	21/02/2021-15/02/2020|
      |11|Motivo 4-2-29-5|	3|	1|	USD-USD-USD|	70/90/99|	21/02/2021-15/02/2020-17/03/2021|
    And ingresa un "<PreCon-Respuesta Cliente>" y un "<PreCon-Agregar Comentarios>" opcional
    And se adjuntara un archivo "<PreCon-Adjuntar Archivo>" en caso sea necesario
    And selecciona o añade segun el "<PreCon-Numero Celular>" y "<PreCon-Operador>"
    And seleciona el medio de respuesta "<PreCon-Medio Respuesta>" segun la configuracion de la respuesta "<PreCon-Nuevo Registro Respuesta>"
    Examples: Registro tramite reclamo cobros indebidos
     |numeroescenario | user   |	 password  |	 profile |	 tipoDocumento |	 numeroDocumento |	 numeroTarjeta    |	 tramite |	 tipologia |	|	Atencion|	Perfil|	No.|	Desestimada|	Version|	PreCon-Tipologia|	PreCon-Agregar Comentarios|	PreCon-Respuesta Cliente|	PreCon-Adjuntar Archivo|	PreCon-Medio Respuesta|	PreCon-Numero Celular|	PreCon-Nuevo Registro Respuesta|	PreCon-Tipo Tarjeta|	PreCon-Numero documento|	PreCon-RazonOperativa|	PreCon-TipoCMPMalConcretado|	PreCon-Operador|	PreCon-FechaConsumoMalConcretado|	PreCon-TipoRazonOperativa|	PreCon-MotivoSucedidoTarjeta|	PreCon-SeleccionarNoBloqueado|	PreCon-DescripcionNoBloqueado|	PreCon-NombreComercio|	PreCon-CantidadTarjetas|
     |1|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Reclamo  |	1|	    |productos|	Especialista|	35|	|	3.0.7|	CI|	No|	Sí|	Sí|	Correo|	Registrado|	Registrado|	Mastercard|	Correcto|	Ninguno|	Ninguno|	CLARO|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Uno|
     |3|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Reclamo  |	1|	     |productos|	Especialista|	38|	|	3.0.7|	CI|	No|	Sí|	No|	Correo|	950276375|	prueba@gmail.com|	Mastercard|	Correcto|	Ninguno|	Ninguno|	MOVISTAR|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Varias|
     |8 | B34097 |	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	54|	|	3.0.7|	CI|	No|	Sí|	Sí|	Correo|	Registrado|	Registrado|	Mastercard|	Correcto|	No|	Ninguno|	BITEL|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Varias|
     |11 | B34097 |	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	62|	|	3.0.7|	CI|	Sí|	Sí|	No|	Dirección|	Registrado|	Registrado|	Mastercard|	Correcto|	No|	Ninguno|	CLARO|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Uno|

  @Automated @Happy_path @functional_testing @COBROS_INDEBIDOS_RAZON_OPERATIVA
  Scenario Outline: [HAPPY PATH] Valida registro tramite reclamo cobros indebidos con razon operativa
    Given que el usuario accede a la aplicacion GIRU
    #When se ingresa usuario <user> y contrasena <password>
    When se ingresa los datos de usuario de giru "<numeroescenario>"
    And selecciona la opcion iniciar sesion
   # Then se valida que el acceso sea correcto con un perfil <profile> determinado
    Then se valida que el acceso sea correcto con un perfil determinado "<numeroescenario>"
   # And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
    And realiza la identificacion del cliente "<numeroescenario>"
      #And realiza la identificacion por cantidad de tarjetas 1
    #And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
    And selecciona tarjeta, elige el tipo de tramite y selecciona la tipologia "<numeroescenario>"
    And se registra Reclamo de la tarjeta
    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
      |test|PreCon-Motivo|	PreCon-Cantidad de Motivos|	PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
      |4|Motivo 4-2|	1|	2|	USD|	20-50|	30/01/2020|
      |7|Motivo 4-2-5|	2|	3|	USD-USD|	20-20-30/30-20-10|	21/02/2021-15/02/2020|
    And en caso los motivos tuviera Comision de Membresia se aplicaria la Razon Operativa "<PreCon-RazonOperativa>" y seleccionar el Tipo de Razon Operativa "<PreCon-TipoRazonOperativa>"
    And ingresa un "<PreCon-Respuesta Cliente>" y un "<PreCon-Agregar Comentarios>" opcional
    And se adjuntara un archivo "<PreCon-Adjuntar Archivo>" en caso sea necesario
    And selecciona o añade segun el "<PreCon-Numero Celular>" y "<PreCon-Operador>"
    And seleciona el medio de respuesta "<PreCon-Medio Respuesta>" segun la configuracion de la respuesta "<PreCon-Nuevo Registro Respuesta>"
    Examples: Registro tramite reclamo cobros indebidos
      |numeroescenario | user   |	 password  |	 profile |	 tipoDocumento |	 numeroDocumento |	 numeroTarjeta    |	 tramite |	 tipologia |	 |	Atencion|	Perfil|	No.|	Desestimada|	Version|	PreCon-Tipologia|	PreCon-Agregar Comentarios|	PreCon-Respuesta Cliente|	PreCon-Adjuntar Archivo|	PreCon-Medio Respuesta|	PreCon-Numero Celular|	PreCon-Nuevo Registro Respuesta|	PreCon-Tipo Tarjeta|	PreCon-Numero documento|	PreCon-RazonOperativa|	PreCon-TipoCMPMalConcretado|	PreCon-Operador|	PreCon-FechaConsumoMalConcretado|	PreCon-TipoRazonOperativa|	PreCon-MotivoSucedidoTarjeta|	PreCon-SeleccionarNoBloqueado|	PreCon-DescripcionNoBloqueado|	PreCon-NombreComercio|	PreCon-CantidadTarjetas|
      |4 | B34097 |	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	42|	|	3.0.7|	CI|	Sí|	Sí|	Sí|	Dirección|	950276375|	Registrado|	Visa|	Correcto|	Si|	Ninguno|	MOVISTAR|	Ninguno|	RO001|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Uno|
      |7 | B34097 |	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	53|	|	3.0.7|	CI|	No|	Sí|	No|	Correo|	Registrado|	prueba@gmail.com|	Visa|	Correcto|	Si|	Ninguno|	ENTEL|	Ninguno|	RO002|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Uno|

  @Automated @Happy_path @functional_testing @COBROS_INDEBIDOS_NUEVA_DIRECCION
  Scenario Outline: [HAPPY PATH] Valida registro tramite reclamo cobros indebidos con una nueva dirección
    Given que el usuario accede a la aplicacion GIRU
    When se ingresa usuario <user> y contrasena <password>
    And selecciona la opcion iniciar sesion
    Then se valida que el acceso sea correcto con un perfil <profile> determinado
    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
    And se registra Reclamo de la tarjeta
    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
      |escenario|PreCon-Motivo|	PreCon-Cantidad de Motivos|	PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
      |5|Motivo 4-2|	1|	1|	PEN|	70|	30/01/2020|
      |6|Motivo 4-2|	1|	1|	PEN|	50|	30/01/2020|
      |12|Motivo 4-2-9-7|	3|	2|	PEN-PEN-PEN|	50-50/30-20/40-40|	21/02/2021-15/02/2020-17/03/2021|
    And ingresa un "<PreCon-Respuesta Cliente>" y un "<PreCon-Agregar Comentarios>" opcional
    And se adjuntara un archivo "<PreCon-Adjuntar Archivo>" en caso sea necesario
    And selecciona o añade segun el "<PreCon-Numero Celular>" y "<PreCon-Operador>"
    And seleciona el medio de respuesta "<PreCon-Medio Respuesta>" segun la configuracion de la respuesta "<PreCon-Nuevo Registro Respuesta>"
    And seleccionar los datos de registro de nueva DIRECCION "<numeroescenario>" "<PreCon-Nuevo Registro Respuesta>"
      |escenario  |Dpto|	Provincia|	Distrito|	Tipo de Via|	Nombre de via|	Urbanizacion| Numero| Manzana|Lote|Interior|Referencia|
      |5|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |6|   14  |     1409     |    140901  |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |12|   14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |

    Examples: Registro tramite reclamo cobros indebidos
      |numeroescenario | user   |	 password  |	 profile |	 tipoDocumento |	 numeroDocumento |	 numeroTarjeta    |	 tramite |	 tipologia |	 |	Atencion|	Perfil|	No.|	Desestimada|	Version|	PreCon-Tipologia|	PreCon-Agregar Comentarios|	PreCon-Respuesta Cliente|	PreCon-Adjuntar Archivo|	PreCon-Medio Respuesta|	PreCon-Numero Celular|	PreCon-Nuevo Registro Respuesta|	PreCon-Tipo Tarjeta|	PreCon-Numero documento|	PreCon-RazonOperativa|	PreCon-TipoCMPMalConcretado|	PreCon-Operador|	PreCon-FechaConsumoMalConcretado|	PreCon-TipoRazonOperativa|	PreCon-MotivoSucedidoTarjeta|	PreCon-SeleccionarNoBloqueado|	PreCon-DescripcionNoBloqueado|	PreCon-NombreComercio|	PreCon-CantidadTarjetas|
      |5 | B34097 |	 Abc12345$ |	 Asesor  |	 CE            |    1001033E|	 377753*****0835  |	 Reclamo  |	1|	|	productos|	Especialista|	43|	|	3.0.7|	CI|	Sí|	Sí|	No|	Dirección|	950276375|	Nuevo|	Visa|	Correcto|	No|	Ninguno|	CLARO|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Uno|
      |6 | B34097 |	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	44|	|	3.0.7|	CI|	Sí|	Sí|	Sí|	Dirección|	950276375|	Nuevo|	Visa|	Correcto|	No|	Ninguno|	CLARO|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Uno|
      |12 | B34097|	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	63|	|	3.0.7|	CI|	Sí|	Sí|	No|	Dirección|	Registrado|	Nuevo|	Visa|	Correcto|	Si|	Ninguno|	CLARO|	Ninguno|	RO005|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Varias|

  @Automated @Unhappy_path @functional_testing @COBROS_INDEBIDOS_FECHA_INCORRECTA
  Scenario Outline: [UNHAPPY PATH] Valida registro tramite pedido exoneracion de cobros con casos de error fecha incorrecta
    Given que el usuario accede a la aplicacion GIRU
    When se ingresa usuario <user> y contrasena <password>
    And selecciona la opcion iniciar sesion
    Then se valida que el acceso sea correcto con un perfil <profile> determinado
    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
    And se registra Reclamo de la tarjeta
    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
     |num |PreCon-Motivo|	PreCon-Cantidad de Motivos|	PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
     |2 |Motivo 1-29-9|	2|	1|	USD-USD|	20/50|	21/02/2023-15/02/2023|
    And se valida fecha incorrecta
    Examples: Registro tramite reclamo cobros indebidos
      |numeroescenario | user   |	 password  |	 profile |	 tipoDocumento |	 numeroDocumento |	 numeroTarjeta    |	 tramite |	 tipologia |	 |	Atencion|	Perfil|	No.|	Desestimada|	Version|	PreCon-Tipologia|	PreCon-Agregar Comentarios|	PreCon-Respuesta Cliente|	PreCon-Adjuntar Archivo|	PreCon-Medio Respuesta|	PreCon-Numero Celular|	PreCon-Nuevo Registro Respuesta|	PreCon-Tipo Tarjeta|	PreCon-Numero documento|	PreCon-RazonOperativa|	PreCon-TipoCMPMalConcretado|	PreCon-Operador|	PreCon-FechaConsumoMalConcretado|	PreCon-TipoRazonOperativa|	PreCon-MotivoSucedidoTarjeta|	PreCon-SeleccionarNoBloqueado|	PreCon-DescripcionNoBloqueado|	PreCon-NombreComercio|	PreCon-CantidadTarjetas|
      |2 | B34097 |	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	36|	|	3.0.7|	CI|	Sí|	Sí|	No|	Correo|	Registrado|	prueba@gmail.com|	Visa|	Correcto|	Ninguno|	Ninguno|	ENTEL|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Uno|

  @Automated @Unhappy_path @functional_testing @COBROS_INDEBIDOS_SIN_RESPUESTA_CLIENTE
  Scenario Outline: [HAPPY PATH] Valida registro tramite pedido exoneracion de cobros con casos de error no agrega respuesta
    Given que el usuario accede a la aplicacion GIRU
    When se ingresa usuario <user> y contrasena <password>
    And selecciona la opcion iniciar sesion
    Then se valida que el acceso sea correcto con un perfil <profile> determinado
    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
    And se registra Reclamo de la tarjeta
    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
      |num |PreCon-Motivo|	PreCon-Cantidad de Motivos|	PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
      |9 |Motivo 4-2-7|	2|	3|	PEN-PEN|	90-90-10/20-30-30|	21/02/2021-15/02/2020|
    And ingresa un "<PreCon-Respuesta Cliente>" y un "<PreCon-Agregar Comentarios>" opcional
    And se valida que no ingresa una respuesta para el cliente
    Examples: Registro tramite reclamo cobros indebidos
      |numeroescenario | user   |	 password  |	 profile |	 tipoDocumento |	 numeroDocumento |	 numeroTarjeta    |	 tramite |	 tipologia |	 |	Atencion|	Perfil|	No.|	Desestimada|	Version|	PreCon-Tipologia|	PreCon-Agregar Comentarios|	PreCon-Respuesta Cliente|	PreCon-Adjuntar Archivo|	PreCon-Medio Respuesta|	PreCon-Numero Celular|	PreCon-Nuevo Registro Respuesta|	PreCon-Tipo Tarjeta|	PreCon-Numero documento|	PreCon-RazonOperativa|	PreCon-TipoCMPMalConcretado|	PreCon-Operador|	PreCon-FechaConsumoMalConcretado|	PreCon-TipoRazonOperativa|	PreCon-MotivoSucedidoTarjeta|	PreCon-SeleccionarNoBloqueado|	PreCon-DescripcionNoBloqueado|	PreCon-NombreComercio|	PreCon-CantidadTarjetas|
      |9 | B34097 |	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	55|	|	3.0.7|	CI|	Sí|	~No|	Sí|	Dirección|	950276375|	Nuevo|	Visa|	Correcto|	Si|	Ninguno|	MOVISTAR|	Ninguno|	RO003|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Varias|



  @Automated @Unhappy_path @functional_testing @COBROS_INDEBIDOS_MONTO_CERO
  Scenario Outline: [HAPPY PATH] Valida registro tramite pedido exoneracion de cobros con casos de error monto cero
    Given que el usuario accede a la aplicacion GIRU
    When se ingresa usuario <user> y contrasena <password>
    And selecciona la opcion iniciar sesion
    Then se valida que el acceso sea correcto con un perfil <profile> determinado
    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
    And se registra Reclamo de la tarjeta
    And elige los datos de registro de los MOTIVOS del tramite <numeroescenario>
      |numeroescenario|PreCon-Motivo|	PreCon-Cantidad de Motivos|	PreCon-Cantidad de Movimientos por Motivo|	PreCon-Moneda|	PreCon-Monto|	PreCon-Fecha|
      |10 |Motivo 4-2-8|	2|	1|	PEN-PEN|	0-0|	21/02/2021-15/02/2020|
    And se valida un monto ingresado incorrecto
    Examples: Registro tramite reclamo cobros indebidos
      |numeroescenario | user   |	 password  |	 profile |	 tipoDocumento |	 numeroDocumento |	 numeroTarjeta    |	 tramite |	 tipologia |	 |	Atencion|	Perfil|	No.|	Desestimada|	Version|	PreCon-Tipologia|	PreCon-Agregar Comentarios|	PreCon-Respuesta Cliente|	PreCon-Adjuntar Archivo|	PreCon-Medio Respuesta|	PreCon-Numero Celular|	PreCon-Nuevo Registro Respuesta|	PreCon-Tipo Tarjeta|	PreCon-Numero documento|	PreCon-RazonOperativa|	PreCon-TipoCMPMalConcretado|	PreCon-Operador|	PreCon-FechaConsumoMalConcretado|	PreCon-TipoRazonOperativa|	PreCon-MotivoSucedidoTarjeta|	PreCon-SeleccionarNoBloqueado|	PreCon-DescripcionNoBloqueado|	PreCon-NombreComercio|	PreCon-CantidadTarjetas|
      |10 | B34097 |	 Abc12345$ |	 Asesor  |	 DNI           |	40614144|	 491337******7390 |	 Reclamo  |	1|	|	productos|	Especialista|	57|	|	3.0.7|	CI|	Sí|	Sí|	No|	Dirección|	Registrado|	Nuevo|	Visa|	Correcto|	Si|	Ninguno|	BITEL|	Ninguno|	RO004|	Ninguno|	Ninguno|	Ninguno|	Ninguno|	Uno|

