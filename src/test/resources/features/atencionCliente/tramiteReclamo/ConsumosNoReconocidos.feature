@allExoneracionCobros
Feature: Exoneración de Cobros

  @Automated @Happy_path @functional_testing @CONSUMOS_NO_RECONOCIDOS
  Scenario Outline: [HAPPY PATH] Valida registro tramite pedido exoneracion de cobros
     #REUTILIZABLE
    Given que el usuario accede a la aplicacion GIRU
    When se ingresa usuario <user> y contrasena <password>
    And selecciona la opcion iniciar sesion
    Then se valida que el acceso sea correcto con un perfil <profile> determinado
    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
 #REUTILIZABLE
    
    Examples: Registro tramite exoneracion de cobros
      | user   | password  | profile | tipoDocumento | numeroDocumento | numeroTarjeta    | tramite | tipologia |
      | B34097 | Abc12345$ | Asesor  | DNI           | 40614144        | 491337******7390 | Reclamo  | 2         |
      | B34097 | Abc12345$ | Asesor  | CE            | 1001033E        | 377753*****0835  | Reclamo  | 2         |


