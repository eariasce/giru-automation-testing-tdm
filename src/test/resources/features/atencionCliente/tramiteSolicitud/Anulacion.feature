Feature: Solicitud de Anulacion

  @Automated @Happy_path @functional_testing @ANULACION
  Scenario Outline: [HAPPY PATH] Valida registro tramite solicitud de anulacion con nueva direccion
    Given que el usuario accede a la aplicacion GIRU
    When se ingresa usuario <user> y contrasena <password>
    And selecciona la opcion iniciar sesion
    Then se valida que el acceso sea correcto con un perfil <profile> determinado
    And realiza la identificacion por <tipoDocumento> y <numeroDocumento> del cliente
    And selecciona tarjeta <numeroTarjeta>, elige el tipo de tramite <tramite> y selecciona la tipologia <tipologia>
    #And se registra Reclamo de la tarjeta
    And ingresa un "<PreCon-Respuesta Cliente>" y un "<PreCon-Agregar Comentarios>" opcional
    And se adjuntara un archivo "<PreCon-Adjuntar Archivo>" en caso sea necesario
    And selecciona o añade segun el "<PreCon-Numero Celular>" y "<PreCon-Operador>"
    And seleciona el medio de respuesta "<PreCon-Medio Respuesta>" segun la configuracion de la respuesta "<PreCon-Nuevo Registro Respuesta>"
    And seleccionar los datos de registro de nueva DIRECCION "<numeroescenario>" "<PreCon-Nuevo Registro Respuesta>"
      |escenario  |Dpto|	Provincia|	Distrito|	Tipo de Via|	Nombre de via|	Urbanizacion| Numero| Manzana|Lote|Interior|Referencia|
      |1|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |2|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |3|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |4|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |5|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |6|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |7|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |8|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |9|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |10|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |
      |11|  14  |     1409     |    140901     |      1      |    pepito     |  los jasmines  |  315  |   B    |  3  |    20    |   Por ahi       |

    Examples: Registro tramite reclamo cobros indebidos
      |numeroescenario | user   |	 password  |	 profile |	 tipoDocumento |	 numeroDocumento |	 numeroTarjeta    |	 tramite |	 tipologia |PreCon-Tipologia|PreCon-Motivo|PreCon-Cantidad de Motivos|PreCon-Cantidad de Movimientos por Motivo|PreCon-Tipo IDC|PreCon-Tipo Producto|PreCon-Estado de Tarjeta|PreCon-Cantidad de Cuentas|PreCon-Tipo Cliente|Input-Nro de Tarjeta|PreCon-Tipo Trámite|PreCon-Moneda|PreCon-Monto| |PreCon-Fecha|PreCon-Agregar Comentarios|PreCon-Respuesta Cliente|PreCon-Adjuntar Archivo|PreCon-Medio Respuesta|PreCon-Numero Celular|PreCon-Nuevo Registro Respuesta|PreCon-Tipo Tarjeta|PreCon-Numero documento|PreCon-RazonOperativa|PreCon-TipoCMPMalConcretado|PreCon-Operador|PreCon-FechaConsumoMalConcretado|PreCon-TipoRazonOperativa|PreCon-MotivoSucedidoTarjeta|PreCon-SeleccionarNoBloqueado|PreCon-DescripcionNoBloqueado|PreCon-NombreComercio|PreCon-CantidadTarjetas|
      |1|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Solicitud  |	5|	    2|ANU|Motivo 5|Ninguno|Ninguno|DNI|TC|Bloqueada|Uno|Adicional Propia|Válido|Solicitud|Ninguno|Ninguno|Ninguno|No|Sí|Sí|Dirección|950276377|Registrado|Visa|Correcto|Ninguno|Ninguno|CLARO|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Varias|
      |2|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Solicitud  |	5|	    4|ANU|Motivo 5|Ninguno|Ninguno|DNI|TC|Activa|Uno|Adicional a Terceros|Válido|Solicitud|Ninguno|Ninguno|Ninguno|Sí|Sí|Sí|Correo|prueba@gmail.com|Nuevo|Visa|Correcto|Ninguno|Ninguno|BITEL|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Varias|
      |3|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Solicitud  |	5|	    7|ANU|Motivo 5|Ninguno|Ninguno|DNI|TC|Activa|Uno|Adicional a Terceros|Válido|Solicitud|Ninguno|Ninguno|Ninguno|Sí|Sí|Sí|Dirección|950276377|Nuevo|Visa|Correcto|Ninguno|Ninguno|ENTEL|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Uno|
      |4|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Solicitud  |	5|	    8|ANU|Motivo 5|Ninguno|Ninguno|CE|TC|Expirada|Uno|Adicional a Terceros|Válido|Solicitud|Ninguno|Ninguno|Ninguno|Sí|Sí|Sí|Correo|Registrado|Registrado|Visa|Correcto|Ninguno|Ninguno|MOVISTAR|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Varias|
      |5|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Solicitud  |	5|	    10|ANU|Motivo 5|Ninguno|Ninguno|DNI|TC|Bloqueada|Uno|Adicional a Terceros|Válido|Solicitud|Ninguno|Ninguno|Ninguno|Sí|Sí|Sí|Correo|Registrado|Nuevo|AMEX|Correcto|Ninguno|Ninguno|CLARO|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Uno|
      |6|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Solicitud  |	5|	    13|ANU|Motivo 5|Ninguno|Ninguno|DNI|TC|Bloqueada|Uno|Adicional Propia|Válido|Solicitud|Ninguno|Ninguno|Ninguno|Sí|Sí|No|Dirección|Registrado|Registrado|Mastercard|Correcto|Ninguno|Ninguno|ENTEL|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Uno|
      |7|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Solicitud  |	5|	    14|ANU|Motivo 5|Ninguno|Ninguno|DNI|TC|Expirada|Uno|Adicional Propia|Válido|Solicitud|Ninguno|Ninguno|Ninguno|No|Sí|No|Correo|Nuevo|prueba@gmail.com|Mastercard|Correcto|Ninguno|Ninguno|BITEL|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Uno|
      |8|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Solicitud  |	5|	    15|ANU|Motivo 5|Ninguno|Ninguno|DNI|TC|Expirada|Uno|Adicional Propia|Válido|Solicitud|Ninguno|Ninguno|Ninguno|No|Sí|No|Dirección|Registrado|Nuevo|Visa|Correcto|Ninguno|Ninguno|MOVISTAR|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Uno|
      |9|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Solicitud  |	5|	    16|ANU|Motivo 5|Ninguno|Ninguno|DNI|TC|Activa|Uno|Adicional a Terceros|Válido|Solicitud|Ninguno|Ninguno|Ninguno|No|~No|Sí|Dirección|Registrado|Registrado|Visa|Correcto|Ninguno|Ninguno|CLARO|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Varias|
      |10|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Solicitud  |	5|	    17|ANU|Motivo 5|Ninguno|Ninguno|DNI|TC|Bloqueada|Uno|Adicional Propia|Válido|Solicitud|Ninguno|Ninguno|Ninguno|Sí|Sí|No|Correo|prueba@gmail.com|Nuevo|AMEX|~Incorrecto|Ninguno|Ninguno|ENTEL|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Uno|
      |11|	 B34097 |	 Abc12345$ |	 Asesor  |	 DNI         |	40614144|	 491337******7390 |	 Solicitud  |	5|	    20|ANU|Motivo 5|Ninguno|Ninguno|DNI|TC|Bloqueada|Uno|Adicional a Terceros|Válido|Solicitud|Ninguno|Ninguno|Ninguno|No|Sí|Sí|Dirección|Registrado|Registrado|Visa|~Vacio|Ninguno|Ninguno|BITEL|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Ninguno|Varias|
