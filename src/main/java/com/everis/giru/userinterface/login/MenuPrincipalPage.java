package com.everis.giru.userinterface.login;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MenuPrincipalPage {
    public static final Target NAV_ADMINISTRADOR = Target.the("Seleccionar menu administrador").located(By.partialLinkText("ADMINISTRAD"));
    public static final Target NAV_DASHBOARD = Target.the("Seleccionar menu dashboard").located(By.partialLinkText("DASHBOA"));
    public static final Target NAV_TRAMITES = Target.the("Seleccionar menu tramites").located(By.partialLinkText("TRÁMIT"));
    public static final Target NAV_ATENCIONALCLIENTE = Target.the("Seleccionar menu atencion al cliente").located(By.partialLinkText("ATENCIÓN AL CLIEN"));
    public static final Target NAV_HISTORIAL = Target.the("Seleccionar menu historial").located(By.partialLinkText("HISTORI"));
    public static final Target NAV_PURECONNECT = Target.the("Seleccionar menu pure connect").located(By.partialLinkText("PURE CONNE"));
}
