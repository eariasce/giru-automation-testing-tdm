package com.everis.giru.userinterface.atencionCliente;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class IdentificarClientePage {

    public static final Target CBX_SELECCIONAR = Target.the("Seleccionar tipo documento").located(By.id("search-form__select-document-type"));
    public static final Target CBX_SELECT_CE = Target.the("Tipo de documento CE").located(By.id("CE"));
    public static final Target CBX_SELECT_DNI = Target.the("Tipo de documento DNI").located(By.id("DNI"));
    public static final Target INP_NUMERO_DOC = Target.the("Número de documento").located(By.id("search-form__txt-document-number"));
    public static final Target BTN_VALIDAR = Target.the("Botón validar").located(By.id("search-form__btn-validate"));

}
