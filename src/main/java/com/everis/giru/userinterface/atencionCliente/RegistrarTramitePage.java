package com.everis.giru.userinterface.atencionCliente;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class RegistrarTramitePage {

    //Tarjeta
    public static final Target CARD_TARJETA = Target.the("Seleccionar card tarjeta").located(By.id("credit-cards"));
    //Elige el tipo de tramite
    public static final Target CBX_TRAMITE = Target.the("Elige el tipo de tramite").located(By.id("requestType"));
    public static final Target DIV_TRAMITE_PEDIDO = Target.the("Select Pedido").located(By.id("TTR0002"));
    public static final Target DIV_TRAMITE_RECLAMO = Target.the("Select Reclamo").located(By.id("TTR0001"));
    public static final Target DIV_TRAMITE_SOLICITUD = Target.the("Select Solicitud").located(By.id("TTR0003"));
    //Selecciona la tipología
    public static final Target CBX_TIPOLOGIA = Target.the("Elige el tipo de tramite").located(By.id("typology"));

    //Selecciona el Motivo
    public static final Target CBX_MOTIVO = Target.the("Elige el motivo").located(By.id("reasonClaim"));

    //Selecciona RazonOperativa
    public static final Target RDB_RAZON_OPERATIVA_SI = Target.the("Elige SI en la razon operativa").located(By.xpath("//*[@for='flag-operative-1']"));
    public static final Target RDB_RAZON_OPERATIVA_NO = Target.the("Elige NO en la razon operativa").located(By.id("flag-operative-0"));
    //ComboBox Razon operativa
    public static final Target CMB_RAZON_OPERATIVA = Target.the("Elige NO en la razon operativa").located(By.id("operativeReason"));

    //TextArea Respuesta Cliente
    public static final Target  TXT_COMENTARIO_ADICIONAL= Target.the("Comentario Adicional").located(By.xpath("//textarea[@placeholder='Describe tu apreciación del trámite']"));

    //TextArea Comentario Adicional
    public static final Target TXT_RESPUESTA_CLIENTE = Target.the("Respuesta Cliente").located(By.xpath("//textarea[@placeholder='Este comentario se visualizará en la hoja de reclamo']"));

    //Button Subir Archivo
    public static final Target BTN_SUBIR_ARCHIVO = Target.the("Subir Archivo").located(By.xpath("//span[@class='upload-items__uploader-description h-bold ml-3p']"));

    //Combobox numtelefono
    public static final Target CBX_NUM_TELEFONO = Target.the("Numeros de Telefono").located(By.xpath("//div[@id='telephone']"));

    //Textbox registro nuevo telefono
    public static final Target TXT_REG_NUM_TELEFONO = Target.the("Registro nuevo numero de Telefono").located(By.xpath("//*[@id='phone-number-form__phone-number']"));

    //Textbox continuar registro nuevo telefono
    public static final Target TXT_REG_TELF_ACEPTAR = Target.the("Registro nuevo numero de Telefono").located(By.xpath("//*[@id='phone-number__btn-add']"));


    //Boton Medio Respuesta Correo
    public static final Target BTN_MEDIO_RESPUESTA_CORREO = Target.the("Medio de respuesta correo").located(By.xpath("//label[@for='responseType-EMAIL']"));
    //Boton Medio Respuesta Direccion
    public static final Target BTN_MEDIO_RESPUESTA_DIRECCION = Target.the("Medio de respuesta correo").located(By.xpath("//label[@for='responseType-ADDRESS']"));
    //Combobox Medio Respuesta Correo
    public static final Target CBX_MEDIO_RESPUESTA_CORREO = Target.the("Seleccionar medio de respuesta correo").located(By.xpath("//*[@id='email']/div[1]"));
    //Combobox Medio Respuesta Direccion
    public static final Target CBX_MEDIO_RESPUESTA_DIRECCION = Target.the("Seleccionar medio de respuesta correo").located(By.xpath("//*[@id='address']/div[1]"));
    //Boton Registro Reclamo
    public static final Target BTN_REGISTRO_RECLAMO = Target.the("Boton registro reclamo").located(By.xpath("//*[@id='claim-form__btn-submit']"));

    //Valida color textbox Fecha
    public static final Target TXT_FECHA_ROJO = Target.the("fecha incorrecta").located(By.xpath("//input[@formcontrolname='Fecha'][1]"));
    //Valida color textbox Monto Incorrecto
    public static final Target TXT_MONTO_ROJO = Target.the("monto incorrecta").located(By.xpath("//input[@formcontrolname='Monto'][1] "));
    //Valida campo obligatorio Respuesta
    public static final Target AREA_RESPUESTA_CLIENTE = Target.the("no ingresa respuesta").located(By.xpath("//textarea[@formcontrolname='comments']"));

}
