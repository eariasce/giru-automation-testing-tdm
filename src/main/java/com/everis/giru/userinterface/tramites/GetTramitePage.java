package com.everis.giru.userinterface.tramites;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class GetTramitePage extends PageObject {

    private Shadow general = new Shadow();
    public String getTotalTramitePorTipologia;
    public int contador = 0;
    public String Scontador;
    public int numTotalRegistros;
    public boolean TotalTramitesEsCERO = false;
    String usuarioSesion = Serenity.sessionVariableCalled("UsuarioSesion").toString();

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_BUSCARTRAMITE;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-to-transactions-in-process/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS;


    @FindBy(xpath = "//*[@id=\"grid\"]/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-supervisor-trx/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS;


    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-task/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR;

    public void EsCeroTotalTramite() {
        TotalTramitesEsCERO = true;
    }

    public void validaBuscarTramite() {
        String nroTramite = "nroTramite";
        String numeroDNI = "DNI";
        String numeroCE = "CE";
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

        if (numeroTramite.equals(nroTramite)) {
            WaitUntil.the((Target) LBL_ID_TABLA_BUSCARTRAMITE, isVisible()).forNoMoreThan(120).seconds();
            WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_BUSCARTRAMITE, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "");
            String getTramite = valueTramite.getText();
            String sSubTramite = getTramite.substring(getTramite.length() - 5);
            System.out.println("Numero tramite: " + sSubTramite);
            Assert.assertEquals(sSubTramite, numeroTramite);
        }
        if (numeroTramite.equals(numeroDNI)) {
            WaitUntil.the((Target) LBL_ID_TABLA_BUSCARTRAMITE, isVisible()).forNoMoreThan(120).seconds();
            WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_BUSCARTRAMITE, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "");
            String getNumeroDNI = valueNumeroDNI.getText();
            System.out.println("Numero DNI: " + getNumeroDNI);
            Assert.assertEquals(getNumeroDNI, numeroTramite);
        } else if (numeroTramite.equals(numeroCE)) {
            WaitUntil.the((Target) LBL_ID_TABLA_BUSCARTRAMITE, isVisible()).forNoMoreThan(120).seconds();
            WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_BUSCARTRAMITE, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "");
            String getNumeroCE = valueNumeroCE.getText();
            System.out.println("Numero CE: " + getNumeroCE);
            Assert.assertEquals(getNumeroCE, numeroTramite);
        }
    }

    public String validaTotalTramitePorTipologia(String tipologia, String perfil, String departamento) {

        getTotalTramitePorTipologia = "";

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TodosMisTramites = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");


                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TodosMisTramites para contador 0 del Perfil Especialista Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "CobrosIndebidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_CobrosIndebidos = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite_CobrosIndebidos.getText();
                                        System.out.println("Contador tipologia CobrosIndebidos = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en CobrosIndebidos para contador 0 del Perfil Especialista Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_CobrosIndebidos = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite_CobrosIndebidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }

                                break;
                            case "ExoneracionCobros":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_ExoneracionCobros = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite_ExoneracionCobros.getText();
                                        System.out.println("Contador tipologia Exoneracion = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");
                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Exoneracion de Cobros para contador 0 del Perfil Especialista Departamento GPYR");
                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_ExoneracionCobros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite_ExoneracionCobros.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }

                                break;
                            case "AnulacionTC":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_AnulacionTC = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite_AnulacionTC.getText();
                                        System.out.println("Contador tipologia AnulacionTC = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");
                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en AnulacionTC para contador 0 del Perfil Especialista Departamento GPYR");
                                    }

                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_AnulacionTC = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite_AnulacionTC.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "FinalizacionTC":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_FinalizacionTC = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite_FinalizacionTC.getText();
                                        System.out.println("Contador tipologia Finalización = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> para contador 0");

                                    }

                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_FinalizacionTC = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite_FinalizacionTC.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("Título equivocado según Bandeja de la tipología !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_GPYR));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TodosMisTramites = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TodosMisTramites para contador 0 del Perfil Supervisor Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_GPYR));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "MiEquipo":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR));
                                        WebElement valueMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueMiEquipo.getText();
                                        System.out.println("Contador tipologia MiEquipo = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en MiEquipo para contador 0 del Perfil Supervisor Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR));
                                    WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueMiEquipo.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Asignar":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR));
                                        WebElement valueAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueAsignar.getText();
                                        System.out.println("Contador tipologia Asignar = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Asignar para contador 0 del Perfil Supervisor Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR));
                                    WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueAsignar.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Resolver":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR));
                                        WebElement valueResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueResolver.getText();
                                        System.out.println("Contador tipologia Resolver = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Resolver para contador 0 del Perfil Supervisor Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR));
                                    WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueResolver.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor GPYR !!!");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_SEGUROS));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TodosMisTramites = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TodosMisTramites para contador 0 del Perfil Especialista Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_SEGUROS));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");

                                }
                                break;
                            case "CobrosIndebidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS));
                                        WebElement valueCobrosIndebidos = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueCobrosIndebidos.getText();
                                        System.out.println("Contador tipologia CobrosIndebidos = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en CobrosIndebidos  para contador 0 del Perfil Especialista Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS));
                                    WebElement valueCobrosIndebidos = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueCobrosIndebidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ExoneracionCobros":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS));
                                        WebElement valueExoneracionCobros = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueExoneracionCobros.getText();
                                        System.out.println("Contador tipologia ExoneracionCobros = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ExoneracionCobros  para contador 0 del Perfil Especialista Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS));
                                    WebElement valueExoneracionCobros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueExoneracionCobros.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS));
                                        WebElement valueConsumoNoReconocidos = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueConsumoNoReconocidos.getText();
                                        System.out.println("Contador tipologia ConsumoNoReconocidos = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumoNoReconocidos  para contador 0 del Perfil Especialista Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS));
                                    WebElement valueConsumoNoReconocidos = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueConsumoNoReconocidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Monitorear":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS));
                                        WebElement valueMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueMonitorear.getText();
                                        System.out.println("Contador tipologia Monitorear = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Monitorear  para contador 0 del Perfil Especialista Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS));
                                    WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueMonitorear.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista SEGUROS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_SEGUROS));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TodosMisTramites = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TodosMisTramites para contador 0 del Perfil Supervisor Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_SEGUROS));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "MiEquipo":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS));
                                        WebElement valueMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueMiEquipo.getText();
                                        System.out.println("Contador tipologia MiEquipo = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en MiEquipo para contador 0 del Perfil Supervisor Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS));
                                    WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueMiEquipo.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Asignar":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS));
                                        WebElement valueAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueAsignar.getText();
                                        System.out.println("Contador tipologia Asignar = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Asignar para contador 0 del Perfil Supervisor Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS));
                                    WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueAsignar.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Resolver":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS));
                                        WebElement valueResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueResolver.getText();
                                        System.out.println("Contador tipologia Resolver = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Resolver para contador 0 del Perfil Supervisor Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS));
                                    WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueResolver.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Monitorear":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS));
                                        WebElement valueMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueMonitorear.getText();
                                        System.out.println("Contador tipologia Monitorear = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Monitorear para contador 0 del Perfil Supervisor Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS));
                                    WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueMonitorear.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor SEGUROS !!!");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TotalTramite = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TotalTramite para contador 0 del Perfil Especialista Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement valueConsumosNoReconocidos = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueConsumosNoReconocidos.getText();
                                        System.out.println("Contador tipologia ConsumosNoReconocidos = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosNoReconocidos para contador 0 del Perfil Especialista Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueConsumosNoReconocidos = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueConsumosNoReconocidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ConsumosMalProcesados":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement valueConsumosMalProcesados = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueConsumosMalProcesados.getText();
                                        System.out.println("Contador tipologia ConsumosMalProcesados = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosMalProcesados para contador 0 del Perfil Especialista Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueConsumosMalProcesados = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueConsumosMalProcesados.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Monitorear":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement valueMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueMonitorear.getText();
                                        System.out.println("Contador tipologia Monitorear = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Monitorear para contador 0 del Perfil Especialista Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueMonitorear.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "TransaccionesEnProceso":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement valueTransaccionesEnProceso = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTransaccionesEnProceso.getText();
                                        System.out.println("Contador tipologia TransaccionesEnProceso = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TransaccionesEnProceso para contador 0 del Perfil Especialista Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueTransaccionesEnProceso = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTransaccionesEnProceso.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista CONTROVERSIAS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TotalTramite = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TotalTramite para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "MiEquipo":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueMiEquipo.getText();
                                        System.out.println("Contador tipologia MiEquipo = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en MiEquipo para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueMiEquipo.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Asignar":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueAsignar.getText();
                                        System.out.println("Contador tipologia Asignar = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Asignar para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueAsignar.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Resolver":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueResolver.getText();
                                        System.out.println("Contador tipologia Resolver = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Resolver para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueResolver.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Monitorear":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueMonitorear.getText();
                                        System.out.println("Contador tipologia Monitorear = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Monitorear para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueMonitorear.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "TransaccionesEnProceso":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueTransaccionesEnProceso = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTransaccionesEnProceso.getText();
                                        System.out.println("Contador tipologia TransaccionesEnProceso = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TransaccionesEnProceso para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueTransaccionesEnProceso = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTransaccionesEnProceso.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor CONTROVERSIAS !!!");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_USPR));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TotalTramite = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TotalTramite para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_USPR));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "CobrosIndebidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR));
                                        WebElement valueCobrosIndebidos = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueCobrosIndebidos.getText();
                                        System.out.println("Contador tipologia CobrosIndebidos = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en CobrosIndebidos para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR));
                                    WebElement valueCobrosIndebidos = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueCobrosIndebidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ExoneracionCobros":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR));
                                        WebElement valueExoneracionCobros = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueExoneracionCobros.getText();
                                        System.out.println("Contador tipologia ExoneracionCobros = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ExoneracionCobros para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR));
                                    WebElement valueExoneracionCobros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueExoneracionCobros.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ConsumosMalProcesados":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR));
                                        WebElement valueConsumosMalProcesados = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueConsumosMalProcesados.getText();
                                        System.out.println("Contador tipologia ConsumosMalProcesados = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosMalProcesados para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR));
                                    WebElement valueConsumosMalProcesados = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueConsumosMalProcesados.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR));
                                        WebElement valueConsumosNoReconocidos = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueConsumosNoReconocidos.getText();
                                        System.out.println("Contador tipologia ConsumosNoReconocidos = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosNoReconocidos para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR));
                                    WebElement valueConsumosNoReconocidos = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueConsumosNoReconocidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Retipificar":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR));
                                        WebElement valueRetipificar = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueRetipificar.getText();
                                        System.out.println("Contador tipologia Retipificar = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Retipificar para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR));
                                    WebElement valueRetipificar = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueRetipificar.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista USPR !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_USPR));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TotalTramite = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TotalTramite para contador 0 del Perfil Supervisor Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_USPR));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "MiEquipo":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR));
                                        WebElement valueMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueMiEquipo.getText();
                                        System.out.println("Contador tipologia MiEquipo = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en MiEquipo para contador 0 del Perfil Supervisor Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR));
                                    WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueMiEquipo.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Asignar":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR));
                                        WebElement valueAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueAsignar.getText();
                                        System.out.println("Contador tipologia Asignar = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Asignar para contador 0 del Perfil Supervisor Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR));
                                    WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueAsignar.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Resolver":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR));
                                        WebElement valueResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                        getTotalTramitePorTipologia = valueResolver.getText();
                                        System.out.println("Contador tipologia Resolver = 0 muestra mensaje satisfactorio " + getTotalTramitePorTipologia + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getTotalTramitePorTipologia = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Resolver para contador 0 del Perfil Supervisor Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR));
                                    WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "");
                                    getTotalTramitePorTipologia = valueResolver.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getTotalTramitePorTipologia + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor USPR !!!");
                        }
                        break;
                }
                break;
        }

        return getTotalTramitePorTipologia;
    }

    public void seleccionarDropdownPaginacion(String tipologia, String perfil, String departamento) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_GPYR));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdown.click();
                                break;
                            case "CobrosIndebidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR));
                                WebElement selectDropdownCI = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownCI.click();
                                break;
                            case "ExoneracionCobros":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR));
                                WebElement selectDropdownEX = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownEX.click();
                                break;
                            case "AnulacionTC":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR));
                                WebElement selectDropdownAnulTC = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownAnulTC.click();
                                break;
                            case "FinalizacionTC":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR));
                                WebElement selectDropdownFinalTC = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownFinalTC.click();
                                break;
                            default:
                                System.out.println("Título equivocado según Bandeja de la tipología !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_GPYR));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdown.click();
                                break;
                            case "MiEquipo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR));
                                WebElement selectDropdownMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownMiEquipo.click();
                                break;
                            case "Asignar":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR));
                                WebElement selectDropdownAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownAsignar.click();
                                break;
                            case "Resolver":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR));
                                WebElement selectDropdownResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownResolver.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor GPYR !!!");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_SEGUROS));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdown.click();
                                break;
                            case "CobrosIndebidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS));
                                WebElement selectDropdownCI = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownCI.click();
                                break;
                            case "ExoneracionCobros":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS));
                                WebElement selectDropdownEX = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownEX.click();
                                break;
                            case "ConsumosNoReconocidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS));
                                WebElement selectDropdownCNR = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownCNR.click();
                                break;
                            case "Monitorear":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS));
                                WebElement selectDropdownMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownMonitorear.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista SEGUROS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_SEGUROS));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdown.click();
                                break;
                            case "MiEquipo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS));
                                WebElement selectDropdownMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownMiEquipo.click();
                                break;
                            case "Asignar":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS));
                                WebElement selectDropdownAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownAsignar.click();
                                break;
                            case "Resolver":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS));
                                WebElement selectDropdownResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownResolver.click();
                                break;
                            case "Monitorear":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS));
                                WebElement selectDropdownMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownMonitorear.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor SEGUROS !!!");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdown.click();
                                break;
                            case "ConsumosNoReconocidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS));
                                WebElement selectDropdownCNR = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownCNR.click();

                                break;
                            case "ConsumosMalProcesados":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS));
                                WebElement selectDropdownCMP = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownCMP.click();
                                break;
                            case "Monitorear":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS));
                                WebElement selectDropdownMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownMonitorear.click();
                                break;
                            case "TransaccionesEnProceso":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS));
                                WebElement selectDropdownTEP = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownTEP.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista CONTROVERSIAS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdown.click();
                                break;
                            case "MiEquipo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdownMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownMiEquipo.click();
                                break;
                            case "Asignar":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdownAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownAsignar.click();
                                break;
                            case "Resolver":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdownResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownResolver.click();
                                break;
                            case "Monitorear":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdownMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownMonitorear.click();
                                break;
                            case "TransaccionesEnProceso":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdownTEP = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownTEP.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor CONTROVERSIAS !!!");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_USPR));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdown.click();
                                break;
                            case "CobrosIndebidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR));
                                WebElement selectDropdownCI = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownCI.click();
                                break;
                            case "ExoneracionCobros":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR));
                                WebElement selectDropdownEXC = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownEXC.click();
                                break;
                            case "ConsumosMalProcesados":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR));
                                WebElement selectDropdownCMP = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownCMP.click();
                                break;
                            case "ConsumosNoReconocidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR));
                                WebElement selectDropdownCNR = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownCNR.click();
                                break;
                            case "Retipificar":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR));
                                WebElement selectDropdownRetificar = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownRetificar.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista USPR !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_USPR));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdown.click();
                                break;
                            case "MiEquipo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR));
                                WebElement selectDropdownMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownMiEquipo.click();
                                break;
                            case "Asignar":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR));
                                WebElement selectDropdownAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownAsignar.click();
                                break;
                            case "Resolver":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR));
                                WebElement selectDropdownResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select");
                                selectDropdownResolver.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor USPR !!!");
                        }
                        break;
                }
                break;
        }


    }

    public void seleccionarPaginacion(String tipologia, String perfil, String registros, String departamento) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "CobrosIndebidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ExoneracionCobros":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "AnulacionTC":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "FinalizacionTC":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("Título equivocado según Bandeja de la tipología !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "MiEquipo":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Asignar":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Resolver":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor GPYR !!!");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "CobrosIndebidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ExoneracionCobros":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Monitorear":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista SEGUROS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "MiEquipo":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Asignar":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Resolver":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Monitorear":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor SEGUROS !!!");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }

                                break;
                            case "ConsumosMalProcesados":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Monitorear":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "TransaccionesEnProceso":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista CONTROVERSIAS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "MiEquipo":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Asignar":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Resolver":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Monitorear":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "TransaccionesEnProceso":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor CONTROVERSIAS !!!");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "CobrosIndebidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ExoneracionCobros":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ConsumosMalProcesados":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Retipificar":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista USPR !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "MiEquipo":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Asignar":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Resolver":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor USPR !!!");
                        }
                        break;
                }
                break;
        }


    }

    public boolean validaPaginacion(String tipologia, String perfil, String registros, String departamento) {
        boolean validaPaginacion = true;

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }

                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");

                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }

                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "AnulacionTC":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "FinalizacionTC":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("Título equivocado según Bandeja de la tipología !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor GPYR !!!");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista SEGUROS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor SEGUROS !!!");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista CONTROVERSIAS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor CONTROVERSIAS !!!");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Retipificar":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista USPR !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor USPR !!!");
                        }
                        break;
                }
                break;
        }

        return validaPaginacion;
    }

    public String traerContador() {
        Scontador = String.valueOf(numTotalRegistros);
        return Scontador;
    }
}
