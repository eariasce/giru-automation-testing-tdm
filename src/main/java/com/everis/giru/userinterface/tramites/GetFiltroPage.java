package com.everis.giru.userinterface.tramites;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.assertj.core.api.Assertions.assertThat;

public class GetFiltroPage extends PageObject {

    private Shadow general;
    public int contador = 0;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_VENCIMIENTO;

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR;

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SEGMENTO;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESTADO;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_NIVELSERVICIO;
    @FindBy(xpath = "//*[@id='radio-list__radio-currency']")
    WebElement LBL_ID_RADIOBUTTOM_MONEDA_MONTO;
    @FindBy(xpath = "//*[@id='radio-list__radio-domain-options']")
    WebElement LBL_ID_RADIOBUTTOM_MASFILTROS;

    //MAPEO DE ELEMENTOS - VALIDACIÓN FILTROS
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-to-transactions-in-process/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-supervisor-trx/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-task/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR;

    public void SeleccionaFiltroVencimiento(String fVencimiento) {

        switch (fVencimiento) {
            case "Nuevos":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_VENCIMIENTO));
                WebElement btnNuevo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_VENCIMIENTO, "div > div:nth-child(1) > label", "", "");
                btnNuevo.click();
                break;
            case "PorVencer":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_VENCIMIENTO));
                WebElement btnPorVencer = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_VENCIMIENTO, "div > div:nth-child(2) > label", "", "");
                btnPorVencer.click();
                break;
            case "VencidosArea":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_VENCIMIENTO));
                WebElement btnVencidosArea = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_VENCIMIENTO, "div > div:nth-child(3) > label", "", "");
                btnVencidosArea.click();
                break;
            case "VencidosCliente":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_VENCIMIENTO));
                WebElement btnVencidosCliente = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_VENCIMIENTO, "div > div:nth-child(4) > label", "", "");
                btnVencidosCliente.click();
                break;

            default:
                System.out.println("No se encuentra Filtro Vencimiento!!!");
        }
    }

    public void SeleccionaFiltroTipoTramite(String fTipoTramite, String perfil, String departamento) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(1) > label", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(2) > label", "", "");
                                btnPedido.click();
                                break;
                            case "Solicitud":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                WebElement btnSolicitud = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(3) > label", "", "");
                                btnSolicitud.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(1) > label", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(2) > label", "", "");
                                btnPedido.click();
                                break;
                            case "Solicitud":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                WebElement btnSolicitud = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(3) > label", "", "");
                                btnSolicitud.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Supervisor del departamento GPYR!!!");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(1) > label", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(2) > label", "", "");
                                btnPedido.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento SEGUROS!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(1) > label", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(2) > label", "", "");
                                btnPedido.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Supervisor del departamento SEGUROS!!!");
                        }
                        break;
                }
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div > label", "", "");
                                btnReclamo.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div > label", "", "");
                                btnReclamo.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Supervisor del departamento CONTROVERSIAS!!!");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "");
                                btnPedido.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento USPR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "");
                                btnPedido.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Supervisor del departamento USPR!!!");
                        }
                        break;
                }

                break;
            default:
                System.out.println("No existe filtro TipoDeTramite en el departamento ingresado: " + departamento + " !!!");
        }
    }

    public void SeleccionaFiltroTipologia(String fTipologia, String perfil, String departamento, String fTipoTramite) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnCobrosIndebidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div > label", "", "");
                                        btnCobrosIndebidos.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div > label", "", "");
                                        btnExoneracionCobros.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            case "Solicitud":
                                switch (fTipologia) {
                                    case "AnulacionTC":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnAnulacionTC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(1) > label", "", "");
                                        btnAnulacionTC.click();
                                        break;
                                    case "FinalizacionTC":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnFinalizacionTC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(2) > label", "", "");
                                        btnFinalizacionTC.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnCobrosIndebidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div > label", "", "");
                                        btnCobrosIndebidos.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div > label", "", "");
                                        btnExoneracionCobros.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            case "Solicitud":
                                switch (fTipologia) {
                                    case "AnulacionTC":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnAnulacionTC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(1) > label", "", "");
                                        btnAnulacionTC.click();
                                        break;
                                    case "FinalizacionTC":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnFinalizacionTC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(2) > label", "", "");
                                        btnFinalizacionTC.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                        WebElement btnCobroIndebido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(1) > label", "", "");
                                        btnCobroIndebido.click();
                                        break;
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                        WebElement btnConsumoNoReconocido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(2) > label", "", "");
                                        btnConsumoNoReconocido.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento SEGUROS!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div > label", "", "");
                                        btnExoneracionCobros.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                        WebElement btnCobroIndebido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(1) > label", "", "");
                                        btnCobroIndebido.click();
                                        break;
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                        WebElement btnConsumoNoReconocido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(2) > label", "", "");
                                        btnConsumoNoReconocido.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento SEGUROS!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div > label", "", "");
                                        btnExoneracionCobros.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement btnConsumosNoReconocidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "");
                                        btnConsumosNoReconocidos.click();
                                        break;
                                    case "ConsumosMalProcesados":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement btnConsumosMalProcesados = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "");
                                        btnConsumosMalProcesados.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                        WebElement btnConsumosNoReconocidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "");
                                        btnConsumosNoReconocidos.click();
                                        break;
                                    case "ConsumosMalProcesados":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                        WebElement btnConsumosMalProcesados = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "");
                                        btnConsumosMalProcesados.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                        WebElement btnCobrosIndebidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "");
                                        btnCobrosIndebidos.click();
                                        break;
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                        WebElement btnConsumosNoReconocidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "");
                                        btnConsumosNoReconocidos.click();
                                        break;
                                    case "ConsumosMalProcesados":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                        WebElement btnConsumosMalProcesados = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(3) > label", "", "");
                                        btnConsumosMalProcesados.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div > label", "", "");
                                        btnExoneracionCobros.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                        WebElement btnCobrosIndebidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "");
                                        btnCobrosIndebidos.click();
                                        break;
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                        WebElement btnConsumosNoReconocidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "");
                                        btnConsumosNoReconocidos.click();
                                        break;
                                    case "ConsumosMalProcesados":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                        WebElement btnConsumosMalProcesados = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(3) > label", "", "");
                                        btnConsumosMalProcesados.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div > label", "", "");
                                        btnExoneracionCobros.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento USPR!!!");
                        }
                        break;
                }

                break;
            default:
                System.out.println("No existe filtro TipoDeTramite en el departamento ingresado: " + departamento + " !!!");
        }

    }

    public void SeleccionaFiltroMotivos(String departamento, String perfil, String fTipoTramite, String fTipologia, String fMotivos) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(9) > label", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(9) > label", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }

                                break;
                            case "Solicitud":
                                switch (fTipologia) {
                                    case "AnulacionTC":
                                        switch (fMotivos) {
                                            case "SinMotivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnSinMotivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div > label", "", "");
                                                btnSinMotivo.click();
                                                break;
                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    case "FinalizacionTC":
                                        switch (fMotivos) {
                                            case "SinMotivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnSinMotivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div > label", "", "");
                                                btnSinMotivo.click();
                                                break;
                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(9) > label", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(9) > label", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }

                                break;
                            case "Solicitud":
                                switch (fTipologia) {
                                    case "AnulacionTC":
                                        switch (fMotivos) {
                                            case "SinMotivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnSinMotivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div > label", "", "");
                                                btnSinMotivo.click();
                                                break;
                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    case "FinalizacionTC":
                                        switch (fMotivos) {
                                            case "SinMotivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnSinMotivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div > label", "", "");
                                                btnSinMotivo.click();
                                                break;
                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(9) > label", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento SEGUROS!!!");
                                        }
                                        break;
                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(1) > label", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(2) > label", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento SEGUROS!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento SEGUROS!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(9) > label", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento SEGUROS!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento SEGUROS!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento SEGUROS!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(9) > label", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(1) > label", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(2) > label", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento SEGUROS!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(9) > label", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                        }
                                        break;

                                    case "ConsumosMalProcesados":
                                        switch (fMotivos) {
                                            case "ConsumoCargoMasUnaVezDuplico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnConsumoCargoMasUnaVezDuplico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "");
                                                btnConsumoCargoMasUnaVezDuplico.click();
                                                break;
                                            case "CargoConsumoNoConcretadoDesistioCompra":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnCargoConsumoNoConcretadoDesistioCompra = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "");
                                                btnCargoConsumoNoConcretadoDesistioCompra.click();
                                                break;
                                            case "CargoConsumoNoConcretadoPagoOtraTarjetaEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(3) > label", "", "");
                                                btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo.click();
                                                break;
                                            case "AnuloConsumoDevolvioDinero":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnAnuloConsumoDevolvioDinero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(4) > label", "", "");
                                                btnAnuloConsumoDevolvioDinero.click();
                                                break;
                                            case "ProductoServicioRecibioAcordeOfrecido":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnProductoServicioRecibioAcordeOfrecido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(5) > label", "", "");
                                                btnProductoServicioRecibioAcordeOfrecido.click();
                                                break;
                                            case "ImporteDifiereOriginalMismaMoneda":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnImporteDifiereOriginalMismaMoneda = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(6) > label", "", "");
                                                btnImporteDifiereOriginalMismaMoneda.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;

                                    case "ConsumosMalProcesados":
                                        switch (fMotivos) {
                                            case "ConsumoCargoMasUnaVezDuplico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnConsumoCargoMasUnaVezDuplico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "");
                                                btnConsumoCargoMasUnaVezDuplico.click();
                                                break;
                                            case "CargoConsumoNoConcretadoDesistioCompra":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnCargoConsumoNoConcretadoDesistioCompra = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "");
                                                btnCargoConsumoNoConcretadoDesistioCompra.click();
                                                break;
                                            case "CargoConsumoNoConcretadoPagoOtraTarjetaEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(3) > label", "", "");
                                                btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo.click();
                                                break;
                                            case "AnuloConsumoDevolvioDinero":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnAnuloConsumoDevolvioDinero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(4) > label", "", "");
                                                btnAnuloConsumoDevolvioDinero.click();
                                                break;
                                            case "ProductoServicioRecibioAcordeOfrecido":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnProductoServicioRecibioAcordeOfrecido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(5) > label", "", "");
                                                btnProductoServicioRecibioAcordeOfrecido.click();
                                                break;
                                            case "ImporteDifiereOriginalMismaMoneda":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnImporteDifiereOriginalMismaMoneda = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(6) > label", "", "");
                                                btnImporteDifiereOriginalMismaMoneda.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {

                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(9) > label", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    case "ConsumosMalProcesados":
                                        switch (fMotivos) {
                                            case "ConsumoCargoMasUnaVezDuplico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnConsumoCargoMasUnaVezDuplico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "");
                                                btnConsumoCargoMasUnaVezDuplico.click();
                                                break;
                                            case "CargoConsumoNoConcretadoDesistioCompra":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnCargoConsumoNoConcretadoDesistioCompra = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "");
                                                btnCargoConsumoNoConcretadoDesistioCompra.click();
                                                break;
                                            case "CargoConsumoNoConcretadoPagoOtraTarjetaEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(3) > label", "", "");
                                                btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo.click();
                                                break;
                                            case "AnuloConsumoDevolvioDinero":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnAnuloConsumoDevolvioDinero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(4) > label", "", "");
                                                btnAnuloConsumoDevolvioDinero.click();
                                                break;
                                            case "ProductoServicioRecibioAcordeOfrecido":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnProductoServicioRecibioAcordeOfrecido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(5) > label", "", "");
                                                btnProductoServicioRecibioAcordeOfrecido.click();
                                                break;
                                            case "ImporteDifiereOriginalMismaMoneda":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnImporteDifiereOriginalMismaMoneda = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(6) > label", "", "");
                                                btnImporteDifiereOriginalMismaMoneda.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(9) > label", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento USPR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {

                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(9) > label", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    case "ConsumosMalProcesados":
                                        switch (fMotivos) {
                                            case "ConsumoCargoMasUnaVezDuplico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnConsumoCargoMasUnaVezDuplico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "");
                                                btnConsumoCargoMasUnaVezDuplico.click();
                                                break;
                                            case "CargoConsumoNoConcretadoDesistioCompra":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnCargoConsumoNoConcretadoDesistioCompra = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "");
                                                btnCargoConsumoNoConcretadoDesistioCompra.click();
                                                break;
                                            case "CargoConsumoNoConcretadoPagoOtraTarjetaEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(3) > label", "", "");
                                                btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo.click();
                                                break;
                                            case "AnuloConsumoDevolvioDinero":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnAnuloConsumoDevolvioDinero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(4) > label", "", "");
                                                btnAnuloConsumoDevolvioDinero.click();
                                                break;
                                            case "ProductoServicioRecibioAcordeOfrecido":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnProductoServicioRecibioAcordeOfrecido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(5) > label", "", "");
                                                btnProductoServicioRecibioAcordeOfrecido.click();
                                                break;
                                            case "ImporteDifiereOriginalMismaMoneda":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnImporteDifiereOriginalMismaMoneda = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(6) > label", "", "");
                                                btnImporteDifiereOriginalMismaMoneda.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(3) > label", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(4) > label", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(5) > label", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(6) > label", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(7) > label", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(8) > label", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(9) > label", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(10) > label", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento USPR!!!");
                        }
                        break;
                }

                break;
            default:
                System.out.println("No existe filtro TipoDeTramite en el departamento ingresado: " + departamento + " !!!");
        }

    }

    public void SeleccionaFiltroSegmento(String fSegmento) {

        switch (fSegmento) {
            case "Joven":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnJoven = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(1) > label", "", "");
                btnJoven.click();
                break;
            case "Premium":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnPorPremium = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(2) > label", "", "");
                btnPorPremium.click();
                break;
            case "Preferente":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnPreferente = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(3) > label", "", "");
                btnPreferente.click();
                break;
            case "Emprendedor":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnEmprendedor = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(4) > label", "", "");
                btnEmprendedor.click();
                break;
            case "Empresario":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnEmpresario = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(5) > label", "", "");
                btnEmpresario.click();
                break;
            case "Pequeño Empresario":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnPequenoEmpresario = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(6) > label", "", "");
                btnPequenoEmpresario.click();
                break;
            case "Microempresario":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnMicroempresario = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(7) > label", "", "");
                btnMicroempresario.click();
                break;
            case "Persona Natural sin Negocio (PNSN)":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnPersonaNaturalsinNegocio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(8) > label", "", "");
                btnPersonaNaturalsinNegocio.click();
                break;
            case "Informal":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnInformal = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(9) > label", "", "");
                btnInformal.click();
                break;
            case "Select":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnSelect = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(10) > label", "", "");
                btnSelect.click();
                break;
            case "Consumo Inicial":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnConsumoInicial = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(11) > label", "", "");
                btnConsumoInicial.click();
                break;
            case "Cliente Nuevo":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnClienteNuevo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(12) > label", "", "");
                btnClienteNuevo.click();
                break;
            case "No Segmentado":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnNoSegmentado = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(13) > label", "", "");
                btnNoSegmentado.click();
                break;

            default:
                System.out.println("No se encuentra Filtro Segmento!!!");
        }

    }

    public void SeleccionaFiltroEstado(String fEstado) {

        switch (fEstado) {
            case "EnProceso":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESTADO));
                WebElement btnEnProceso = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESTADO, "div > div:nth-child(1) > label", "", "");
                btnEnProceso.click();
                break;
            case "ProcedeValidacion":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESTADO));
                WebElement btnProcedeValidacion = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESTADO, "div > div:nth-child(2) > label", "", "");
                btnProcedeValidacion.click();
                break;
            case "NoProcedeValidacion":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESTADO));
                WebElement btnNoProcedeValidacion = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESTADO, "div > div:nth-child(3) > label", "", "");
                btnNoProcedeValidacion.click();
                break;

            default:
                System.out.println("No se encuentra Filtro Estado!!!");
        }

    }

    public void SeleccionaFiltroNivelServicio(String fNivelServicio) {

        switch (fNivelServicio) {
            case "Platino":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnPlatino = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(1) > label", "", "");
                btnPlatino.click();
                break;
            case "Oro":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnOro = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(2) > label", "", "");
                btnOro.click();
                break;
            case "Plata":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnPlata = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(3) > label", "", "");
                btnPlata.click();
                break;
            case "Bronce":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnBronce = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(4) > label", "", "");
                btnBronce.click();
                break;
            case "SinEspecificar":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnSinEspecificar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(5) > label", "", "");
                btnSinEspecificar.click();
                break;
            case "Premium":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnPremium = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(6) > label", "", "");
                btnPremium.click();
                break;

            default:
                System.out.println("No se encuentra ningún filtro Nivel Servicio !!!");
        }

    }

    public void SeleccionaFiltroMonedaMonto(String fMoneda, String fMontoDesde, String fMontoHasta) {

        switch (fMoneda) {
            case "PEN":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_MONEDA_MONTO));
                WebElement btnSoles = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_MONEDA_MONTO, "div > div:nth-child(1) > label", "", "");
                btnSoles.click();

                WebElement inpFiltroAmountMin = getDriver().findElement(By.cssSelector("input[formcontrolname='amountMin']"));
                inpFiltroAmountMin.sendKeys(fMontoDesde);

                WebElement inpFiltroAmountMax = getDriver().findElement(By.cssSelector("input[formcontrolname='amountMax']"));
                inpFiltroAmountMax.sendKeys(fMontoHasta);

                break;
            case "USD":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_MONEDA_MONTO));
                WebElement btnDolares = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_MONEDA_MONTO, "div > div:nth-child(2) > label", "", "");
                btnDolares.click();

                WebElement txtFiltroAmountMin = getDriver().findElement(By.cssSelector("input[formcontrolname='amountMin']"));
                txtFiltroAmountMin.sendKeys(fMontoDesde);

                WebElement txtFiltroAmountMax = getDriver().findElement(By.cssSelector("input[formcontrolname='amountMax']"));
                txtFiltroAmountMax.sendKeys(fMontoHasta);
                break;
            default:
                System.out.println("No se encuentra ningún tipo de moneda !!!");
        }

    }

    public void SeleccionaFiltroMasFiltros(String fMasFiltros) {

        switch (fMasFiltros) {
            case "Email":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_MASFILTROS));
                WebElement btnEmail = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_MASFILTROS, "div > div:nth-child(2) > label", "", "");
                btnEmail.click();
                break;
            case "Domicilio":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_MASFILTROS));
                WebElement btnDomicilio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_MASFILTROS, "div > div:nth-child(3) > label", "", "");
                btnDomicilio.click();
                break;
            default:
                System.out.println("No se encuentra ningún filtro MAS FILTROS !!!");
        }
    }

    //TODO: Validacion de filtros
    public boolean validaFiltroSegmento(String tipologia, String perfil, String fSegmento, String departamento) {
        boolean validaPaginacion = false;

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;
                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;
                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;
                                    }
                                }
                                break;
                            case "AnulacionTC":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "FinalizacionTC":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }
                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }
                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }
                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor GPYR !!!");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":

                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "CobrosIndebidos":

                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }
                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":

                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista SEGUROS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":

                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "MiEquipo":

                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Asignar":

                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Resolver":

                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Monitorear":

                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor SEGUROS !!!");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":

                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":

                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista CONTROVERSIAS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor CONTROVERSIAS !!!");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Retipificar":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista USPR !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }


                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueSegmento = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)", "");
                                            String getSegmento = valueSegmento.getText();
                                            System.out.println("Filtro Segmento encontrado: " + getSegmento + " vs Filtro Segmento Deaseado: " + fSegmento);
                                            assertThat(getSegmento).isEqualTo(fSegmento);
                                            contador++;
                                        }
                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor USPR !!!");
                        }
                        break;
                }
                break;
        }

        return validaPaginacion;
    }

    public boolean validaFiltroTipoTramite(String departamento, String perfil, String tipologia, String fTipoTramite) {

        boolean validaPaginacion = false;

        switch (departamento) {

            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    WebElement valueNoCobrosIndebidos = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoCobrosIndebidos.equals("No se encontraron coincidencias") || valueNoCobrosIndebidos.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoCobrosIndebidos);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueNoExoneracionCobros = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoExoneracionCobros.equals("No se encontraron coincidencias") || valueNoExoneracionCobros.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoExoneracionCobros);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "AnulacionTC":
                                try {
                                    WebElement valueNoAnulacionTC = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoAnulacionTC.equals("No se encontraron coincidencias") || valueNoAnulacionTC.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoAnulacionTC);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "FinalizacionTC":
                                try {
                                    WebElement valueNoFinalizacionTC = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoFinalizacionTC.equals("No se encontraron coincidencias") || valueNoFinalizacionTC.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoFinalizacionTC);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Especialista GPYR !!!");
                        }
                        break;
                    //TODO: Tipo Tramite Supervisor GPYR - No muestra en tabla
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueNoMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoMiEquipo.equals("No se encontraron coincidencias") || valueNoMiEquipo.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoMiEquipo);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueNoAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoAsignar.equals("No se encontraron coincidencias") || valueNoAsignar.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoAsignar);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueNoResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoResolver.equals("No se encontraron coincidencias") || valueNoResolver.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoResolver);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor GPYR !!!");
                        }
                        break;
                }
                break;

            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    WebElement valueNoCobrosIndebidos = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoCobrosIndebidos.equals("No se encontraron coincidencias") || valueNoCobrosIndebidos.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoCobrosIndebidos);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueNoExoneracionCobros = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoExoneracionCobros.equals("No se encontraron coincidencias") || valueNoExoneracionCobros.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoExoneracionCobros);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    WebElement valueNoConsumosNoReconocidos = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoConsumosNoReconocidos.equals("No se encontraron coincidencias") || valueNoConsumosNoReconocidos.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoConsumosNoReconocidos);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueNoMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoMonitorear.equals("No se encontraron coincidencias") || valueNoMonitorear.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoMonitorear);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista SEGUROS !!!");
                        }
                        break;
                    //TODO: Tipo Tramite Supervisor Seguros - No muestra en tabla
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueNoMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoMiEquipo.equals("No se encontraron coincidencias") || valueNoMiEquipo.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoMiEquipo);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueNoAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoAsignar.equals("No se encontraron coincidencias") || valueNoAsignar.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoAsignar);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueNoResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoResolver.equals("No se encontraron coincidencias") || valueNoResolver.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoResolver);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueNoMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoMonitorear.equals("No se encontraron coincidencias") || valueNoMonitorear.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoMonitorear);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor SEGUROS !!!");
                        }
                        break;
                }
                break;

            case "Retenciones":
                break;

            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    WebElement valueNoConsumosNoReconocidos = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoConsumosNoReconocidos.equals("No se encontraron coincidencias") || valueNoConsumosNoReconocidos.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoConsumosNoReconocidos);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    WebElement valueNoConsumosMalProcesados = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoConsumosMalProcesados.equals("No se encontraron coincidencias") || valueNoConsumosMalProcesados.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoConsumosMalProcesados);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueNoMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoMonitorear.equals("No se encontraron coincidencias") || valueNoMonitorear.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoMonitorear);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    WebElement valueNoTransaccionesEnProceso = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTransaccionesEnProceso.equals("No se encontraron coincidencias") || valueNoTransaccionesEnProceso.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTransaccionesEnProceso);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista CONTROVERSIAS !!!");
                        }
                        break;
                    //TODO: Tipo Tramite Supervisor Controversias - No muestra en tabla
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueNoMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoMiEquipo.equals("No se encontraron coincidencias") || valueNoMiEquipo.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoMiEquipo);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueNoAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoAsignar.equals("No se encontraron coincidencias") || valueNoAsignar.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoAsignar);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueNoResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoResolver.equals("No se encontraron coincidencias") || valueNoResolver.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoResolver);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueNoMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoMonitorear.equals("No se encontraron coincidencias") || valueNoMonitorear.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoMonitorear);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    WebElement valueNoTransaccionesEnProceso = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTransaccionesEnProceso.equals("No se encontraron coincidencias") || valueNoTransaccionesEnProceso.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTransaccionesEnProceso);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor CONTROVERSIAS !!!");
                        }
                        break;
                }
                break;

            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    WebElement valueNoCobrosIndebidos = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoCobrosIndebidos.equals("No se encontraron coincidencias") || valueNoCobrosIndebidos.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoCobrosIndebidos);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueNoExoneracionCobros = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoExoneracionCobros.equals("No se encontraron coincidencias") || valueNoExoneracionCobros.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoExoneracionCobros);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    WebElement valueNoConsumosMalProcesados = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoConsumosMalProcesados.equals("No se encontraron coincidencias") || valueNoConsumosMalProcesados.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoConsumosMalProcesados);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    WebElement valueNoConsumosNoReconocidos = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoConsumosNoReconocidos.equals("No se encontraron coincidencias") || valueNoConsumosNoReconocidos.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoConsumosNoReconocidos);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Retipificar":
                                try {
                                    WebElement valueNoRetipificar = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoRetipificar.equals("No se encontraron coincidencias") || valueNoRetipificar.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoRetipificar);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista USPR !!!");
                        }
                        break;
                    //TODO: Tipo Tramite Supervisor USPR - No muestra en tabla
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueNoTodosMisTramites = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoTodosMisTramites.equals("No se encontraron coincidencias") || valueNoTodosMisTramites.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoTodosMisTramites);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueNoMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoMiEquipo.equals("No se encontraron coincidencias") || valueNoMiEquipo.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoMiEquipo);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueNoAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoAsignar.equals("No se encontraron coincidencias") || valueNoAsignar.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoAsignar);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueNoResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p");
                                    if (valueNoResolver.equals("No se encontraron coincidencias") || valueNoResolver.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + valueNoResolver);
                                        validaPaginacion = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro tipo tramite encontrado: " + getTipoTramite + " vs Filtro tipo tramite Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaPaginacion = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor USPR !!!");
                        }
                        break;
                }
                break;
        }

        return validaPaginacion;
    }


}
