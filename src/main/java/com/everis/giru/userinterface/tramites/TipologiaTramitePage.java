package com.everis.giru.userinterface.tramites;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class TipologiaTramitePage {

    public static final Target LBL_TIPOLOGIA_ESPECIALISTA = Target.the("Perfil Tipologia especialista").located(By.cssSelector(".title"));

}
