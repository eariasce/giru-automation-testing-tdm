package com.everis.giru.userinterface.tramites;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class TramitesPage {



    //Combo tipo de documento
    public static final Target CBX_TIPO_DOC = Target.the("Combo tramites por tipo de documento").located(By.id("search-claim-form__document-type"));
    public static final Target DIV_SELECT_NRO_TRAMITE = Target.the("Select N° de tramite").located(By.id("TRAMITE"));
    public static final Target DIV_SELECT_DNI = Target.the("Select DNI").located(By.id("DNI"));
    public static final Target DIV_SELECT_CE = Target.the("Select CE").located(By.id("CE"));
    //Button Buscar
    public static final Target INP_NUMERO_DOC = Target.the("Número de documento").located(By.id("search-claim-form__txt-document-number"));
    public static final Target BTN_BUSCAR = Target.the("Botón buscar").located(By.xpath("//img[@alt='buscar']"));


    ///////////////////////////////////////////////////////////////Departamento GPYR//////////////////////////////////////////////////////////////////////////////
    //Especialista GPYR
                        //Clic en Menú
    public static final Target SIDENAV_MENU_TRAMITES = Target.the("Menu todos mis tramites").located(By.id("item-5"));
    public static final Target SIDENAV_MENU_TRAMITES_ESPECIALISTA_GPYR = Target.the("Menu todos mis tramites ESPECIALISTA_GPYR").located(By.id("item-5"));
    public static final Target SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_GPYR = Target.the("Menu Cobros Indebidos ESPECIALISTA_GPYR").located(By.id("item-7"));
    public static final Target SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_GPYR = Target.the("Menu Exoneracion de Cobros ESPECIALISTA_GPYR").located(By.id("item-8"));
    public static final Target SIDENAV_MENU_ANULACION_TC_ESPECIALISTA_GPYR = Target.the("Menu Anulacion de TC ESPECIALISTA_GPYR").located(By.id("item-15"));
    public static final Target SIDENAV_MENU_FINALIZ_TC_ESPECIALISTA_GPYR = Target.the("Menu Finalizacion de TC ESPECIALISTA_GPYR").located(By.id("item-16"));

                        //Contadores
    public static final Target LBL_TOTAL_TRAMITE_ESPECIALISTA_GPYR = Target.the("Total de tramites").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-0.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_COBROSINDEBIDOS_ESPECIALISTA_GPYR = Target.the("Total de CobrosIndebidos ESPECIALISTA_GPYR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-2.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_EXONERACION_ESPECIALISTA_GPYR = Target.the("Total de Exoneracion ESPECIALISTA_GPYR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-3.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_ANULACIONTC_ESPECIALISTA_GPYR = Target.the("Total de AnulacionTC ESPECIALISTA_GPYR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-4.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_FINALIZACIONTC_ESPECIALISTA_GPYR = Target.the("Total de FinalizacionTC ESPECIALISTA_GPYR" ).located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-5.ng-star-inserted"));

    //Supervisor GPYR
    public static final Target SIDENAV_MENU_TRAMITES_SUPERVISOR_GPYR = Target.the("MENU_TRAMITES_SUPERVISOR_GPYR").located(By.id("item-1"));
    public static final Target SIDENAV_MENU_MIEQUIPO_SUPERVISOR_GPYR = Target.the("MENU_MIEQUIPO_SUPERVISOR_GPYR").located(By.id("item-4"));
    public static final Target SIDENAV_MENU_ASIGNAR_SUPERVISOR_GPYR = Target.the("MENU_ASIGNAR_SUPERVISOR_GPYR ").located(By.id("item-2"));
    public static final Target SIDENAV_MENU_RESOLVER_SUPERVISOR_GPYR = Target.the("MENU_RESOLVER_SUPERVISOR_GPYR").located(By.id("item-3"));

                        //Contadores
    public static final Target LBL_TOTAL_TRAMITE_SUPERVISOR_GPYR = Target.the("Total de tramites SUPERVISOR_GPYR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c15-0.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_MIEQUIPO_SUPERVISOR_GPYR = Target.the("Total de Mi Equipo SUPERVISOR_GPYR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c15-1.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_ASIGNAR_SUPERVISOR_GPYR = Target.the("Total de Asignar SUPERVISOR_GPYR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c15-14.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_RESOLVER_SUPERVISOR_GPYR = Target.the("Total de Resolver SUPERVISOR_GPYR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c15-15.ng-star-inserted"));

    ///////////////////////////////////////////////////////////////Departamento SEGUROS//////////////////////////////////////////////////////////////////////////////
    //Especialista Seguros
    public static final Target SIDENAV_MENU_TRAMITES_ESPECIALISTA_SEGUROS = Target.the("Menu todos mis tramites ESPECIALISTA_SEGUROS" ).located(By.id("item-5"));
    public static final Target SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_SEGUROS = Target.the("Menu Cobros Indebidos ESPECIALISTA_SEGUROS").located(By.id("item-7"));
    public static final Target SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_SEGUROS = Target.the("Menu Exoneracion de Cobros ESPECIALISTA_SEGUROS").located(By.id("item-8"));
    public static final Target SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS = Target.the("Menu CNR ESPECIALISTA_SEGUROS").located(By.id("item-11"));
    public static final Target SIDENAV_MENU_MONITOREAR_ESPECIALISTA_SEGUROS = Target.the("Menu Monitorear ESPECIALISTA_SEGUROS").located(By.id("item-12"));

                        //Contadores
    public static final Target LBL_TOTAL_TRAMITE_ESPECIALISTA_SEGUROS = Target.the("Total de tramites ESPECIALISTA_SEGUROS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-0.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_COBROSINDEBIDOS_ESPECIALISTA_SEGUROS = Target.the("Total de CobrosIndebidos ESPECIALISTA_SEGUROS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-2.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_EXONERACION_ESPECIALISTA_SEGUROS = Target.the("Total de Exoneracion ESPECIALISTA_SEGUROS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-3.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS = Target.the("Total de CNR ESPECIALISTA_SEGUROS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-4.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_MONITOREAR_ESPECIALISTA_SEGUROS = Target.the("Total de Monitorear ESPECIALISTA_SEGUROS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-6.ng-star-inserted"));

    //Supervisor Seguros
    public static final Target SIDENAV_MENU_TRAMITES_SUPERVISOR_SEGUROS = Target.the("MENU_TRAMITES_SUPERVISOR_SEGUROS").located(By.id("item-1"));
    public static final Target SIDENAV_MENU_MIEQUIPO_SUPERVISOR_SEGUROS = Target.the("MENU_MIEQUIPO_SUPERVISOR_SEGUROS").located(By.id("item-4"));
    public static final Target SIDENAV_MENU_ASIGNAR_SUPERVISOR_SEGUROS = Target.the("MENU_ASIGNAR_SUPERVISOR_SEGUROS").located(By.id("item-2"));
    public static final Target SIDENAV_MENU_RESOLVER_SUPERVISOR_SEGUROS = Target.the("MENU_RESOLVER_SUPERVISOR_SEGUROS").located(By.id("item-3"));
    public static final Target SIDENAV_MENU_MONITOREAR_SUPERVISOR_SEGUROS = Target.the("MENU_MONITOREAR_SUPERVISOR_SEGUR").located(By.id("item-12"));

                        //Contadores
    public static final Target LBL_TOTAL_TRAMITE_SUPERVISOR_SEGUROS = Target.the("Total de tramites SUPERVISOR_SEGUROS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c6-0.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_MIEQUIPO_SUPERVISOR_SEGUROS = Target.the("Total de MIEQUIPO_SUPERVISOR_SEGUROS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c6-1.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_ASIGNAR_SUPERVISOR_SEGUROS = Target.the("Total de ASIGNAR_SUPERVISOR_SEGUROS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c6-7.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_RESOLVER_SUPERVISOR_SEGUROS = Target.the("Total de RESOLVER_SUPERVISOR_SEGUROS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c6-8.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_MONITOREAR_SUPERVISOR_SEGUROS = Target.the("Total de MONITOREAR_SUPERVISOR_SEGUROS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c6-9.ng-star-inserted"));

    ////////////////////////////////////////////////////////////Departamento CONTROVERSIAS//////////////////////////////////////////////////////////////////////////////
    //Especialista Controversias
    public static final Target SIDENAV_MENU_TRAMITES_ESPECIALISTA_CONTROVERSIAS = Target.the("Menu todos mis tramites ESPECIALISTA_CONTROVERSIAS").located(By.id("item-5"));
    public static final Target SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS = Target.the("MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS").located(By.id("item-11"));
    public static final Target SIDENAV_MENU_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS = Target.the("MENU_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS").located(By.id("item-13"));
    public static final Target SIDENAV_MENU_MONITOREAR_ESPECIALISTA_CONTROVERSIAS = Target.the("MENU_MONITOREAR_ESPECIALISTA_CONTROVERSIAS").located(By.id("item-12"));
    public static final Target SIDENAV_MENU_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS = Target.the("MENU_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS").located(By.id("item-17"));

                        //Contadores
    public static final Target LBL_TOTAL_TRAMITE_ESPECIALISTA_CONTROVERSIAS = Target.the("Total de tramites ESPECIALISTA_CONTROVERSIAS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-17.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS = Target.the("Total de CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-19.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS = Target.the("Total de MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-20.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_MONITOREAR_ESPECIALISTA_CONTROVERSIAS = Target.the("Total de MONITOREAR_ESPECIALISTA_CONTROVERSIAS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-22.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS = Target.the("Total de TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-23.ng-star-inserted"));

    //Supervisor Controversias
    public static final Target SIDENAV_MENU_TRAMITES_SUPERVISOR_CONTROVERSIAS = Target.the("Menu todos mis tramites SUPERVISOR_CONTROVERSIAS").located(By.id("item-1"));
    public static final Target SIDENAV_MENU_MIEQUIPO_SUPERVISOR_CONTROVERSIAS = Target.the("Menu MIEQUIPO_SUPERVISOR_CONTROVERSIAS").located(By.id("item-4"));
    public static final Target SIDENAV_MENU_ASIGNAR_SUPERVISOR_CONTROVERSIAS = Target.the("Menu ASIGNAR_SUPERVISOR_CONTROVERSIAS").located(By.id("item-2"));
    public static final Target SIDENAV_MENU_RESOLVER_SUPERVISOR_CONTROVERSIAS = Target.the("Menu RESOLVER_SUPERVISOR_CONTROVERSIAS").located(By.id("item-3"));
    public static final Target SIDENAV_MENU_MONITOREAR_SUPERVISOR_CONTROVERSIAS = Target.the("Menu MONITOREAR_SUPERVISOR_CONTROVERSIAS").located(By.id("item-12"));
    public static final Target SIDENAV_MENU_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS = Target.the("Menu TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS").located(By.id("item-17"));

                        //Contadores
    public static final Target LBL_TOTAL_TRAMITE_SUPERVISOR_CONTROVERSIAS = Target.the("Total de tramites").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-24.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_MIEQUIPO_SUPERVISOR_CONTROVERSIAS = Target.the("Total de CobrosIndebidos").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-25.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_ASIGNAR_SUPERVISOR_CONTROVERSIAS = Target.the("Total de Exoneracion").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-50.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_RESOLVER_SUPERVISOR_CONTROVERSIAS = Target.the("Total de CNR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-51.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_MONITOREAR_SUPERVISOR_CONTROVERSIAS = Target.the("Total de Monitorear").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-52.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS = Target.the("Total de Monitorear").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-53.ng-star-inserted"));

    //////////////////////////////////////////////////////////Departamento U SEGTO. PEDIDOS RECLAMOS//////////////////////////////////////////////////////////////////////////////
    //Especialista USPR
    public static final Target SIDENAV_MENU_TRAMITES_ESPECIALISTA_USPR = Target.the("Menu todos mis tramites").located(By.id("item-5"));
    public static final Target SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_USPR = Target.the("Menu Cobros Indebidos").located(By.id("item-7"));
    public static final Target SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_USPR = Target.the("Menu Exoneracion de Cobros").located(By.id("item-8"));
    public static final Target SIDENAV_MENU_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR = Target.the("Menu Exoneracion de Cobros").located(By.id("item-13"));
    public static final Target SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR = Target.the("Menu Exoneracion de Cobros").located(By.id("item-11"));
    public static final Target SIDENAV_MENU_CONSUMOS_RETIPIFICAR_ESPECIALISTA_USPR = Target.the("Menu Exoneracion de Cobros").located(By.id("item-10"));

                        //Contadores
    public static final Target LBL_TOTAL_TRAMITE_ESPECIALISTA_USPR = Target.the("Total de tramites ESPECIALISTA_USPR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-54.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_COBROS_INDEBIDOS_ESPECIALISTA_USPR = Target.the("Total de CobrosIndebidos ESPECIALISTA_USPR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-56.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_EXO_COBROS_ESPECIALISTA_USPR = Target.the("Total de Exoneracion ESPECIALISTA_USPR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-57.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR = Target.the("Total de CMP ESPECIALISTA_USPR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-58.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR = Target.the("Total de CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-58.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_RETIPIFICAR_ESPECIALISTA_USPR = Target.the("Total de RETIPIFICAR_ESPECIALISTA_USPR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-61.ng-star-inserted"));

    //Supervisor USPR
    public static final Target SIDENAV_MENU_TRAMITES_SUPERVISOR_USPR = Target.the("Menu todos mis tramites SUPERVISOR_USPR").located(By.id("item-1"));
    public static final Target SIDENAV_MENU_MIEQUIPO_SUPERVISOR_USPR = Target.the("Menu MIEQUIPO_SUPERVISOR_USPR").located(By.id("item-4"));
    public static final Target SIDENAV_MENU_ASIGNAR_SUPERVISOR_USPR = Target.the("Menu ASIGNAR_SUPERVISOR_USPR").located(By.id("item-2"));
    public static final Target SIDENAV_MENU_RESOLVER_SUPERVISOR_USPR = Target.the("Menu RESOLVER_SUPERVISOR_USPR").located(By.id("item-3"));

                        //Contadores
    public static final Target LBL_TOTAL_TRAMITE_SUPERVISOR_USPR  = Target.the("Total de tramites SUPERVISOR_USPR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-62.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_MIEQUIPO_SUPERVISOR_USPR = Target.the("Total de MIEQUIPO_SUPERVISOR_USPR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-63.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_ASIGNAR_SUPERVISOR_USPR = Target.the("Total de ASIGNAR_SUPERVISOR_USPR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-87.ng-star-inserted"));
    public static final Target LBL_TOTAL_TRAMITE_RESOLVER_SUPERVISOR_USPR = Target.the("Total de RESOLVER_SUPERVISOR_USPR").located(By.cssSelector(".num-claim.fs-14.ng-tns-c11-88.ng-star-inserted"));


}
