package com.everis.giru.exceptions;

public class DocumentoException extends IllegalStateException {

    public DocumentoException(String message) {
        super(message);
    }
    
}
