package com.everis.giru.exceptions;

public class CodigoException extends IllegalStateException {

    public CodigoException(String message) {
        super(message);
    }
    
}
