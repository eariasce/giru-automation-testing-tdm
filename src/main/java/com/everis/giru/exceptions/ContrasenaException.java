package com.everis.giru.exceptions;

public class ContrasenaException extends IllegalStateException {

    public ContrasenaException(String message) {
        super(message);
    }
    
}
