package com.everis.giru.tasks.login;

import com.everis.giru.userinterface.login.MenuPrincipalPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SeleccionarMenuPrincipal implements Task {

    private final String menuPrincipal;

    public SeleccionarMenuPrincipal(String menuPrincipal) {
        this.menuPrincipal = menuPrincipal;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        switch (menuPrincipal) {
            case "Administrador":
                actor.attemptsTo(
                        Click.on(MenuPrincipalPage.NAV_ADMINISTRADOR)
                );
                break;
            case "Dashboard":
                actor.attemptsTo(
                        Click.on(MenuPrincipalPage.NAV_DASHBOARD)
                );
                break;
            case "Tramites":
                actor.attemptsTo(
                        Click.on(MenuPrincipalPage.NAV_TRAMITES)
                );
                break;
            case "AtencionCliente":
                actor.attemptsTo(
                        Click.on(MenuPrincipalPage.NAV_ATENCIONALCLIENTE)
                );
                break;
            case "Historial":
                actor.attemptsTo(
                        Click.on(MenuPrincipalPage.NAV_HISTORIAL)
                );
                break;
            case "Pure Connect":
                actor.attemptsTo(
                        Click.on(MenuPrincipalPage.NAV_PURECONNECT)
                );
                break;
            default:
                System.out.println("No se encuentra ningún Menú Principal !!!");
        }
    }

    public static Performable  withData(String menuPrincipal) {
        return instrumented(SeleccionarMenuPrincipal.class, menuPrincipal);
    }
}
