package com.everis.giru.tasks.login;

import com.everis.giru.questions.login.PerfilQuestions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class PerfilDeterminado implements Task {

    private final String profile;

    public PerfilDeterminado(String profile) {
        this.profile = profile;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        switch (profile) {
            case "Asesor":
                theActorInTheSpotlight().should(
                        seeThat(PerfilQuestions.perfilAsesor(), equalTo("Ahora, identifiquemos a nuestro cliente"))
                );
                break;
            case "Asesor Retencion":
                theActorInTheSpotlight().should(
                        seeThat(PerfilQuestions.PerfilAsesorRetencion(), equalTo("Todos los trámites"))
                );
                break;
            case "Especialista":
                theActorInTheSpotlight().should(
                        seeThat(PerfilQuestions.perfilEspecialista(), equalTo("Mis trámites"))
                );
                break;
            default:
                System.out.println("No se encuentra perfil determinado !!!");
        }

    }

    public static Performable withData(String profile) {
        return instrumented(PerfilDeterminado.class, profile);
    }

}
