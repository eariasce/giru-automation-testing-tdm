package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.RegistrarTramitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RegistraComentariosYRespuesta implements Task {

    private final String respuestaCliente;
    private final String comentarioAdicional;

    public RegistraComentariosYRespuesta(String respuestaCliente, String comentarioAdicional) {
        this.respuestaCliente = respuestaCliente;
        this.comentarioAdicional = comentarioAdicional;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if(respuestaCliente.equals("Sí"))
        actor.attemptsTo(
                Enter.theValue("Respuesta Prueba").into(RegistrarTramitePage.TXT_RESPUESTA_CLIENTE)
        );
        if(comentarioAdicional.equals("Sí"))
        actor.attemptsTo(
                Enter.theValue("Comentario adicional de prueba").into(RegistrarTramitePage.TXT_COMENTARIO_ADICIONAL)
        );

    }

    public static Performable withData(String respuestaCliente, String comentariosAdicionales) {
        return instrumented(RegistraComentariosYRespuesta.class, respuestaCliente,comentariosAdicionales);

    }
}
