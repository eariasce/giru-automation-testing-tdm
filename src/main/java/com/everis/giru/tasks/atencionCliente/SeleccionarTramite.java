package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.RegistrarTramitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarTramite implements Task {

    private final String tramite;

    public SeleccionarTramite(String tramite) {
        this.tramite = tramite;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the(RegistrarTramitePage.CBX_TRAMITE, isVisible()).forNoMoreThan(100).seconds(),
                Click.on(RegistrarTramitePage.CBX_TRAMITE)
        );

        switch (tramite) {
            case "Pedido":
                actor.attemptsTo(
                        Click.on(RegistrarTramitePage.DIV_TRAMITE_PEDIDO)
                );
                break;
            case "Reclamo":
                actor.attemptsTo(
                        Click.on(RegistrarTramitePage.DIV_TRAMITE_RECLAMO)
                );
                break;
            case "Solicitud":
                actor.attemptsTo(
                        Click.on(RegistrarTramitePage.DIV_TRAMITE_SOLICITUD)
                );
                break;

            default:
                System.out.println("No se encuentra tipo de tramite !!!");
        }

    }

    public static Performable withData(String tramite) {
        return instrumented(SeleccionarTramite.class, tramite);
    }

}
