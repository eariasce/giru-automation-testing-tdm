package com.everis.giru.tasks.atencionCliente;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RegistraDireccion implements Task {

    private final String departamento;
    private final String provincia;
    private final String distrito;
    private final String tipoVia;
    private final String nombreVia;
    private final String urbanizacion;
    private final String numero_urb;
    private final String manzana;
    private final String lote;
    private final String interior;
    private final String referencia;
    public RegistraDireccion(String departamento,String provincia,String distrito,String tipoVia, String nombreVia, String urbanizacion, String numero_urb, String manzana, String lote, String interior, String referencia) {

        this.departamento = departamento;
        this.provincia = provincia;
        this.distrito = distrito;
        this.tipoVia = tipoVia;
        this.nombreVia = nombreVia;
        this.urbanizacion = urbanizacion;
        this.numero_urb = numero_urb;
        this.manzana  = manzana;
        this.lote = lote;
        this.interior = interior;
        this.referencia = referencia;

    }


    @Override
    public <T extends Actor> void performAs(T actor) {

        //DEPARTAMENTO
        actor.attemptsTo( Click.on(Target.the("Seleccionar departamento").located(By.xpath("//*[@id='department']"))));


        actor.attemptsTo( Click.on(Target.the("Seleccionar departamento").located(By.xpath("//*[@id='department']/div[2]/ul/li[@id='"+departamento+"']"))));


        //PROVINCIA
        actor.attemptsTo( Click.on(Target.the("Seleccionar provincia").located(By.xpath("//*[@id='province']"))));


        actor.attemptsTo( Click.on(Target.the("Seleccionar provincia").located(By.xpath("//*[@id='province']/div[2]/ul/li[@id='"+provincia+"']"))));
        //DISTRITO
        actor.attemptsTo( Click.on(Target.the("Seleccionar distrito").located(By.xpath("//*[@id='district']"))));


        actor.attemptsTo( Click.on(Target.the("Seleccionar distrito").located(By.xpath("//*[@id='district']/div[2]/ul/li[@id='"+distrito+"']"))));

        //TIPO DE VIA
        actor.attemptsTo( Click.on(Target.the("Seleccionar tipo de via").located(By.xpath("//*[@id='streetType']"))));


        actor.attemptsTo( Click.on(Target.the("Seleccionar tipo de via").located(By.xpath("//*[@id='streetType']/div[2]/ul/li[@id='"+tipoVia+"']"))));


        //NOMBRE DE VIA
        actor.attemptsTo(Enter.theValue(nombreVia).into(By.xpath("//*[@id='streetName']")));


        //NOMBRE DE URBANIZACION
        actor.attemptsTo(Enter.theValue(urbanizacion).into(By.xpath("//*[@id='neighborhood']")));

        //NUMERO DE CALLE
        actor.attemptsTo(Enter.theValue(numero_urb).into(By.xpath("//*[@id='streetNumber']")));

        //MANZANA
        actor.attemptsTo(Enter.theValue(manzana).into(By.xpath("//*[@id='block']")));

        //LOTE
        actor.attemptsTo(Enter.theValue(lote).into(By.xpath("//*[@id='lot']")));

        //INTERIOR
        actor.attemptsTo(Enter.theValue(interior).into(By.xpath("//*[@id='apartment']")));
        //REFERENCIA
        actor.attemptsTo(Enter.theValue(referencia).into(By.xpath("//label[text()='Referencia']/../textarea")));

        //BOTON REGISTRO DIRECCION
        actor.attemptsTo(Enter.theValue(referencia).into(By.xpath("//*[@id='add-address__btn-add']")));



        //Termino de registrar reclamo
        // actor.attemptsTo(Click.on(RegistrarTramitePage.BTN_REGISTRO_RECLAMO));

        try{
            Thread.sleep(5000);

        }catch (Exception e)
        {
            System.out.println(e);
        }

    }

    public static Performable withData(String departamento,String provincia,String distrito,String tipoVia, String nombreVia, String urbanizacion, String numero_urb, String manzana, String lote, String interior, String referencia) {
        return instrumented(RegistraDireccion.class, departamento, provincia, distrito, tipoVia, nombreVia, urbanizacion, numero_urb, manzana, lote, interior, referencia);
    }


}
