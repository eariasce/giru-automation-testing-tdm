package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.RegistrarTramitePage;
import com.everis.giru.userinterface.atencionCliente.tramiteReclamo.ButtonContinuarMotivo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class RegistraMotivo implements Task {

    private ButtonContinuarMotivo driver = new ButtonContinuarMotivo();
    private final String motivo;
    private final String cantidadmotivos;
    private final String fecha;

    private final String tipomoneda;
    private final String monto;
    private final String cantidadmontomotivo;

    public RegistraMotivo(String cantidadmotivos, String motivo, String fecha, String tipomoneda, String monto, String cantidadmontomotivo) {

        this.motivo = motivo;
        this.cantidadmotivos = cantidadmotivos;
        this.fecha = fecha;
        this.tipomoneda = tipomoneda;
        this.monto = monto;
        this.cantidadmontomotivo= cantidadmontomotivo;


    }


    @Override
    public <T extends Actor> void performAs(T actor) {


        String motivo2 ="";
        String[] motivos = motivo.split("-");
        String[] fechas = fecha.split("-");
        String[] tipomonedas =tipomoneda.split("-");

        int cantidad = motivo.split("-").length;

        //##


        try {

            if(cantidadmotivos.equals("2"))
                motivo2 = motivo.split("-")[1];
                if(cantidadmotivos.equals("1"))
                    motivo2 = motivo.split("-")[1];
        }
        catch (Exception e) {
            System.out.println(e);
        }

        for(int i = 0; i<cantidad-1;i++) {



            int separador = motivo.split("-").length;
            try {

                if (motivos[i+1].equals("3")) //caso especifico de valor 3
                {

                    actor.attemptsTo(
                            WaitUntil.the(RegistrarTramitePage.CBX_MOTIVO, isVisible()).forNoMoreThan(100).seconds(),
                            Click.on(RegistrarTramitePage.CBX_MOTIVO),
                            WaitUntil.the(RegistrarTramitePage.CBX_MOTIVO, isVisible()).forNoMoreThan(100).seconds(),
                            Click.on(RegistrarTramitePage.CBX_MOTIVO),
                    Click.on(Target.the("Select exoneracion de cobros").located(By.xpath("//*[@id='3']/span"))));

                }

                else if (separador == 1) {

                    actor.attemptsTo(
                            WaitUntil.the(RegistrarTramitePage.CBX_MOTIVO, isVisible()).forNoMoreThan(100).seconds(),
                            Click.on(RegistrarTramitePage.CBX_MOTIVO),


                            Click.on(Target.the("Select exoneracion de cobros").located(By.id(motivo.split("-")[i])))

                    );

                } else {
                    actor.attemptsTo(
                            WaitUntil.the(RegistrarTramitePage.CBX_MOTIVO, isVisible()).forNoMoreThan(100).seconds(),
                            Click.on(RegistrarTramitePage.CBX_MOTIVO),
                            Click.on(Target.the("Select exoneracion de cobros").located(By.id(motivo.split("-")[i+1])))
                    );

                    actor.attemptsTo(
                            Enter.theValue(fechas[i]).into(Target.the("fecha de motivo").located(By.id("Fecha_"+i+"_0")))
                            );


                    actor.attemptsTo(
                            Click.on(Target.the("tipo de moneda").located(By.id("Moneda_"+i+"_0"))),


                            Click.on(Target.the("seleccion de moneda").located(By.xpath("//*[@id='Moneda_"+i+"_0']/div[2]/ul/li[@id='"+tipomonedas[i]+"']")))



                    );
                    String[] montos2 = monto.split("/");

                    driver.clickContinuar(i);

                        actor.attemptsTo(

                                Enter.theValue(montos2[i]).into(Target.the("monto de motivo").located(By.id("Monto_" + i + "_0")))
                        );



                    if(Integer.parseInt(cantidadmontomotivo)>1) {
                        String[] montosmotivo = montos2[i].split("-");

                        for (int j = 1; j < montosmotivo.length; j++)
                            actor.attemptsTo(
                                    Click.on(Target.the("agregar nuevo movimiento").located(By.id("Add_Amount_" + i))),


                                    Enter.theValue(fechas[i]).into(Target.the("fecha de motivo").located(By.id("Fecha_" + i + "_" + j))),

                                    Click.on(Target.the("tipo de moneda").located(By.id("Moneda_" + i + "_" + j))),
                                    Click.on(Target.the("seleccion de moneda").located(By.xpath("//*[@id='Moneda_" + i + "_" + j + "']/div[2]/ul/li[@id='" + tipomonedas[i] + "']"))),


                                    Enter.theValue(montosmotivo[j - 1]).into(Target.the("monto de motivo").located(By.id("Monto_" + i + "_" + j)))


                            );


                    }

                    driver.clickContinuar(i);

                }


            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public static Performable withData(String cantidadmotivos,String motivo, String fecha,String tipomoneda,String monto,String cantidadmontomotivo) {
        return instrumented(RegistraMotivo.class, cantidadmotivos,motivo, fecha,tipomoneda,monto,cantidadmontomotivo);
    }


}
