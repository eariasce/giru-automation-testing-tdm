package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.RegistrarTramitePage;
import lombok.SneakyThrows;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import org.springframework.core.io.*;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SubirArchivo implements Task {

    private final String subirArchivo;

    public SubirArchivo(String subirArchivo) {
        this.subirArchivo = subirArchivo;

    }

    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {

        if(subirArchivo.equals("Sí")) {
            actor.attemptsTo(Click.on(RegistrarTramitePage.BTN_SUBIR_ARCHIVO));

            Robot robo = new Robot();

            //Copia la ruta de archivo en el portapapeles
           Resource rsrc = new ClassPathResource("archivoReclamo\\TestAuto.txt");
            String path =  rsrc.getFile().getAbsolutePath();
                       StringSelection str2 = new StringSelection(path);

            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str2,null);
            Thread.sleep(3000);

            //Presiona control+v
            robo.keyPress(KeyEvent.VK_CONTROL);
            robo.keyPress(KeyEvent.VK_V);

            //Suelta control+v
            robo.keyRelease(KeyEvent.VK_CONTROL);
            robo.keyRelease(KeyEvent.VK_V);

            //Presiona enter
            robo.keyPress(KeyEvent.VK_ENTER);

            //Suelta enter
            robo.keyRelease(KeyEvent.VK_ENTER);
            //Thread.sleep(5000);

        }

        try
        {
            Thread.sleep(2000);
        }catch (Exception e)
        {
            System.out.println(e);
        }
    }

    public static Performable withData(String subirArchivo) {
        return instrumented(SubirArchivo.class, subirArchivo);

    }
}
