package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.RegistrarTramitePage;
import lombok.SneakyThrows;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SeleccionaCelularYOperador implements Task {

    private final String numCelular;
    private final String operador;

    public SeleccionaCelularYOperador(String numCelular, String operador) {
        this.numCelular = numCelular;
        this.operador = operador;

    }

    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(RegistrarTramitePage.CBX_NUM_TELEFONO)
                    );

        if(numCelular.equals("Registrado")) {

       actor.attemptsTo(Click.on(Target.the("numero registrado").located(By.xpath("//div[@id='telephone']/div[2]/ul[@id='list']/li[1][@class='li-select flex b-l-gray-three b-r-gray-three b-b-gray-three']"))));

        }
        else
        {
            actor.attemptsTo(Click.on(Target.the("numero registrado").located(By.xpath("//div[@id='telephone']/div[2]/ul[@id='list']/li[@id='CREATE']"))));
            actor.attemptsTo(
            Enter.theValue(numCelular).into(RegistrarTramitePage.TXT_REG_NUM_TELEFONO),
            Click.on(RegistrarTramitePage.TXT_REG_TELF_ACEPTAR));

        }


        actor.attemptsTo(
                Click.on(Target.the("Boton de operador").located(By.xpath("//label[@for='tab-"+ operador+ "']"))));



    }

    public static Performable withData(String numCelular, String operador) {
        return instrumented(SeleccionaCelularYOperador.class, numCelular,operador);

    }
}
