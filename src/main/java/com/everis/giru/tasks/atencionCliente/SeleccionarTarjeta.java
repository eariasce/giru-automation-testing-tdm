package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.RegistrarTramitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarTarjeta implements Task {

    private final String numeroTarjeta;

    public SeleccionarTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the(RegistrarTramitePage.CARD_TARJETA, isVisible()).forNoMoreThan(100).seconds(),
                Click.on(Target.the("Seleccionar tarjeta").located(By.id("credit-cards__cards-" + numeroTarjeta)))
        );

    }

    public static Performable withData(String numeroTarjeta) {
        return instrumented(SeleccionarTarjeta.class, numeroTarjeta);
    }

}
