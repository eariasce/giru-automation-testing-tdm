package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.IdentificarClientePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class IdentificarCliente implements Task {

    private final String tipoDocumento;
    private final String numeroDocumento;

    public IdentificarCliente(String tipoDocumento, String numeroDocumento) {
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumento = numeroDocumento;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(IdentificarClientePage.CBX_SELECCIONAR)
        );

        if (tipoDocumento.equals("CE")) {
            actor.attemptsTo(
                    Click.on(IdentificarClientePage.CBX_SELECT_CE)
            );
        } else {
            actor.attemptsTo(
                    Click.on(IdentificarClientePage.CBX_SELECT_DNI)
            );
        }

        actor.attemptsTo(
                Enter.theValue(numeroDocumento).into(IdentificarClientePage.INP_NUMERO_DOC),
                Click.on(IdentificarClientePage.BTN_VALIDAR)
        );

    }

    public static Performable withData(String tipoDocumento, String numeroDocumento) {
        return instrumented(IdentificarCliente.class, tipoDocumento, numeroDocumento);
    }

}
