package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.RegistrarTramitePage;
import lombok.SneakyThrows;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SeleccionaMedioRespuestaYRegistro implements Task {

    private final String mediorespuesta;
    private final String nuevoregistro;

    public SeleccionaMedioRespuestaYRegistro(String mediorespuesta, String nuevoregistro) {
        this.mediorespuesta = mediorespuesta;
        this.nuevoregistro = nuevoregistro;

    }

    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {

        if (mediorespuesta.equals("Correo")) {
            actor.attemptsTo(Click.on(RegistrarTramitePage.BTN_MEDIO_RESPUESTA_CORREO));

            actor.attemptsTo(Click.on(RegistrarTramitePage.CBX_MEDIO_RESPUESTA_CORREO));

            if (nuevoregistro.equals("Registrado")) {
                actor.attemptsTo(Click.on(Target.the("Primer elemento de combobox correos").located(By.xpath("//*[@id='email']/div[2]/ul/li[1]"))));
            } else {
                actor.attemptsTo(Click.on(Target.the("ultimo elemento de combobox correos").located(By.xpath("//*[@id='email']/div[2]/ul/li[@id='CREATE']"))));


            }
        }
        if (mediorespuesta.equals("Dirección")) {
            actor.attemptsTo(Click.on(RegistrarTramitePage.BTN_MEDIO_RESPUESTA_DIRECCION)
            );

            actor.attemptsTo(Click.on(RegistrarTramitePage.CBX_MEDIO_RESPUESTA_DIRECCION));


            if (nuevoregistro.equals("Registrado")) {
                actor.attemptsTo(Click.on(Target.the("Primer elemento de combobox direccion").located(By.xpath("//*[@id='address']/div[2]/ul/li[1]"))));
            } else {
                actor.attemptsTo(Click.on(Target.the("ultimo elemento de combobox direccion").located(By.xpath("//*[@id='address']/div[2]/ul/li[@id='CREATE']"))));

            }
        }

    }

    public static Performable withData(String mediorespuesta, String nuevoregistro) {
        return instrumented(SeleccionaMedioRespuestaYRegistro.class, mediorespuesta,nuevoregistro);

    }
}
