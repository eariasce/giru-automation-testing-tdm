package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.RegistrarTramitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarTipologia implements Task {

    private final String tipologia;

    public SeleccionarTipologia(String tipologia) {
        this.tipologia = tipologia;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(RegistrarTramitePage.CBX_TIPOLOGIA, isVisible()).forNoMoreThan(100).seconds(),
                Click.on(RegistrarTramitePage.CBX_TIPOLOGIA),
                Click.on(Target.the("Select exoneracion de cobros").located(By.id(tipologia)))
        );

    }

    public static Performable withData(String tipologia) {
        return instrumented(SeleccionarTipologia.class, tipologia);
    }

}
