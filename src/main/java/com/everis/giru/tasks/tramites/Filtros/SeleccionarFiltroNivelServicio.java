package com.everis.giru.tasks.tramites.Filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.GetFiltroPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroNivelServicio implements Task {

    private final String fNivelServicio;
    GetFiltroPage getFiltroPage;

    public SeleccionarFiltroNivelServicio(String fNivelServicio) {
        this.fNivelServicio = fNivelServicio;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fNivelServicio.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_NIVELSERVICIO, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_NIVELSERVICIO)
            );

            getFiltroPage.SeleccionaFiltroNivelServicio(fNivelServicio);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );
        }

    }

    public static Performable withData(String fNivelServicio) {
        return instrumented(SeleccionarFiltroNivelServicio.class, fNivelServicio);
    }
}
