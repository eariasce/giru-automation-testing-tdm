package com.everis.giru.tasks.tramites.Filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.GetFiltroPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroMasFiltros implements Task {

    private final String fMasFiltros;
    GetFiltroPage getFiltroPage;

    public SeleccionarFiltroMasFiltros(String fMasFiltros) {
        this.fMasFiltros = fMasFiltros;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fMasFiltros.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_MASFILTROS, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_MASFILTROS)
            );

            getFiltroPage.SeleccionaFiltroMasFiltros(fMasFiltros);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );

        }

    }

    public static Performable withData(String fMasFiltros) {
        return instrumented(SeleccionarFiltroMasFiltros.class, fMasFiltros);
    }

}
