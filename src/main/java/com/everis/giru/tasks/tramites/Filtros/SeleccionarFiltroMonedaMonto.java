package com.everis.giru.tasks.tramites.Filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.GetFiltroPage;
import lombok.SneakyThrows;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroMonedaMonto implements Task {

    private final String fMoneda;
    private final String fMontoDesde;
    private final String fMontoHasta;
    GetFiltroPage getFiltroPage;

    public SeleccionarFiltroMonedaMonto(String fMoneda, String fMontoDesde, String fMontoHasta) {
        this.fMoneda = fMoneda;
        this.fMontoDesde = fMontoDesde;
        this.fMontoHasta = fMontoHasta;
    }

    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fMoneda.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_MONEDAyMONTO, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_MONEDAyMONTO)
            );

            getFiltroPage.SeleccionaFiltroMonedaMonto(fMoneda, fMontoDesde, fMontoHasta);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );
        }

    }

    public static Performable withData(String fMoneda, String fMontoDesde, String fMontoHasta) {
        return instrumented(SeleccionarFiltroMonedaMonto.class, fMoneda, fMontoDesde, fMontoHasta);
    }

}
