package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.tramites.GetContador;
import com.everis.giru.userinterface.tramites.GetTramitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.assertj.core.api.Assertions.assertThat;

public class ValidarPaginacion implements Task {

    private final String registros;
    private final String tipologia;
    private final String perfil;
    private final String departamento;
    GetTramitePage getTramitePage;
    GetContador getContador = new GetContador();

    public ValidarPaginacion(String registros, String tipologia, String perfil, String departamento) {
        this.registros = registros;
        this.tipologia = tipologia;
        this.perfil = perfil;
        this.departamento = departamento;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (getContador.contadorText(departamento, perfil, tipologia).equals("") || getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
            getTramitePage.EsCeroTotalTramite();
            if (tipologia.equals("TodosMisTramites") || tipologia.equals("MiEquipo") || tipologia.equals("Asignar") || tipologia.equals("Resolver") || tipologia.equals("Monitorear") || tipologia.equals("TransaccionesEnProceso") || tipologia.equals("Retipificar")) {
                if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                    assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                } else {
                    assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                }

            } else {
                if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                    assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                } else {
                    assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                }
            }

        } else {
            getTramitePage.seleccionarDropdownPaginacion(tipologia, perfil, departamento);
            getTramitePage.seleccionarPaginacion(tipologia, perfil, registros, departamento);
            if (getTramitePage.validaPaginacion(tipologia, perfil, registros, departamento)) {
                assertThat(registros).isEqualTo(registros);
            } else {
                assertThat(registros).isEqualTo(getTramitePage.traerContador());
            }
        }
        getContador.SetContadorANull();
    }

    public static Performable withData(String registros, String tipologia, String perfil, String departamento) {
        return instrumented(ValidarPaginacion.class, registros, tipologia, perfil, departamento);
    }

}
