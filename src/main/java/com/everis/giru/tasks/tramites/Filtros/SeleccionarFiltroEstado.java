package com.everis.giru.tasks.tramites.Filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.GetFiltroPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroEstado implements Task {

    private final String fEstado;
    GetFiltroPage getFiltroPage;

    public SeleccionarFiltroEstado(String fEstado) {
        this.fEstado = fEstado;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fEstado.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_ESTADO, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_ESTADO)
            );

            getFiltroPage.SeleccionaFiltroEstado(fEstado);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );
        }

    }

    public static Performable withData(String fEstado) {
        return instrumented(SeleccionarFiltroEstado.class, fEstado);
    }

}
