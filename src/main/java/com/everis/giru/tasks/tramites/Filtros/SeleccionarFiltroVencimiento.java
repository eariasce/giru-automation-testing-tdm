package com.everis.giru.tasks.tramites.Filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.GetFiltroPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroVencimiento implements Task {

    private final String fVencimiento;
    GetFiltroPage getFiltroPage;

    public SeleccionarFiltroVencimiento(String fVencimiento) {
        this.fVencimiento = fVencimiento;
    }


    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fVencimiento.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_VENCIMIENTO, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_VENCIMIENTO)
            );
            getFiltroPage.SeleccionaFiltroVencimiento(fVencimiento);
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );

        }

    }

    public static Performable withData(String fVencimiento) {
        return instrumented(SeleccionarFiltroVencimiento.class, fVencimiento);
    }
}
