package com.everis.giru.tasks.tramites.Filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.GetFiltroPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroTipologia implements Task {

    private final String fTipologia;
    private final String perfil;
    private final String departamento;
    private final String fTipoTramite;
    GetFiltroPage getFiltroPage;

    public SeleccionarFiltroTipologia(String fTipologia, String perfil, String departamento, String fTipoTramite) {
        this.fTipologia = fTipologia;
        this.perfil = perfil;
        this.departamento = departamento;
        this.fTipoTramite = fTipoTramite;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fTipologia.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_TIPOLOGIA, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_TIPOLOGIA)
            );
            getFiltroPage.SeleccionaFiltroTipologia(fTipologia, perfil, departamento, fTipoTramite);
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );
        }

    }

    public static Performable withData(String fTipologia, String perfil, String departamento, String fTipoTramite) {
        return instrumented(SeleccionarFiltroTipologia.class, fTipologia, perfil, departamento, fTipoTramite);
    }

}
