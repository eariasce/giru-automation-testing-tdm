package com.everis.giru.tasks.tramites.Filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.GetFiltroPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroSegmento implements Task {

    private final String fSegmento;
    private final String tipologia;
    private final String perfil;
    private final String departamento;
    GetFiltroPage getFiltroPage;

    public SeleccionarFiltroSegmento(String fSegmento, String tipologia, String perfil, String departamento) {
        this.fSegmento = fSegmento;
        this.tipologia = tipologia;
        this.perfil = perfil;
        this.departamento = departamento;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fSegmento.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_SEGMENTO, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_SEGMENTO)
            );

            getFiltroPage.SeleccionaFiltroSegmento(fSegmento);
            
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );
            getFiltroPage.validaFiltroSegmento(tipologia,perfil,fSegmento,departamento);

        }

    }

    public static Performable withData(String fSegmento,String tipologia,String perfil,String departamento) {
        return instrumented(SeleccionarFiltroSegmento.class, fSegmento,tipologia,perfil,departamento);
    }

}
