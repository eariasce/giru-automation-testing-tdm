package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.tramites.GetContador;
import com.everis.giru.userinterface.tramites.GetTramitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.assertj.core.api.Assertions.assertThat;
public class ValidarContadoresBandeja implements Task {

    GetContador getContador = new GetContador();
    GetTramitePage getTramitePage = new GetTramitePage();
    private final String tipologia;
    private final String perfil;
    private final String departamento;

    public ValidarContadoresBandeja(String tipologia, String perfil, String departamento) {
        this.tipologia = tipologia;
        this.perfil = perfil;
        this.departamento = departamento;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            case "CobrosIndebidos":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "ExoneracionCobros":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "AnulacionTC":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }
                                break;
                            case "FinalizacionTC":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            default:
                                System.out.println("Título equivocado según Bandeja de la tipología !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "MiEquipo":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }
                                break;
                            case "Asignar":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "Resolver":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            default:
                                System.out.println("No se encuentra tipología " + tipologia + " en Supervisor GPYR !!!");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            case "CobrosIndebidos":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            case "ExoneracionCobros":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            case "ConsumosNoReconocidos":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }
                                break;
                            case "Monitorear":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista SEGUROS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            case "MiEquipo":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            case "Asignar":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            case "Resolver":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            case "Monitorear":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                    break;
                                }

                            default:
                                System.out.println("No se encuentra tipología en Supervisor SEGUROS !!!");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "ConsumosNoReconocidos":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "ConsumosMalProcesados":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            case "Monitorear":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "TransaccionesEnProceso":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista CONTROVERSIAS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "MiEquipo":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "Asignar":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "Resolver":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "Monitorear":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "TransaccionesEnProceso":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor CONTROVERSIAS !!!");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "CobrosIndebidos":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "ExoneracionCobros":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "ConsumosMalProcesados":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "ConsumosNoReconocidos":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            case "Retipificar":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista USPR !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "MiEquipo":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }

                                break;
                            case "Asignar":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));

                                }
                                break;
                            case "Resolver":
                                if (getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
                                    getTramitePage.EsCeroTotalTramite();
                                    if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContador.contadorText(departamento, perfil, tipologia)).isEqualTo(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento));
                                }

                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor USPR !!!");
                        }
                        break;
                }
                break;
        }
        getContador.SetContadorANull();
    }

    public static Performable withData(String tipologia, String perfil, String departamento) {
        return instrumented(ValidarContadoresBandeja.class, tipologia, perfil, departamento);
    }

}
