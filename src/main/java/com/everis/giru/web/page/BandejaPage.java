package com.everis.giru.web.page;

import com.everis.giru.web.fact.Shadow;
import com.everis.giru.web.fact.Util;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileFilter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.everis.giru.web.steps.BandejaWebStepDefinition.nroreclamo;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class BandejaPage extends PageObject {

    private String valortipologia = "";
    private Shadow general;
    Util util;

    @FindBy(id = "radio-list__radio-option")
    WebElement listaRadioButton;

    @FindBy(id = "radio-list__radio-currency")
    WebElement listaRadioButtonMonedaMonto;

    @FindBy(linkText = "Aplicar")
    WebElement btnAplicar;

    @FindBy(id = "EXPIRATION_INDICATOR")
    WebElement btnVencimiento;

    @FindBy(id = "PROCEDURE")
    WebElement btnTipoTramite;

    @FindBy(id = "TYPE")
    WebElement btnTipologia;

    @FindBy(id = "REASON")
    WebElement btnMotivo;

    @FindBy(id = "SEGMENT")
    WebElement btnSegmento;

    @FindBy(id = "CLAIM_STATUS_FILTER")
    WebElement btnEstado;

    @FindBy(id = "SERVICE_LEVEL")
    WebElement btnNivelServicio;

    @FindBy(id = "CURRENCY")
    WebElement btnMonedaMonto;

    @FindBy(xpath = "//input[@formcontrolname='amountMin']")
    WebElement txtMontoMinimo;

    @FindBy(xpath = "//input[@formcontrolname='amountMax']")
    WebElement txtMontoMax;

    @FindBy(id = "MORE_FILTERS")
    WebElement btnMasFiltros;

    @FindBy(xpath = "//div[@class='popover-body']//div[1]//ibk-inputgroup-radio[1]")
    WebElement btnRegistroAutonomia;

    @FindBy(xpath = "//div[@class='cdk-overlay-connected-position-bounding-box']//div[2]//ibk-inputgroup-radio[1]")
    WebElement btnConformidadSupervisor;

    @FindBy(xpath = "//div[3]//ibk-inputgroup-radio[1]")
    WebElement btnSolicitarRespuesta;

    @FindBy(id = "search-claim-form__txt-document-number")
    WebElement txtNroTramite;

    @FindBy(xpath = "//ibk-grid-group[@class='hydrated']")
    WebElement lblNumeroTramite;


    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement listCheckAsignar;


    @FindBy(xpath = "//*[@id=\"assign-claim__btn-assign\"]")
    WebElement rootAsignar;


    @FindBy(xpath = "//label[text()='Número de trámite:']/following-sibling::label")
    WebElement lblNumeroTramiteResumen;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement txtNumeroReclamoEspecialista;

    //*[@id="grid"]/giru-specialist-all-procedures/div[3]/ibk-grid-group
    //*[@id="search-claim-form__txt-document-number"]

    @FindBy(xpath = "//*[@id=\"header__giru\"]")
    WebElement nombreArea;


    public void bandejaTramite(String Valor) throws InterruptedException {

     //   WebElement txtnumeroreclamo = general.expandContainer("2css", txtNumeroReclamoEspecialista, "ibk-input", "input", "");

        WebElement txtnumeroreclamo = getDriver().findElement(By.id("search-claim-form__txt-document-number"));
        txtnumeroreclamo.sendKeys(Valor);

       WebElement clickBuscar = getDriver().findElement(By.xpath("//*[@id=\"grid\"]/giru-specialist-all-procedures/div[2]/giru-search-inbox-form/form/div/div[2]/span/img"));
      clickBuscar.click();
       Thread.sleep(4000);

        WebElement clicknumeroreclamo = general.expandContainer("2css", txtNumeroReclamoEspecialista, "ibk-grid", "td:nth-child(2)", "");
        clicknumeroreclamo.click();
        Serenity.takeScreenshot();


    }



    public void bandejaTramite3(String Valor) throws InterruptedException {

        //   WebElement txtnumeroreclamo = general.expandContainer("2css", txtNumeroReclamoEspecialista, "ibk-input", "input", "");

        WebElement txtnumeroreclamo = getDriver().findElement(By.id("search-claim-form__txt-document-number"));
        txtnumeroreclamo.sendKeys(Valor);

        //arreglar por elemento buscar
        WebElement clickBuscar = getDriver().findElement(By.xpath("//*[@id=\"grid\"]/giru-all-procedures/div[2]/giru-search-inbox-form/form/div/div[2]/span/img"));
        clickBuscar.click();
        Thread.sleep(4000);

    }

    public void bandejaTramite4(String Valor) throws InterruptedException {

        //   WebElement txtnumeroreclamo = general.expandContainer("2css", txtNumeroReclamoEspecialista, "ibk-input", "input", "");

        WebElement txtnumeroreclamo = getDriver().findElement(By.id("search-claim-form__txt-document-number"));
        txtnumeroreclamo.sendKeys(Valor);

        //arreglar por elemento buscar
        WebElement clickBuscar = getDriver().findElement(By.xpath("//*[@id=\"grid\"]/giru-to-resolve/div[2]/giru-search-inbox-form/form/div/div[2]/span/img"));
        clickBuscar.click();
        Thread.sleep(4000);

    }
    public void bandejaTramite2(String Valor) throws InterruptedException {

        //   WebElement txtnumeroreclamo = general.expandContainer("2css", txtNumeroReclamoEspecialista, "ibk-input", "input", "");

        WebElement txtnumeroreclamo = getDriver().findElement(By.id("search-claim-form__txt-document-number"));
        txtnumeroreclamo.sendKeys(Valor);

        //arreglar por elemento buscar
        WebElement clickBuscar = getDriver().findElement(By.xpath("//*[@id=\"grid\"]/giru-to-assigned/div[2]/giru-search-inbox-form/form/div/div[2]/span/img"));
        clickBuscar.click();
        Thread.sleep(4000);

    }
    public void seleccionarVencimiento(String vencimiento) {
        if (!vencimiento.isEmpty()) {
            btnVencimiento.click();

            WebElement filtroVencimiento = general.expandContainer("1css", listaRadioButton, vencimiento, "", "");
            filtroVencimiento.click();
            Serenity.takeScreenshot();
            btnAplicar.click();
        }
    }

    public void seleccionarTipoTramite(String tipotramite) {
        if (!tipotramite.isEmpty()) {
            btnTipoTramite.click();

            WebElement filtroTipoTramite = general.expandContainer("1css", listaRadioButton, tipotramite, "", "");
            filtroTipoTramite.click();
            Serenity.takeScreenshot();
            btnAplicar.click();
            //valortipotramite = tipotramite;
        }
    }

    public void seleccionarTipologia(String tipologia) {
        btnTipologia.click();
        WebElement filtroTipologiaCiExo = general.expandContainer("1css", listaRadioButton, "div > div > label", "", "");
        filtroTipologiaCiExo.click();
        Serenity.takeScreenshot();
        btnAplicar.click();
        valortipologia = tipologia;
    }

    public void seleccionarMotivo(String motivo) {
        btnMotivo.click();
        WebElement filtroMotivo = null;
        if (valortipologia.trim().equalsIgnoreCase("CI")) {
            switch (motivo) {
                case "Penalidad por incumplimiento de Pago":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(1) > label", "", "");
                    break;
                case "Comisión de membresía":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(2) > label", "", "");
                    break;
                case "Interés de crédito normal y/o a financiar":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(3) > label", "", "");
                    break;
                case "Interés por Disposición de Efectivo":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(4) > label", "", "");
                    break;
                case "Comisión por uso de canales":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(5) > label", "", "");
                    break;
                case "Comisión por envío físico de EECC":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(6) > label", "", "");
                    break;
                case "Comisión por reposición de plástico":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(7) > label", "", "");
                    break;
                case "Seguro de desgravamen":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(8) > label", "", "");
                    break;
                case "Tipo de Cambio":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(9) > label", "", "");
                    break;
                default:
            }
        } else if (valortipologia.trim().equalsIgnoreCase("EC")) {
            switch (motivo) {
                case "Comisión por envío físico de EECC":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(1) > label", "", "");
                    break;
                case "Comisión de membresía":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(2) > label", "", "");
                    break;
                case "Comisión por reposición de plástico":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(3) > label", "", "");
                    break;
                case "Comisión de retiro ATM otro banco local/extranjero":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(4) > label", "", "");
                    break;
                case "Comisión por uso de canales":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(5) > label", "", "");
                    break;
                case "Interés de crédito normal y/o a financiar":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(6) > label", "", "");
                    break;
                case "Interés por Disposición de Efectivo":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(7) > label", "", "");
                    break;
                case "Penalidad por incumplimiento de Pago":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(8) > label", "", "");
                    break;
                case "Seguro de desgravamen":
                    filtroMotivo = general.expandContainer("1css", listaRadioButton, "div > div:nth-child(9) > label", "", "");
                    break;
                default:
            }
        }
        assert filtroMotivo != null;
        filtroMotivo.click();
        Serenity.takeScreenshot();
        btnAplicar.click();
    }

    public void seleccionarSegmento(String segmento) {
        if (!segmento.isEmpty()) {
            btnSegmento.click();

            WebElement filtroSegmento = general.expandContainer("1css", listaRadioButton, segmento, "", "");
            filtroSegmento.click();
            Serenity.takeScreenshot();
            btnAplicar.click();
        }
    }

    public void seleccionarEstado(String estado) {
        if (!estado.isEmpty()) {
            btnEstado.click();

            WebElement filtroEstado = general.expandContainer("1css", listaRadioButton, estado, "", "");
            filtroEstado.click();
            Serenity.takeScreenshot();
            btnAplicar.click();
        }
    }

    public void seleccionarNivelServicio(String nivelservicio) {
        if (!nivelservicio.isEmpty()) {
            btnNivelServicio.click();

            WebElement filtroNivelServ = general.expandContainer("1css", listaRadioButton, nivelservicio, "", "");
            filtroNivelServ.click();
            Serenity.takeScreenshot();
            btnAplicar.click();
        }
    }

    public void seleccionarMonedaMonto(String moneda) {
        if (!moneda.isEmpty()) {
            btnMonedaMonto.click();
            WebElement rdbTipoMoneda = null;
            switch (util.separarValoresTrama(0, moneda, "-")) {
                case "S":
                    rdbTipoMoneda = general.expandContainer("1css", listaRadioButtonMonedaMonto, "div > div:nth-child(1) > label", "", "");
                    break;
                case "USD":
                    rdbTipoMoneda = general.expandContainer("1css", listaRadioButtonMonedaMonto, "div > div:nth-child(2) > label", "", "");
                    break;
                default:
            }
            assert rdbTipoMoneda != null;
            rdbTipoMoneda.click();
            txtMontoMinimo.sendKeys(util.separarValoresTrama(1, moneda, "-"));
            txtMontoMax.sendKeys(util.separarValoresTrama(2, moneda, "-"));
            Serenity.takeScreenshot();
            btnAplicar.click();
        }
    }

    public void seleccionarRegistroAutonomia(String masfiltros) {
        btnMasFiltros.click();
        WebElement registroAutonomia = null;
        if (util.separarValoresTrama(0, masfiltros, "-").equalsIgnoreCase("Si")) {
            registroAutonomia = general.expandContainer("1css", btnRegistroAutonomia, "div > div:nth-child(2) > label", "", "");
        } else if (util.separarValoresTrama(0, masfiltros, "-").equalsIgnoreCase("No")) {
            registroAutonomia = general.expandContainer("1css", btnRegistroAutonomia, "div > div:nth-child(3) > label", "", "");
        } else if (util.separarValoresTrama(0, masfiltros, "-").equalsIgnoreCase("No Aplica")) {
            registroAutonomia = general.expandContainer("1css", btnRegistroAutonomia, "div > div:nth-child(4) > label", "", "");
        }
        assert registroAutonomia != null;
        registroAutonomia.click();
    }

    public void seleccionarConformidadSupervisor(String masfiltros) {
        WebElement registroConformidadSupervisor = null;
        if (util.separarValoresTrama(1, masfiltros, "-").equalsIgnoreCase("Si")) {
            registroConformidadSupervisor = general.expandContainer("1css", btnConformidadSupervisor, "div > div:nth-child(2) > label", "", "");
        } else if (util.separarValoresTrama(1, masfiltros, "-").equalsIgnoreCase("No")) {
            registroConformidadSupervisor = general.expandContainer("1css", btnConformidadSupervisor, "div > div:nth-child(3) > label", "", "");
        } else if (util.separarValoresTrama(1, masfiltros, "-").equalsIgnoreCase("No Aplica")) {
            registroConformidadSupervisor = general.expandContainer("1css", btnConformidadSupervisor, "div > div:nth-child(4) > label", "", "");
        }
        assert registroConformidadSupervisor != null;
        registroConformidadSupervisor.click();
    }

    public void seleccionarSolicitaRespuesta(String masfiltros) {
        WebElement registroSolicitarRespuesta = null;
        if (util.separarValoresTrama(2, masfiltros, "-").equalsIgnoreCase("Email")) {
            registroSolicitarRespuesta = general.expandContainer("1css", btnSolicitarRespuesta, "div > div:nth-child(2) > label", "", "");
        } else if (util.separarValoresTrama(2, masfiltros, "-").equalsIgnoreCase("Domicilio")) {
            registroSolicitarRespuesta = general.expandContainer("1css", btnSolicitarRespuesta, "div > div:nth-child(3) > label", "", "");
        }
        assert registroSolicitarRespuesta != null;
        registroSolicitarRespuesta.click();
    }

    public void ingresarNroTramite() throws InterruptedException {
        WebElement numeroTramite = general.expandContainer("2css", txtNroTramite, "ibk-input", "input", "");
        numeroTramite.sendKeys(nroreclamo);
        WebElement clicknumeroreclamo = general.expandContainer("2css", txtNroTramite, "ibk-input", "div > span", "");
        clicknumeroreclamo.click();
        Thread.sleep(10000);
    }

    public void verificarTramite() {
        WebElement tramite = general.expandContainer("2css", lblNumeroTramite, "ibk-grid", "td:nth-child(1)", "");
        tramite.click();
        waitForCondition().until(ExpectedConditions.visibilityOf(lblNumeroTramiteResumen));
        Assert.assertEquals("Este es el Tramite", nroreclamo, lblNumeroTramiteResumen.getText());
        Serenity.takeScreenshot();
    }

    public void seleccionarMenuAsignar() {
        getDriver().findElement(By.xpath("//*[@id=\"item-3\"]/div")).click();

    }


    public void seleccionarCheckTramites() {
        WebElement tramite = general.expandContainer("3css", listCheckAsignar, "div > div.ibkTableGroup__table > div.grid-wrapper > ibk-grid", "div > table > tbody > tr > td.index-column > ibk-checkbox", "div > label");
        tramite.click();
    }

    public void seleccionoBotonReasignarTramites() {
        getDriver().findElement(By.xpath("//*[@id=\"grid\"]/giru-to-resolve/div[3]/div/giru-render-buttons/div/div/button[1]")).click();

    }

    public void seleccionoEspecialista() {

        List<WebElement> Nombres = getDriver().findElements(By.xpath("//ibk-button-check[@class='assign-claim__check-specialist hydrated']"));

       Nombres.get(0).click();
    }

    public void seleccionoBotonAsignar() {
        WebElement tramite = general.expandContainer("1css", rootAsignar, "button", "", "");
        tramite.click();
    }

    public void validoNombreYAreaAsesor(String area, String asesorNombre) {
       String nombreAsesorValidar =  Serenity.sessionVariableCalled("nombreAsesor");
       String areaAsesorValidar = Serenity.sessionVariableCalled("nombreArea");


          //  WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

            if (area.toUpperCase().equals(areaAsesorValidar.toUpperCase())) {
                //Bien

            } else {
                Assert.assertEquals(areaAsesorValidar, area);
            }

        String[] nombres = nombreAsesorValidar.split(" ");

        WebElement texto2 = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(1)", "div > p", "");

        for(int i = 0; i<nombres.length;i++) {
            if (texto2.getText().toUpperCase().contains(nombres[i].toUpperCase())) {

            } else {
                Assert.assertEquals(nombres[i], texto2.getText());
            }
        }


    }

    public void seleccionoUsuarioEnSesion() {

       String nombreAsesorValidar = Serenity.sessionVariableCalled("nombreAsesorValidacion");

        String[] partesnombre = nombreAsesorValidar.split(" ");

        List<WebElement> Nombres = getDriver().findElements(By.xpath("//ibk-button-check[@class='assign-claim__check-specialist hydrated']"));

        for(int i = 0;i<Nombres.size();i++)
        {
            WebElement nombre = general.expandContainer("1css", Nombres.get(i), "button > div > div", "", "");
        if(nombre.getText().toLowerCase().contains(partesnombre[0].toLowerCase()))
        {

            nombre.click();
             break;
        }
            if(nombre.getText().toLowerCase().contains(partesnombre[1].toLowerCase()))
            {
                nombre.click();
                break;
            }


        }

    }

    public void seleccionoUsuarioEspecialistaEspecifico(String usuario) throws InterruptedException {


        List<WebElement> Nombres = getDriver().findElements(By.xpath("//ibk-button-check[@class='assign-claim__check-specialist hydrated']"));

        for(int i = 0;i<Nombres.size();i++)
        {
            WebElement nombre = general.expandContainer("1css", Nombres.get(i), "button > div > div", "", "");


            if(nombre.getText().toUpperCase().contains(usuario.toUpperCase()))
            {

                nombre.click();

                break;
            }


        }

    }

    public void validoDescarga(String perfil) throws AWTException, InterruptedException {
        getDriver().findElement(By.xpath("//*[@id=\"grid\"]/giru-all-procedures/div[2]/div/span")).click();


     //   String sPerfil = Serenity.sessionVariableCalled("perfilUsuario");
//"Valido GIRU -SUPERVISOR archivo";


        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();

        String fecha2 = dateFormat.format(date);
        String fechafinal = fecha2.replace("/","_");


        LocalTime now = LocalTime.now().minus(Duration.ofSeconds(1));
        int hour12 = now.getHour();
        int min12 = now.getMinute();
        //   int sec12 = now.getSecond() -1;
        //   String filecreated = "GIRU -"+sPerfil.toUpperCase() +fechafinal+" "+hour12+"_"+min12+"_"+sec12+".xlsx";
        String filecreated = "GIRU -"+perfil.toUpperCase() +fechafinal+" "+hour12+"_"+min12;


        String filePath = "C:\\Users\\XT8363\\Downloads\\";
        File theNewestFile = null;
        File dir = new File(filePath);
        FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
        File[] files = dir.listFiles(fileFilter);

        if (files.length > 0) {
            /** The newest file comes first **/
            Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
            theNewestFile = files[0];
        }

        //GIRU -SUPERVISOR04_05_2021 11_04_11


        String filename = theNewestFile.getName();

        if(filename.contains(filecreated)) {
            //Bien
        }
        else
        {
            Assert.assertEquals(filename,filecreated);
        }

    }
}
