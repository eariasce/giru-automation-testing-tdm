package com.everis.giru.web.page;

import com.everis.giru.web.fact.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

public class CmpPage extends PageObject {

    private Shadow general;

    @FindBy(css = "#claim-detail-tabs__tracking")
    WebElement lblActividadSinArchivo;

    @FindBy(id = "claim-detail-tabs__tracking")
    WebElement lblTrackingSeguimiento;

    @FindBy(id = "claim-detail-tabs__tracking")
    WebElement lblAreaSeguimiento;

    @FindBy(id = "claim-detail-tabs__tracking")
    WebElement lblEspecialistaAsignado;

    public String obtenerActividadSeguimientoSinArchivo() {
        WebElement actividadSeguimientoSinArchivo = general.expandContainer("2css", lblActividadSinArchivo, "#stage_documentacion-pendiente > div.detail-process-container > div > div.body-detail-process > div > div.container-subdetail-process > div.content-subdetail-process.detail_actividad.false > ibk-giru-text-info", "div > div.name-value > div", "");
        actividadSeguimientoSinArchivo.getText();
        return actividadSeguimientoSinArchivo.getText();
    }

    public String obtenerTrackingSeguimiento() {
        WebElement trackingSeguimiento = general.expandContainer("1css", lblTrackingSeguimiento, "span.process-description", "", "");
        return trackingSeguimiento.getText();
    }

    public String obtenerAreaSeguimiento() {
        WebElement areaSeguimiento = general.expandContainer("1css", lblAreaSeguimiento, "div.header-detail-process > div", "", "");
        return areaSeguimiento.getText();
    }

    public void obtenerEspecialista() {
        WebElement especialistaAsignado = general.expandContainer("1css", lblEspecialistaAsignado, "div.title-subdetail-process", "", "");
        especialistaAsignado.getText();
        especialistaAsignado.getText();
        switch (especialistaAsignado.getText()) {
            case "Brenda Malory Caceda Aquino":
                Assert.assertEquals("Brenda Malory Caceda Aquino", especialistaAsignado.getText());
                break;

            case "Elizabeth Consuelo Gallardo Palomino":
                Assert.assertEquals("Elizabeth Consuelo Gallardo Palomino", especialistaAsignado.getText());
                break;
            default:
        }
    }
}
