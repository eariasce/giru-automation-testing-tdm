package com.everis.giru.web.page;

import com.everis.giru.web.fact.Shadow;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

import javax.swing.plaf.basic.BasicViewportUI;

import static com.everis.giru.web.steps.ReclamoCNRWebStepDefinition.*;

public class CnrPage extends PageObject {
    private Shadow general;
    HomePage homePage;

//    private static double montoTotal;
    private double montoTotal=0;

    //private static String productoSeguro;
    private String productoSeguro = "";
    //private static String areaSeguro;
    private String areaSeguro = "";

    private String especialistaasignado = "";

    @FindBy(xpath = "//input[@formcontrolname='totalAmountPEN']")
    WebElement cantidadSoles;

    @FindBy(xpath = "//input[@formcontrolname='totalAmountUSD']")
    WebElement cantidadDolares;

    @FindBy(id = "cardFraudSituation")
    WebElement motivotarjeta;

    @FindBy(xpath = "//textarea[@formcontrolname='internal_comments']")
    WebElement comentarioInterno;

    @FindBy(xpath = "//textarea[@formcontrolname='notLockCardComment']")
    WebElement comentarioBloqueo;

    @FindBy(xpath = "//label[@for='flagLockCard-0']")
    WebElement bloqueoTarjeta;

    @FindBy(xpath = "//label[text()='Producto con seguro:']/following-sibling::label")
    WebElement productoseguro;

    @FindBy(id = "claim-detail-tabs__tracking")
    WebElement areaseguro;


    @FindBy(id = "claim-detail-tabs__tracking")
    WebElement especialistaAsignado;


    public double totalSolesDolares() {
        double tipoCambio = 3.34;
        double cantidadsoles = Double.parseDouble(cantidadSoles.getAttribute("value"));
        double cantidaddolares = Double.parseDouble(cantidadDolares.getAttribute("value")) * tipoCambio;
        montoTotal = Math.max(cantidadsoles, cantidaddolares);
        return montoTotal;
    }

    public void motivoTarjeta(String motivoTarjeta) {
        motivotarjeta.click();
        motivotarjeta.findElement(By.id(motivoTarjeta)).click();
    }

    public void indicacionTarjeta(String indicacionTarjeta) {
        if (indicacionTarjeta.isEmpty()) {
            comentarioBloqueo.sendKeys("Comentario bloqueo");
        } else {
            bloqueoTarjeta.click();
        }
        comentarioInterno.sendKeys("Comentario Interno");
        homePage.ingresarComentarioOpcional("Comentario opcional");
    }

    public void productohistorialResumen() {
        productoSeguro = productoseguro.getText();
    }

    public void areahistorialSeguimiento() {
        WebElement area = general.expandContainer("1css", areaseguro, "div.header-detail-process > div", "", "");
        areaSeguro = area.getText();
    }

    public void comparacionAreaConSinSeguro() {
        if (productoSeguro.equalsIgnoreCase("SI")) {
            Assert.assertEquals("Dpto-seguros", areaSeguro);
        } else {
            Assert.assertEquals("Controversias", areaSeguro);
        }
    }

    public void validacionMotivo() {
        WebElement especialista = general.expandContainer("1css", especialistaAsignado, "div.title-subdetail-process", "", "");
        especialistaasignado = especialista.getText();

        if (tipoMotivo.equalsIgnoreCase("El cliente nunca contrató el producto")) {
            if (productoSeguro.equalsIgnoreCase("SI")) {
                System.out.println("No se asigno a ningun especialista predeterminado");
            }
            if (productoSeguro.equalsIgnoreCase("NO")) {
                Assert.assertEquals("Controversias", areaSeguro);
                Assert.assertEquals("Daniel Chavez Angulo", especialistaasignado);
            }
        }
    }

    public void validacionEspecialistaVirtual() {
        WebElement especialista = general.expandContainer("1css", especialistaAsignado, "div.title-subdetail-process", "", "");
        especialistaasignado = especialista.getText();
        if (productoSeguro.equalsIgnoreCase("SI")) {
            System.out.println("No se asigno a ningun especialista predeterminado");
        }
        if (productoSeguro.equalsIgnoreCase("NO")) {
            Assert.assertEquals("Controversias", areaSeguro);
            if (montoTotal >= 1000) {
                Assert.assertEquals("Ivo Destefano Gastulo Sanchez", especialistaasignado);
            } else if (montoTotal < 1000) {
                Assert.assertEquals("Andrea Soley Garcia Zambrano", especialistaasignado);
            }
        }
    }

    public void validacionEspecialistaPresencial() {
        WebElement especialista = general.expandContainer("1css", especialistaAsignado, "div.title-subdetail-process", "", "");
        especialistaasignado = especialista.getText();
        if (productoSeguro.equalsIgnoreCase("SI")) {
            System.out.println("No se asigno a ningun especialista predeterminado");
        }
        if (productoSeguro.equalsIgnoreCase("NO")) {
            Assert.assertEquals("Controversias", areaSeguro);
            if (montoTotal >= 1000) {
                Assert.assertEquals("Jose Alejandro Ganoza Lopéz", especialistaasignado);
            } else if (montoTotal < 1000) {
                Assert.assertEquals("Allison Amalia Silva Gonzales", especialistaasignado);
            }
        }
    }

    public void seleccionarOpcionSeguro(String opcion) {
    if(opcion.equals("si")) {
        getDriver().findElement(By.xpath("/html/body/app-assi-modal/div/div/giru-valid-insurace/div/div/div/form/div[1]/div/div/div[1]")).click();
    }
    else {
        getDriver().findElement(By.xpath("/html/body/app-assi-modal/div/div/giru-valid-insurace/div/div/div/form/div[1]/div/div/div[2]")).click();
    }
    }

    public void escribeComentario(String comentario) {
        getDriver().findElement(By.xpath("/html/body/app-assi-modal/div/div/giru-valid-insurace/div/div/div/form/div[2]/div/textarea")).sendKeys(comentario);
        getDriver().findElement(By.id("btn-confirm")).click();

        try {


            Thread.sleep(4000);
        }catch (Exception e){
            Assert.assertEquals("error", e);
        }


    }
}

