package com.everis.giru.web.page;

import com.everis.giru.web.fact.Shadow;
import com.everis.giru.web.steps.ValidacionesWebStepDefinition;
//import com.google.inject.internal.cglib.core.$MethodWrapper;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.annotations.findby.By;

import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.net.Inet4Address;
import java.util.List;

public class ValidacionesPage extends PageObject {

    private Shadow general;

    @FindBy(id = "header__giru")
    WebElement lblArea;

    @FindBy(xpath = "//body//div[@class='w-100']//div//div[1]//div[2]")
    WebElement tarjeta1;

    @FindBy(xpath = "//body//div[@class='w-100']//div//div//div[3]")
    WebElement tarjeta2;

    @FindBy(xpath = "//div[@class='text-title']")
    WebElement mensajeCambioTarjeta;

    @FindBy(id = "reasonClaim")
    WebElement btnMotivo;

    @FindBy(id = "Add_Amount_0")
    WebElement btnAgregarMonto;

    @FindBy(id = "claim-form__delete-record-0-1")
    WebElement btnEliminarMonto;

    @FindBy(id = "requestType")
    WebElement cmbTramite;

    @FindBy(xpath = "//div[@class='text']")
    WebElement mensajeCambioTramite;

    @FindBy(xpath = "//button[@class='close']")
    WebElement btnCerrarModal;

    @FindBy(xpath = "//*[@id=\"header__giru\"]")
    WebElement nombreArea;

    @FindBy(xpath = "//*[@id=\"PROCEDURE\"]")
    WebElement btnFiltroTramite;

    @FindBy(xpath = "//*[@id=\"TYPE\"]")
    WebElement btnFiltroTipologia;


    @FindBy(xpath = "//*[@id=\"CURRENCY\"]")
    WebElement btnFiltroMoneda;

    @FindBy(xpath = "//*[@id=\"EXPIRATION_INDICATOR\"]")
    WebElement btnExpDateo;

    @FindBy(xpath = "//*[@id=\"REASON\"]")
    WebElement btnFiltroMotivo;

    @FindBy(xpath = "//input[@formcontrolname='amountMin']")
    WebElement txtDesdeMonto;

@FindBy(xpath = "//input[@formcontrolname='amountMax']")
WebElement txtHastaMonto;

    @FindBy(id = "radio-list__radio-option")
    WebElement listaReclamos;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-to-assigned/div[3]/ibk-grid-group")
WebElement tablaReclamos;
    @FindBy(id = "radio-list__radio-currency")
    WebElement listaMonedas;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement idTabla;
    public String validarArea() {
        waitForCondition().until(ExpectedConditions.visibilityOf(lblArea));
        WebElement area = general.expandContainer("2css", lblArea, "ibk-breadcrum:nth-child(2)", "p", "");
        return area.getText();
    }

    public void seleccionarTarjeta1() {
        tarjeta1.click();
        Serenity.takeScreenshot();
    }

    public void seleccionarTarjeta2() {
        tarjeta2.click();
        Serenity.takeScreenshot();
    }

    public void mensajeAlertaCambioTarjeta(String alerta) {
        try {
            if (mensajeCambioTarjeta.getText().isEmpty()) {
                Assert.fail("Error, el mensaje no aparecio");
            } else {
                Assert.assertEquals("Este es el Mensaje de Alerta Comparado: ", mensajeCambioTarjeta.getText(), alerta);
            }
        } catch (Exception e) {
            System.out.println("Error en el metodo mensajeAlerta" + e.getMessage());
        }
        btnCerrarModal.click();
    }

    public void seleccionarMotivo(String motivo) throws InterruptedException {
        Thread.sleep(4000);
        btnMotivo.click();
        btnMotivo.findElement(By.id(motivo)).click();
    }

    public void agregarMontoyEliminarMonto() {
        WebElement agregarMonto = general.expandContainer("1css", btnAgregarMonto, "p", "", "");
        agregarMonto.click();
        Serenity.takeScreenshot();

        WebElement eliminarMonto = general.expandContainer("1css", btnEliminarMonto, "svg", "", "");
        eliminarMonto.click();
        Serenity.takeScreenshot();
    }

    public void mensajeAlertaEliminarMonto(String alerta) {
        try {
            if (mensajeCambioTarjeta.getText().isEmpty()) {
                Assert.fail("Error, el mensaje no aparecio");
            } else {
                Assert.assertEquals("Este es el Mensaje de Alerta Comparado: ", mensajeCambioTarjeta.getText(), alerta);
            }
        } catch (Exception e) {
            System.out.println("Error en el metodo mensajeAlerta" + e.getMessage());
        }
        btnCerrarModal.click();
    }

    public void seleccionarTramite2() {
        if (ValidacionesWebStepDefinition.valorTramite.equalsIgnoreCase("Reclamo")) {
            cmbTramite.click();
            cmbTramite.findElement(By.id("Pedido")).click();
        } else {
            cmbTramite.click();
            cmbTramite.findElement(By.id("Reclamo")).click();
        }
        Serenity.takeScreenshot();
    }

    public void mensajeAlertaCambioTramite(String alerta) {
        try {
            if (mensajeCambioTramite.getText().isEmpty()) {
                Assert.fail("Error, el mensaje no aparecio");
            } else {
                Assert.assertEquals("Este es el Mensaje de Alerta Comparado: ", mensajeCambioTramite.getText(), alerta);
            }
        } catch (Exception e) {
            System.out.println("Error en el metodo mensajeAlerta" + e.getMessage());
        }
        btnCerrarModal.click();
    }

    public void validoopciones(String perfil) {

        if (perfil.equals("Supervisor")) {
            List<WebElement> franjas = getDriver().findElements(By.xpath("//*[@class='pl-20p pr-20p ng-star-inserted']"));
            //valido cantidad 5 franjas superiores
            if (franjas.size() == 5) {//Tamano correcto
            }
            else { Assert.assertEquals(franjas.size() , 5); }
        }

        if (perfil.equals("Especialista")) {
            List<WebElement> franjas = getDriver().findElements(By.xpath("//*[@class='pl-20p pr-20p']"));
            //valido cantidad 4 franjas superiores
            if (franjas.size() == 4) {//Tamano correcto
               }
            else { Assert.assertEquals(franjas.size() , 4); }
        }


        if (perfil.equals("Asesor")) {
            List<WebElement> franjas = getDriver().findElements(By.xpath("//*[@class='pl-20p pr-20p ng-star-inserted']"));
            //valido cantidad 4 franjas superiores
            if (franjas.size() == 4) {//Tamano correcto
            }
            else { Assert.assertEquals(franjas.size() , 4); }
        }

        if (perfil.equals("Jefe")) {
            List<WebElement> franjas = getDriver().findElements(By.xpath("//*[@class='pl-20p pr-20p ng-star-inserted']"));
            //valido cantidad 5 franjas superiores
            if (franjas.size() == 5) {//Tamano correcto
            }
            else { Assert.assertEquals(franjas.size() , 5); }
        }
        if (perfil.equals("Asesor_R")) {
            List<WebElement> franjas = getDriver().findElements(By.xpath("//*[@class='pl-20p pr-20p ng-star-inserted']"));
            //valido cantidad 3 franjas superiores
            if (franjas.size() == 3) {//Tamano correcto
            }
            else { Assert.assertEquals(franjas.size() , 3); }
        }


    }

    public void validoArea(String area, String perfil) {


        String nombreAsesorValidar = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(1)", "div > p", "").getText();

        Serenity.setSessionVariable("nombreAsesorValidacion").to(nombreAsesorValidar);

        if (area.equals("USPR")) {
                WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

                if (texto.getText().equals("U-SEGTO-PEDIDOS RECL")) {
                    //Bien

                } else {
                    Assert.assertEquals(area, texto.getText());
                }


            }

            if (area.equals("GDPR")) {
                WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

                if (texto.getText().equals("GESTIÓN DE PEDIDOS Y RECLAMOS TC")) {
                    //Bien

                } else {
                    Assert.assertEquals(area, texto.getText());
                }


            }

        if (area.equals("RMTC")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

            if (texto.getText().equals("RETENCIÓN MULTIPRODUCTO TC")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }


        }


            if (area.equals("GDPR")) {
                WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

                if (texto.getText().equals("GESTIÓN DE PEDIDOS Y RECLAMOS TC")) {
                    //Bien

                } else {
                    Assert.assertEquals(area, texto.getText());
                }


            }

        if (area.equals("Seguros")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

            if (texto.getText().equals("DPTO-SEGUROS")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }



        }

        if (area.equals("CONTROVERSIAS")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

            if (texto.getText().equals("CONTROVERSIAS")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }
        }

    }

    public void aplicoFiltroTramite(String tramite) {
       btnFiltroTramite.click();
       general.expandContainer("1css", listaReclamos, "div > div:nth-child("+tramite+") > label", "", "").click();

       getDriver().findElement(By.xpath("//a[@class='link green']")).click();


        try {


            Thread.sleep(8000);
        }catch (Exception e){
            Assert.assertEquals("error", e);
        }



    }

    public void validarValorTabla(String columna, String valor) {

        for(int i =1; i<2; i++) {
            WebElement valorobtenido = general.expandContainer("2css", idTabla, "div > div.ibkTableGroup__table > div.grid-wrapper > ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(" + columna + ")", "");

            if(valorobtenido.getText().equals(valor))
            {
                //bien
            }
            else
            {
                Assert.assertEquals(valorobtenido.getText(), valor);
            }
        }
    }



    public void validarValorTablaMonto(String columna, String valor) {

        for(int i =1; i<2; i++) {
            WebElement valorobtenido = general.expandContainer("2css", idTabla, "div > div.ibkTableGroup__table > div.grid-wrapper > ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(" + columna + ")", "");


                Double numero = Double.valueOf(valorobtenido.getText());
                Double valordouble = Double.valueOf(valor);
            if(numero <= valordouble)
            {
                //bien
            }
            else
            {
                Assert.assertEquals(numero, valordouble);
            }
        }
    }

    public void aplicoFiltroTipologia(String tipologia) {
        btnFiltroTipologia.click();
        general.expandContainer("1css", listaReclamos, "div > div:nth-child("+tipologia+") > label", "", "").click();

        getDriver().findElement(By.xpath("//a[@class='link green']")).click();


        try {


            Thread.sleep(8000);
        }catch (Exception e){
            Assert.assertEquals("error", e);
        }


    }

    public void aplicoFiltroTipoMotivo(String motivo) {

        btnFiltroMotivo.click();
        general.expandContainer("1css", listaReclamos, "div > div:nth-child("+motivo+") > label", "", "").click();

        getDriver().findElement(By.xpath("//a[@class='link green']")).click();


        try {


            Thread.sleep(8000);
        }catch (Exception e){
            Assert.assertEquals("error", e);
        }

    }

    public void aplicoFiltroMonedaMonto(String moneda, String monto) {
        btnFiltroMoneda.click();
        general.expandContainer("1css", listaMonedas, "div > div:nth-child(" + moneda +") > label", "", "").click();
        txtDesdeMonto.sendKeys("1");
        txtHastaMonto.sendKeys(monto);

        getDriver().findElement(By.xpath("//a[@class='link green']")).click();


        try {
            Thread.sleep(8000);
        }catch (Exception e){
            Assert.assertEquals("error", e);
        }
    }

    public void validoAreaSupervisorJefe(String area, String sPerfil) {

        String nombreAsesorValidar = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(1)", "div > p", "").getText();

        Serenity.setSessionVariable("nombreAsesorValidacion").to(nombreAsesorValidar);

        if (area.equals("USPR")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

            if (texto.getText().equals("U-SEGTO-PEDIDOS RECL")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }


        }

        if (area.equals("GDPR")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

            if (texto.getText().equals("GESTIÓN DE PEDIDOS Y RECLAMOS TC")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }


        }

        if (area.equals("RMTC")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

            if (texto.getText().equals("RETENCIÓN MULTIPRODUCTO TC")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }


        }


        if (area.equals("GDPR")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

            if (texto.getText().equals("GESTIÓN DE PEDIDOS Y RECLAMOS TC")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }


        }


        if (area.equals("Seguros")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

            if (texto.getText().equals("DPTO-SEGUROS")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }


        }


    }

    public void seleccionoTipologiaEnMenu(String tipologia) {

        List<WebElement> tipologiasMenu = getDriver().findElements(By.xpath("//div[@class='block-item-description']"));

        tipologiasMenu.get(Integer.parseInt(tipologia)).click();
    }

    public void seleccionaVencimiento(String vencimiento) {

        btnExpDateo.click();
        general.expandContainer("1css", listaReclamos, "div > div:nth-child("+vencimiento+") > label", "", "").click();

        getDriver().findElement(By.xpath("//a[@class='link green']")).click();


        try {


            Thread.sleep(8000);
        }catch (Exception e){
            Assert.assertEquals("error", e);
        }
    }

    public void ordenoTramitesActual() {

        general.expandContainer("3css", tablaReclamos, "div > div.ibkTableGroup__table > div.grid-wrapper > ibk-grid", "div > table > thead > tr > th:nth-child(10) > div > div.arrow-container > div > ibk-icon-svg:nth-child(2)", "i > svg > path").click();

    }
}
