//package com.everis.giru.web.page;
//
//import com.everis.giru.web.fact.Util;
//import net.serenitybdd.core.Serenity;
//import org.openqa.selenium.By;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.WebElement;
//
//import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;
//
//public class Derivacion {
//    public void consultaReclamo(String nroreclamo) throws InterruptedException {
//
//        WebElement txtReclamoSh = new Util().expandContainer(2, "search-claim-form__txt-document-number", "ibk-input", "");
//        txtReclamoSh.findElement(By.cssSelector("input")).sendKeys(nroreclamo, Keys.ENTER);
//        Thread.sleep(10000);
//
//        WebElement btnReclamoSh = new Util().expandContainerV2(2, "ibk-grid-group", "ibk-grid");
//        btnReclamoSh.findElement(By.cssSelector("tbody > tr")).click();
//        Thread.sleep(10000);
//        Serenity.takeScreenshot();
//
//        WebElement btnToogle = getDriver().findElement(By.xpath("//div[@class='toggle-action hide']"));
//        btnToogle.click();
//        Serenity.takeScreenshot();
//    }
//
//    public void evaluar(String evaluacion,/* String Carta, String comentario, String razon,*/String areaderivacion/*,String comentarioderivar*/) throws Exception {
//        /*String IndicadorAniadirSeleccionar = "";*/
//        String EstadoSeguimiento = new Util().separarValoresTrama(0, evaluacion, "-");
//
//        if (EstadoSeguimiento.trim().equalsIgnoreCase("PF") || EstadoSeguimiento.trim().equalsIgnoreCase("NF") || EstadoSeguimiento.trim().equalsIgnoreCase("PV") || EstadoSeguimiento.trim().equalsIgnoreCase("NV")) {
//            WebElement btnProcede = getDriver().findElement(By.id(EstadoSeguimiento));
//            btnProcede.click();
//            //Serenity.takeScreenshot();
//            Thread.sleep(5000);
//            /*IndicadorAniadirSeleccionar = new Util().SepararValoresTrama(0, evaluacion, "-");
//            if (IndicadorAniadirSeleccionar.equalsIgnoreCase("A")) {
//                new HomePage().AniadirRespuesta(new Util().SepararValoresTrama(1, evaluacion, "-"), new Util().SepararValoresTrama(2, evaluacion, "-"));
//
//            } else if (IndicadorAniadirSeleccionar.equalsIgnoreCase("S")) {
//                new HomePage().SeleccionarRespuesta(new Util().SepararValoresTrama(1, evaluacion, "-"), new Util().SepararValoresTrama(2, evaluacion, "-"));
//
//            }
//            WebElement carta = new Util().ExpandContainer(1, "sendLetter", "", "");
//            if (Carta.trim().equalsIgnoreCase("Si")) {
//
//                carta.findElement(By.cssSelector("div > div > div:nth-child(1) > label")).click();
//            } else if (Carta.trim().equalsIgnoreCase("No")) {
//                carta.findElement(By.cssSelector("div > div > div:nth-child(2) > label")).click();
//            }
//
//            WebElement txtComentarioInternoSh = new Util().ExpandContainer(2, "areaComments", "ibk-textarea", "");
//            txtComentarioInternoSh.findElement(By.cssSelector("textarea")).sendKeys(comentario);*/
//
//            if (EstadoSeguimiento.trim().equalsIgnoreCase("NF")){
//                WebElement btncarta = new Util().expandContainer(1,"sendLetter","","");
//                btncarta.findElement(By.cssSelector("div > div > div:nth-child(2) > label")).click();
//                Thread.sleep(2000);
//                Serenity.takeScreenshot();
//            }
//
//
//            if (EstadoSeguimiento.trim().equalsIgnoreCase("PV") || EstadoSeguimiento.trim().equalsIgnoreCase("NV")) {
//                WebElement btnConfirmarProcedeSh = new Util().expandContainer(1, "claim-validation-proceed__btn-confirm", "", "");
//                Serenity.takeScreenshot();
//                btnConfirmarProcedeSh.findElement(By.cssSelector("button")).click();
//
//                Thread.sleep(5000);
//
//            } else {
//                WebElement btnConfirmarSh = new Util().expandContainer(1, "claim-proceed__btn-confirm", "", "");
//
//                WebElement btnupload = getDriver().findElement(By.name("UPLOAD"));
//                Util.scrollToElement(btnupload);
//                Serenity.takeScreenshot();
//
//                btnConfirmarSh.findElement(By.cssSelector("button")).click();
//                Thread.sleep(5000);
//            }
//            Thread.sleep(5000);
//            Serenity.takeScreenshot();
//
//        } else if (EstadoSeguimiento.trim().equalsIgnoreCase("DE")) {
//            WebElement btnDeriva = getDriver().findElement(By.id(EstadoSeguimiento));
//            btnDeriva.click();
//
///*            WebElement cboRazonSh = new Util().ExpandContainer(2, "derivationReason", "div > ibk-select", "");
//            cboRazonSh.findElement(By.cssSelector("div.arrow-down")).click();
//            cboRazonSh.findElement(By.id(razon)).click();
//
//            WebElement txtComentarioAreaSh = new Util().ExpandContainer(2, "areaComments", "ibk-textarea", "");
//            txtComentarioAreaSh.findElement(By.cssSelector("textarea")).sendKeys(comentarioderivar);*/
//            WebElement cboAreaSh = new Util().expandContainer(2, "area", "ibk-select", "");
//            cboAreaSh.findElement(By.cssSelector("div.arrow-down")).click();
//            cboAreaSh.findElement(By.id(areaderivacion)).click();
//            Thread.sleep(2000);
//            Serenity.takeScreenshot();
//
//            WebElement btnConfirmarSh = new Util().expandContainer(1, "claim-derive__btn-confirm", "", "");
//            btnConfirmarSh.findElement(By.cssSelector("button")).click();
//            Thread.sleep(2000);
//            Serenity.takeScreenshot();
//        }
//    }
//}