package com.everis.giru.web.page;

import com.everis.giru.web.fact.Shadow;
import com.everis.giru.web.fact.Util;
import net.serenitybdd.core.Serenity;
import org.openqa.selenium.By;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;

public class ReclamoPedidoCIEXOPage extends PageObject {
    Util util;
    private Shadow general;

    String sNombreMotivo = "";
    String sOrdenMonto = "";
    int iIndicador = 0;
    String sFechaMonto = "";
    String sMontoMoneda = "";
    String sTipoMoneda = "";

    @FindBy(id = "reasonClaim")
    WebElement cmbMotivo;

    public void selectReasonMoney(int IndicadorMotivo, String Motivo, String OrdenMonto, String sMontos, String TipoMoneda, String sFecha) throws Exception {
        WebElement btnMotivo = getDriver().findElement(By.id("reasonClaim"));
        util.scrollToElement(btnMotivo);
        Thread.sleep(5000);
        for (int i = 0; i < IndicadorMotivo; i++) {
            sNombreMotivo = new Util().separarValoresTrama(i, Motivo, "-");
            sOrdenMonto = new Util().separarValoresTrama(i, OrdenMonto, "-");
            selectReason(sNombreMotivo);
            for (int j = 0; j < Integer.parseInt(sOrdenMonto); j++) {
                sFechaMonto = new Util().separarValoresTrama(iIndicador, sFecha, "-");
                sTipoMoneda = new Util().separarValoresTrama(iIndicador, TipoMoneda, "-");
                sMontoMoneda = new Util().separarValoresTrama(iIndicador, sMontos, "-");

                this.enterDateReason(sFechaMonto, i, j);
                this.selectMoneyTypeReason(sTipoMoneda, i, j);
                this.enterRodeReason(sMontoMoneda, i, j);
                if (Integer.parseInt(sOrdenMonto) - 1 > j) {
                    this.addRodeReason(i);
                }
                iIndicador++;
            }
            Serenity.takeScreenshot();
            this.continueReason(i);
        }
    }

    public void selectReason(String sReason) {
        cmbMotivo.click();
        cmbMotivo.findElement(By.id(sReason)).click();
    }

    public void enterDateReason(String sDateReason, int iIndiceX, int iIndiceY) {
        getDriver().findElement(By.id("Fecha_" + iIndiceX + "_" + iIndiceY + "")).sendKeys(sDateReason);
    }

    public void selectMoneyTypeReason(String currency, int iIndiceX, int iIndiceY) {
        WebElement TipoMoneda = getDriver().findElement(By.id("Moneda_" + iIndiceX + "_" + iIndiceY + ""));
        TipoMoneda.click();
        TipoMoneda.findElement(By.id(currency)).click();
    }

    public void enterRodeReason(String sRodeReason, int iIndiceX, int iIndiceY) {
        getDriver().findElement(By.id("Monto_" + iIndiceX + "_" + iIndiceY + "")).sendKeys(sRodeReason);
    }

    public void addRodeReason(int iIndicadorMontos) {
        WebElement agregarmonto = getDriver().findElement(By.id("Add_Amount_" + iIndicadorMontos + ""));
      //  WebElement lnkAddMountReasonSH = general.expandContainer("1css", agregarmonto, "p", "","");
        agregarmonto.click();

    }

    public void continueReason(int iIndicadorBoton) {
/*        WebElement rootPrincipal = getDriver().findElements(By.id("claim-form__footer-continue")).get(iIndicadorBoton);
        WebElement shadowRootPrincipal = general.expandRootElement(rootPrincipal);
        shadowRootPrincipal.findElement(By.cssSelector("button")).click();*/

        getDriver().findElements(By.id("claim-form__footer-continue")).get(iIndicadorBoton).click();


    }
}