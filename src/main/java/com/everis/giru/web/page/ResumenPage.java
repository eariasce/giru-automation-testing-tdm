package com.everis.giru.web.page;

import com.everis.giru.web.fact.Shadow;
import com.everis.giru.web.fact.Util;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.List;


public class ResumenPage extends PageObject
{

    private Shadow general;
    private Util util;

    @FindBy(xpath = "//*[@id=\"claim-detail-tabs__buttons\"]/div/div/div/div[1]")
    WebElement btn;
    @FindBy(id = "derivationReason")
    WebElement cmbRazon;
    @FindBy(id = "area")
    WebElement cmbArea;
    @FindBy(id = "validationComments")
    WebElement cmbComentariosSeguro;
    @FindBy(id="PF")
    WebElement btnProcede;
    @FindBy(xpath = "//*[@id=\"claim-derive__btn-confirm\"]")
    WebElement btnDerivarConfirmar;

    @FindBy(xpath = "//*[@id=\"claim-detail-tabs__buttons\"]/div/div/div/div[2]/div[2]/div[1]/div")
    WebElement btnProcedeSeguros;

    @FindBy(id = "add-email__btn-add")
    WebElement confirmarSeguros;
    @FindBy(xpath = "//*[@id=\"claim-detail-tabs__buttons\"]/div/div/div/div[2]/div[2]/div[2]/div")
    WebElement btnNoProcedeSeguros;
    @FindBy(id="NF")
    WebElement btnNoProcede;

    @FindBy(id="DE")
    WebElement btnDerivar;

    @FindBy(id="PV")
    WebElement btnProcedeDerivacion;
    @FindBy(id="DE")
    WebElement btnDerivacionDerivacion;
    @FindBy(id="NV")
    WebElement btnNoProcedeDerivacion;
    @FindBy(id = "responseType")
    WebElement rdbtnmediorespuesta;


    @FindBy(id = "email")
    WebElement cboMediorespuesta;

    @FindBy(id= "newEmail")
    WebElement txtEmail;


    @FindBy(id= "address")
    WebElement cboMediorespuestaDirec;

    @FindBy(id= "add-email__btn-add")
    WebElement btnAniadirEmail;

    @FindBy(id= "department")
    WebElement cmbDepartamento;

    @FindBy(id= "province")
    WebElement cmbProvincia;

    @FindBy(id= "district")
    WebElement cmbDistrito;

    @FindBy(id= "streetType")
    WebElement cmbTipoVia;

    @FindBy(id= "streetName")
    WebElement txtNombreVia;

    @FindBy(id= "neighborhood")
    WebElement txtUrb;

    @FindBy(id= "streetNumber")
    WebElement txtNumero;

    @FindBy(id= "block")
    WebElement txtManzana;

    @FindBy(id= "lot")
    WebElement txtLote;

    @FindBy(id= "apartment")
    WebElement txtInterior;

    @FindBy(xpath= "/html/body/app-assi-modal/div/div/giru-add-address/div/div/div/form/div[5]/div/textarea")
    WebElement txtReferencia;

    @FindBy(xpath= "//button[@id='add-address__btn-add']")
    WebElement btnAniadirDireccion;

    @FindBy(id = "sendLetter")
    WebElement rdbtncartas;

    @FindBy(id = "template")
    WebElement cboCarta;

    @FindBy( id = "claim-proceed__download")
    WebElement icnDescargaCarta;

    @FindBy(name = "UPLOAD")
    WebElement btnUpload;


    @FindBy(xpath = "//input[@id='files_uploader']")
    WebElement icnAdjuntarArchivo;

    @FindBy(id = "claim-proceed__btn-confirm")
    WebElement btnconfirmar;

    @FindBy(xpath = "//ibk-grid-group[@class='hydrated']")
    WebElement lblNumeroTramite;

    @FindBy(xpath = "//*[@id=\"grid\"]/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement txtNumeroReclamoEspecialista;

    @FindBy(id = "areaComments")
    WebElement txtcomentinterno;

    public void VerificarTramite() {

        WebElement clicknumeroreclamo = general.expandContainer("2css", txtNumeroReclamoEspecialista, "ibk-grid", "td:nth-child(1)", "");
        clicknumeroreclamo.click();
        Serenity.takeScreenshot();
    }
    public void seleccionarbtnOpciones() {

        btn.click();
    }
    public void seleccionarBtnProcede() {

        btnProcede.click();
    }

    public void seleccionarBtnNoProcede() {

        btnNoProcede.click();
    }

    public void seleccionarBtnDerivar() {

        btnDerivacionDerivacion.click();
    }

    public void seleccionarBtnProcedeDer() {

        btnProcedeDerivacion.click();
    }

    public void seleccionarBtnNoProcedeDer() {

        btnNoProcedeDerivacion.click();
    }

    public void seleccionarBtnDerivarDer() {

        btnDerivar.click();
    }
    public void btnconfirmarResolución() {

        WebElement btnregistrar = getDriver().findElement(By.id("add-email__btn-add"));
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", btnregistrar);
        btnregistrar.click();
    }

    public void comentarioOpcionalModal(String Comentariomodal){
       // WebElement comentinternoModal = general.expandContainer("2css", txtcomentinterno, "ibk-textarea", "textarea", "");
        getDriver().findElement(By.xpath("//*[@id=\"areaComments\"]")).sendKeys(Comentariomodal);

//        comentinternoModal.sendKeys(Comentariomodal);
    }

    public void comentarioOpcionalModal2(String Comentariomodal){
        // WebElement comentinternoModal = general.expandContainer("2css", txtcomentinterno, "ibk-textarea", "textarea", "");
        getDriver().findElement(By.xpath("//*[@id=\"validationComments\"]")).sendKeys(Comentariomodal);
//        comentinternoModal.sendKeys(Comentariomodal);
    }

    public void seleccionarMedioRespuesta(String respuesta, String configuracionRespuesta) {
        if (respuesta.trim().equalsIgnoreCase("EMAIL")) {
           //  String Correo = new Util().separarValoresTrama(0, configuracionRespuesta, "-");
            // WebElement medioE = general.expandContainer("1css", rdbtnmediorespuesta, "div > div > div:nth-child(1) > label", "", "");
            // medioE.click();
            WebElement medioE = getDriver().findElement(By.xpath("//*[@id=\"responseType-EMAIL\"]"));
            medioE.click();

            // WebElement select = general.expandContainer("2css", cboMediorespuesta, "div > ibk-select", "#dropdown-select", "");
            // select.click();
            WebElement select1 = getDriver().findElement(By.xpath("//*[@id=\"email\"]"));
            select1.click();

            select1.findElements(By.cssSelector("#list li")).get(Integer.parseInt(configuracionRespuesta)).click();

        } else if (respuesta.trim().equalsIgnoreCase("ADDRESS")) {
            WebElement medioD = general.expandContainer("1css", rdbtnmediorespuesta, "div > div > div:nth-child(2) > label", "", "");
            medioD.click();

            WebElement selectD = general.expandContainer("2css", cboMediorespuestaDirec, "div > ibk-select", "#dropdown-select > div", "");
            selectD.click();
            selectD.findElements(By.cssSelector("#list li")).get(Integer.parseInt(configuracionRespuesta)).click();
        }
        Serenity.takeScreenshot();
    }
    public void aniadirRespuesta(String tipoResolucion, String respuesta, String configuracionRespuesta) throws InterruptedException {
        if (respuesta.trim().equalsIgnoreCase("EMAIL")) {
            // WebElement medioE = general.expandContainer("1css", rdbtnmediorespuesta, "div > div > div:nth-child(1) > label", "", "");
            // medioE.click();

            WebElement medioE = getDriver().findElement(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-proceed/div/div/div[2]/giru-default-proceed/form/giru-customer-letter/form/div[3]/div/div/div/div[1]"));
            medioE.click();

            // WebElement select = general.expandContainer("2css", cboMediorespuesta, "div > ibk-select", "#dropdown-select", "");
            // select.click();

            WebElement select2 = getDriver().findElement(By.xpath("//*[@id=\"email\"]"));
            select2.click();

            // WebElement addEmail = general.expandContainer("2css", cboMediorespuesta, "div > ibk-select", "#CREATE", "");
            ////addEmail.click();

            WebElement addEmail = getDriver().findElement(By.xpath("//*[@id=\"CREATE\"]"));
            addEmail.click();

            String Correo = new Util().separarValoresTrama(0, configuracionRespuesta, "-");
            //WebElement agregarEmail = general.expandContainer("2css", txtEmail, "div > ibk-input", "div", "");
            WebElement correo = getDriver().findElement(org.openqa.selenium.By.xpath("//*[@id=\"newEmail\"]"));
            correo.sendKeys(Correo);
            Serenity.takeScreenshot();


            List<WebElement> btnaniadirEmail = getDriver().findElements(org.openqa.selenium.By.xpath("//*[@id=\"add-email__btn-add\"]"));
            Thread.sleep(2000);
            btnaniadirEmail.get(1).click();

        } else if (respuesta.trim().equalsIgnoreCase("ADDRESS")) {
            //  WebElement medioD = general.expandContainer("1css", rdbtnmediorespuesta, "div > div > div:nth-child(2) > label", "", "");
            //  util.scrollToElement(rdbtnmediorespuesta);
            //  medioD.click();

            //Direccion label
            if(tipoResolucion.equals("P"))
            getDriver().findElement(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-proceed/div/div/div[2]/giru-default-proceed/form/giru-customer-letter/form/div[3]/div/div/div/div[2]/label")).click();
        else
            getDriver().findElement(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-no-proceed/div/div/div[2]/giru-default-no-proceed/form/giru-customer-letter/form/div[3]/div/div/div/div[2]/label")).click();

            //   WebElement selectD = general.expandContainer("2css", cboMediorespuestaDirec, "div > ibk-select", "#dropdown-select", "");
         //   selectD.click();
           WebElement adresses = getDriver().findElement(By.xpath("//*[@id=\"address\"]"));
        adresses.click();

//            WebElement newdireccion = general.expandContainer("2css", cboMediorespuestaDirec, "div > ibk-select", "#CREATE", "");
 //           newdireccion.click();
            adresses.findElement(By.id("CREATE")).click();

            formularioNuevaDireccion(configuracionRespuesta);
        }
        Serenity.takeScreenshot();
    }

    public void formularioNuevaDireccion(String TramaUbigeo) throws InterruptedException {
        //Agrega una nueva Direccion
        //        waitForCondition().until(ExpectedConditions.visibilityOf(lblTitulo));
        String Departamento = util.separarValoresTrama(0, TramaUbigeo, "-");
        cmbDepartamento.click();
        cmbDepartamento.findElement(By.id(Departamento)).click();
        //Thread.sleep(3000);

        String Provincia = util.separarValoresTrama(0, TramaUbigeo, "-") + util.separarValoresTrama(1, TramaUbigeo, "-");
        /*WebElement provincia = general.expandContainer("2css", cmbProvincia, "ibk-select", "#dropdown-select", "");
        provincia.click();
        provincia.findElement(By.id(Provincia)).click();*/
        cmbProvincia.click();
        cmbProvincia.findElement(By.id(Provincia)).click();
        //Thread.sleep(3000);


        String Distrito = util.separarValoresTrama(0, TramaUbigeo, "-") + util.separarValoresTrama(1, TramaUbigeo, "-") + util.separarValoresTrama(2, TramaUbigeo, "-");
        /*WebElement distrito = general.expandContainer("2css", cmbDistrito, "ibk-select", "#dropdown-select", "");
        distrito.click();
        distrito.findElement(By.id(Distrito)).click();*/
        cmbDistrito.click();
        cmbDistrito.findElement(By.id(Distrito)).click();
        //Thread.sleep(3000);


        String TipoVia = util.separarValoresTrama(3, TramaUbigeo, "-");
        /*WebElement tipoVia = general.expandContainer("2css", cmbTipoVia, "ibk-select", "#dropdown-select", "");
        tipoVia.click();
        tipoVia.findElement(By.id(TipoVia)).click();*/
        cmbTipoVia.click();
        cmbTipoVia.findElement(By.id(TipoVia)).click();


        String NombreVia = util.separarValoresTrama(4, TramaUbigeo, "-");
        /*WebElement nombreVia = general.expandContainer("2css", txtNombreVia, "ibk-input", "input", "");
        nombreVia.sendKeys(NombreVia);*/
        txtNombreVia.sendKeys(NombreVia);

        String Urb = util.separarValoresTrama(5, TramaUbigeo, "-");
        /*WebElement urb = general.expandContainer("2css", txtUrb, "ibk-input", "input", "");
        urb.sendKeys(Urb);*/
        txtUrb.sendKeys(Urb);

        String Numero = util.separarValoresTrama(6, TramaUbigeo, "-");
/*        WebElement numero = general.expandContainer("2css", txtNumero, "ibk-input", "input", "");
        numero.sendKeys(Numero);*/
        txtNumero.sendKeys(Numero);

        String Manzana = util.separarValoresTrama(7, TramaUbigeo, "-");
        /*WebElement manzana = general.expandContainer("2css", txtManzana, "ibk-input", "input", "");
        manzana.sendKeys(Manzana);*/
        txtManzana.sendKeys(Manzana);

        String Lote = util.separarValoresTrama(8, TramaUbigeo, "-");
        /*WebElement lote = general.expandContainer("2css", txtLote, "ibk-input", "input", "");
        lote.sendKeys(Lote);*/
        txtLote.sendKeys(Lote);

        String Interior = util.separarValoresTrama(9, TramaUbigeo, "-");
       /* WebElement interior = general.expandContainer("2css", txtInterior, "ibk-input", "input", "");
        interior.sendKeys(Interior);*/
        txtInterior.sendKeys(Interior);

        String Referencia = util.separarValoresTrama(10, TramaUbigeo, "-");
        /*WebElement referencia = general.expandContainer("2css", txtReferencia, "ibk-textarea", "textarea", "");
        referencia.sendKeys(Referencia);*/
        txtReferencia.sendKeys(Referencia);
        Serenity.takeScreenshot();

        //WebElement agregarDireccion = general.expandContainer("1css", btnAniadirDireccion, "button", "", "");
        //btnAniadirDireccion.click();
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", btnAniadirDireccion);
    }

    public void seleccionarMedioCIYEXO (String tipoResolucion, String tipo) throws InterruptedException {
    ///ACTUALIZADO
    //WebElement envioCarta = general.expandContainer("1css", rdbtncartas, "div > div > div:nth-child(1) > label", "", "");
  //  envioCarta.click();
        Thread.sleep(1000);
        if(tipoResolucion.equals("P"))
        getDriver().findElement(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-proceed/div/div/div[2]/giru-default-proceed/form/giru-customer-letter/form/div[1]/div/div/div/div[1]")).click();
            else
        getDriver().findElement(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-no-proceed/div/div/div[2]/giru-default-no-proceed/form/giru-customer-letter/form/div[1]/div/div/div/div[1]/label")).click();
  //  WebElement selectCarta = general.expandContainer("1css", cboCarta, "#dropdown-select", "", "");
   // selectCarta.click();
    WebElement selectCarta = getDriver().findElement(By.xpath("//*[@id=\"template\"]"));
    selectCarta.click();
        selectCarta.findElement(By.id(tipo)).click();
    //String tipo = new Util().separarValoresTrama(0, Tipocarta, "-");

    }

    public void seleccionarMedioEnvioNoCNR () throws InterruptedException {
        WebElement envioCartaNO = general.expandContainer("1css", rdbtncartas, "div > div > div:nth-child(1) > label", "", "");
        envioCartaNO.click();
    }
    public void descargarCarta() throws InterruptedException {
    WebElement iconDescargar = general.expandContainer("1css", icnDescargaCarta, "i > svg", "", "");
    iconDescargar.click();
    Thread.sleep(5000);
    util.scrollToElement(btnUpload);
    }


    public void seleccionarRazonYArea(String Razon, String Area) {
        try {


            Thread.sleep(2000);
        }catch (Exception e){
            Assert.assertEquals("error", e);
        }

        WebElement envioCartaNO = general.expandContainer("2css", cmbRazon, "div > ibk-select", "#dropdown-select", "");
        envioCartaNO.click();

        WebElement cmbArrow =  general.expandContainer("2css", cmbRazon, "div > ibk-select", "#list", "");
     //   cmbArrow.findElements(By.xpath("//*[contains(@id, 'R000')]")).get(0).click();

        cmbArrow.findElements(By.cssSelector("li")).get(Integer.parseInt(Razon)).click();

        WebElement envioCartaNO2 = general.expandContainer("2css", cmbArea, "div > ibk-select", "#dropdown-select", "");
        envioCartaNO2.click();

        try {


            Thread.sleep(1000);
        }catch (Exception e){
            Assert.assertEquals("error", e);
        }

        WebElement cmbArrow2 =  general.expandContainer("2css", cmbArea, "div > ibk-select", "#list", "");
        cmbArrow2.findElements(By.cssSelector("li")).get(Integer.parseInt(Area)).click();

        btnDerivarConfirmar.click();

        try {


            Thread.sleep(12000);
        }catch (Exception e){
            Assert.assertEquals("error", e);
        }



    }

    public void seleccionarBtnNoProcedeSeguros() {
        btnNoProcedeSeguros.click();
    }

    public void seleccionarBtnProcedeSeguros() {

        btnProcedeSeguros.click();
    }

    public void escribirComentario(String comentario) {
        WebElement cmbArrow2 =  general.expandContainer("2css", cmbComentariosSeguro, "div > ibk-textarea", "div > textarea", "");
        cmbArrow2.sendKeys(comentario);
    }

    public void seleccionoConfirmar() {
        confirmarSeguros.click();
    }
}