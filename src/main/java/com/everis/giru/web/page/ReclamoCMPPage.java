package com.everis.giru.web.page;

//import com.everis.giru.web.fact.Shadow;
import com.everis.giru.web.fact.Util;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;

public class ReclamoCMPPage extends PageObject {
    //private Shadow general;
    private String valorPs = "";
    Util util;

    String sNombreMotivo = "";
    String sOrdenMonto = "";
    int iIndicador = 0;
    String sFechaMonto = "";
    String sMontoMoneda = "";
    String sTipoMoneda = "";
    String sComercio = "";
    String sPs = "";
    String sServicios = "";
    String sFechaRespuesta = "";

    @FindBy(id = "reasonClaim")
    WebElement cmbMotivo;

    public void seleccionReclamoCmp(boolean doCmp,int IndicadorMotivo, String Motivo, String OrdenMonto, String Comercio, String TipoMoneda, String sFecha, String sMontos, String ps, String servicios, String fecharespuesta) throws Exception {
        WebElement btnMotivo = getDriver().findElement(org.openqa.selenium.By.id("reasonClaim"));
        util.scrollToElement(btnMotivo);

        Thread.sleep(5000);

        for (int i = 0; i < IndicadorMotivo; i++) {

            sNombreMotivo = new Util().separarValoresTrama(i, Motivo, "-");
            sOrdenMonto = new Util().separarValoresTrama(i, OrdenMonto, "-");
            selectReason(sNombreMotivo);
            this.addRodeReason(i);

            for (int j = 0; j < Integer.parseInt(sOrdenMonto); j++) {
                sTipoMoneda = new Util().separarValoresTrama(iIndicador, TipoMoneda, "-");
                sMontoMoneda = new Util().separarValoresTrama(iIndicador, sMontos, "-");
                sFechaMonto = new Util().separarValoresTrama(iIndicador, sFecha, "-");
                sComercio = new Util().separarValoresTrama(iIndicador, Comercio, "-");
                this.selectMoneyTypeReason(sTipoMoneda, i, j);
                this.enterRodeReason(sMontoMoneda, i, j);
                this.enterDateReason(sFechaMonto, i, j);
                this.addCommerce(sComercio, i, j);
                if (doCmp) {
                    sPs = new Util().separarValoresTrama(iIndicador, ps, "-");
                    sFechaRespuesta = new Util().separarValoresTrama(iIndicador, fecharespuesta, "-");
                    sServicios = new Util().separarValoresTrama(iIndicador, servicios, "-");
                    this.addPs(sPs, i, j);
                    this.addServiciosFecharespuesta(sServicios, sFechaRespuesta, i, j);
                }
                if (Integer.parseInt(sOrdenMonto) - 1 > j) {
                    this.addRodeReason(i);
                }
                iIndicador++;
            }
            Serenity.takeScreenshot();
            this.continueReason(i);
        }
    }

    public void selectReason(String sReason) {
        cmbMotivo.click();
        cmbMotivo.findElement(By.id(sReason)).click();
    }

    public void enterDateReason(String sDateReason, int iIndiceX, int iIndiceY) {
        getDriver().findElement(By.id("Fecha_" + iIndiceX + "_" + iIndiceY + "")).sendKeys(sDateReason);
    }

    public void selectMoneyTypeReason(String currency, int iIndiceX, int iIndiceY) {
        WebElement TipoMoneda = getDriver().findElement(By.id("Moneda_" + iIndiceX + "_" + iIndiceY + ""));
        TipoMoneda.click();
        TipoMoneda.findElement(By.id(currency)).click();
    }

    public void enterRodeReason(String sRodeReason, int iIndiceX, int iIndiceY) {
        getDriver().findElement(By.id("Monto_" + iIndiceX + "_" + iIndiceY + "")).sendKeys(sRodeReason);
    }

    public void addRodeReason(int iIndicadorMontos) {
        WebElement agregarmonto = getDriver().findElement(By.id("Add_Amount_" + iIndicadorMontos + ""));
        ///WebElement lnkAddMountReasonSH = general.expandContainer("1css", agregarmonto, "p", "","");

        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", agregarmonto);
    }

    public void addCommerce(String sCommerce, int iIndiceX, int iIndiceY) {
        getDriver().findElement(By.id("Comercio" + iIndiceX + "_" + iIndiceY + "")).sendKeys(sCommerce);
    }

    public void addPs(String ps, int iIndiceX, int iIndiceY) {
        if (ps.equalsIgnoreCase("P")) {
            WebElement Ps = getDriver().findElement(By.xpath("//label[@for='ps_PS001_" + iIndiceX + "_" + iIndiceY + "']"));
            Ps.click();

        } else if (ps.equalsIgnoreCase("S")) {
            WebElement Ps = getDriver().findElement(By.xpath("//label[@for='ps_PS002_" + iIndiceX + "_" + iIndiceY + "']"));
            Ps.click();
        }
        valorPs = ps;
    }

    public void addServiciosFecharespuesta(String servicios, String FechaRespuesta, int iIndiceX, int iIndiceY) throws InterruptedException {
        if (valorPs.equals("P")) {
            getDriver().findElement(By.id("hopeDate_" + iIndiceX + "_" + iIndiceY + "")).sendKeys(FechaRespuesta);

        } else {
            switch (servicios) {
                case "RH":
                    getDriver().findElement(By.xpath("//label[@for='tp_TSVC001_" + iIndiceX + "_" + iIndiceY + "']")).click();
                    break;

                case "PT":
                    getDriver().findElement(By.xpath("//label[@for='tp_TSVC002_" + iIndiceX + "_" + iIndiceY + "']")).click();
                    break;

                case "CP":
                    getDriver().findElement(By.xpath("//label[@for='tp_TSVC003_" + iIndiceX + "_" + iIndiceY + "']")).click();
                    break;
                default:
            }
            getDriver().findElement(By.id("hopeDate_" + iIndiceX + "_" + iIndiceY + "")).sendKeys(FechaRespuesta);
            Thread.sleep(2000);
        }
    }

    public void continueReason(int iIndicadorBoton) {
/*        WebElement rootPrincipal = (WebElement) getDriver().findElements(By.id("claim-form__footer-continue")).get(iIndicadorBoton);
        WebElement shadowRootPrincipal = general.expandRootElement(rootPrincipal);
        shadowRootPrincipal.findElement(By.cssSelector("button")).click();*/
        getDriver().findElements(org.openqa.selenium.By.id("claim-form__footer-continue")).get(iIndicadorBoton).click();
    }
}