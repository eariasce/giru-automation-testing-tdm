package com.everis.giru.web.page;

import com.everis.giru.web.fact.Shadow;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class HistorialPage extends PageObject {

    private Shadow general;

    @FindBy(id = "search-claim-form__txt-number")
    WebElement txtNumeroReclamoAsesor;

    @FindBy(id = "search-claim-form__txt-document-number")
    WebElement txtNumeroReclamoEspecialistaP;

    @FindBy(xpath = "//body/app-root[1]/giru-layout[1]/div[1]/div[3]/giru-general-history[1]/div[1]/div[2]/giru-history-search-form[1]/form[1]/div[1]/div[2]/ibk-inputgroup-text[1]")
    WebElement txtNumeroReclamoEspecialista;

    @FindBy(xpath = "//ibk-grid-group[@class='hydrated']")
    WebElement btnFecha;

    @FindBy(xpath = "//ibk-grid-group[@class='hydrated']")
    WebElement btnNumeroReclamo;

    @FindBy(xpath = "//ibk-grid-group[@class='hydrated']")
    WebElement btnActividad;

/*    @FindBy(xpath = "(//label[@class='reason-number']/following-sibling::label)[2]")
    WebElement lblTipoTramite;*/

    @FindBy(xpath = "//label[text()='Tipo de trámite:']/following-sibling::label")
    WebElement lblTipoTramite;

    @FindBy(xpath = "//label[text()='Tipología:']/following-sibling::label")
    WebElement lblTipologia;

/*    @FindBy(xpath = "(//label[@class='reason-number']/following-sibling::label)[3]")
    WebElement lblNrotarjeta;*/

    @FindBy(xpath = "//label[text()='Número de tarjeta:']/following-sibling::label")
    WebElement lblNrotarjeta;

    @FindBy(id = "claim-detail-tabs__tab-detail")
    WebElement btnSeguimiento;


    @FindBy(id = "claim-detail-tabs__tracking")
    WebElement lblActividad;

    @FindBy(id = "claim-detail-tabs__tracking")
    WebElement lblArchivo;

    @FindBy(xpath = " //*[@id=\"menus\"]/div[2]") // ELEMENTO  HISTORIAL DE PERFIL ASESOR
            WebElement menuhistorialAse;

    @FindBy(xpath = "/html/body/app-root/giru-layout/div/div[3]/giru-general-history/div/div[2]/giru-history-search-form/form/div/div[2]/ibk-inputgroup-text")
    WebElement txtnumeroreclamo;
    @FindBy(xpath = "//body/app-root[1]/giru-layout[1]/div[1]/div[3]/giru-client-history[1]/div[1]/div[1]/div[1]/form[1]/span[1]/img[1]")
    WebElement btnBuscarReclamo;
    @FindBy(xpath = "//*[@id=\"grid\"]/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement tablaReclamos;
    @FindBy(xpath = "//*[@id=\"claim-detail-tabs__tracking\"]")
    WebElement tablaReclamos2;


    public void menuHistorialAsesor() throws InterruptedException {
        menuhistorialAse.click();
        Serenity.takeScreenshot();
        Thread.sleep(3000);
    }

    public void historialAsesor(String Valor) throws InterruptedException {
        //  WebElement txtnumeroreclamo = general.expandContainer("2css", txtNumeroReclamoAsesor, "ibk-input", "input", "");
        txtnumeroreclamo.sendKeys(Valor);
        Serenity.takeScreenshot();
        Thread.sleep(8000);

        //btnBuscarReclamo.click();
        // Thread.sleep(8000);

    }

    public void clickFechaRegistroASesor() throws InterruptedException {
        WebElement fecha = general.expandContainer("3css", btnFecha, "ibk-grid", "ibk-icon-svg:nth-child(2)", "i > svg");
        fecha.click();
        Thread.sleep(12000);
        Serenity.takeScreenshot();
    }

    public void seleccionarNroReclamoHistorialAsesor() {
        //waitForCondition().until(ExpectedConditions.presenceOfElementLocated((By) btnNumeroReclamo));
        WebElement btnnumeroReclamo = general.expandContainer("2css", btnNumeroReclamo, "ibk-grid", "tr:nth-child(1) > td:nth-child(2)", "");
        waitFor(btnnumeroReclamo);
        btnnumeroReclamo.click();
        Serenity.takeScreenshot();
    }

    public String obtenerActividadHistorialAsesor() {
        WebElement actividadHistorialAsesor = general.expandContainer("2css", btnActividad, "ibk-grid", "td:nth-child(7)", "");
        Serenity.takeScreenshot();
        return actividadHistorialAsesor.getText();
    }
    public void historial2(String Valor) throws InterruptedException {
        // WebElement txtnumeroreclamo = general.expandContainer("2css", txtNumeroReclamoEspecialista, "ibk-input", "input", "");
        Thread.sleep(2000);
        txtnumeroreclamo.sendKeys(Valor);

    }
    public void historial(String Valor) throws InterruptedException {
        WebElement txtnumeroreclamo = general.expandContainer("2css", txtNumeroReclamoEspecialista, "div > ibk-input", "div > input", "");
        Thread.sleep(2000);
        txtnumeroreclamo.sendKeys(Valor);

        WebElement clickBuscar = general.expandContainer("2css", txtNumeroReclamoEspecialista, "div > ibk-input", "div > span", "");
       // WebElement clickBuscar = getDriver().findElement(By.xpath("/html/body/app-root/giru-layout/div/div[3]/giru-general-history/div/div[2]/giru-search-inbox-form/form/div/div[2]/span/img"));
        clickBuscar.click();
        Thread.sleep(7000);
        Serenity.takeScreenshot();

//        WebElement btnnumeroReclamo = general.expandContainer("2css", btnNumeroReclamo, "ibk-grid", "td:nth-child(1)", "");
        //       ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", btnnumeroReclamo);

    }

    public void seleccionarNroReclamoHistorialEspecialista() {
        WebElement btnnumeroReclamo = general.expandContainer("2css", btnNumeroReclamo, "ibk-grid", "td:nth-child(1)", "");
        btnnumeroReclamo.click();
    }

    public String obtenerActividadHistorialEspecialista() {
        WebElement actividadHistorialEspecialista = general.expandContainer("2css", btnActividad, "ibk-grid", "td:nth-child(7)", "");
        waitForCondition().until(ExpectedConditions.visibilityOf(actividadHistorialEspecialista));
        Serenity.takeScreenshot();
        return actividadHistorialEspecialista.getText();
    }

    public String obtenerTipoTramiteHistorial() {
        waitForCondition().until(ExpectedConditions.visibilityOf(lblTipoTramite));
        return lblTipoTramite.getText();
    }

    public String obtenerTipologiaHistorial() {
        return lblTipologia.getText();
    }

    public String obtenerNroTarjetaHistorial() {
        return lblNrotarjeta.getText();
    }

    public void seleccionarSeguimientoHistorial() {
        WebElement btnseguimiento = general.expandContainer("1css", btnSeguimiento, "ibk-tab:nth-child(2)", "", "");
        Serenity.takeScreenshot();
        btnseguimiento.click();
    }

    public String obtenerActividadSeguimientoHistorial() {
        WebElement actividadSeguimientoHistorial = general.expandContainer("2css", lblActividad, "#stage_en-proceso > div.detail-process-container > div > div.body-detail-process > div > div.container-subdetail-process > div.content-subdetail-process.detail_actividad.false > ibk-giru-text-info", "div > div.name-value > div", "");
        return actividadSeguimientoHistorial.getText();
    }

    public String obtenerArchivoSeguimientoHistorial() {
        WebElement actividadArchivoSeguimientoHistorial = general.expandContainer("2css", lblArchivo, "ibk-giru-link-icon", "div > div.link > a", "");
        return actividadArchivoSeguimientoHistorial.getText();
    }

    public String obtenerActividadSeguimientoHistorialResol() {
        WebElement actividadSeguimientoHistorial = general.expandContainer("2css", lblActividad, "#stage_procede > div.detail-process-container > div > div.body-detail-process > div > div.container-subdetail-process > div.content-subdetail-process.detail_actividad.false > ibk-giru-text-info", "div > div.name-value > div", "");
        return actividadSeguimientoHistorial.getText();
    }

    public String obtenerActividadSeguimientoHistorialResolNoProcede() {
        WebElement actividadSeguimientoHistorial = general.expandContainer("2css", lblActividad, "#stage_no-procede > div.detail-process-container > div > div.body-detail-process > div > div.container-subdetail-process > div.content-subdetail-process.detail_actividad.false > ibk-giru-text-info", "div > div.name-value > div", "");
        return actividadSeguimientoHistorial.getText();
    }
    public void seleccionarBuscar() {
        WebElement clickBuscar = getDriver().findElement(By.xpath("/html/body/app-root/giru-layout/div/div[3]/giru-general-history/div/div[2]/giru-search-inbox-form/form/div/div[2]/span/img"));
        clickBuscar.click();
        //Thread.sleep(11000);
    }

    public void seleccionarDescargarExcel() {
        WebElement clickDesgargarExcel = getDriver().findElement(By.xpath("/html/body/app-root/giru-layout/div/div[3]/giru-general-history/div/div[3]/div/giru-render-buttons/div/div/button/div"));
        clickDesgargarExcel.click();
        Serenity.takeScreenshot();
    }

    public String obtenerPrimerTramite() {

        WebElement primerTramite = general.expandContainer("2css", tablaReclamos, "div > div.ibkTableGroup__table > div.grid-wrapper > ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(1)", "");

        String tramite = primerTramite.getText();
        String substr =   tramite.substring(tramite.length() - 5);
        return substr;


    }

    public String obtenerAreaTramiteActual() {
        String area = general.expandContainer("1css", tablaReclamos2, "#stage_en-proceso > div.detail-process-container > div > div.header-detail-process > div", "", "").getText();

            return area;
    }

    public String obtenerNombreAsesorActual() {
        WebElement nombres = general.expandContainer("1css", tablaReclamos2, "#stage_en-proceso > div.detail-process-container > div > div.body-detail-process", "", "");

        //general.expandContainer("1className", nombres, "title-subdetail-process", "", "");

        String nombre = nombres.findElements(By.xpath("//div[@class='title-subdetail-process']")).get(0).getText();
        return nombre;
    }
}
