package com.everis.giru.web.page;

import com.everis.giru.web.fact.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.NoSuchElementException;
//@DefaultUrl("http://52.251.49.64/#/login")
//@DefaultUrl("http://20.110.10.236//#/login")
@DefaultUrl("http://20.114.188.69/#/login")
public class LoginPage extends PageObject {
    private Shadow general;

    @FindBy(xpath = "//ibk-inputgroup-text[@id='username']")
    WebElement txtUsuario;

    @FindBy(xpath = "//ibk-inputgroup-text[@id='password']")
    WebElement txtPassword;

    @FindBy(xpath = "/html/body/app-root/giru-login/giru-login-view/div/div[2]/div/div[1]/giru-login-form/form/div/div[2]/div/ibk-button")
    WebElement btnIniciarSesion;

    @FindBy(tagName = "h2")
    WebElement perfilAsesor;

    @FindBy(className = "title")
    WebElement perfilEspecialista;

    @FindBy(className = "breadcrumb__text")
    WebElement perfilSupervisor;

    @FindBy(tagName = "p")
    WebElement mensaje;
    public void ingresarUsuario(String usuario) {
      //  WebElement ingresarUsuario = general.expandContainer("2css", txtUsuario, "ibk-input", "input[id=Usuario]", "");
      //   ingresarUsuario.sendKeys(usuario);
        WebElement txtnumeroreclamo = getDriver().findElement(By.id("username"));
        txtnumeroreclamo.sendKeys(usuario);
    }

    public void ingresarPassword(String password) {
        try {
            //WebElement ingresarPassword = general.expandContainer("2css", txtPassword, "ibk-input", "input[id=Contraseña]", "");
            //ingresarPassword.sendKeys(password);
           WebElement txtnumeroreclamo = getDriver().findElement(By.id("password"));
           txtnumeroreclamo.sendKeys(password);
        } catch (Exception e) {
            System.out.println("Error en ingresarPassword"+ e.getMessage());
        }
        // Serenity.takeScreenshot();
    }

    public void clickIniciarSesion() {
        WebElement bntIniciarSesion = general.expandContainer("1css", btnIniciarSesion, "button", "", "");
     //getDriver().findElement(By.xpath("/html/body/app-root/giru-login/giru-login-view/div/div[2]/div/div[1]/giru-login-form/form/div/div[2]/div/button")).click();
     //  getDriver().findElement(By.xpath("//*[@id=\"submit\"]")).click();

     bntIniciarSesion.click();


    }

    public void verificarLoginPerfilAsesor() {
        try {
            if (perfilAsesor.isDisplayed()) {
                String mensaje = perfilAsesor.getText();
                Assert.assertEquals("Ahora, identifiquemos a nuestro cliente", mensaje);
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    public void verificarLoginPerfilEspecialista() {
        try {
            if (perfilEspecialista.isDisplayed()) {
                String mensaje = perfilEspecialista.getText();
                Assert.assertEquals("Mis trámites", mensaje);
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    public void verificarLoginPerfilSupervisor() {
        try {
            if (perfilSupervisor.isDisplayed()) {
                String mensaje = perfilSupervisor.getText();
                Assert.assertEquals("ADMINISTRADOR", mensaje);
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    public void verificarMensajeError(String mensajeError) {
        try {
            if (mensaje.isDisplayed()) {
                String mensajeE = mensaje.getText();
                Assert.assertEquals(mensajeError, mensajeE);
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }
}
