package com.everis.giru.web.page;

import com.everis.giru.web.fact.Shadow;
import com.everis.giru.web.fact.Util;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.annotations.findby.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.Locale;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class HomePage extends PageObject {

    private String valoractual = "";
    private Shadow general;
    private Util util;
    private String lblCliente;

    @FindBy(id = "search-form__select-document-type")
    WebElement tipoDocumento;

    @FindBy(id = "search-form__txt-document-number")
    WebElement numeroDocumento;

    @FindBy(id = "search-form__btn-validate")
     WebElement btnValidar;

    @FindBy(id = "main__client-info")
    WebElement lblNombreCliente;

    @FindBy(xpath = "//*[@id=\"generic-error__btn-understood\"]")
    WebElement btnCampana;

    @FindBy(xpath = "//div[contains(@class,'title_name o-light')]//span[1]")
    WebElement lblNombreClienteModal;

    @FindBy(id = "requestType")
    WebElement cmbTramite;

    @FindBy(xpath = "//*[@id=\"typology\"]")
    WebElement cmbTipologia;

    @FindBy(xpath = "//textarea[@formcontrolname='internal_comments']")
    WebElement txtComentarioTramite;

    @FindBy(xpath = "//textarea[@formcontrolname='comments']")
    WebElement txtComentarioOpcional;


    @FindBy(id = "telephone")
    WebElement cmbTelefono;

    @FindBy(id = "phone-number-form__phone-number")
    WebElement txtTelefono;

    @FindBy(id = "phone-number__btn-add")
    WebElement btnAniadirTelefono;

    @FindBy(id = "email")
    WebElement cmbEmail;

    @FindBy(id = "newEmail")
    WebElement txtEmail;

    @FindBy(id = "add-email__btn-add")
    WebElement btnAniadirEmail;

    @FindBy(id = "address")
    WebElement cmbAddress;

    @FindBy(id = "department")
    WebElement cmbDepartamento;

    @FindBy(id = "province")
    WebElement cmbProvincia;

    @FindBy(id = "district")
    WebElement cmbDistrito;

    @FindBy(id = "streetType")
    WebElement cmbTipoVia;

    @FindBy(id = "streetName")
    WebElement txtNombreVia;

    @FindBy(id = "neighborhood")
    WebElement txtUrb;

    @FindBy(id = "streetNumber")
    WebElement txtNumero;

    @FindBy(id = "block")
    WebElement txtManzana;

    @FindBy(id = "lot")
    WebElement txtLote;

    @FindBy(id = "apartment")
    WebElement txtInterior;

    @FindBy(xpath = "//textarea[@formcontrolname='landmark']")
    WebElement txtReferencia;

    @FindBy(id = "add-address__btn-add")
    WebElement btnAniadirDireccion;

    @FindBy(name = "UPLOAD")
    WebElement btnUpload;

    @FindBy(linkText = "Actualizate")
    WebElement btnActualizate;

    @FindBy(id = "claim-form__btn-submit")
    WebElement btnRegistrar;

    @FindBy(id = "end-atention")
    WebElement btnEntendidoRegistrar;

    @FindBy(xpath = "//span[@class='parrafo h-bold']")
    WebElement lblNroReclamoTipologiaProcede;

    @FindBy(xpath = "//span[@class='parrafo h-bold']")
    WebElement lblNroReclamoTipologiaProcedeAuto;

    @FindBy(xpath = "//div[@class='text-title']")
    WebElement lblTitulo;

    @FindBy(xpath = "//span[@class='parrafo h-regular']")
    WebElement textoModal;

    @FindBy(xpath = "//span[@class='span-result']")
    WebElement lblNroReclamoTipologiaNueva;

    @FindBy(className = "primary")
    WebElement btnTerminarAtencion;

    @FindBy(id = "header__giru")
    WebElement btnPerfil;

    @FindBy(id = "header__giru")
    WebElement btnSalir;

    @FindBy(xpath = "//*[@id=\"menus\"]/div[3]/a")
    WebElement btnAtencion;

    @FindBy(xpath = "//*[@id=\"search-form__select-document-type\"]/div[1]")
    WebElement tipodocumento;

    @FindBy(id = "DNI")
    WebElement selectDNI;

    @FindBy(id = "CE")
    WebElement selectCE;

    @FindBy(xpath = "//*[@id=\"search-form__txt-document-number\"]")
    WebElement numerodocumento;

    @FindBy(id = "search-form__btn-validate")
    WebElement validardocumentoCliente;

    @FindBy(xpath = "/html/body/app-root/giru-layout/div/div[3]/giru-customer-support/giru-procedure/div/section[1]/div[2]/div[1]/label")
    WebElement nombreCliente;


    public void seleccionaMenuSuperior() {
          util.menuSuperior("Atencion al Cliente");
        // btnAtencion.click();
    }
    public void seleccionaMenuEspecialista(){
        btnAtencion.click();
    }

    public void seleccionarTipoNumeroDocumento(String tipoDoc, String numeroDoc) {
       // WebElement tipodocumento = general.expandContainer("2css", tipoDocumento, "ibk-select", "#dropdown-select", "");
        tipodocumento.click();
    //    tipodocumento.findElement(By.id(tipoDoc)).click();
        if(tipoDoc.equals("DNI"))
        selectDNI.click();
        else
            selectCE.click();
        //WebElement numerodocumento = general.expandContainer("2css", numeroDocumento, "ibk-input", "input", "");
        numerodocumento.sendKeys(numeroDoc);
        Serenity.takeScreenshot();

       // WebElement validardocumento = general.expandContainer("1css", btnValidar, "button", "", "");
        validardocumentoCliente.click();
    }

    public void obtenerNombreCliente() {
        //WebElement nombreCliente = general.expandContainer("2css", lblNombreCliente, "div.info-personal > ibk-h", "p", "");

        lblCliente = nombreCliente.getText().toUpperCase(new Locale("es", "Peru"));
    }

    public void seleccionarTarjeta(String numeroTarjeta) {
        WebElement btntarjeta = getDriver().findElement(By.id("credit-cards__cards-" + numeroTarjeta));
        btntarjeta.click();
/*        WebElement Tarjeta = general.expandContainer("1css", btntarjeta, "div > div", "", "");
        Tarjeta.findElement(By.id(numeroTarjeta.substring(numeroTarjeta.length() - 4))).click();*/
//        btntarjeta.findElement(By.id(numeroTarjeta.substring(numeroTarjeta.length() - 4))).click();
        Serenity.takeScreenshot();
    }

    public void seleccionarTramiteyTipologia(String tramite, String tipologia) {
        //WebElement errorcampana = general.expandContainer("1css", btnCampana, "button", "", "");

     //   errorcampana.click();

        waitForCondition().until(ExpectedConditions.elementToBeClickable(cmbTramite));

        cmbTramite.click();

        switch (tramite) {
            case "Reclamo":
                cmbTramite.findElement(By.id("TTR0001")).click();
                break;
            case "Pedido":
                cmbTramite.findElement(By.id("TTR0002")).click();
                break;
            case "Solicitud":
                cmbTramite.findElement(By.id("TTR0003")).click();
                break;
            default:
        }

        //cmbTramite.findElement(By.id(tramite)).click();
        waitForCondition().until(ExpectedConditions.elementToBeClickable(cmbTipologia)).click();

        //cmbTipologia.click();
        cmbTipologia.findElement(By.id(tipologia)).click();
        Serenity.takeScreenshot();

        /*WebElement btnMotivo = getDriver().findElement(By.id("reasonClaim"));
        util.scrollToElement(btnMotivo);*/
    }

    public void seleccionarCantidadMotivos(String razonOperativa, String tiporazon) {
        if (razonOperativa.equals("Si")) {
            getDriver().findElement(By.xpath("//label[@for='flag-operative-1']")).click();
            getDriver().findElement(By.id("operativeReason")).click();
            getDriver().findElement(By.id(tiporazon)).click();
        } else {
            getDriver().findElement(By.xpath("//label[@for='flag-operative-0']")).click();
        }
        Serenity.takeScreenshot();
    }

    public void ingresarComentarioCliente(String comentarioCliente) {
        txtComentarioTramite.sendKeys(comentarioCliente);
    }

    public void ingresarComentarioOpcional(String comentarioOpcional) {
        txtComentarioOpcional.sendKeys(comentarioOpcional);
    }

    public String obtenerTextoArchivo() {
        return getDriver().findElement(By.xpath("//span[@class='upload-items__item-description']")).getText();
    }

    public void seleccionarCelular(String numeroTelefono, String operador) {
        if (util.separarValoresTrama(0, numeroTelefono, "-").equalsIgnoreCase("S")) {
            cmbTelefono.click();
            cmbTelefono.findElements(By.cssSelector("#list li")).get(0).click();
            Serenity.takeScreenshot();

        } else if (util.separarValoresTrama(0, numeroTelefono, "-").equalsIgnoreCase("A")) {
            cmbTelefono.click();
            cmbTelefono.findElement(By.xpath("//*[@id=\"CREATE\"]/span")).click();

            /*WebElement agregarTelefono = general.expandContainer("2css", txtTelefono, "ibk-input", "input", "");
            agregarTelefono.sendKeys(util.separarValoresTrama(1, numeroTelefono, "-"));*/

            txtTelefono.sendKeys(util.separarValoresTrama(1, numeroTelefono, "-"));
            Serenity.takeScreenshot();

/*            WebElement btnaniadirTelefono = general.expandContainer("1css", btnAniadirTelefono, "button", "", "");
            btnaniadirTelefono.click();*/
            btnAniadirTelefono.click();
        }
        WebElement Operador = getDriver().findElement(By.id("tab-" + operador));
        Operador.click();
        Serenity.takeScreenshot();
    }

    public void seleccionarRespuesta(String respuesta, String configuracionRespuesta) {
        if (respuesta.trim().equalsIgnoreCase("EMAIL")) {
            String Correo = new Util().separarValoresTrama(0, configuracionRespuesta, "-");
            getDriver().findElement(By.id("responseType-" + respuesta)).click();
            cmbEmail.click();
            cmbEmail.findElement(By.id(Correo)).click();

        } else if (respuesta.trim().equalsIgnoreCase("ADDRESS")) {
            getDriver().findElement(By.id("responseType-" + respuesta)).click();
            cmbAddress.click();
            cmbAddress.findElements(By.cssSelector("#list li")).get(Integer.parseInt(configuracionRespuesta)).click();
        }
        Serenity.takeScreenshot();
    }

    public void aniadirRespuesta(String respuesta, String configuracionRespuesta) throws InterruptedException {
        if (respuesta.trim().equalsIgnoreCase("EMAIL")) {
            String Correo = new Util().separarValoresTrama(0, configuracionRespuesta, "-");
            getDriver().findElement(By.id("responseType-" + respuesta)).click();
            cmbEmail.click();
            cmbEmail.findElement(By.xpath("(//li[@id='CREATE'])[2]")).click();

/*            WebElement agregarEmail = general.expandContainer("2css", txtEmail, "ibk-input", "input", "");
            agregarEmail.sendKeys(Correo);
            Serenity.takeScreenshot();*/
            txtEmail.sendKeys(Correo);
            Serenity.takeScreenshot();

/*            WebElement btnaniadirEmail = general.expandContainer("1css", btnAniadirEmail, "button", "", "");
            btnaniadirEmail.click();*/
            btnAniadirEmail.click();

        } else if (respuesta.trim().equalsIgnoreCase("ADDRESS")) {
            getDriver().findElement(By.id("responseType-" + respuesta)).click();

            cmbAddress.click();
            cmbAddress.findElement(By.xpath("//div[@id='address']//li[@id='CREATE']")).click();
            formularioNuevaDireccion(configuracionRespuesta);
        }
        Serenity.takeScreenshot();
    }

    public void formularioNuevaDireccion(String TramaUbigeo) {
        //Agrega una nueva Direccion
        //        waitForCondition().until(ExpectedConditions.visibilityOf(lblTitulo));
        String Departamento = util.separarValoresTrama(0, TramaUbigeo, "-");
        cmbDepartamento.click();
        cmbDepartamento.findElement(By.id(Departamento)).click();
        //Thread.sleep(3000);

        String Provincia = util.separarValoresTrama(0, TramaUbigeo, "-") + util.separarValoresTrama(1, TramaUbigeo, "-");
        /*WebElement provincia = general.expandContainer("2css", cmbProvincia, "ibk-select", "#dropdown-select", "");
        provincia.click();
        provincia.findElement(By.id(Provincia)).click();*/
        cmbProvincia.click();
        cmbProvincia.findElement(By.id(Provincia)).click();
        //Thread.sleep(3000);


        String Distrito = util.separarValoresTrama(0, TramaUbigeo, "-") + util.separarValoresTrama(1, TramaUbigeo, "-") + util.separarValoresTrama(2, TramaUbigeo, "-");
        /*WebElement distrito = general.expandContainer("2css", cmbDistrito, "ibk-select", "#dropdown-select", "");
        distrito.click();
        distrito.findElement(By.id(Distrito)).click();*/
        cmbDistrito.click();
        cmbDistrito.findElement(By.id(Distrito)).click();
        //Thread.sleep(3000);


        String TipoVia = util.separarValoresTrama(3, TramaUbigeo, "-");
        /*WebElement tipoVia = general.expandContainer("2css", cmbTipoVia, "ibk-select", "#dropdown-select", "");
        tipoVia.click();
        tipoVia.findElement(By.id(TipoVia)).click();*/
        cmbTipoVia.click();
        cmbTipoVia.findElement(By.id(TipoVia)).click();


        String NombreVia = util.separarValoresTrama(4, TramaUbigeo, "-");
        /*WebElement nombreVia = general.expandContainer("2css", txtNombreVia, "ibk-input", "input", "");
        nombreVia.sendKeys(NombreVia);*/
        txtNombreVia.sendKeys(NombreVia);

        String Urb = util.separarValoresTrama(5, TramaUbigeo, "-");
        /*WebElement urb = general.expandContainer("2css", txtUrb, "ibk-input", "input", "");
        urb.sendKeys(Urb);*/
        txtUrb.sendKeys(Urb);

        String Numero = util.separarValoresTrama(6, TramaUbigeo, "-");
/*        WebElement numero = general.expandContainer("2css", txtNumero, "ibk-input", "input", "");
        numero.sendKeys(Numero);*/
        txtNumero.sendKeys(Numero);

        String Manzana = util.separarValoresTrama(7, TramaUbigeo, "-");
        /*WebElement manzana = general.expandContainer("2css", txtManzana, "ibk-input", "input", "");
        manzana.sendKeys(Manzana);*/
        txtManzana.sendKeys(Manzana);

        String Lote = util.separarValoresTrama(8, TramaUbigeo, "-");
        /*WebElement lote = general.expandContainer("2css", txtLote, "ibk-input", "input", "");
        lote.sendKeys(Lote);*/
        txtLote.sendKeys(Lote);

        String Interior = util.separarValoresTrama(9, TramaUbigeo, "-");
       /* WebElement interior = general.expandContainer("2css", txtInterior, "ibk-input", "input", "");
        interior.sendKeys(Interior);*/
        txtInterior.sendKeys(Interior);

        String Referencia = util.separarValoresTrama(10, TramaUbigeo, "-");
        /*WebElement referencia = general.expandContainer("2css", txtReferencia, "ibk-textarea", "textarea", "");
        referencia.sendKeys(Referencia);*/
        txtReferencia.sendKeys(Referencia);
        Serenity.takeScreenshot();

        //WebElement agregarDireccion = general.expandContainer("1css", btnAniadirDireccion, "button", "", "");
        //btnAniadirDireccion.click();
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", btnAniadirDireccion);
    }


    public void btnActualizate() throws InterruptedException {
        util.scrollToElement(btnUpload);
        try {
            if (btnActualizate.isDisplayed()) {
                btnActualizate.click();
                Thread.sleep(16000);
                ArrayList<String> tabs = new ArrayList<>(getDriver().getWindowHandles());
                Serenity.takeScreenshot();
                getDriver().switchTo().window(tabs.get(0));
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

/*    public String clickRegistrar() {
        WebElement registar = general.expandContainer("1css", btnRegistrar, "button", "", "");
        registar.click();

        if (lblCliente != null && lblCliente.contains(lblNombreClienteModal.getText())) {
            assertTrue("El cliente es el mismo", true);
        } else {
            fail("Los clientes son diferentes");
        }

        waitForCondition().until(ExpectedConditions.visibilityOf(lblTitulo));
        Serenity.takeScreenshot();
        if (textoModal.getText().equalsIgnoreCase("el abono se realizará dentro de 48 horas.")) {
            valoractual = lblNroReclamoTipologiaProcedeAuto.getText();

        } else if (textoModal.getText().equalsIgnoreCase("Se le ha enviado el HR vía email del cliente.")) {
            valoractual = lblNroReclamoTipologiaProcede.getText();
        }
        btnEntendidoRegistrar.click();
        return valoractual;
    }*/


    public void clickRegistrar() {
        btnRegistrar.click();
        waitForCondition().until(ExpectedConditions.visibilityOf(lblNombreClienteModal));
        if (lblCliente != null && lblCliente.contains(lblNombreClienteModal.getText().toUpperCase(new Locale("es", "Perú")))) {
            assertTrue("El cliente es el mismo", true);
        } else {
            fail("Los clientes son diferentes");
        }
        Serenity.takeScreenshot();
    }

    public String guardarNroReclamo() {
        valoractual = lblNroReclamoTipologiaProcedeAuto.getText();
        btnEntendidoRegistrar.click();
        return valoractual;
    }

/*    public String clickRegistarTipologiaNueva() {
        WebElement registar = general.expandContainer("1css", btnRegistrar, "button", "", "");
        registar.click();

        if (lblCliente != null && lblCliente.contains(lblNombreClienteModal.getText())) {
            assertTrue("El cliente es el mismo", true);
        } else {
            fail("Los clientes son diferentes");
        }

        waitForCondition().until(ExpectedConditions.visibilityOf(lblNroReclamoTipologiaNueva));
        Serenity.takeScreenshot();
        valoractual = lblNroReclamoTipologiaNueva.getText();
        btnTerminarAtencion.click();
        return valoractual;
    }*/

    public void clickPerfil() {
        WebElement cerrarSesion = general.expandContainer("2css", btnPerfil, "ibk-menu", "ibk-h", "");
        cerrarSesion.click();
        Serenity.takeScreenshot();
    }

    public void salir() {
        WebElement salir = general.expandContainer("2css", btnSalir, "ibk-menu", "ibk-link", "");
        salir.click();
    }
}
