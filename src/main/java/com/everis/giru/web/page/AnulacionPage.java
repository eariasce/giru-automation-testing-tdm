package com.everis.giru.web.page;

import com.everis.giru.web.fact.Shadow;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static com.everis.giru.web.steps.SolicitudAnulacionStepDefinition.nroTramite;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class AnulacionPage extends PageObject {
    private Shadow general;
    private final String textoBaner = "SE BLOQUEARÁ CUENTA\nCliente no tiene deudas, saldo acreedor ni movimientos en proceso.";

    @FindBy(xpath = "//div[contains(@class,'flex flex-column')]")
    WebElement lblBaner;

    @FindBy(xpath = "//label[text()=' TITULAR ']/following-sibling::div")
    WebElement btnTarjeta;

    @FindBy(xpath = "//ibk-grid-group[@class='hydrated']")
    WebElement lblActividadAsesor;

    @FindBy(id = "claim-detail-tabs__tracking")
    WebElement lblActividadSeguimiento;

    @FindBy(id = "search-claim-form__txt-document-number")
    WebElement txtNroTramite;

    @FindBy(xpath = "//ibk-grid-group[@class='hydrated']")
    WebElement checkTramite;

    @FindBy(xpath = "(//button[@class='resolve-button ng-star-inserted'])[1]")
    WebElement btnAsignar;

    @FindBy(id = "1096")
    WebElement lblAsesor;

    @FindBy(id = "assign-claim__btn-assign")
    WebElement btnConfirmarAsignar;

    @FindBy(id = "toaster-item__body")
    WebElement alertaAsignacion;

    @FindBy(xpath = "//ibk-grid-group[@class='hydrated']")
    WebElement seleccionarTramite;

    @FindBy(xpath = "//div[@class='toggle-action hide']")
    WebElement btnToggle;

    @FindBy(className = "derive-body__description")
    WebElement lblClienteNoRetenido;


    @FindBy(xpath = "//div[@class='selected-list']//div[1]")
    WebElement dropdownModalDerivacion1;

    @FindBy(id = "options")
    WebElement dropdownModalOpciones;

    @FindBy(id = "areaComments")
    WebElement txtComentarioInterno;


    @FindBy(xpath = "//p[@class='h-regular fs-14']")
    WebElement lblClienteRetenido;


    @FindBy(xpath = "//div[@class='selected-list']")
    WebElement dropdownModalNF;


    @FindBy(xpath = "//textarea[@formcontrolname='internalComment']")
    WebElement txtComentarioNF;


    @FindBy(id = "add-email__btn-add")
    WebElement btnConfirmar;

    public void clickTarjetaTitular() {
        waitForCondition().until(ExpectedConditions.elementToBeClickable(btnTarjeta));
        btnTarjeta.click();
    }

    public void obtenerTextoBaner() {
        waitForCondition().until(ExpectedConditions.visibilityOf(lblBaner));
        lblBaner.getText();
        Assert.assertEquals(textoBaner, lblBaner.getText());
    }

    public String obtenerTextoActividadHistorial() {
        WebElement actividadHistorialAsesor = general.expandContainer("2css", lblActividadAsesor, "ibk-grid", "td:nth-child(7)", "");
        return actividadHistorialAsesor.getText();
        //Assert.assertEquals("Retención", actividadHistorialAsesor);
    }

    public void compararTextoActividadSeguimiento() {
        WebElement actividadSeguimientoAsesor = general.expandContainer("2css", lblActividadSeguimiento, "div.content-subdetail-process.detail_actividad.false > ibk-giru-text-info", "div > div.name-value > div", "");
        Assert.assertEquals(obtenerTextoActividadHistorial(), actividadSeguimientoAsesor.getText());

        getDriver().findElement(By.cssSelector("body > app-assi-modal > div > div > giru-claim-detail-tabs")).click();
    }

    public void ingresarNroTramiteyBuscar(String nroReclamo) throws InterruptedException {
        WebElement numeroTramite = general.expandContainer("2css", txtNroTramite, "ibk-input", "input", "");
        numeroTramite.sendKeys(nroReclamo);
        Thread.sleep(11000);
        WebElement clicknumeroreclamo = general.expandContainer("2css", txtNroTramite, "ibk-input", "div > span", "");
        clicknumeroreclamo.click();
    }

    public void seleccionarTramiteparaAsignar() {
        WebElement checktramite = general.expandContainer("2css", checkTramite, "ibk-grid", "div > table > tbody > tr > td.index-column > ibk-checkbox", "");
        checktramite.click();
    }

    public void clickAsignar() {
        waitForCondition().until(ExpectedConditions.elementToBeClickable(btnAsignar));
        Serenity.takeScreenshot();
        btnAsignar.click();
    }

    public void seleccionarAsesor() {
        waitForCondition().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class,'col-xs-3 mb-1')]")));
        lblAsesor.click();
        Serenity.takeScreenshot();
        WebElement btnAsignar = general.expandContainer("1css", btnConfirmarAsignar, "button", "", "");
        btnAsignar.click();
    }

    public void verificacion() {
        waitForCondition().until(ExpectedConditions.visibilityOf(alertaAsignacion));
        if (alertaAsignacion.isDisplayed()) {
            assertTrue("La alerta se mostró con éxito", true);
            Serenity.takeScreenshot();
        } else {
            fail("No se puedo asignar con éxito el trámite");
        }
    }

    public void clickTramite() throws InterruptedException {
        Thread.sleep(10000);
        WebElement tramite = general.expandContainer("2css", seleccionarTramite, "ibk-grid", "td:nth-child(5)", "");
        Serenity.takeScreenshot();
        try {
            if (tramite.getText().equalsIgnoreCase(String.format("%015d", Integer.parseInt(nroTramite)))) {
                tramite.click();
                Serenity.takeScreenshot();
            }
        } catch (Exception e) {
            System.out.println("Los tramites son diferentes " + e.getMessage());
        }
    }

    public void evaluacion(String evaluar, String lista, String motivo, String comentario) throws InterruptedException {
        waitForCondition().until(ExpectedConditions.elementToBeClickable(btnToggle)).click();
        Serenity.takeScreenshot();
        getDriver().findElement(By.id(evaluar)).click();

        switch (evaluar) {
            case "DE":
                waitForCondition().until(ExpectedConditions.visibilityOf(lblClienteNoRetenido));
                Assert.assertEquals("La cuenta se anulará definitivamente en las próximas 24 horas hábiles", lblClienteNoRetenido.getText());
                dropdownModalDerivacion1.click();

                dropdownModal(lista);
                dropdownModalDerivacion1.click();
                WebElement dropdown = general.expandContainer("1css", dropdownModalOpciones, "#dropdown-select", "", "");
                dropdown.click();
                dropdown.findElement(By.id(motivo)).click();

                WebElement txtComentario = general.expandContainer("2css", txtComentarioInterno, "ibk-textarea", "textarea", "");
                txtComentario.sendKeys(comentario);
                Serenity.takeScreenshot();
                break;

            case "NF":
                waitForCondition().until(ExpectedConditions.visibilityOf(lblClienteRetenido));
                Assert.assertEquals("La cuenta se liberará para que el cliente pueda volver a realizar sus consumos.", lblClienteRetenido.getText());
                dropdownModalNF.click();
                dropdownModal(lista);
                dropdownModalNF.click();
                txtComentarioNF.sendKeys(comentario);
                break;
            default:

        }
        btnConfirmar.click();
        Thread.sleep(5000);
    }

    public void dropdownModal(String lista) {
        String[] digitos = lista.split("(?<=.)");
        for (int i = 0; i < digitos.length; i++) {
            getDriver().findElement(By.cssSelector("body app-assi-modal li:nth-child(" + digitos[i] + ")")).click();
        }
        Serenity.takeScreenshot();
    }
}




