package com.everis.giru.web.entidades;

public class Data {

    private String tipoTramite;
    private String tipolgia;
    private String nroTarjeta;
    private String area;
    private String tipoDocumento;
    private String nroDocumento;
    private String tipoPerfil;
    private String tipoMotivo;
    private String cantidadMotivos;
    private String respuesta;

    private String nroReclamoBd;
    private String appCodeBd;
    private String tipoRespuestaBd;

    public String getTipoRespuestaBd() {
        return tipoRespuestaBd;
    }

    public void setTipoRespuestaBd(String tipoRespuestaBd) {
        this.tipoRespuestaBd = tipoRespuestaBd;
    }

    public String getAppCodeBd() {
        return appCodeBd;
    }

    public void setAppCodeBd(String appCodeBd) {
        this.appCodeBd = appCodeBd;
    }

    public String getNroReclamoBd() {
        return nroReclamoBd;
    }

    public void setNroReclamoBd(String nroReclamoBd) {
        this.nroReclamoBd = nroReclamoBd;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getCantidadMotivos() {
        return cantidadMotivos;
    }

    public void setCantidadMotivos(String cantidadMotivos) {
        this.cantidadMotivos = cantidadMotivos;
    }

    public String getTipoMotivo() {
        return tipoMotivo;
    }

    public void setTipoMotivo(String tipoMotivo) {
        this.tipoMotivo = tipoMotivo;
    }

    public String getTipoPerfil() {
        return tipoPerfil;
    }

    public void setTipoPerfil(String tipoPerfil) {
        this.tipoPerfil = tipoPerfil;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getNroTarjeta() {
        return nroTarjeta;
    }

    public void setNroTarjeta(String nroTarjeta) {
        this.nroTarjeta = nroTarjeta;
    }

    public String getTipolgia() {
        return tipolgia;
    }

    public void setTipolgia(String tipolgia) {
        this.tipolgia = tipolgia;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }
}