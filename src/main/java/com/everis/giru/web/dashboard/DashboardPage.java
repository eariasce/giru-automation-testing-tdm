package com.everis.giru.web.dashboard;

import org.openqa.selenium.By;

public class DashboardPage {

    //Menu Lateral
    public static final By TODOS_LOS_TRAMITES = By.cssSelector("div");
    public static final By POR_ASIGNAR = By.cssSelector("div");
    public static final By POR_RESOLVER = By.cssSelector("div");
    public static final By MI_EQUIPO = By.cssSelector("div");
    public static final By COBROS_INDEBIDOS = By.cssSelector("div>div");
    public static final By EXONERACION_COBROS = By.cssSelector("div>div");
}