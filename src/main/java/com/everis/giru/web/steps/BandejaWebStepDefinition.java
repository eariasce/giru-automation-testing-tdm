package com.everis.giru.web.steps;

import com.everis.giru.web.fact.Util;
import com.everis.giru.web.page.BandejaPage;
import com.everis.giru.web.page.LoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;

import java.io.IOException;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class BandejaWebStepDefinition {
    LoginPage loginPage;
    BandejaPage bandejaPage;
    Util util;

    public static String nroreclamo = "";
    private static final String ruta = "src/test/resources/ReclamosGenerados/Reclamo_CIEXO.txt";

    @Given("^se ingresara a la web de Giru para verificar los datos de un registro con filtros$")
    public void seIngresaraALaWebDeGiruParaVerificarLosDatosDeUnRegistroConFiltros() {
        loginPage.open();
        loginPage.ingresarUsuario("XT9656");
        loginPage.ingresarPassword("Callao2121++");
        loginPage.clickIniciarSesion();
    }

    @And("^accedemos a tramites y elegiremos el (.*) y (.*)$")
    public void accedemosATramitesYElegiremosElVencimientoYTipoTrámite(String vencimiento, String tipotramite) {
        util.menuSuperior("Tramites");
        bandejaPage.seleccionarVencimiento(util.obtenerFiltroVencimiento(vencimiento));
        bandejaPage.seleccionarTipoTramite(util.obtenerFiltroTipoTramite(tipotramite));
    }

    @And("^despues de haber seleccionado el tipo de tramite se habilitara la opcion (.*) respectiva y (.*)$")
    public void despuesDeHaberSeleccionadoElTipoDeTramiteSeHabilitaraLaOpcionTipologiaRespectivaYMotivos(String tipologia, String motivo) {
        if (!tipologia.isEmpty()) {
            bandejaPage.seleccionarTipologia(tipologia);
        }
        if (!motivo.isEmpty()) {
            bandejaPage.seleccionarMotivo(motivo);
        }
    }

    @And("^elegiremos el (.*), (.*), (.*), (.*)$")
    public void elegiremosElSegmentoEstadoNivelServicioMonedaMonto(String segmento, String estado, String nivelservicio, String moneda) {
        bandejaPage.seleccionarSegmento(util.obtenerFiltroSegmento(segmento));
        bandejaPage.seleccionarEstado(util.obtenerFiltroEstado(estado));
        bandejaPage.seleccionarNivelServicio(util.obtenerFiltroNivelServicio(nivelservicio));
        bandejaPage.seleccionarMonedaMonto(moneda);
    }

    @And("^terminaremos eligiendo (.*)$")
    public void terminaremosEligiendoMásFiltros(String masfiltros) {
        if (!util.separarValoresTrama(0, masfiltros, "-").equalsIgnoreCase("NA")) {
            bandejaPage.seleccionarRegistroAutonomia(masfiltros);
        } else getDriver().findElement(By.id("MORE_FILTERS")).click();
        if (!util.separarValoresTrama(1, masfiltros, "-").equalsIgnoreCase("NA")) {
            bandejaPage.seleccionarConformidadSupervisor(masfiltros);
        }
        if (!util.separarValoresTrama(2, masfiltros, "-").equalsIgnoreCase("NA")) {
            bandejaPage.seleccionarSolicitaRespuesta(masfiltros);
        }
        Serenity.takeScreenshot();
        getDriver().findElement(By.linkText("Aplicar")).click();
    }

    @And("^ingresaremos el numero de reclamo para verificar los filtros$")
    public void ingresaremosElNumeroDeReclamoParaVerificarLosFiltros() throws IOException, InterruptedException {
        nroreclamo = util.separarValoresTrama(0, util.muestraContenido(ruta), "-");
        bandejaPage.ingresarNroTramite();
        bandejaPage.verificarTramite();
    }

    @Then("^eliminamos el registro usado y reescribimos el registro a utilizar$")
    public void eliminamosElRegistroUsadoYReescribimosElRegistroAUtilizar() throws IOException {
        util.almacenarEliminarReclamos(ruta, "", 1);
    }
}