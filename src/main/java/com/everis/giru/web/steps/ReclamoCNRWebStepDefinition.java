package com.everis.giru.web.steps;

import com.everis.giru.rest.steps.ConsultaFilterInboxSteps;
import com.everis.giru.web.entidades.Data;
import com.everis.giru.web.fact.SubirArchivo;
import com.everis.giru.web.fact.Util;
import com.everis.giru.web.page.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import net.serenitybdd.core.Serenity;
import org.junit.Assert;

import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

public class ReclamoCNRWebStepDefinition {
    LoginPage loginPage;
    HomePage homePage;
    HistorialPage historial;
    ReclamoCMPPage cmp;
    CnrPage cnrpage;
    Util util;
    String snumerotramite ="";
    Data data = new Data();
    BandejaPage bandeja;
    String nroAsesorReasignado = "";
    ConsultaFilterInboxSteps consultaFilterInboxSteps;
    private String archivoHome;
    public static String tipoMotivo = "";
    private static final String ruta = "src/test/resources/ReclamosGenerados/Reclamo_CNR.txt";

    @Given("^se accedera al sistema con su (.*) y (.*) con un (.*) determinado$")
    public void seAccederaAlSistemaConSuUsuarioYContraConUnPerfilDeterminado(String usuario, String password, String perfil) {
        loginPage.open();
        loginPage.ingresarUsuario(usuario);
        loginPage.ingresarPassword(password);
        loginPage.clickIniciarSesion();
        data.setTipoPerfil(perfil);
    }

    @When("^realiza la consulta por (.*) y (.*) de un cliente$")
    public void realizaLaConsultaPorTipodocumentoYNumerodocumentoDeUnCliente(String tipodocumento, String numerodocumento) {
        if (data.getTipoPerfil().equalsIgnoreCase("Especialista") || data.getTipoPerfil().equalsIgnoreCase("Supervisor") || data.getTipoPerfil().equalsIgnoreCase("Jefe")) {
            homePage.seleccionaMenuSuperior();
        }
        homePage.seleccionarTipoNumeroDocumento(tipodocumento, numerodocumento);
        data.setTipoDocumento(tipodocumento);
        data.setNroDocumento(numerodocumento);
    }

    @And("^selecciona el (.*), (.*) y (.*)$")
    public void seleccionaElNumerotarjetaTramiteYTipologia(String numeroTarjeta, String tramite, String tipologia) {
        homePage.obtenerNombreCliente();
        homePage.seleccionarTarjeta(numeroTarjeta);
        homePage.seleccionarTramiteyTipologia(tramite, tipologia);
        data.setNroTarjeta(numeroTarjeta);
        data.setTipoTramite(tramite);
        data.setTipolgia(tipologia);
    }

    @And("^elegimos la (.*) de (.*) y configura el tipo de moneda (.*), monto (.*), fecha (.*) y nombre de comercio (.*) segun la (.*)$")
    public void elegimosLaCantidadDeMotivosYConfiguraElTipoDeMonedaTipomonedaMontoMontoFechaFechaYNombreDeComercioNombrecomSegunLaConfiguracion(
            String cantidad, String motivos, String fecha, String tipomoneda, String monto, String nombrecom, String configuracion) throws Exception {
        cmp.seleccionReclamoCmp(false, Integer.parseInt(cantidad), motivos, configuracion, nombrecom, fecha, monto, tipomoneda, "", "", "");
        data.setTipoMotivo(motivos);
        data.setCantidadMotivos(cantidad);
    }

    @And("^escogemos el motivo (.*) de lo sucedido con la tarjeta$")
    public void escogemosElMotivoMotivosucedidoDeLoSucedidoConLaTarjeta(String tipomotivo) {
        cnrpage.totalSolesDolares();
        cnrpage.motivoTarjeta(util.obtenerMotivoTarjeta(tipomotivo));
        tipoMotivo = tipomotivo;
    }

    @And("^se (.*) si el cliente presenta morosidad o ha bloqueado su tarjeta$")
    public void seIndicaraSiElClientePresentaMorosidadOHaBloqueadoSuTarjeta(String indicacion) {
        cnrpage.indicacionTarjeta(indicacion);
    }

    @And("^se agregara un archivo (.*) en caso sea necesario$")
    public void seAgregaraUnArchivoArchivoEnCasoSeaNecesario(String archivo) throws AWTException, InterruptedException {
        if (archivo.equals("Si")) {
            new SubirArchivo().subirArchivo();
            archivoHome = homePage.obtenerTextoArchivo();
        } else if (archivo.equals("No")) {
            System.out.println("No se agregaron archivos");
        }
    }

    @When("^ingresa en bandeja supervisor el (.*) para seleccionar$")
    public void ingresaEnLaBusquedaElTramiteParaSeleccionarloBandejaSupervisor(String nrotramite) throws InterruptedException {

        util.menuSuperior("Tramites");
        Thread.sleep(3000);

        bandeja.seleccionarMenuAsignar();

        if(snumerotramite.equals("")) {
            snumerotramite = nrotramite;
        }
        Thread.sleep(12000);

        bandeja.bandejaTramite4(snumerotramite);
        // Resumen.VerificarTramite();
    }


    @When("^ingresa en bandeja supervisor el (.*) para validar")
    public void ingresaEnLaBusquedaElTramiteParaSeleccionarloBandejaSupervisorValidar(String nrotramite) throws InterruptedException {

        util.menuSuperior("Tramites");
        Thread.sleep(3000);

     //   bandeja.seleccionarMenuAsignar();

        if(snumerotramite.equals("")) {
            snumerotramite = nrotramite;
        }
        Thread.sleep(12000);

        bandeja.bandejaTramite3(snumerotramite);
        // Resumen.VerificarTramite();
    }



    @And("^se selecciona un usuario con los tramites para realizar la derivacion$")
    public void seSeleccionaTramitesDerivacionUsuario() {
        bandeja.seleccionarCheckTramites();
        bandeja.seleccionoBotonReasignarTramites();
        bandeja.seleccionoUsuarioEnSesion();
        bandeja.seleccionoBotonAsignar();
    }

    @And("^se selecciona un (.*) en especifico con los tramites para realizar la derivacion$")
    public void seSeleccionaTramitesDerivacionUsuarioEspecifico(String usuario) throws InterruptedException {
        bandeja.seleccionarCheckTramites();
        bandeja.seleccionoBotonReasignarTramites();
        bandeja.seleccionoUsuarioEspecialistaEspecifico(usuario);
        bandeja.seleccionoBotonAsignar();
    }
    @And("^se selecciona los tramites para realizar la derivacion$")
    public void seSeleccionaTramitesDerivacion() {
            bandeja.seleccionarCheckTramites();
            bandeja.seleccionoBotonReasignarTramites();
            bandeja.seleccionoEspecialista();
            bandeja.seleccionoBotonAsignar();
    }

    @And("^valido descarga del reclamo del (.*)$")

    public void validoDescarga(String perfil) throws InterruptedException, AWTException {
        bandeja.validoDescarga(perfil);
     }

    @And("^valido nombre y area del asesor$")
    public void validoNombreAreaAsesor() {
        String area = historial.obtenerAreaTramiteActual();
      //  String asesorNombre = historial.obtenerNombreAsesorActual();
        bandeja.validoNombreYAreaAsesor(area,"asesorNombre");
    }

    @When("^ingresa en bandeja la busqueda el (.*) para seleccionar$")
    public void ingresaEnLaBusquedaElTramiteParaSeleccionarloBandeja(String nrotramite) throws InterruptedException {

        // util.menuSuperior("Tramites");
        Thread.sleep(4000);

        if(snumerotramite.equals("")) {
            snumerotramite = nrotramite;
        }

        bandeja.bandejaTramite(snumerotramite);
        // Resumen.VerificarTramite();
    }


    @When("^ingresa en cnr la busqueda el (.*) para seleccionar$")
    public void ingresaEnLaBusquedaElTramiteParaSeleccionarlo(String nrotramite) throws InterruptedException {

        // util.menuSuperior("Tramites");
        Thread.sleep(4000);

        if(snumerotramite.equals("")) {
            snumerotramite = nrotramite;
        }

        bandeja.bandejaTramite(snumerotramite);
        // Resumen.VerificarTramite();
    }

    @And("^se valida el usuario reasignado por servicios en cnr$")
    public void seValidaElUsuarioReasignadoPorServicios() {
        consultaFilterInboxSteps = new ConsultaFilterInboxSteps();
        consultaFilterInboxSteps.seArmaElCuerpoDePeticionCreacionCliente(snumerotramite);
        consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticion();
        consultaFilterInboxSteps.validoLaRespuestaObtenida();
        nroAsesorReasignado = consultaFilterInboxSteps.validoCamposObtenidos();
    }

    @And("^se valida el usuario reasignado por servicios en bandeja$")
    public void seValidaElUsuarioReasignadoPorServiciosBandeja() {
        consultaFilterInboxSteps = new ConsultaFilterInboxSteps();
        consultaFilterInboxSteps.seArmaElCuerpoDePeticionCreacionCliente(snumerotramite);
        consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticion();
        consultaFilterInboxSteps.validoLaRespuestaObtenida();
        nroAsesorReasignado = consultaFilterInboxSteps.validoCamposObtenidos();

        Serenity.setSessionVariable("nrotramite").to(snumerotramite);
        Serenity.setSessionVariable("nroAsesorReasignado").to(nroAsesorReasignado);
    }

    @And("^se valida el usuario reasignado por servicios en cnr seguros$")
    public void seValidaElUsuarioReasignadoPorServicios2() {
        consultaFilterInboxSteps = new ConsultaFilterInboxSteps();
        consultaFilterInboxSteps.seArmaElCuerpoDePeticionCreacionCliente(snumerotramite);
        consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticion();
        consultaFilterInboxSteps.validoLaRespuestaObtenida();
        nroAsesorReasignado = consultaFilterInboxSteps.validoCamposObtenidos2();
        System.out.println(nroAsesorReasignado + " AAA");
    }


    @Given("^Ingresar con el usuario y password que tenga el tramite reasignado en bandeja")
    public void ingresarConElUsuarioYPasswordQueTengaElTramiteAsignadoBandeja() {
        //loginPage.open();
        loginPage.ingresarUsuario(Serenity.sessionVariableCalled("nroAsesorReasignado"));
        loginPage.ingresarPassword("XTPRUEBAS1254");
        loginPage.clickIniciarSesion();
    }
    @Given("^Ingresar con el usuario y password que tenga el tramite reasignado en cnr$")
    public void ingresarConElUsuarioYPasswordQueTengaElTramiteAsignado() {
        //loginPage.open();
        loginPage.ingresarUsuario(nroAsesorReasignado);
        loginPage.ingresarPassword("XTPRUEBAS1254");
        loginPage.clickIniciarSesion();
    }
    @Then("^segun del perfil se verificara el reclamo en Resumen y Seguimiento$")
    public void dependiendoDelPerfilSeVerificaraElReclamoEnResumenYSeguimiento() throws IOException, InterruptedException, SQLException {
        /* conexion.cadenaConexion();*/
        String nroReclamo;
        homePage.clickRegistrar();
        nroReclamo =  homePage.guardarNroReclamo();
        snumerotramite = nroReclamo;
        util.almacenarEliminarReclamos(ruta, nroReclamo, 0);

       /* conexion.consultaDatosReclamo(nroReclamo);
        conexion.verficarDatosReclamo(String.format("%015d", Integer.parseInt(nroReclamo)), util.separarValoresTrama(1, data.getRespuesta(), "-"));*/

        if (data.getTipoPerfil().equals("Asesor")) {
            homePage.seleccionarTipoNumeroDocumento(data.getTipoDocumento(), data.getNroDocumento()); // SE CAMBIÓ VERSION APARTIR DE LA 3.0 MODELO DE ATENCION PARA ASESORES
            historial.menuHistorialAsesor(); //click
            // util.menuSuperior("Historial");
            Thread.sleep(12000);
            historial.historialAsesor(nroReclamo);
            historial.clickFechaRegistroASesor();
            historial.seleccionarNroReclamoHistorialAsesor();
            historial.obtenerActividadHistorialAsesor();

           //* historial.menuHistorialAsesor(); //click
            // Thread.sleep(4000);
            // homePage.seleccionarTipoNumeroDocumento(data.getTipoDocumento(), data.getNroDocumento()); // SE CAMBIÓ VERSION APARTIR DE LA 3.0 MODELO DE ATENCION PARA ASESORES
            // Thread.sleep(6000);
            //ustil.menuSuperior("Historial");
            // historial.historialAsesor(nroReclamo);
            // historial.clickFechaRegistroASesor();
            // historial.seleccionarNroReclamoHistorialAsesor();
            // historial.obtenerActividadHistorialAsesor();*//*
        } else {
            util.menuSuperior("Historial");
            // historial.menuHistorial2(); //click
            Thread.sleep(12000);
            historial.historial(nroReclamo);
            historial.seleccionarNroReclamoHistorialEspecialista();
            historial.obtenerActividadHistorialEspecialista();
        }
        Assert.assertEquals(data.getTipoTramite(), historial.obtenerTipoTramiteHistorial());
        Assert.assertEquals(util.obtenerTextoTipologia(data.getTipolgia()).toUpperCase(new Locale("es", "Peru")), historial.obtenerTipologiaHistorial().toUpperCase(new Locale("es", "Peru")));
        Assert.assertEquals(data.getNroTarjeta(), historial.obtenerNroTarjetaHistorial());
        historial.seleccionarSeguimientoHistorial();

/*        conexion.verificarInstanciaReclamo(nroReclamo);
        conexion.cerrarConexion();*/

        if (data.getTipoPerfil().equalsIgnoreCase("Asesor")) {
         //   Assert.assertEquals(historial.obtenerActividadHistorialAsesor(), historial.obtenerActividadSeguimientoHistorial());
        } else {
        //    Assert.assertEquals(historial.obtenerActividadHistorialEspecialista(), historial.obtenerActividadSeguimientoHistorial());
        }
        if (archivoHome == null) {
            System.out.println("No se cargaron archivos para comparar");
        } else {
           // Assert.assertEquals(archivoHome, historial.obtenerArchivoSeguimientoHistorial());
        }
    }

    @And("^selecciona la opcion (.*) si cuenta con seguro y comentario (.*)$")
    public void seleccionaLaOpcionOpcionSiCuentaConSeguroYComentarioComentario(String opcion, String comentario) {

        try {
            Thread.sleep(8000);
        }catch (Exception e){
            Assert.assertEquals("error", e);
        }

        cnrpage.seleccionarOpcionSeguro(opcion);
            cnrpage.escribeComentario(comentario);

    }
}
