package com.everis.giru.web.steps;

import com.everis.giru.web.entidades.Data;
import com.everis.giru.web.fact.Util;
import com.everis.giru.web.page.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
//import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.junit.Assert;

import java.io.IOException;
import java.util.Locale;

/**
 * @author York
 */
public class SolicitudAnulacionStepDefinition {

    LoginPage loginPage;
    HomePage homePage;
    HistorialPage historial;
    AnulacionPage anulacionPage;

    Util util;
    public static String nroTramite = "32576";
    Data data = new Data();

    private static final String ruta = "src/test/resources/ReclamosGenerados/Solicitud_ANULACION.txt";

    @Given("^ingresamos al sistema con un (.*), (.*) y un (.*) determinado$")
    public void ingresamosAlSistemaConUnUsuarioContraYUnPerfilDeterminado(String usuario, String password, String perfil) {
        loginPage.open();
        loginPage.ingresarUsuario(usuario);
        loginPage.ingresarPassword(password);
        loginPage.clickIniciarSesion();
        data.setTipoPerfil(perfil);
    }

    @When("^hacemos la consulta por (.*) y (.*) de un cliente$")
    public void hacemosLaConsultaPorTipodocumentoYNumerodocumentoDeUnCliente(String tipodocumento, String numerodocumento) {
        if (data.getTipoPerfil().equalsIgnoreCase("Especialista") || data.getTipoPerfil().equalsIgnoreCase("Supervisor") || data.getTipoPerfil().equalsIgnoreCase("Jefe")) {
            homePage.seleccionaMenuSuperior();
        }
        homePage.seleccionarTipoNumeroDocumento(tipodocumento, numerodocumento);
        data.setTipoDocumento(tipodocumento);
        data.setNroDocumento(numerodocumento);
    }

    @And("^indicamos la tarjeta del titular, (.*) y (.*)$")
    public void indicamosLaTarjetaDelTitularSolicitudYTipologia(String tramite, String tipologia) {
        homePage.obtenerNombreCliente();
        anulacionPage.clickTarjetaTitular();
        homePage.seleccionarTramiteyTipologia(tramite, tipologia);
        data.setTipoTramite(tramite);
        data.setTipolgia(tipologia);
    }

    @And("^verificaremos los mensajes segun el tipo de anulación$")
    public void verificaremosLosMensajesSegunElTipoDeAnulación() {
        anulacionPage.obtenerTextoBaner();
    }

    @And("^dependiendo del perfil se verificara el reclamo en Resumen, Seguimiento y cerramos sesión$")
    public void dependiendoDelPerfilSeVerificaraElReclamoEnResumenSeguimientoYCerramosSesión() throws IOException, InterruptedException {
        homePage.clickRegistrar();
        nroTramite = homePage.guardarNroReclamo();
        util.almacenarNroReclamo(nroTramite, ruta);
        if (data.getTipoPerfil().equals("Asesor")) {
            homePage.seleccionarTipoNumeroDocumento(data.getTipoDocumento(), data.getNroDocumento());
            Thread.sleep(6000);
            util.menuSuperior("Historial");
            historial.historialAsesor(nroTramite);
            historial.clickFechaRegistroASesor();
            historial.seleccionarNroReclamoHistorialAsesor();

        } else {
            util.menuSuperior("Historial");
            historial.historial(nroTramite);
            historial.seleccionarNroReclamoHistorialEspecialista();
        }
        anulacionPage.obtenerTextoActividadHistorial();
        Assert.assertEquals(data.getTipoTramite(), historial.obtenerTipoTramiteHistorial());
        Assert.assertEquals(util.obtenerTextoTipologia(data.getTipolgia()).toUpperCase(new Locale("es", "Peru")), historial.obtenerTipologiaHistorial().toUpperCase(new Locale("es", "Peru")));
        historial.seleccionarSeguimientoHistorial();
        anulacionPage.compararTextoActividadSeguimiento();
        homePage.clickPerfil();
        homePage.salir();
    }

    @And("^iniciamos sesión con el supervisor de retención$")
    public void iniciamosSesiónConElSupervisorDeRetención() {
        //loginPage.open();
        loginPage.ingresarUsuario("B12999");
        loginPage.ingresarPassword("Callao2121++");
        loginPage.clickIniciarSesion();
    }

    @And("^buscamos el número de reclamo para poder asignarlo a un asesor$")
    public void buscamosElNúmeroDeReclamoParaPoderAsignarloAUnAsesor() throws InterruptedException {
        anulacionPage.ingresarNroTramiteyBuscar(nroTramite);
        anulacionPage.seleccionarTramiteparaAsignar();
        anulacionPage.clickAsignar();
        anulacionPage.seleccionarAsesor();
        anulacionPage.verificacion();
        homePage.clickPerfil();
        homePage.salir();
    }

    @And("^iniciamos sesión con el asesor$")
    public void iniciamosSesiónConElAsesor() {
        //loginPage.open();
        loginPage.ingresarUsuario("B31407");
        loginPage.ingresarPassword("Callao2121++");
        loginPage.clickIniciarSesion();
    }

    @And("^elegimos el tramite para que sea (.*), en caso derive indicara en una (.*) que se le ofrecio al cliente, el (.*) por el cual el cliente se retira y un comentario (.*)")
    public void elegimosElTramiteParaQueSeaEvaluadoEnCasoDeriveIndicaraEnUnaListaQueSeLeOfrecioAlClienteElMotivoPorElCualElClienteSeRetiraYUnComentarioComentario(String evaluacion, String lista, String motivo, String comentario) throws InterruptedException {
        anulacionPage.ingresarNroTramiteyBuscar(nroTramite);
        anulacionPage.clickTramite();
        anulacionPage.evaluacion(evaluacion, lista, motivo, comentario);
    }
}
