package com.everis.giru.web.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DerivacionWebStepDefinition {


    @Given("^se verificara si existen los numeros de reclamo con el especialista asignado$")
    public void seVerificaraSiExistenLosNumerosDeReclamoConElEspecialistaAsignado() {
    }

    @When("^se accede con el usuario y contrasenia del especialista$")
    public void seAccedeConElUsuarioYContraseniaDelEspecialista() {
    }

    @And("^si deriva, se verifica en Historial el especialista asignado$")
    public void siDerivaSeVerificaEnHistorialElEspecialistaAsignado() {
    }

    @And("^verificamos el estado del reclamo en historial$")
    public void verificamosElEstadoDelReclamoEnHistorial() {
    }

    @Then("^eliminamos el numero de reclamo usado y reescribimo los numeros de reclamos a utilizar$")
    public void eliminamosElNumeroDeReclamoUsadoYReescribimoLosNumerosDeReclamosAUtilizar() {
    }
}
