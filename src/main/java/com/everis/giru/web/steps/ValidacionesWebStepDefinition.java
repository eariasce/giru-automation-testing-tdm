package com.everis.giru.web.steps;

import com.everis.giru.web.entidades.Data;
import com.everis.giru.web.fact.Util;
import com.everis.giru.web.page.HistorialPage;
import com.everis.giru.web.page.HomePage;
import com.everis.giru.web.page.LoginPage;
import com.everis.giru.web.page.ValidacionesPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.junit.Assert;

import javax.swing.plaf.basic.BasicTreeUI;
import java.awt.*;
import java.io.File;
import java.io.FileFilter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.TemporalUnit;
import java.util.*;
import java.util.List;

public class ValidacionesWebStepDefinition {

    LoginPage loginPage;
    HomePage homePage;
    ValidacionesPage validacionesPage;
    Util util;
    Data data = new Data();
    HistorialPage historial;
    String sPerfil = "";
    public static String valorTramite = "";

    @Given("^se ingresaran al sistema con su (.*) y (.*) con un (.*) determinado$")
    public void seIngresaranAlSistemaConSuUsuarioYContraConUnPerfilDeterminado(String usuario, String password, String perfil) {

sPerfil = perfil;
        loginPage.open();
        loginPage.ingresarUsuario(usuario);
        loginPage.ingresarPassword(password);
        loginPage.clickIniciarSesion();
        data.setTipoPerfil(perfil);

    }
    @Given("^nos encontramos logueados en Giru con usuario (.*) y password (.*)$")
    public void nosEncuentramosLogueadosEnGiruConUsuarioUsuarioYPasswordPassword(String usuario, String password) {
        loginPage.open();
        loginPage.ingresarUsuario(usuario);
        loginPage.ingresarPassword(password);
        loginPage.clickIniciarSesion();
    }

    @Then("^se validara que el usuario tenga el (.*) correcta$")
    public void seValidaraQueElUsuarioTengaElArearegistradaCorrecta(String arearegistrada) {
        data.setArea(arearegistrada);
        Assert.assertEquals(data.getArea(), validacionesPage.validarArea());
        homePage.clickPerfil();
        homePage.salir();
    }

    @When("^realizara la consulta por (.*) y numero (.*)$")
    public void realizaraLaConsultaPorDNIYNumero(String tipodocumento, String numerodocumento) {
        util.menuSuperior("Atencion al Cliente");
        homePage.seleccionarTipoNumeroDocumento(tipodocumento, numerodocumento);
    }

    @And("^seleccionamos un numero de tarjeta y despues seleccionamos otro numero de tarjeta$")
    public void seleccionamosUnNumeroDeTarjetaYDespuesSeleccionamosOtroNumeroDeTarjeta() {
        validacionesPage.seleccionarTarjeta1();
        validacionesPage.seleccionarTarjeta2();
    }

    @Then("^nos deberia aparecer el mensaje (.*)$")
    public void nosDeberiaAparecerElMensajeEstásSeguroQueDeseasCambiarDeProducto(String alerta) {
        validacionesPage.mensajeAlertaCambioTarjeta(alerta);
    }

    @And("^seleccionamos un numero de tarjeta$")
    public void seleccionamosUnNumeroDeTarjeta() {
        validacionesPage.seleccionarTarjeta1();
    }

    @And("^seleccionamos el (.*), (.*) y (.*)$")
    public void seleccionamosElTramiteTipologiaYMotivos(String tramite, String tipologia, String motivos) throws InterruptedException {
        valorTramite = tramite;
        homePage.seleccionarTramiteyTipologia(tramite, tipologia);
        validacionesPage.seleccionarMotivo(motivos);
    }

    @And("^agregamos un monto, al eliminar un monto debe aparecer el mensaje (.*)$")
    public void agregamosUnMontoAlEliminarUnMontoDebeAparecerElMensajeEstásSeguroQueDeseasEliminarElMovimiento(String alerta) {
        validacionesPage.agregarMontoyEliminarMonto();
        validacionesPage.mensajeAlertaEliminarMonto(alerta);
    }

    @And("^al seleccionar otro tramite nos debe aparecer el mensaje (.*)$")
    public void alSeleccionarOtroTramiteNosDebeAparecerElMensajeSiCambiasDeTipoDeTrámiteLaTipologíaLosMotivosYMontosIngresadosSePerderánEstásSeguroQueDeseasProceder(String alerta) {
        validacionesPage.seleccionarTramite2();
        validacionesPage.mensajeAlertaCambioTramite(alerta);
    }

    @Then("^dependiendo del perfil se verificara el reclamo descargando el Historial$")
    public void dependiendoDelPerfilSeVerificaraElReclamoDescargandoElHistorial() throws InterruptedException {

        /* conexion.cadenaConexion();*/
        String nroReclamo;
        homePage.clickRegistrar();
        nroReclamo =  homePage.guardarNroReclamo();
       // util.almacenarEliminarReclamos(ruta, nroReclamo, 0);

       /* conexion.consultaDatosReclamo(nroReclamo);
        conexion.verficarDatosReclamo(String.format("%015d", Integer.parseInt(nroReclamo)), util.separarValoresTrama(1, data.getRespuesta(), "-"));*/

        if (data.getTipoPerfil().equals("Asesor")) {

            homePage.seleccionarTipoNumeroDocumento(data.getTipoDocumento(), data.getNroDocumento()); // SE CAMBIÓ VERSION APARTIR DE LA 3.0 MODELO DE ATENCION PARA ASESORES
            historial.menuHistorialAsesor(); //click
            // util.menuSuperior("Historial");
            Thread.sleep(6000);
            historial.historialAsesor(nroReclamo);
            historial.clickFechaRegistroASesor();
            historial.seleccionarNroReclamoHistorialAsesor();
            historial.obtenerActividadHistorialAsesor();
        } else {
            util.menuSuperior("Historial");
            historial.historial(nroReclamo);
            historial.seleccionarNroReclamoHistorialEspecialista();
            historial.obtenerActividadHistorialEspecialista();
        }
        Assert.assertEquals(data.getTipoTramite(), historial.obtenerTipoTramiteHistorial());
        Assert.assertEquals(util.obtenerTextoTipologia(data.getTipolgia()).toUpperCase(new Locale("es", "Peru")), historial.obtenerTipologiaHistorial().toUpperCase(new Locale("es", "Peru")));
        Assert.assertEquals(data.getNroTarjeta(), historial.obtenerNroTarjetaHistorial());
        historial.seleccionarSeguimientoHistorial();

/*        conexion.verificarInstanciaReclamo(nroReclamo);
        conexion.cerrarConexion();*/

        if (data.getTipoPerfil().equalsIgnoreCase("Asesor")) {
            Assert.assertEquals(historial.obtenerActividadHistorialAsesor(), historial.obtenerActividadSeguimientoHistorial());
        } else {
            Assert.assertEquals(historial.obtenerActividadHistorialEspecialista(), historial.obtenerActividadSeguimientoHistorial());
        }

      /*  if (archivoHome == null) {
            System.out.println("No se cargaron archivos para comparar");
        } else {
            Assert.assertEquals(archivoHome, historial.obtenerArchivoSeguimientoHistorial());
        }*/
    }

    @And("^ingresa al menu de historial$")
    public void ingresaAlMenuDeHistorial() throws InterruptedException {

        if (data.getTipoPerfil().equals("Asesor")) {

            homePage.seleccionarTipoNumeroDocumento(data.getTipoDocumento(), data.getNroDocumento()); // SE CAMBIÓ VERSION APARTIR DE LA 3.0 MODELO DE ATENCION PARA ASESORES
            historial.menuHistorialAsesor(); //click
            util.menuSuperior("Historial");


        } else {
            util.menuSuperior("Historial");

        }

    }

    @And("^ingresa el numero de (.*)$")
    public void ingresaElNumeroDeTranmite(String nrotranmite) throws InterruptedException {
        if (data.getTipoPerfil().equals("Asesor")) {

           historial.historialAsesor(nrotranmite);

        } else {
                historial.historial2(nrotranmite);
        }
    }



    @And("^ingresa el primer tramite de la lista$")
    public void ingresaPrimerTranmite() throws InterruptedException {

        String nrotranmite = historial.obtenerPrimerTramite();

        if (data.getTipoPerfil().equals("Asesor")) {

            historial.historialAsesor(nrotranmite);

        } else {
            historial.historial2(nrotranmite);
        }
    }

    @And("^le click al boton de buscar$")
    public void leClickAlBotonDeBuscar() {
        historial.seleccionarBuscar();

    }

    @And("^selecciona el boton de descargar$")
    public void seleccionaElBotonDeDescargar() {
        historial.seleccionarDescargarExcel();
    }

    @Then("^se valida la descarga del archivo excel$")
    public void seValidaLaDescargaDelArchivoExcel() throws InterruptedException {

//Thread.sleep(5000);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();

        String fecha2 = dateFormat.format(date);
        String fechafinal = fecha2.replace("/","_");


        LocalTime now = LocalTime.now().minus(Duration.ofSeconds(1));
       int hour12 = now.getHour();
        int min12 = now.getMinute();
     //   int sec12 = now.getSecond() -1;
     //   String filecreated = "GIRU -"+sPerfil.toUpperCase() +fechafinal+" "+hour12+"_"+min12+"_"+sec12+".xlsx";
          String filecreated = "GIRU -"+sPerfil.toUpperCase() +fechafinal+" "+hour12+"_"+min12;

      String filePath = "C:\\Users\\XT8363\\Downloads\\";
            File theNewestFile = null;
            File dir = new File(filePath);
            FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
            File[] files = dir.listFiles(fileFilter);

            if (files.length > 0) {
                /** The newest file comes first **/
                Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
                theNewestFile = files[0];
            }

         String filename = theNewestFile.getName();

        if(filename.contains(filecreated)) {
            //Bien
        }
        else
        {
            Assert.assertEquals(filename,filecreated);
        }
    }



    @And("^valido las opciones en la franja superior del usuario por su (.*)$")
    public void validoLasOpcionesEnLaFranjaSuperiorDelUsuarioPorSuPerfil(String perfil) {
        sPerfil = perfil;
            validacionesPage.validoopciones(perfil);
    }
    @And("^valido area seleccionada en panel tramites(.*)$")
    public void validoElAreaSeleccionadaAreaSupervisorEspecialista(String area) throws InterruptedException {
        util.menuSuperior("Tramites");
        Thread.sleep(3000);

        validacionesPage.validoAreaSupervisorJefe(area, sPerfil);
    }

    @And("^valido el area seleccionada (.*)$")
    public void validoElAreaSeleccionadaArea(String area) {
        validacionesPage.validoArea(area, sPerfil);
    }


    @And("^se selecciona (.*) en el menu de tipologias$")
    public void seSeleccionaTipologiaAValidarEnMenu(String tipologia) {
        validacionesPage.seleccionoTipologiaEnMenu(tipologia);
    }


    @And("^se aplica el filtro de Vencimiento (.*)$")
    public void seSeleccionaTipoVencimiento(String vencimiento) {
        validacionesPage.seleccionaVencimiento(vencimiento);
    }


    @And("^se aplica el filtro de tipo tramite (.*)$")
    public void seValidaElFiltroDeTipoTramite(String tramite) {
        validacionesPage.aplicoFiltroTramite(tramite);
    }

    @And("^se aplica el filtro de tipo tipologia (.*)$")
    public void seValidaElFiltroDeTipoTipologia(String tipologia) {
        validacionesPage.aplicoFiltroTipologia(tipologia);
    }

    @And("^se valida la columna de (.*) el valor de (.*)$")
    public void seValidaLaColumnaDeElValorDe(String columna, String valor) {
        validacionesPage.validarValorTabla(columna,valor);
    }


    @And("^se valida la columna monto (.*) el valor de (.*)$")
    public void seValidaLaColumnaMontoDeElValorDe(String columna, String valor) {
        validacionesPage.validarValorTablaMonto(columna,valor);
    }

    @And("^se aplica el filtro de tipo motivo (.*)$")
    public void seValidaElFiltroDeTipoMotivo(String motivo) {
        validacionesPage.aplicoFiltroTipoMotivo(motivo);
    }

    @And("^se aplica el filtro de tipo moneda (.*) y monto (.*)$")
    public void seValidaElFiltroDeTipoMonedaMonto(String moneda, String monto) {
        validacionesPage.aplicoFiltroMonedaMonto(moneda, monto);
    }

    @And("^ingresa al menu de tramites depdendiendo del (.*)$")
    public void ingresaAlMenuDeTramitesDepdendiendoDel(String Perfil) throws InterruptedException {

        if(Perfil.equals("Jefe")|| Perfil.equals("Supervisor")) {
            util.menuSuperior("Tramites");
            Thread.sleep(3000);
        }
    }

    @And("ordena la bandeja de tramites con fecha actual a anterior")
    public void ordenaLaBandejaDeTramitesConFechaActualAAnterior() {
        validacionesPage.ordenoTramitesActual();
    }
}


//
//    @And("^agregamos dos montos y hacemos click al boton Registrar$")
//    public void agregamosDosMontosYHacemosClickAlBotonRegistrar() {
//    }
//
//    @Then("^se pondra en color rojo los campos faltantes con el <mensaje>$")
//    public void sePondraEnColorRojoLosCamposFaltantesConElMensaje() {
//    }

