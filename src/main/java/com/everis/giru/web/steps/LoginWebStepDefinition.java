package com.everis.giru.web.steps;

import com.everis.giru.web.entidades.Data;
import com.everis.giru.web.page.HomePage;
import com.everis.giru.web.page.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class LoginWebStepDefinition {
    LoginPage loginPage;
    HomePage homePage;
    final Data data = new Data();

    @Given("^se accedera a la web de Giru$")
    public void seAccederaALaWebDeGiru() {
        loginPage.open();
    }

    @When("^ingresara al sistema con su (.*) y (.*) con un perfil (.*) determinado$")
    public void seIngresaraAlSistemaConSuUsuarioYContraseniaConUnPerfilPerfilDeterminado(String usuario, String password, String perfil) {
        loginPage.ingresarUsuario(usuario);
        loginPage.ingresarPassword(password);
        loginPage.clickIniciarSesion();
        data.setTipoPerfil(perfil);
    }

    @And("^verificara que el acceso sea correcto segun el perfil$")
    public void verificaraQueElAccesoSeaCorrectoSegunElPerfil() {
        switch (data.getTipoPerfil()) {
            case "Asesor":
                loginPage.verificarLoginPerfilAsesor();
                break;
            case "Especialista":
                loginPage.verificarLoginPerfilEspecialista();
                break;
            case "Supervisor":
                loginPage.verificarLoginPerfilSupervisor();
                break;
            default:
        }
    }

    @Then("^cerrara sesion$")
    public void cerraraSesion() {
        homePage.clickPerfil();
        homePage.salir();
    }

    @When("^ingresara con usuario (.*) y contrasenia (.*)$")
    public void ingresaraConUsuarioUsuarioYContraseniaContrasenia(String usuario, String password) {
        loginPage.ingresarUsuario(usuario);
        loginPage.ingresarPassword(password);
        loginPage.clickIniciarSesion();
    }

    @Then("^se validara el mensaje de error (.*) en caso el usuario y/o la contrasenia sean incorrectas$")
    public void seValidaraElMensajeDeErrorELIDOCONTRASEÑANOCOINCIDENEnCasoElUsuarioYOLaContraseniaSeanIncorrectas(String mensajeError) {
        loginPage.verificarMensajeError(mensajeError);
    }


}
