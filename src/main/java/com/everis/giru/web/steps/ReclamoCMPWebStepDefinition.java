package com.everis.giru.web.steps;

import com.everis.giru.rest.steps.ConsultaFilterInboxSteps;
import com.everis.giru.web.entidades.Data;
import com.everis.giru.web.fact.SubirArchivo;
import com.everis.giru.web.fact.Util;
import com.everis.giru.web.page.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.junit.Assert;

import java.awt.*;
import java.io.IOException;
import java.util.Locale;

public class ReclamoCMPWebStepDefinition {

    LoginPage loginPage;
    HomePage homePage;
    HistorialPage historial;
    CmpPage cmpPage;
    ReclamoCMPPage cmp;
    String snumerotramite = "";
    BandejaPage bandeja;
    Util util;
    Data data = new Data();
    String nroAsesorReasignado = "";
    ConsultaFilterInboxSteps consultaFilterInboxSteps;
    private String archivoHome;
    private static final String ruta = "src/test/resources/ReclamosGenerados/Reclamo_CMP.txt";

    @Given("^se ingresa al sistema con su (.*) y (.*) con un (.*) determinado$")
    public void seIngresaAlSistemaConSuUsuarioYContraConUnPerfilDeterminado(String usuario, String password, String perfil) {
        loginPage.open();
        loginPage.ingresarUsuario(usuario);
        loginPage.ingresarPassword(password);
        loginPage.clickIniciarSesion();
        data.setTipoPerfil(perfil);
    }

    @When("^efectua la consulta por (.*) y (.*) de un cliente$")
    public void efectuaLaConsultaPorTipodocumentoYNumerodocumentoDeUnCliente(String tipodocumento, String numerodocumento) {
        if (data.getTipoPerfil().equalsIgnoreCase("Especialista") || data.getTipoPerfil().equalsIgnoreCase("Supervisor") || data.getTipoPerfil().equalsIgnoreCase("Jefe")) {
            homePage.seleccionaMenuSuperior();
        }
        homePage.seleccionarTipoNumeroDocumento(tipodocumento, numerodocumento);
        data.setTipoDocumento(tipodocumento);
        data.setNroDocumento(numerodocumento);
    }

    @And("^elige el (.*), (.*) y (.*)$")
    public void eligeElNumerotarjetaTramiteYTipologia(String numeroTarjeta, String tramite, String tipologia) {
        homePage.obtenerNombreCliente();
        homePage.seleccionarTarjeta(numeroTarjeta);
        homePage.seleccionarTramiteyTipologia(tramite, tipologia);
        data.setNroTarjeta(numeroTarjeta);
        data.setTipoTramite(tramite);
        data.setTipolgia(tipologia);
    }

    @And("^elegimos la (.*) de (.*) y configura el tipo de moneda (.*), monto (.*), fecha (.*), nombre de comercio (.*), se adquiere un (.*), un servicio (.*) y una fecha de recepcion (.*) segun la (.*)$")
    public void elegimosLaCantidadDeMotivosYConfiguraElTipoDeMonedaTipomonedaMontoMontoFechaFechaNombreDeComercioNombrecomSeAdquiereUnPsUnServicioServicioYUnaFechaDeRecepcionFecharecepSegunLaConfiguracion(
            String cantidad, String motivos, String fecha, String tipomoneda, String monto, String nombrecom, String ps, String servicios, String fecharecep, String configuracion) throws Exception {
        cmp.seleccionReclamoCmp(true, Integer.parseInt(cantidad), motivos, configuracion, nombrecom, fecha, monto, tipomoneda, ps, servicios, fecharecep);
        data.setCantidadMotivos(cantidad);
        data.setTipoMotivo(motivos);
    }

    @And("^escogemos la (.*) de (.*) y configura el tipo de moneda (.*), monto (.*), fecha (.*) y nombre de comercio (.*) segun la (.*)$")
    public void escogemosLaCantidadDeMotivosYConfiguraElTipoDeMonedaTipomonedaMontoMontoFechaFechaYNombreDeComercioNombrecomSegunLaConfiguracion(
            String cantidad, String motivos, String fecha, String tipomoneda, String monto, String nombrecom, String configuracion) throws Exception {
        cmp.seleccionReclamoCmp(false, Integer.parseInt(cantidad), motivos, configuracion, nombrecom, fecha, monto, tipomoneda, "", "", "");
        data.setCantidadMotivos(cantidad);
        data.setTipoMotivo(motivos);
    }

    @And("^se anexara un archivo (.*) en caso sea necesario$")
    public void seAnexaraUnArchivoArchivoEnCasoSeaNecesario(String archivo) throws AWTException, InterruptedException {
        if (archivo.equals("Si")) {
            new SubirArchivo().subirArchivo();
            archivoHome = homePage.obtenerTextoArchivo();
        } else if (archivo.equals("No")) {
            System.out.println("No se agregaron archivos");
        }
    }

    @And("^elegimos el medio de respuesta (.*) segun la configuracion de la respuesta (.*)$")
    public void elegimosElMedioDeRespuestaRespuestaSegunLaConfiguracionDeLaRespuestaConfiguracionrespuesta(String respuesta, String configuracionrespuesta) throws InterruptedException {
        data.setRespuesta(respuesta);
        String TipoMedioRespuesta = util.separarValoresTrama(1, respuesta, "-"); // S-EMAIL o
        if (util.separarValoresTrama(0, respuesta, "-").equalsIgnoreCase("S")) {
            homePage.seleccionarRespuesta(TipoMedioRespuesta, configuracionrespuesta);
        } else if (util.separarValoresTrama(0, respuesta, "-").equalsIgnoreCase("A")) {
            homePage.aniadirRespuesta(TipoMedioRespuesta, configuracionrespuesta);
        }
    }

    @And("^se procedera a revisar la lista de documentos que el cliente tiene que enviar en caso se haya habilitado la opcion$")
    public void seProcederaARevisarLaListaDeDocumentosQueElClienteTieneQueEnviarEnCasoSeHayaHabilitadoLaOpcion() throws InterruptedException {
        if (!data.getTipoMotivo().equalsIgnoreCase("21") && data.getCantidadMotivos().equalsIgnoreCase("1") || data.getCantidadMotivos().equalsIgnoreCase("2") || data.getCantidadMotivos().equalsIgnoreCase("3")) {
            if (data.getRespuesta().equalsIgnoreCase("S-ADDRESS") || data.getRespuesta().equalsIgnoreCase("A-ADDRESS")) {
                homePage.btnActualizate();
            }
        } else {
            System.out.println("No verificara el documento PDF");
        }
    }

    @When("^ingresa en cmp la busqueda el (.*) para seleccionar$")
    public void ingresaEnLaBusquedaElTramiteParaSeleccionarlo(String nrotramite) throws InterruptedException {

        // util.menuSuperior("Tramites");
        Thread.sleep(4000);

        if(snumerotramite.equals("")) {
            snumerotramite = nrotramite;
        }

        bandeja.bandejaTramite(snumerotramite);
        // Resumen.VerificarTramite();
    }

    @And("^se valida el usuario reasignado por servicios en cmp$")
    public void seValidaElUsuarioReasignadoPorServicios() {
        consultaFilterInboxSteps = new ConsultaFilterInboxSteps();
        consultaFilterInboxSteps.seArmaElCuerpoDePeticionCreacionCliente(snumerotramite);
        consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticion();
        consultaFilterInboxSteps.validoLaRespuestaObtenida();
        nroAsesorReasignado = consultaFilterInboxSteps.validoCamposObtenidos();
    }

    @Given("^Ingresar con el usuario y password que tenga el tramite reasignado en cmp$")
    public void ingresarConElUsuarioYPasswordQueTengaElTramiteAsignado() {
        //loginPage.open();
        loginPage.ingresarUsuario(nroAsesorReasignado);
        loginPage.ingresarPassword("XTPRUEBAS1254");
        loginPage.clickIniciarSesion();
    }
    @Then("^segun el perfil ingresado se podra verificar el reclamo en su Resumen y Seguimiento$")
    public void segunElPerfilIngresadoSePodraVerificarElReclamoEnSuResumenYSeguimiento() throws IOException, InterruptedException {
        String nroReclamo;
        homePage.clickRegistrar();
        nroReclamo = homePage.guardarNroReclamo();
        snumerotramite = nroReclamo;
        util.almacenarNroReclamo(nroReclamo, ruta);
        if (data.getTipoPerfil().equals("Asesor")) {
            homePage.seleccionarTipoNumeroDocumento(data.getTipoDocumento(), data.getNroDocumento()); // SE CAMBIÓ VERSION APARTIR DE LA 3.0 MODELO DE ATENCION PARA ASESORES
            historial.menuHistorialAsesor(); //click
            // util.menuSuperior("Historial");
            Thread.sleep(6000);
            historial.historialAsesor(nroReclamo);
            historial.clickFechaRegistroASesor();
            historial.seleccionarNroReclamoHistorialAsesor();
            historial.obtenerActividadHistorialAsesor();

          // homePage.seleccionarTipoNumeroDocumento(data.getTipoDocumento(), data.getNroDocumento());
          //  Thread.sleep(6000);
          // / util.menuSuperior("Historial");
          //  historial.historialAsesor(nroReclamo);
           // historial.clickFechaRegistroASesor();
           // historial.seleccionarNroReclamoHistorialAsesor();
           // historial.obtenerActividadHistorialAsesor();

        } else {
            util.menuSuperior("Historial");
            historial.historial(nroReclamo);
            historial.seleccionarNroReclamoHistorialEspecialista();
            historial.obtenerActividadHistorialEspecialista();
        }
        Assert.assertEquals(data.getTipoTramite(), historial.obtenerTipoTramiteHistorial());
        Assert.assertEquals(util.obtenerTextoTipologia(data.getTipolgia()).toUpperCase(new Locale("es", "Peru")), historial.obtenerTipologiaHistorial().toUpperCase(new Locale("es", "Peru")));
        Assert.assertEquals(data.getNroTarjeta(), historial.obtenerNroTarjetaHistorial());
        historial.seleccionarSeguimientoHistorial();

        if (!data.getTipoMotivo().equalsIgnoreCase("21") && data.getCantidadMotivos().equalsIgnoreCase("1") || data.getCantidadMotivos().equalsIgnoreCase("2") || data.getCantidadMotivos().equalsIgnoreCase("3")) {
            if (archivoHome == null) {
                if (data.getTipoPerfil().equalsIgnoreCase("Asesor")) {
                    Assert.assertEquals(historial.obtenerActividadHistorialAsesor(), cmpPage.obtenerActividadSeguimientoSinArchivo());
                } else {
                    Assert.assertEquals(historial.obtenerActividadHistorialEspecialista(), cmpPage.obtenerActividadSeguimientoSinArchivo());
                }
                Assert.assertEquals("DOCUMENTACIÓN PENDIENTE", cmpPage.obtenerTrackingSeguimiento());

            } else if (data.getTipoPerfil().equalsIgnoreCase("Asesor")) {
                Assert.assertEquals(historial.obtenerActividadHistorialAsesor(), historial.obtenerActividadSeguimientoHistorial());

            } else {
                Assert.assertEquals(historial.obtenerActividadHistorialEspecialista(), historial.obtenerActividadSeguimientoHistorial());

                Assert.assertEquals(archivoHome, historial.obtenerArchivoSeguimientoHistorial());
                Assert.assertEquals("Controversias", cmpPage.obtenerAreaSeguimiento());
                Assert.assertEquals("EN PROCESO", cmpPage.obtenerTrackingSeguimiento());
            }
        }
        if (data.getTipoMotivo().equalsIgnoreCase("21") && data.getCantidadMotivos().equalsIgnoreCase("1")) {
            Assert.assertEquals("Controversias", cmpPage.obtenerAreaSeguimiento());
            if (data.getTipoPerfil().equalsIgnoreCase("Asesor")) {
                Assert.assertEquals(historial.obtenerActividadHistorialAsesor(), historial.obtenerActividadSeguimientoHistorial());

            } else {
                Assert.assertEquals(historial.obtenerActividadHistorialEspecialista(), historial.obtenerActividadSeguimientoHistorial());
            }
            Assert.assertEquals("EN PROCESO", cmpPage.obtenerTrackingSeguimiento());

            if (archivoHome == null) {
                System.out.println("No se cargaron archivos para comparar");
            } else {
                Assert.assertEquals(archivoHome, historial.obtenerArchivoSeguimientoHistorial());
            }
        }
        cmpPage.obtenerEspecialista();
    }
}
