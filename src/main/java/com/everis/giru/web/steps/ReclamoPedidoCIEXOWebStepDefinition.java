package com.everis.giru.web.steps;

import com.everis.giru.rest.steps.ConsultaFilterInboxSteps;
import com.everis.giru.web.entidades.Data;
import com.everis.giru.web.fact.Conexion;
import com.everis.giru.web.fact.SubirArchivo;
import com.everis.giru.web.fact.Util;
import com.everis.giru.web.page.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.rest.questions.TheResponse;
import org.junit.Assert;

import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class ReclamoPedidoCIEXOWebStepDefinition {
    LoginPage loginPage;
    HomePage homePage;
    ResumenPage Resumen;
    ReclamoPedidoCIEXOPage ciexo;
    HistorialPage historial;
    Util util;
    Conexion conexion;
    String snumerotramite = "";
    String tipoResolución = "";
    String tipocarta = "";
    String nroAsesorReasignado = "";
    ConsultaFilterInboxSteps consultaFilterInboxSteps;
    BandejaPage bandeja;
    Data data = new Data();
    private String archivoHome;
    private static final String ruta = "src/test/resources/ReclamosGenerados/Reclamo_CIEXO.txt";

    @Given("^se ingresara al sistema con su (.*) y (.*) con un (.*) determinado$")
    public void seIngresaraAlSistemaConSuUsuarioYContraConUnPerfilDeterminado(String usuario, String password, String perfil) {
        loginPage.open();
        loginPage.ingresarUsuario(usuario);
        loginPage.ingresarPassword(password);
        loginPage.clickIniciarSesion();
        data.setTipoPerfil(perfil);

        Serenity.setSessionVariable("perfilUsuario").to(perfil);


    }

    @When("^hace la consulta por (.*) y (.*) de un cliente$")
    public void haceLaConsultaPorTipodocumentoYNumerodocumentoDeUnCliente(String tipodocumento, String numerodocumento) {
        if (data.getTipoPerfil().equalsIgnoreCase("Supervisor") || data.getTipoPerfil().equalsIgnoreCase("Jefe")) {
            homePage.seleccionaMenuSuperior();
            homePage.seleccionarTipoNumeroDocumento(tipodocumento, numerodocumento);
            data.setTipoDocumento(tipodocumento);
            data.setNroDocumento(numerodocumento);
        } else if (data.getTipoPerfil().equalsIgnoreCase("Especialista")){
            homePage.seleccionaMenuEspecialista();
            homePage.seleccionarTipoNumeroDocumento(tipodocumento, numerodocumento);
            data.setTipoDocumento(tipodocumento);
            data.setNroDocumento(numerodocumento);
        } else
        homePage.seleccionarTipoNumeroDocumento(tipodocumento, numerodocumento);
        data.setTipoDocumento(tipodocumento);
        data.setNroDocumento(numerodocumento);
    }

    @And("^indica el (.*), (.*) y (.*)$")
    public void indicaElNumerotarjetaTramiteYTipologia(String numeroTarjeta, String tramite, String tipologia) {
        homePage.obtenerNombreCliente();
        homePage.seleccionarTarjeta(numeroTarjeta);
        homePage.seleccionarTramiteyTipologia(tramite, tipologia);
        data.setNroTarjeta(numeroTarjeta);
        data.setTipoTramite(tramite);
        data.setTipolgia(tipologia);
    }

    @When("^ingresa en la busqueda el (.*) para seleccionar$")
    public void ingresaEnLaBusquedaElTramiteParaSeleccionarlo(String nrotramite) throws InterruptedException {

        // util.menuSuperior("Tramites");
        Thread.sleep(4000);

        if(snumerotramite.equals("")) {
            snumerotramite = nrotramite;
        }

        bandeja.bandejaTramite(snumerotramite);
        // Resumen.VerificarTramite();
    }

    @And("^elige la (.*) de (.*) y configura la fecha (.*) tipo de moneda (.*) y monto (.*) por motivo segun la (.*)$")
    public void eligeLaCantidadDeMotivosYConfiguraLaFechaFechaTipoDeMonedaTipomonedaYMontoMontoPorMotivoSegunLaConfiguracion(
       String cantidad, String motivos, String fecha, String tipomoneda, String monto, String configuracion) throws Exception {
        ciexo.selectReasonMoney(Integer.parseInt(cantidad), motivos, configuracion, monto, tipomoneda, fecha);
        data.setTipoMotivo(motivos);
    }

    @And("^en caso los motivos tuviera Comision de Membresia se aplicaria la Razon Operativa (.*) y seleccionar el Tipo de Razon Operativa (.*)$")
    public void enCasoLosMotivosTuvieraComisionDeMembresiaSeAplicariaLaRazonOperativaRazonoperativaYSeleccionarElTipoDeRazonOperativaTiporazon(String razonopertiva, String tiporazon) {
        if (util.separarValoresTrama(0, data.getTipoMotivo(), "-").equalsIgnoreCase("2") || util.separarValoresTrama(0, data.getTipoMotivo(), "-").equalsIgnoreCase("11")) {
            homePage.seleccionarCantidadMotivos(razonopertiva, tiporazon);
        }
        if (util.separarValoresTrama(1, data.getTipoMotivo(), "-").equalsIgnoreCase("2") || util.separarValoresTrama(0, data.getTipoMotivo(), "-").equalsIgnoreCase("11")) {
            homePage.seleccionarCantidadMotivos(razonopertiva, tiporazon);
        }
        if (util.separarValoresTrama(2, data.getTipoMotivo(), "-").equalsIgnoreCase("2") || util.separarValoresTrama(0, data.getTipoMotivo(), "-").equalsIgnoreCase("11")) {
            homePage.seleccionarCantidadMotivos(razonopertiva, tiporazon);
        }
    }

    @And("^ingresa un (.*) y un (.*) opcional$")
    public void ingresaUnComentarioclienteYUnComentariointernoOpcional(String comentariocliente, String comentariointerno) {
        homePage.ingresarComentarioCliente(comentariocliente);
        homePage.ingresarComentarioOpcional(comentariointerno);
    }

    @And("^se adjuntara un archivo (.*) en caso sea necesario$")
    public void seAdjuntaraUnArchivoArchivoEnCasoSeaNecesario(String archivo) throws InterruptedException, AWTException {
        if (archivo.equals("Si")) {
            new SubirArchivo().subirArchivo();
            archivoHome = homePage.obtenerTextoArchivo();
        } else if (archivo.equals("No")) {
            System.out.println("No se agregaron archivos");
        }
    }

    @And("^selecciona o añade segun el (.*) y (.*)$")
    public void seleccionaOAñadeSegunElIndicadorYOperador(String indicador, String operador) {
        homePage.seleccionarCelular(indicador, operador);
    }

    @And("^seleciona el medio de respuesta (.*) segun la configuracion de la respuesta (.*)$")
    public void selecionaElMedioDeRespuestaRespuestaSegunLaConfiguracionDeLaRespuestaConfiguracionrespuesta(String respuesta, String configuracionrespuesta) throws InterruptedException {
        data.setRespuesta(respuesta);
        String TipoMedioRespuesta = util.separarValoresTrama(1, respuesta, "-"); // S-EMAIL o
        if (util.separarValoresTrama(0, respuesta, "-").equalsIgnoreCase("S")) {
            homePage.seleccionarRespuesta(TipoMedioRespuesta, configuracionrespuesta);
        } else if (util.separarValoresTrama(0, respuesta, "-").equalsIgnoreCase("A")) {
            homePage.aniadirRespuesta(TipoMedioRespuesta, configuracionrespuesta);
        }
    }
    @Then("^se verificara el reclamo derivado en la bandeja Historial en su Seguimiento$")
    public void seVerificaraElReclamoEnLaBandejaHistorialEnSuSeguimiento2() throws InterruptedException {
        Thread.sleep(6000);
        util.menuSuperior("Historial");
        historial.historial(snumerotramite);
        historial.seleccionarNroReclamoHistorialEspecialista();
        String actividadhistorial = historial.obtenerActividadHistorialEspecialista();
        historial.seleccionarSeguimientoHistorial();
       /* if(tipoResolución.equals("P"))
            Assert.assertEquals(actividadhistorial , historial.obtenerActividadSeguimientoHistorialResol());
        else
            Assert.assertEquals(actividadhistorial , historial.obtenerActividadSeguimientoHistorialResolNoProcede());
    */
    }

    @Then("^dependiendo del perfil se verificara el reclamo en Resumen y Seguimiento$")
    public void dependiendoDelPerfilSeVerificaraElReclamoEnResumenYSeguimiento() throws IOException, InterruptedException, SQLException {
        /* conexion.cadenaConexion();*/
        String nroReclamo;
        homePage.clickRegistrar();
        nroReclamo =  homePage.guardarNroReclamo();
        snumerotramite = nroReclamo;
        util.almacenarEliminarReclamos(ruta, nroReclamo, 0);

        Serenity.setSessionVariable("nroReclamo").to(snumerotramite);

       /* conexion.consultaDatosReclamo(nroReclamo);
        conexion.verficarDatosReclamo(String.format("%015d", Integer.parseInt(nroReclamo)), util.separarValoresTrama(1, data.getRespuesta(), "-"));*/

        if (data.getTipoPerfil().equals("Asesor")) {

            homePage.seleccionarTipoNumeroDocumento(data.getTipoDocumento(), data.getNroDocumento()); // SE CAMBIÓ VERSION APARTIR DE LA 3.0 MODELO DE ATENCION PARA ASESORES
            historial.menuHistorialAsesor(); //click
            // util.menuSuperior("Historial");
            Thread.sleep(10000);
            historial.historialAsesor(nroReclamo);
            historial.clickFechaRegistroASesor();
            historial.seleccionarNroReclamoHistorialAsesor();
            historial.obtenerActividadHistorialAsesor();
        } else {
            util.menuSuperior("Historial");
            Thread.sleep(12000);
            historial.historial(nroReclamo);
            historial.seleccionarNroReclamoHistorialEspecialista();
            historial.obtenerActividadHistorialEspecialista();
        }
        Assert.assertEquals(data.getTipoTramite(), historial.obtenerTipoTramiteHistorial());
        Assert.assertEquals(util.obtenerTextoTipologia(data.getTipolgia()).toUpperCase(new Locale("es", "Peru")), historial.obtenerTipologiaHistorial().toUpperCase(new Locale("es", "Peru")));
        Assert.assertEquals(data.getNroTarjeta(), historial.obtenerNroTarjetaHistorial());
        historial.seleccionarSeguimientoHistorial();

/*        conexion.verificarInstanciaReclamo(nroReclamo);
        conexion.cerrarConexion();*/

        if (data.getTipoPerfil().equalsIgnoreCase("Asesor")) {
            //Assert.assertEquals(historial.obtenerActividadHistorialAsesor(), historial.obtenerActividadSeguimientoHistorial());
        } else {
            //Assert.assertEquals(historial.obtenerActividadHistorialEspecialista(), historial.obtenerActividadSeguimientoHistorial());
        }

        if (archivoHome == null) {
            System.out.println("No se cargaron archivos para comparar");
        } else {
            Assert.assertEquals(archivoHome, historial.obtenerArchivoSeguimientoHistorial());
        }
    }

    @And("^se valida el usuario reasignado por servicios$")
    public void seValidaElUsuarioReasignadoPorServicios() {
        consultaFilterInboxSteps = new ConsultaFilterInboxSteps();
        consultaFilterInboxSteps.seArmaElCuerpoDePeticionCreacionCliente(snumerotramite);
        consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticion();
        consultaFilterInboxSteps.validoLaRespuestaObtenida();
        nroAsesorReasignado = consultaFilterInboxSteps.validoCamposObtenidos();
    }

    @And("^se cierra sesion$")
    public void seCierraSesion() {
      //  getDriver().navigate().to("http://52.251.49.64/#/");
        getDriver().navigate().to("http://40.75.68.80/#/");

        try {


            Thread.sleep(4000);
        }catch (Exception e){
            Assert.assertEquals("error", e);
        }


        //homePage.clickPerfil();
//        homePage.salir();

    }

    @Given("^Ingresar con el usuario y password que tenga el tramite reasignado$")
    public void ingresarConElUsuarioYPasswordQueTengaElTramiteAsignado() {
        //loginPage.open();
        loginPage.ingresarUsuario(nroAsesorReasignado);
        loginPage.ingresarPassword("XTPRUEBAS1254");
        loginPage.clickIniciarSesion();
    }
}
