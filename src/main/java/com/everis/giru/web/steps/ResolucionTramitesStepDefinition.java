package com.everis.giru.web.steps;

import com.everis.giru.rest.steps.ConsultaFilterInboxSteps;
import com.everis.giru.web.entidades.Data;
import com.everis.giru.web.fact.Conexion;
import com.everis.giru.web.fact.Shadow;
import com.everis.giru.web.fact.SubirArchivo;
import com.everis.giru.web.fact.Util;
import com.everis.giru.web.page.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.awt.*;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;


public class ResolucionTramitesStepDefinition {
    LoginPage loginPage;
    HomePage homePage;
    HistorialPage historial;

    Util util;
    Data data = new Data();
    private String archivoHome;
    BandejaPage bandeja;
    ResumenPage Resumen;
    String tipoResolución = "";
    String snumerotramite = "";
    String tipocarta = "";
Shadow general;
    String nroAsesorReasignado = "";
    ConsultaFilterInboxSteps consultaFilterInboxSteps;



    @Given("^Ingresar con el (.*) y password que tenga el tramite asignado$")
    public void ingresarConElUsuarioYPasswordQueTengaElTramiteAsignado(String usuario) {

        loginPage.open();
        loginPage.ingresarUsuario(usuario);
        loginPage.ingresarPassword("XTPRUEBAS1254");
        loginPage.clickIniciarSesion();
    }


    @When("^ingresa en la busqueda el (.*) para seleccionarlo$")
    public void ingresaEnLaBusquedaElTramiteParaSeleccionarlo(String nrotramite) throws InterruptedException {

        // util.menuSuperior("Tramites");
        Thread.sleep(4000);

        if(snumerotramite.equals("")) {
            snumerotramite = nrotramite;
        }

        bandeja.bandejaTramite(snumerotramite);
        // Resumen.VerificarTramite();
        historial.seleccionarSeguimientoHistorial();
    }

    @And("se abre la vista del tramite de Resumen seguros selecciona (.*)$")
    public void seAbreLaVistaDelTramiteDeResumenSeleccionaTipoResolSeguros(String tipoResol) {

        Resumen.seleccionarbtnOpciones();
        tipoResolución=tipoResol;

        if (tipoResol.equals("P")){
            Resumen.seleccionarBtnProcedeSeguros();
        }else {
                Resumen.seleccionarBtnNoProcedeSeguros();
        }
    }
    @And("se abre la vista del tramite de Resumen selecciona (.*)$")
    public void seAbreLaVistaDelTramiteDeResumenSeleccionaTipoResol(String tipoResol) {

        Resumen.seleccionarbtnOpciones();
        tipoResolución=tipoResol;

        if (tipoResol.equals("P")){
            Resumen.seleccionarBtnProcede();
       }else {
            if (tipoResol.equals("D"))
            {
                Resumen.seleccionarBtnDerivar();
            }
            else {
                Resumen.seleccionarBtnNoProcede();
            }
        }
       }


    @And("se abre la vista del tramite de Resumen derivado selecciona (.*)$")
    public void seAbreLaVistaDelTramiteDeResumenSeleccionaTipoResol2(String tipoResol) {

        Resumen.seleccionarbtnOpciones();
        tipoResolución=tipoResol;

        if (tipoResol.equals("P")){
            Resumen.seleccionarBtnProcedeDer();
        }else {
            if (tipoResol.equals("D"))
            {
                Resumen.seleccionarBtnDerivarDer();
            }
            else {
                Resumen.seleccionarBtnNoProcedeDer();
            }
        }
    }

    @And("eliges si se envia (.*) , descarga y la adjunta$")
    public void eligeSiSeEnviaCartaSeleccionaPlantillaTipoCartaDescargaYLaAdjuntaCNR(String carta) throws AWTException, InterruptedException {
        if (carta.equals("Si")) {
            tipocarta = "R";
            //  Resumen.seleccionarMedio(tipo);
            Resumen.descargarCarta();
            new SubirArchivo().subirCarta3(tipoResolución,snumerotramite,tipocarta);
            Thread.sleep(4000);
        } else if (carta.equals("No")) {
            Resumen.seleccionarMedioEnvioNoCNR();
            System.out.println("No se agregó Carta");
        }

    }

    @And("^elige si se envia (.*) , selecciona plantilla (.*) descarga y la adjunta$")
    public void eligeSiSeEnviaCartaSeleccionaPlantillaTipoCartaDescargaYLaAdjuntaCIYEXO(String carta, String tipo) throws AWTException, InterruptedException {

        if (carta.equals("Si")) {
            if(tipoResolución.equals("P")) {
                if (tipo.equals("2")) {
                    tipocarta = "R";
                } else
                    tipocarta = "P";
            }
            else
            {
                if (tipo.equals("1")) {
                    tipocarta = "R";
                } else
                    tipocarta = "P";
            }

            Resumen.seleccionarMedioCIYEXO(tipoResolución, tipo);
            Resumen.descargarCarta();
           // String reclamo = Serenity.sessionVariableCalled("nroReclamo");
           // new SubirArchivo().subirCarta2(tipoResolución,reclamo,tipocarta);
            new SubirArchivo().subirCarta2(tipoResolución,snumerotramite,tipocarta);
            Thread.sleep(4000);
            //archivoHome = homePage.obtenerTextoArchivo();
        } else if (carta.equals("No")) {
            System.out.println("No se agregó Carta");
        }
    }

    @And("^selecciona medio respuesta (.*) y selecciona o agrega (.*) segun medio de respuesta$")
    public void seleccionaMedioRespuestaMediorespuestaYSeleccionaOAgregaConfiguraciónSegunMedioDeRespuesta(String confirmarMedioResp , String configuracionResp) throws InterruptedException {
        String TipoMedioRespuesta = util.separarValoresTrama(1, confirmarMedioResp, "-"); // S-EMAIL
        if (util.separarValoresTrama(0, confirmarMedioResp, "-").equalsIgnoreCase("S")) {
            Resumen.seleccionarMedioRespuesta(TipoMedioRespuesta, configuracionResp);
        } else if (util.separarValoresTrama(0, confirmarMedioResp, "-").equalsIgnoreCase("A")) { // A-ADDRESS
            Resumen.aniadirRespuesta(tipoResolución, TipoMedioRespuesta, configuracionResp);
        }
    }

    @And("ingresa comentario (.*) en el modal$")
    public void ingresaComentarioComentariomodalEnElModal(String comentariomodal) throws InterruptedException {
        Resumen.comentarioOpcionalModal(comentariomodal);
        Thread.sleep(4000);
    }

    @And("ingresa comentarios validacion (.*) en el modal$")
    public void ingresaComentarioComentariomodalEnElModal2(String comentariomodal) throws InterruptedException {
        Resumen.comentarioOpcionalModal2(comentariomodal);
        Thread.sleep(4000);
    }
    @And("^se adjunta un archivo (.*) en seguros en caso sea necesario$")
    public void seAdjuntaUnArchivoArchivoEnCasoSeaNecesarioS(String archivo) throws AWTException, InterruptedException {
        if (archivo.equals("Si")) {
            if(tipoResolución.equals("P")) {
                new SubirArchivo().subirArchivoModalSeg();
            }
            else {
                new SubirArchivo().subirArchivoModalNoProcedeSeg();
            }
            //archivoHome = homePage.obtenerTextoArchivo();
        } else if (archivo.equals("No")) {
            System.out.println("No se adjuntaron archivos");
        }
        Resumen.btnconfirmarResolución();
        // WebElement btnregistrar = getDriver().findElement(By.id("add-email__btn-add"));
        // ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", btnregistrar);
        Thread.sleep(7000);
    }

    @And("^se adjunta un archivo (.*) en caso sea necesario$")
    public void seAdjuntaUnArchivoArchivoEnCasoSeaNecesario(String archivo) throws AWTException, InterruptedException {
        if (archivo.equals("Si")) {
            if(tipoResolución.equals("P")) {
                //new SubirArchivo().subirArchivoModalDer();
                 new SubirArchivo().subirArchivoModal();
            }
            else {
                //new SubirArchivo().subirArchivoModalNoProcedeDer();
                new SubirArchivo().subirArchivoModalNoProcede();
            }
            //archivoHome = homePage.obtenerTextoArchivo();
        } else if (archivo.equals("No")) {
            System.out.println("No se adjuntaron archivos");
        }
        Resumen.btnconfirmarResolución();
        // WebElement btnregistrar = getDriver().findElement(By.id("add-email__btn-add"));
        // ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", btnregistrar);
        Thread.sleep(7000);
    }

    @Then("^se verificara el reclamo en la bandeja Historial en su Seguimiento$")
    public void seVerificaraElReclamoEnLaBandejaHistorialEnSuSeguimiento() throws InterruptedException {
             Thread.sleep(6000);
            util.menuSuperior("Historial");
            historial.historial(snumerotramite);
            historial.seleccionarNroReclamoHistorialEspecialista();
            String actividadhistorial = historial.obtenerActividadHistorialEspecialista();
            historial.seleccionarSeguimientoHistorial();
            if(tipoResolución.equals("P"))
            Assert.assertEquals(actividadhistorial , historial.obtenerActividadSeguimientoHistorialResol());
            else
                Assert.assertEquals(actividadhistorial , historial.obtenerActividadSeguimientoHistorialResolNoProcede());

    }

    @And("^eliges los movimientos si desea actualizar (.*) con montos (.*) y moneda (.*)$")
    public void eligesLosMovimientosSiDeseaActualizarActualizaConMontosMontoYMonedaMoneda(String actualiza, String monto, String moneda) {

        getDriver().findElement(By.xpath("//*[@id=\"row-0\"]/td[1]/div/label")).click();

    }

    @And("^eliges los movimientos con el tipo de abono de la carta (.*)$")
    public void eligesLosMovimientosConElTipoDeAbonoDeLaCartaTipoAbono(String tipoabono) throws InterruptedException {
        WebElement cmb = getDriver().findElement(By.xpath("//*[@id=\"template\"]"));

        if(tipoabono.equals("Definitivo"))

        {
            getDriver().findElement(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-proceed/div/div/div[2]/giru-cnr-proceed/div/form/div[2]/giru-movements-to-pay/div[2]/div/table/thead/th[1]/div/label")).click();
                                          //    /html/body/app-assi-modal[2]/div/div/giru-proceed/div/div/div[2]/giru-cnr-proceed/div/form/div[2]/giru-movements-to-pay/div[2]/div/table/thead/th[1]/div/label
            WebElement cmbCartaNoShadow = getDriver().findElement(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-proceed/div/div/div[2]/giru-cnr-proceed/div/form/giru-customer-letter/form/div[6]/div[1]"));
           // cmbCartaNoShadow.click();

            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", cmbCartaNoShadow);
           // cmbCartaNoShadow.click();

            WebElement cmbCarta =  general.expandContainer("1css", cmb, "#dropdown-select > div > div.arrow-down", "", "");
            cmbCarta.click();
            Thread.sleep(2000);
           WebElement select1 =  general.expandContainer("1css", cmb, "ul > li", "", "");
       //    WebElement selectfinal = select1.findElement(By.xpath("//*[@id=\"11\"]"));
            select1.click();
           Thread.sleep(4000);
            ////*[@id="sendLetter"]//div/div/div[2]/label
        }

        if(tipoabono.equals("Provisional"))

        {
           // getDriver().findElement(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-proceed/div/div/div[2]/giru-cnr-proceed/div/form/div[2]/giru-movements-to-pay/div[2]/div/table/thead/th[1]/div/label")).click();
            //    /html/body/app-assi-modal[2]/div/div/giru-proceed/div/div/div[2]/giru-cnr-proceed/div/form/div[2]/giru-movements-to-pay/div[2]/div/table/thead/th[1]/div/label
            WebElement cmbCartaNoShadow = getDriver().findElement(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-proceed/div/div/div[2]/giru-cnr-proceed/div/form/giru-customer-letter/form/div[6]/div[1]"));
            // cmbCartaNoShadow.click();

            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", cmbCartaNoShadow);
            // cmbCartaNoShadow.click();

            WebElement cmbCarta =  general.expandContainer("1css", cmb, "#dropdown-select > div > div.arrow-down", "", "");
            cmbCarta.click();
            Thread.sleep(2000);
            WebElement select1 =  general.expandContainer("1css", cmb, "ul > li", "", "");
            //    WebElement selectfinal = select1.findElement(By.xpath("//*[@id=\"11\"]"));
            select1.click();
            Thread.sleep(4000);
            ////*[@id="sendLetter"]//div/div/div[2]/label
        }


        if(tipoabono.equals("Mixto"))

        {
             getDriver().findElement(By.xpath("//*[@id=\"row-0\"]/td[1]/div/label")).click();

             WebElement cmbCartaNoShadow = getDriver().findElement(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-proceed/div/div/div[2]/giru-cnr-proceed/div/form/giru-customer-letter/form/div[6]/div[1]"));
            // cmbCartaNoShadow.click();

            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", cmbCartaNoShadow);
            // cmbCartaNoShadow.click();

            WebElement cmbCarta =  general.expandContainer("1css", cmb, "#dropdown-select > div > div.arrow-down", "", "");
            cmbCarta.click();
            Thread.sleep(2000);
            WebElement select1 =  general.expandContainer("1css", cmb, "ul > li", "", "");
            //    WebElement selectfinal = select1.findElement(By.xpath("//*[@id=\"11\"]"));
            select1.click();
            Thread.sleep(4000);
            ////*[@id="sendLetter"]//div/div/div[2]/label
        }
    }



    @And("^se selecciona la razon (.*) y el area (.*) al cual se derivara el tranmite$")
    public void seSeleccionaLaRazonYElAreaAlCualSeDerivaraElTranmite(String razon, String area) {
        Resumen.seleccionarRazonYArea(razon, area);
    }

    @And("^se abre la vista del tramite del Resumen selecciona derivacion$")
    public void seAbreLaVistaDelTramiteDelResumenSeleccionaDerivacion() {

        Resumen.seleccionarbtnOpciones();
                Resumen.seleccionarBtnDerivar();
    }


    @And("^escribe comentario interno (.*)$")
    public void escribeComentarioInterno(String comentario) {
        Resumen.escribirComentario(comentario);
    }

    @And("selecciona boton confirmar")
    public void seleccionaBotonConfirmar() {
        Resumen.seleccionoConfirmar();
    }
}




