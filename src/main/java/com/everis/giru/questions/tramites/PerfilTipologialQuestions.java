package com.everis.giru.questions.tramites;

import com.everis.giru.userinterface.tramites.TipologiaTramitePage;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class PerfilTipologialQuestions {

    public static Question<String> perfilTipologiaEspecialista() {
        WaitUntil.the(TipologiaTramitePage.LBL_TIPOLOGIA_ESPECIALISTA, isVisible()).forNoMoreThan(100).seconds();
        return actor -> TipologiaTramitePage.LBL_TIPOLOGIA_ESPECIALISTA.resolveFor(actor).waitUntilVisible().getText();
    }

}
