package com.everis.giru.rest.question;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class GetToken implements Question<String> {

    public static Question<String> was() {
        return new GetToken();
    }

    public String retornarToken() {
        String path = "access_token";
        return SerenityRest.lastResponse().body().path(path);
    }

    @Override
    public String answeredBy(Actor actor) {
        return null;
    }

    public String retornarEspecialista() {
        String path = "content.assignedSpecialist";
        return SerenityRest.lastResponse().then().extract().jsonPath().getString(path);
    }

    public String retornarIdInstance() {
        String path = "content.idInstance";
        return SerenityRest.lastResponse().then().extract().jsonPath().getString(path);


    }

}