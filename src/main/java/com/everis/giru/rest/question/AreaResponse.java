package com.everis.giru.rest.question;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class AreaResponse implements Question<String> {

    public static Question<String> was() {
        return new AreaResponse();
    }

    @Override
    public String answeredBy(Actor actor) {
        String path = "userinfo.areaCode";
        return SerenityRest.lastResponse().body().path(path);
    }
}