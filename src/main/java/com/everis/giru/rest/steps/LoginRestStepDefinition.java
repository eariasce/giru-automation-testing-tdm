package com.everis.giru.rest.steps;

import com.everis.giru.rest.fact.Access;
import com.everis.giru.rest.question.AreaResponse;
import com.everis.giru.rest.question.StatusCode;
import com.everis.giru.rest.task.LoginGiru;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class LoginRestStepDefinition {


    @Given("^that ([^\"]*) is registered in Giru$")
    public void thatIsRegisteredInGiru(String actorName) {
        theActorCalled(actorName).has(Access.toGiru());
    }

    @When("^he requests to login enter your ([^\"]*) and ([^\"]*)")
    public void heRequestsToLoginWithHisCredentialsAnd(String use, String pas) {
        theActorInTheSpotlight().attemptsTo(LoginGiru.withCredentials(use, pas));
    }

    @Then("^the data is displayed: ([^\"]*) and ([^\"]*)")
    public void heGetsAResponseShowingHis(String actor, String area) {
        theActorInTheSpotlight().should(
                seeThat(StatusCode.was(), equalTo(200))
                        .because("Response HTTP OK"));
        theActorInTheSpotlight().should(
                seeThat(AreaResponse.was(), equalTo(area)).because("Response data is correct"));
    }


}
