package com.everis.giru.rest.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ConsultaFilterInboxSD {

    @Steps
    private ConsultaFilterInboxSteps consultaFilterInboxSteps;


    @Given("se arma el cuerpo de peticion consulta restriccion cliente")
    public void seArmaElCuerpoDePeticionConsultaRestriccionCliente() {
        consultaFilterInboxSteps.seArmaElCuerpoDePeticionCreacionCliente("texto");
    }

    @When("agrego los encabezados en la peticion y envio la consulta")
    public void agregoLosEncabezadosEnLaPeticionYEnvioLaConsulta() {
        consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticion();
    }

    @And("valido la respuesta obtenida de la consulta")
    public void validoLaRespuestaObtenidaDeLaConsulta() {
        consultaFilterInboxSteps.validoLaRespuestaObtenida();
    }

    @Then("valido campos obtenidos de la creacion del cliente consulta")
    public void validoCamposObtenidosDeLaCreacionDelClienteConsulta() {
        consultaFilterInboxSteps.validoCamposObtenidos();
    }
}
