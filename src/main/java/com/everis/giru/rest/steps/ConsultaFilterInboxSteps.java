package com.everis.giru.rest.steps;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import org.apache.commons.io.IOUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;


import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConsultaFilterInboxSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaFilterInboxSteps.class);

   //static private final String BASE_URL = "https://eu2-ibk-apm-stg-ext-001.azure-api.net/giru-stg-inbox/stg/inbox/search/filterHistorical?page=0&size=20&sortBy=&direction=";
  //  static private final String TEMPLATE_CONSULTA_CLIENTE = "/request/getfilterinboxstg.json";

    static private final String BASE_URL = "https://eu2-ibk-apm-uat-ext-001.azure-api.net/giru-inbox/uat/inbox/search/filterHistorical?page=0&size=20&sortBy=&direction=";
    static private final String TEMPLATE_CONSULTA_CLIENTE = "/request/getfilterinbox.json";

    public String bodyRequest;
    public Response response;
    public static String getTemplate(String templatePath) {
      /*  try {
            return IOUtils.toString(new ClassPathResource(templatePath).getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }*/
        return "hola";
    }


    public void seArmaElCuerpoDePeticionCreacionCliente(String reclamo) {
        RestAssured.useRelaxedHTTPSValidation();
        bodyRequest = getTemplate(TEMPLATE_CONSULTA_CLIENTE).replace("{nroreclamo}", reclamo);

    }

    @Step("construyo request y hago POST")
    public void agregoLosEncabezadosEnLaPeticion() {
        response = given().log().all().
                header("Content-Type", "application/json").

                //STG
                //header("Ocp-Apim-Subscription-Key", "c99a61f28e87494c91b1f49d39854d0d").
               //UAT
                 header("Ocp-Apim-Subscription-Key", "f954d6fcccd34705a727a228c6bc772e").
                header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTI5OTkiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjE3NzMwMDU3LCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiN2VmMDRjMDktZGE5My00ODE5LWFmM2MtMWE0MDlhODVhNWE4IiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTI5OTkiLCJuYW1lIjoiUmV5bmEgUm9zc2ksIE1vbmljYSBNaXJlbGxhIiwidGl0bGUiOiJTVVBFUlZJU09SIFJFVEVOQ0lPTiBDTElFTlRFUyIsImRlcGFydG1lbnQiOiJEUFRPLlRFTEVWRU5UQVMgVEFSSkVUQVMiLCJkaXZpc2lvbiI6IlZQLk5FR09DSU9TIFJFVEFJTCBZIENBTkFMRVMiLCJkZWxpdmVyeU9mZmljZU5hbWUiOiJSRVRFTkNJT04gTVVMVElQUk9EVUNUTyBUQyAxIiwibW9iaWxlIjpudWxsLCJlbWFpbCI6Ik1yZXluYUBpbnRlcmNvcnAuY29tLnBlIiwiZ3JvdXBzIjpbIkdJUlVfUFJEX1NVUEVSVklTT1JfUkVURU5DSU9OIiwiR0lSVV9QUkRfU1VQRVJWSVNPUiJdLCJzdXBlcnZpc29yIjoiRGUgbGEgdmVnYSBTYXJtaWVudG8sIEthdGhlcmluYSIsInN1cGVydmlzb3JVc2VyTmFtZSI6IlMzMDkzMSIsImFwcGxpY2F0aW9uT3JpZ2luIjoiZ2lydWZyb250IiwiYXJlYUNvZGUiOiIwMDAwMjAiLCJhcmVhRGVzY3JpcHRpb24iOiJSZXRlbmNpw7NuIE11bHRpcHJvZHVjdG8gVEMiLCJwcm9maWxlQ29kZSI6IlNWUjAwMDMiLCJwcm9maWxlTmFtZSI6IlNVUEVSVklTT1JfUiIsImN1cnJlbnREYXRlIjoxNjE3NzI2NDU3LCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5IjoiQUxMIn1dfSwiY2xpZW50X2lkIjpudWxsfQ.21PU7LgEmy0jdjScGY9kVOCqrwppTnAZ7LLa9DVkUig").
                body(bodyRequest).
                when().post(BASE_URL);

    }

    public void validoLaRespuestaObtenida() {
        assertThat(response.statusCode(), is(200));
    }

    public String validoCamposObtenidos() {

        Serenity.setSessionVariable("nombreAsesor").to(response.body().path("content[0].assignedSpecialistJson[0].currentAreaUserName"));
        Serenity.setSessionVariable("nombreArea").to(response.body().path("content[0].assignedSpecialistJson[0].currentAreaDescription"));


       return response.body().path("content[0].assignedSpecialistJson[0].currentAreaUserCode");
    }



    public String validoCamposObtenidos2() {

        return response.body().path("content[0].assignedSpecialistJson[1].currentAreaUserCode");
    }

}
