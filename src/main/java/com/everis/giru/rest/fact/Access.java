package com.everis.giru.rest.fact;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.facts.Fact;

public class Access implements Fact {


    public static Access toGiru() {
        return new Access();
    }

    public String toString() {
        return "Access{ API GIRU }";
    }

    @Override
    public void setup(Actor actor) {
    }
}


