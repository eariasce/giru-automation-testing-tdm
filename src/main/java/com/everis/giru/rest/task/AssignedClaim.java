package com.everis.giru.rest.task;

import com.everis.giru.util.Util;
import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

public class AssignedClaim implements Task {
    private static final String URL = "/giru-bpm-tasks/uat/v2/bpm-tasks/assignee-masive-task";
    private static final String TEMPLATE_ASSIGNED = "/templates/Assigned.json";

    private String areacode;
    private String areadescription;
    private String usercode;
    private String username;
    private String idInstance;
    private String numberclaim;
    private String token;

    public AssignedClaim(String areacode, String areadescription, String usercode, String username, String idInstance, String numberclaim, String token) {
        this.areacode = areacode;
        this.areadescription = areadescription;
        this.usercode = usercode;
        this.username = username;
        this.numberclaim = numberclaim;
        this.token = token;
        this.idInstance = idInstance;
    }

    public static Performable assignedClaim(String areacode, String areadescription, String usercode, String username, String idInstance, String numberclaim, String token) {
        return Tasks.instrumented(AssignedClaim.class, areacode, areadescription, usercode, username, idInstance, numberclaim, token);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(URL).with(request -> request
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + token)
                .header("Ocp-Apim-Subscription-Key", "f954d6fcccd34705a727a228c6bc772e")
                .body(Util.getTemplate(TEMPLATE_ASSIGNED)
                        .replace("{areacode}", areacode)
                        .replace("{areadescription}", areadescription)
                        .replace("{usercode}", usercode)
                        .replace("{username}", username)
                        .replace("{idInstance}", idInstance.replace("[","").replace("]",""))
                        .replace("{numberclaim}", numberclaim)
                )
        ));
    }
}