package com.everis.giru.rest.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

public class TokenGetFilterInbox implements Task {

    private static final String URL = "https://eu2-ibk-apm-uat-ext-001.azure-api.net/giru-security/uat/oauth/token?grant_type=password&scope=all&username=B26192";


    public static Performable generateToken() {
        return Tasks.instrumented(TokenGetFilterInbox.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(URL).with(requestSpecification -> requestSpecification
                .header("Authorization","Basic YnBpOnM2ZDdmOTBnMHM4ZDlzMA==")
                .header("Content-Type","application/x-www-form-urlencoded")
                .header("Ocp-Apim-Subscription-Key","2e1d1dd1d85c4d65a14fc9c1b1f3ebae")

        ));

    }
}
