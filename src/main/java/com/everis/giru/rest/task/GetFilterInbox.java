package com.everis.giru.rest.task;

import com.everis.giru.util.Util;
import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

public class GetFilterInbox implements Task {
    private static final String URL = "/giru-inbox/uat/inbox/search/filter/v2.0.1";
    private static final String TEMPLATE_GETFILTER = "/templates/GetFilterInbox.json";

    private String numberclaim;
    private String token;

    public GetFilterInbox(String numberclaim, String token) {
        this.numberclaim = numberclaim;
        this.token=token;
    }

    public static Performable getFilterInbox(String numberclaim,String token) {
        return Tasks.instrumented(GetFilterInbox.class, numberclaim,token);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(URL).with(request -> request
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + token)
                .header("Ocp-Apim-Subscription-Key", "f954d6fcccd34705a727a228c6bc772e")
                .queryParam("page", "0")
                .queryParam("size", "6")
                .queryParam("sortBy", "FLAG_AUTONOMY")
                .queryParam("direction", "DESCENDING")
                .body(Util.getTemplate(TEMPLATE_GETFILTER)
                        .replace("{numberclaim}", numberclaim))
        ));
    }
}