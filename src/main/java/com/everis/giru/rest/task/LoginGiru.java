package com.everis.giru.rest.task;

import com.everis.giru.util.Util;
import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

public class LoginGiru implements Task {
    private static final String URL = "/giru-security/uat/login";
    private static final String TEMPLATE_LOGIN = "/templates/login.json";

    private String user;
    private String pass;

    public LoginGiru(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    public static Performable withCredentials(String user, String pass) {
        return Tasks.instrumented(LoginGiru.class, user, pass);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(URL).with(request -> request
                .contentType(ContentType.JSON)
                .header("Authorization", "Basic Z2lydWZyb250OnMyazM0bzRwNXBrbA==")
                .header("Ocp-Apim-Subscription-Key", "2e1d1dd1d85c4d65a14fc9c1b1f3ebae")
                .body(Util.getTemplate(TEMPLATE_LOGIN)
                        .replace("{valueUser}", user)
                        .replace("{valuePass}", pass)))
        );
    }
}