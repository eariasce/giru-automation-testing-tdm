package com.everis.giru.stepsdefinitions.tramites;

import com.everis.giru.tasks.login.SeleccionarMenuPrincipal;
import com.everis.giru.tasks.tramites.*;
import com.everis.giru.tasks.tramites.Filtros.*;
import com.everis.giru.userinterface.tramites.GetContador;
import com.everis.giru.userinterface.tramites.GetTramitePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.serenitybdd.core.Serenity;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.assertj.core.api.Assertions.assertThat;
public class TramitesStepDefinitions {

    GetTramitePage getTramitePage;
    GetContador getContador = new GetContador();

    @And("^se selecciona (.*) del menu principal$")
    public void validaSeleccionarOpcionMenuPrincipal(String menuPrincipal) {
        theActorInTheSpotlight().attemptsTo(
                SeleccionarMenuPrincipal.withData(menuPrincipal)
        );

    }

    @And("^se selecciona la opcion (.*) del perfil (.*) asignado al departamento (.*)$")
    public void validaSeleccionaOpcionTipologiaPerfilPorDepartamento(String tipologia, String perfil, String departamento) throws InterruptedException {

        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipologia.withData(tipologia, perfil, departamento)
        );

        Thread.sleep(1000);
    }

    @Then("^se visualiza un perfil (.*) de la tipologia (.*) del departamento (.*)$")
    public void validaVisualizaPerfilPorTipologiaDepartamento(String perfil, String tipologia, String departamento) {
        theActorInTheSpotlight().attemptsTo(
                ValidaTipologiaDeterminada.withData(perfil, tipologia, departamento)
        );
    }

    @And("^realiza la opcion buscar por (.*) y (.*) de tramites$")
    public void validaBuscarTipoNumeroDocumentoDeTramites(String tipoTramite, String numeroTramite) {

        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipoTramites.withData(tipoTramite, numeroTramite)
        );

        theActorInTheSpotlight().attemptsTo(
                new ButtonBuscar()
        );

        Serenity.setSessionVariable("numeroTramite").to(numeroTramite);

    }

    @Then("se verifica el tramite que existe en bandeja")
    public void validaTramiteExisteBandeja() {
        getTramitePage.validaBuscarTramite();
    }

    @Then("^se valida contador de (.*) de perfil (.*) del departamento (.*) que sean iguales$")
    public void validaContadorTotalTramites(String tipologia, String perfil, String departamento) {

        theActorInTheSpotlight().attemptsTo(
                ValidarContadoresBandeja.withData(tipologia, perfil, departamento)
        );

    }

    @Then("^se valida la paginacion de (.*) registros en la tipologia (.*) de determinado (.*) del departamento (.*)$")
    public void validaPaginacionPorTipologia(String registros, String tipologia, String perfil, String departamento) throws InterruptedException {

        theActorInTheSpotlight().attemptsTo(
                ValidarPaginacion.withData(registros, tipologia, perfil, departamento)
        );

        Thread.sleep(1000);
    }

    @Then("^se selecciona filtros (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*) de la tipologia (.*) del perfil (.*) del departamento (.*)$")
    public void seleccionaFiltros(String fVencimiento, String fTipoTramite, String fTipologia, String fMotivos, String fSegmento, String fEstado, String fNivelServicio, String fMoneda, String fMontoDesde, String fMontoHasta, String fMasFiltros, String tipologia, String perfil, String departamento) throws InterruptedException {
        Thread.sleep(500);
        Serenity.setSessionVariable("Tipologia").to(tipologia);
        if (getContador.contadorText(departamento, perfil, tipologia).equals("") || getContador.contadorText(departamento, perfil, tipologia).equals("0")) {
            getTramitePage.EsCeroTotalTramite();
            if (tipologia.equals("TodosMisTramites") || tipologia.equals("MiEquipo") || tipologia.equals("Asignar") || tipologia.equals("Resolver") || tipologia.equals("Monitorear") || tipologia.equals("TransaccionesEnProceso") || tipologia.equals("Retipificar")) {
                if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("Ingresa una búsqueda para ver resultados")) {
                    assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                } else {
                    assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("Ingresa una búsqueda para ver resultados");
                }

            } else {
                if (getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento).equals("No se encontraron coincidencias")) {
                    assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                } else {
                    assertThat(getTramitePage.validaTotalTramitePorTipologia(tipologia, perfil, departamento)).isEqualTo("No se encontraron coincidencias");
                }
            }

        } else {
            //Filtro Vencimiento
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroVencimiento.withData(fVencimiento)
            );
            //Filtro TipoTramite
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroTipoTramite.withData(departamento, perfil, tipologia, fTipoTramite)
            );
            //Filtro Tipologia
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroTipologia.withData(fTipologia, perfil, departamento, fTipoTramite)
            );
            //Filtro Motivos - Nilo
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroMotivos.withData(departamento, perfil, fTipoTramite, fTipologia, fMotivos)
            );
            //Filtro Segmento - Renato
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroSegmento.withData(fSegmento, tipologia, perfil, departamento)
            );
            //Filtro Estado - Nilo
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroEstado.withData(fEstado)
            );
            //Filtro Nivel de Servicio - Renato
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroNivelServicio.withData(fNivelServicio)
            );
            //Filtro Moneda y Monto - Nilo
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroMonedaMonto.withData(fMoneda, fMontoDesde, fMontoHasta)
            );
            //Filtro Mas FIltros - Renato
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroMasFiltros.withData(fMasFiltros)
            );

        }
        getContador.SetContadorANull();
    }


}
