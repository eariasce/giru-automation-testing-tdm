package com.everis.giru.stepsdefinitions.atencionCliente.tramitePedido;

import com.everis.tdm.Do;
import com.everis.tdm.model.giru.Cliente;
import com.everis.tdm.model.giru.Escenario;
import com.everis.giru.questions.atencionCliente.atencionClienteQuestions;
import com.everis.giru.tasks.atencionCliente.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;

import java.util.List;
import java.util.Map;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class ExoCobrosStepDefinitions {

    @And("^realiza la identificacion por (.*) y (.*) del cliente$")
    public void validaIdentificarCliente(String tipoDocumento, String numeroDocumento) {
        theActorInTheSpotlight().attemptsTo(
                IdentificarCliente.withData(tipoDocumento, numeroDocumento)
        );
    }

    @And("realiza la identificacion del cliente {string}")
    public void validaIdentificarCliente2(String numescenario) {

        List<Escenario> escenarios= Do.getListEscenariosPorNumero(numescenario);
        Escenario escenario = escenarios.get(0);

        theActorInTheSpotlight().attemptsTo(
                IdentificarCliente.withData(escenario.getTipoDocumento(), escenario.getNumeroDocumento())
        );
    }


    @And("^realiza la identificacion por cantidad de tarjetas (.*)$")
    public void validaIdentificarCliente3(String cantidadtarjetas) {


            List<Cliente> clientes= Do.getListUsuariosPorTarjetas(cantidadtarjetas);
            Cliente cliente = clientes.get(0);
            theActorInTheSpotlight().attemptsTo(
                    IdentificarCliente.withData(cliente.getTipodocumento(), cliente.getNrodocumento())
            );
    //     new AssertionError("no se encontraron tarjetas");


    }


    @And("selecciona tarjeta, elige el tipo de tramite y selecciona la tipologia {string}")
    public void seleccionarTarjetaTramiteTipologia2(String numescenario) {

        List<Escenario> escenarios= Do.getListEscenariosPorNumero(numescenario);
        Escenario escenario = escenarios.get(0);

        theActorInTheSpotlight().attemptsTo(
                SeleccionarTarjeta.withData(escenario.getNumeroTarjeta())
        );

        theActorInTheSpotlight().attemptsTo(
                SeleccionarTramite.withData(escenario.getTramite())
        );

        System.out.println("TIPOLOGIA ENCONTRADA "+ escenario.getTipologia());
        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipologia.withData(escenario.getTipologia())
        );

    }


    @And("^selecciona tarjeta (.*), elige el tipo de tramite (.*) y selecciona la tipologia (.*)$")
    public void seleccionarTarjetaTramiteTipologia(String numeroTarjeta, String tramite, String tipologia) {

        theActorInTheSpotlight().attemptsTo(
                SeleccionarTarjeta.withData(numeroTarjeta)
        );

        theActorInTheSpotlight().attemptsTo(
                SeleccionarTramite.withData(tramite)
        );

        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipologia.withData(tipologia)
        );

    }

    @And("elige la {string} de {string} y configura la fecha {string} tipo de moneda {string} y monto {string} por motivo segun la {string}")
    public void eligeLaDeYConfiguraLaFechaTipoDeMonedaYMontoPorMotivoSegunLa(String cantidad_motivos, String motivo_numero, String fecha, String tipomoneda, String monto, String cantidadmontomotivo) {
        theActorInTheSpotlight().attemptsTo(RegistraMotivo.withData(cantidad_motivos,motivo_numero,fecha,tipomoneda,monto,cantidadmontomotivo));
    }

    @And("en caso los motivos tuviera Comision de Membresia se aplicaria la Razon Operativa {string} y seleccionar el Tipo de Razon Operativa {string}")
    public void enCasoLosMotivosTuvieraComisionDeMembresiaSeAplicariaLaRazonOperativaYSeleccionarElTipoDeRazonOperativa(String razonOperativa, String tipoRazonOperativa) {
        theActorInTheSpotlight().attemptsTo(RegistraRazonOperativa.withData(razonOperativa,tipoRazonOperativa));
    }

    @And("en caso los motivos tuviera Comision de Membresia se aplicaria la Razon Operativa y seleccionar el Tipo de Razon Operativa {string}")
    public void enCasoLosMotivosTuvieraComisionDeMembresiaSeAplicariaLaRazonOperativaYSeleccionarElTipoDeRazonOperativa2(String numescenario) {
        List<Escenario> escenarios= Do.getListEscenarios();
        Escenario escenario = escenarios.get(Integer.parseInt(numescenario));

        theActorInTheSpotlight().attemptsTo(RegistraRazonOperativa.withData(escenario.getRazonOperativa(),escenario.getTipoRazonOperativa()));
    }


    @And("^se registra (Reclamo|Pedido|Solicitud) de la tarjeta$")
    public void seRegistraReclamoDeLaTarjeta(String tipoTranmite) {
    }

    @And("^elige los datos de registro de los MOTIVOS del tramite (.*)$")
    public void eligeLosDatosDeRegistroDeLosMOTIVOSDelTramite(String nombreEscenario,DataTable dataTable) {

        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);

        List<List<String>> rows = dataTable.asLists(String.class);

        int resultado = 0;
        for(int i =0;i<rows.size();i++)
        {
            List<String> columns = rows.get(i);
            if(columns.get(0).equals(nombreEscenario))
            {
                resultado=i-1;
            }
        }

        String motivo =  list.get(resultado).get("PreCon-Motivo");
        String cantidadmotivos =  list.get(resultado).get("PreCon-Cantidad de Motivos");
        String fecha =  list.get(resultado).get("PreCon-Fecha");
        String tipomoneda =  list.get(resultado).get("PreCon-Moneda");
        String monto =  list.get(resultado).get("PreCon-Monto");
        String cantidadmontomotivo=  list.get(resultado).get("PreCon-Cantidad de Movimientos por Motivo");

        theActorInTheSpotlight().attemptsTo(RegistraMotivo.withData(cantidadmotivos,motivo,fecha,tipomoneda,monto,cantidadmontomotivo));

    }

    @And("ingresa un {string} y un {string} opcional")
    public void ingresaUnYUnOpcional(String respuestaCliente, String comentarioAdicional) {
        theActorInTheSpotlight().attemptsTo(RegistraComentariosYRespuesta.withData(respuestaCliente,comentarioAdicional));
    }

    @And("se adjuntara un archivo {string} en caso sea necesario")
    public void seAdjuntaraUnArchivoEnCasoSeaNecesario(String enviarArchivo) {
        theActorInTheSpotlight().attemptsTo(SubirArchivo.withData(enviarArchivo));
    }

    @And("selecciona o añade segun el {string} y {string}")
    public void seleccionaOAñadeSegunElY(String numcelular, String operador) {
        theActorInTheSpotlight().attemptsTo(SeleccionaCelularYOperador.withData(numcelular,operador));
    }

    @And("seleciona el medio de respuesta {string} segun la configuracion de la respuesta {string}")
    public void selecionaElMedioDeRespuestaSegunLaConfiguracionDeLaRespuesta(String mediorespuesta, String nuevoregistro) {
        theActorInTheSpotlight().attemptsTo(SeleccionaMedioRespuestaYRegistro.withData(mediorespuesta, nuevoregistro));
    }

    @And("seleccionar los datos de registro de nueva DIRECCION {string} {string}")
    public void seleccionarLosDatosDeRegistroDeNuevaDIRECCIONNumeroescenario(String nombreEscenario, String registro,DataTable dataTable) {

        if(registro.equals("Nuevo")) {
            List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
        List<List<String>> rows = dataTable.asLists(String.class);

        int resultado = 0;
            for(int i =0;i<rows.size();i++)
        {
            List<String> columns = rows.get(i);
            if(columns.get(0).equals(nombreEscenario))
            {
                    resultado=i;
                }
        }
            String departamento = list.get(resultado).get("Dpto");
            String provincia = list.get(resultado).get("Provincia");
            String distrito = list.get(resultado).get("Distrito");
            String tipoVia = list.get(resultado).get("Tipo de Via");
            String nombreVia = list.get(resultado).get("Nombre de via");
            String urbanizacion = list.get(resultado).get("Urbanizacion");
            String numero_urb = list.get(resultado).get("Numero");
            String manzana = list.get(resultado).get("Manzana");
            String lote = list.get(resultado).get("Lote");
            String interior = list.get(resultado).get("Interior");
            String referencia = list.get(resultado).get("Referencia");

            theActorInTheSpotlight().attemptsTo(RegistraDireccion.withData(departamento, provincia, distrito, tipoVia, nombreVia, urbanizacion, numero_urb, manzana, lote, interior, referencia));
        }

    }

    @And("se valida fecha incorrecta")
    public void seValidaFechaIncorrecta()
    {
        theActorInTheSpotlight().should(
                seeThat(atencionClienteQuestions.validarFechaIncorrecta(), equalTo(true)));

    }

    @And("se valida un monto ingresado incorrecto")
    public void seValidaUnMontoIngresadoIncorrecto() {
        theActorInTheSpotlight().should(
                seeThat(atencionClienteQuestions.validarMontoIncorrecto(), equalTo(true)));
    }

    @And("se valida que no ingresa una respuesta para el cliente")
    public void seValidaQueNoIngresaUnaRespuestaParaElCliente() {
        theActorInTheSpotlight().should(
                seeThat(atencionClienteQuestions.validarSinRespuesta(), equalTo(true)));
    }


}
