package com.everis.giru.stepsdefinitions.login;

import com.everis.tdm.Do;
import com.everis.tdm.model.giru.Escenario;
import com.everis.giru.tasks.atencionCliente.IdentificarCliente;
import com.everis.giru.tasks.login.AbrirGiru;
import com.everis.giru.tasks.login.AutenticarGiru;
import com.everis.giru.tasks.login.ButtonIniciarSesion;
import com.everis.giru.tasks.login.PerfilDeterminado;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class LoginStepDefinitions {


    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^que el (.*) accede a la aplicacion GIRU$")
    public void validaUsuarioAccedeAplicacionGiru(String usuario) {
        theActorCalled(usuario).attemptsTo(
                AbrirGiru.loginGiruPage()
        );
    }

    @When("^se ingresa usuario (.*) y contrasena (.*)$")
    public void validaIngresoUsuarioContrasena(String user, String password) {


        theActorInTheSpotlight().attemptsTo(
                IdentificarCliente.withData(user,password)
        );

        theActorInTheSpotlight().attemptsTo(
                AutenticarGiru.withData(user, password)
        );
        Serenity.setSessionVariable("UsuarioSesion").to(user);
    }

    @When("se ingresa los datos de usuario de giru {string}")
    public void seIngresaLosDatosDeUsuarioDeGiru(String numescenario) {

        List<Escenario> escenarios= Do.getListEscenariosPorNumero(numescenario);
        Escenario escenario = escenarios.get(0);
        theActorInTheSpotlight().attemptsTo(
                AutenticarGiru.withData(escenario.getUser(), escenario.getPassword())
        );

        Serenity.setSessionVariable("UsuarioSesion").to(escenario.getUser());
    }

    @And("selecciona la opcion iniciar sesion")
    public void seleccionaLaOpcionIniciarSesion() {



        theActorInTheSpotlight().attemptsTo(
                new ButtonIniciarSesion()
        );
    }

    @Then("^se valida que el acceso sea correcto con un perfil (.*) determinado$")
    public void validaAccesoCorrectoPerfilDeterminado(String profile) {
        theActorInTheSpotlight().attemptsTo(
                PerfilDeterminado.withData(profile)
        );
    }

    @Then("se valida que el acceso sea correcto con un perfil determinado {string}")
    public void validaAccesoCorrectoPerfilDeterminado2(String numescenario) {

        List<Escenario> escenarios= Do.getListEscenariosPorNumero(numescenario);
        Escenario escenario = escenarios.get(0);
        theActorInTheSpotlight().attemptsTo(
                PerfilDeterminado.withData(escenario.getProfile())
        );
    }

    @And("selecciona la opcion cerrar sesion")
    public void seleccionaOpcionCerrarSesion() {

    }

}
